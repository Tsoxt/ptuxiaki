##Ecs Installation

##Create sd card
    -burn image prepacked image to sd card
        *linux (ddrescue -D --force <path/to/image>.img /dev/<sd-card-device)
        *windows (use win32 disk imager) 
    
##Initial steps
    -change rasp name in hosts and hostname(the name must be the same as the LAN macAddress wich is also the device serial number)
        (ip addr for the mac address and sudo nano /etc/hosts and sudo  nano /etc/hostname)
    -expand file system
        *sudo raspi-config
        *in purple screen menu select 5 advanced options and then a1 expand file system, press ok and select finish      
    -reboot    
    -install anydesk{
            * open chromium-browser in raspberry
            * type https://anydesk.com/en/downloads/raspberry-pi
            * go to old versions, find anydesk_2.9.4-1_armhf.deb and download
            * sudo dpkg --add-architecture armhf
            * sudo apt update && sudo apt upgrade && sudo apt autoremove
            * sudo apt install ./Downloads/anydesk <press tab to get full name and enter>
            * rm /home/ECSPos/Downloads/anydesk <press tab to get full name and enter> 
            * anydesk --start-service (start anydesk)
            * anydesk --get-alias (anydesk pc name)
            * echo <password> | anydesk --set-password (set password for unatented login the usual *****)
        }
    -unzip the bundle in home folder
    -open terminal (Ctrl+Alt+t) 
    -run chmod +x /home/ecs/installscript/install.sh
    -cd /home/ecs/installscripts
    -run ./install.sh
    -if promt put the linux user pass
        
##Command Line prompts
    -for passwords see the passwords file on server
    -promt for bitbucket ssh security press yes and enter
    -current root password none press enter
    -set root password press y and Enter
    -new password from the passwords file
    -remove anonymous users press y and Enter
    -disallow root login remotely press y and Enter
    -remove test database press y and Enter
    -reload privilleges y and Enter
    -phpmyadmin (purple screen){ 
        * choose server select none press only ok
        * configure database yes/ναι
        * use mysql phpmyadmin password twice
    }
    -give mysql root password
    -give password for sql application user
    -no remotes found -make a new one
        *press n and enter
        *name gdrive enter
        *press 7 and enter
        *client id none enter
        *client secret none enter
        *use auto-config press n and enter
        *copy paste url
        *login with gmail user
        *copy code and paste to verification code to terminal
        *type y and q and finish
        
##Fix error with phpMyAdmin
    -go to <ip of raspberry>:81
    -when you view a table of ecs you will propably get a warning error
    -follow the steps from here https://stackoverflow.com/questions/48001569/phpmyadmin-count-parameter-must-be-an-array-or-an-object-that-implements-co
        to fix it       
        