function formReset(){
    $('input').off();
    $('select').off();
    $(".itembody").empty();
}

function bootboxMessage(msg,type) {
    if (parseInt(type)===1) {
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="errormsg">'+msg+'</div>'
        });
    }else if (parseInt(type)===2){
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="successmsg">'+msg+'</div>'
        });
    }else {
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="infomsg">'+msg+'</div>'
        });
    }
    return false;
}

function setSidemenuDefault() {
    $('.sidebuts').css({
            "background-color":"#f6f6f6",
            "color":"#665e5e"
        });
}

function sideButtonsListeners() {
    $(".sidebuts").click(function () {
        var id=$(this).attr('id');
        if (id==="collapseSidemenu"){
            $('.sidebuts span').toggle();
            $('#collapseSidemenu i').toggle();
        } else {
            var i;
            for (i=1;i<7;i++){
                $('#groupset'+i).hide();
            }
            $('.ajax-loader').show(0).delay(500).hide(0);
            if(parseInt(access)>1){
                bootboxMessage('Δεν έχετε τα δικαιώματα για να επεξεργαστείτε την αποθήκη');
            }else{
                formReset();
                setSidemenuDefault();
                $(this).css({
                    "background-color":"#665e5e",
                    "color":"#f6f6f6"
                });
                $('.sidebuts span').hide();
                switch (id){
                    case "btn1":
                        start(0);
                        $('#groupset1').show();
                        $('#barcode').focus();
                        break;
                    case "btn2":
                        start(2);
                        $('#groupset2').show();
                        break;
                    case "btn3":
                        start(1);
                        $('#groupset3').show();
                        break;
                    case "btn4":
                        start(3);
                        $('#groupset4').show();
                        break;
                    case "btn5":
                        start(4);
                        $('#groupset5').show();
                        break;
                    case "btn6":
                        var result = updateScale();
                        result.then(function (json) {
                            console.log(json);
                            var data=JSON.parse(json);
                            if (data.response){
                                alert('Η ζυγαριά ενημερώθηκε με επιτυχία');
                            } else{
                                alert(data.error);
                            }
                        });
                        break;
                }
            }
        }
    });
}


function gvars() {
    var aksia =new Array();
    var stock =new Array();
    window.aksia = aksia;
    window.stock = stock;
    window.item=new Array();
    window.omada=new Array();
    window.category=new Array();
}

function start(type){//type=0 klisi apo items && type=1 klisi apo category && type=2 klisi apo omada && type=3 klisi apo tmimata
    if(parseInt(type)===0){
        item = {id:"", barcode:"", mothercode:"", perigrafi:"", perigrafi2:"", id_om:"", timi:"", timi_agoras:"", flag_timi:0, tmima:"", apothema:0, alert:1,recommend:1,
            siskevasia:1, energo:1, new:1, seira:"", epistrofi:"", zugistiko:0, om_name:"", cat_name:"", id_cat:"", from_api:0, zugaria:0, temaxiako:0, alt_id:""};
        stock={package:0, apothema:0, sunolo:0};
    }else if(parseInt(type)===1){
        omada = {id:"", altid:"", perigrafi:"", id_cat:"", active:1, order:"", from_api:0, new:1, omada_zugaria:"", isozigio:0};
    }else if (parseInt(type)===2) {
        category = {id:"", altid:"", perigrafi:"", active:1, order:"", from_api:0, new:1};
    }else if (parseInt(type)===3) {
        $('input').off();
    }else{
        $('input').off();
    }


}

$(document).ready(function () {
    initCss();
    menuCss();
    sidebuts();
    apothikiCss();
    appendHead();
    specialBcodes();
    gvars();
    access;
    for (i=1;i<7;i++){
        $('#groupset'+i).hide();
    }
    sideButtonsListeners();
    
    $("#barcode").on('keypress', function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            var value=$('#barcode_search').val();
            $('#barcode_search').val('');
            if (value.length===8 || value.length===13 || value.length===15 || value.length===26){
                callItem(value, 0);
            }
            return true;
        }else{
            var input=$('#barcode_search').val();
            var key=event.key;
            $('#barcode_search').val(input+key);
        }
    });

    $('#barcode').on('focus',function () {
        $('#barcode').css('border','2px solid green').css('border-radius','5px');
    });

    $('#barcode').on('focusout',function () {
        $('#barcode').css('border','none').css('border-radius','unset');
    });

    $('#bcodeModal').on('shown.bs.modal', function () {
        $('#bcodenumbers').focus();
        $('#bcodenumbers').keypress(function (e) {
            if(e.keyCode===13) {
                var value=$('#bcodenumbers').val();
                if ((value.length===8) || (value.length===13) || (value.length===15) || (value.length>=17)){
                    callItem(value,0);
                    $('#bcodeModal').modal('hide');
                }
                return true;
            }
        });
    });

    $('#bcodeModal').on('hidden.bs.modal', function () {
        $('#bcodenumbers').val('');
        $('#bcodenumbers').off();
        $('#barcode').focus();
    });
});
