$.ajaxSetup({
    type: "POST",
    datatype:"json",
    beforeSend: function () {
        $('.ajax-loader').show();
    },
    url:    "../include/resources/dispatcher.php",
    timeout: 7000,
    cache:false,
    complete: function (xhr){
        xhr.done(function(json){
            var data=JSON.parse(json);
            if(!data['loginStatus']){
                logout();
            }
            $('.ajax-loader').hide();
        });
    },
    error: function(xhr,ajaxOptions,thrownError){
        $('.ajax-loader').hide();
        var msg="Πρόβλημα δικτύου \n http status: "+xhr.status+", σφάλμα: "+thrownError;
        bootboxMessage(msg,1);
    }
});

function callSystemInfo() {
    var action='getsysteminfo';
    return $.ajax({
        data: {"action":action},
        datatype: "json"
    });

}

function bootboxMessage(msg,type) {
    if (parseInt(type)===1) {
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="errormsg">'+msg+'</div>'
        });
    }else if (parseInt(type)===2){
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="successmsg">'+msg+'</div>'
        });
    }else {
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="infomsg">'+msg+'</div>'
        });
    }
    return false;
}

function checkForUpdates() {
    //todo check updates
    $('.ajax-loader').show(0).delay(2000).hide(0);
    setTimeout(function () {
        bootboxMessage('Έχετε την τελευταία έκδοση λογισμικού',3);
    },3000);

}

function getSystemInfo() {
    var result=callSystemInfo();
    result.then(function (json) {
        console.log(json);
        var data=JSON.parse(json);
        if (data.response){
            $('#customer').html(data.body.customer);
            $('#afm').html(data.body.afm);
            $('#phone').html(data.body.phone);
            $('#seller').html(data.body.seller);
            $('#seller_phone').html(data.body.seller_phone);
            $('#seller_email').html(data.body.seller_email);
            $('#program').html(data.body.program);
            $('#version').html(data.body.version);
            $('#serial').html(data.body.serial);
            $('#start_date').html(data.body.start_date);
            $('#exp_date').html(data.body.exp_date);
        }else {
            bootboxMessage(data.error);
        }

    })
}

function setSidemenuDefault() {
    $('.sidebuts').css({
        "background-color":"#f6f6f6",
        "color":"#665e5e"
    });
}

function sideButtonsListeners() {
    $(".sidebuts").click(function () {
        var id=$(this).attr('id');
        if (id==="collapseSidemenu"){
            $('.sidebuts span').toggle();
            $('#collapseSidemenu i').toggle();
        }else{
            $('#groupset1').hide();
            $('.ajax-loader').show(0).delay(500).hide(0);
            if(parseInt(access)>1){
                bootboxMessage('Δεν έχετε τα δικαιώματα για να δείτε αυτές τις ρυθμίσεις');
            }else{
                setSidemenuDefault();
                $(this).css({
                    "background-color":"#665e5e",
                    "color":"#f6f6f6"
                });
                $('.sidebuts span').hide();
                switch (id){
                    case "btn1":
                        getSystemInfo();
                        $('#groupset1').show();
                        break;
                }
            }
        }
    });
}

function start() {
    $('.ajax-loader').show(0).delay(500).hide(0);
    $('#groupset1').hide();
}

$(document).ready(function () {
    initCss();
    menuCss();
    appendHead();
    access;
    start();
    sideButtonsListeners();
});