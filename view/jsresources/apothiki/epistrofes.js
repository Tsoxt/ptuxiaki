function searchByDay(event) {
    formReset();
    var date = new Date();
    var month = date.getMonth()+1;
    var day = date.getDate();
    var imera = event.target.id;
    if (imera == "today"){
        var imerominia = date.getFullYear() + '-' +
            (month<10 ? '0' : '') + month + '-' +
            (day<10 ? '0' : '') + day;
    }
    else{
        var day = date.getDate()+1;
        var imerominia = date.getFullYear() + '-' +
            (month<10 ? '0' : '') + month + '-' +
            (day<10 ? '0' : '') + day;
    }
    var result = searchItemByDate(imerominia);
    result.then(function (json) {
        console.log(json);
        var data = JSON.parse(json);
        if(data.response) {
            $('#groupset5 .itembody').append('<div class="table-responsive"><table class="table table-hover"><thead><tr><th>Όνομα Προϊόντος</th><th>Απόθεμα</th>' +
                '<th>Ημ/νία Επιστροφής</th></thead><tbody id="pinakas_epistrofwn"></tbody></table></div>');
            data.items.forEach(function (item) {
                var newrow = '<tr><td>'+item.perigrafi+'</td><td>'+item.stock+'</td><td>'+item.epistrofi+'</td></tr>';
                $('#pinakas_epistrofwn').append(newrow);

            });
        }
        else {
            bootboxMessage(data.error,1);
        }
    });
}

function searchBetweenDates() {
    formReset();
    var fromdate = $('input[name="returnsfrom"]').val(),
        todate = $('input[name="returnsto"]').val();
    if (fromdate.length>0 && todate.length>0){
        var datefrom = new Date(fromdate);
        var month = (datefrom.getMonth()+1);
        var day = datefrom.getDate();
        var from_day = datefrom.getFullYear() + '-' +
            (month<10 ? '0' : '') + month + '-' +
            (day<10 ? '0' : '') + day;
        var dateto = new Date(todate);
        var month = (dateto.getMonth()+1);
        var day = dateto.getDate();
        var to_day = dateto.getFullYear() + '-' +
            (month<10 ? '0' : '') + month + '-' +
            (day<10 ? '0' : '') + day;
        var result = searchItemBetweenDates(from_day,to_day);
        result.then(function (json) {
            console.log(json);
            var data = JSON.parse(json);
            if(data.response) {
                $('#groupset5 .itembody').append('<div class="table-responsive"><table class="table table-hover"><thead><tr><th>Όνομα Προϊόντος</th><th>Απόθεμα</th>' +
                    '<th>Ημ/νία Επιστροφής</th></thead><tbody id="pinakas_epistrofwn"></tbody></table></div>');
                data.items.forEach(function (item) {
                    var newrow = '<tr><td>'+item.perigrafi+'</td><td>'+item.stock+'</td><td>'+item.epistrofi+'</td></tr>';
                    $('#pinakas_epistrofwn').append(newrow);

                });
            }
            else {
                bootboxMessage(data.error,1);
            }

        });

    }else {
        bootboxMessage('Ελέγξτε τις ημερομηνίες',3);
    }
    
}

function showFromDay() {
    var date = new Date();
    var month = (date.getMonth()+1);
    var today = date.getDate();
    var day = date.getFullYear() + '-' +
        (month<10 ? '0' : '') + month + '-' +
        (today<10 ? '0' : '') + today;
    $('input[name="returnsfrom"]').prop('min',function () {
        return day;
    });
   $('input[name="returnsfrom"]').on('change',function () {
       var datefrom = new Date($('input[name="returnsfrom"]').val());
       var month = (datefrom.getMonth()+1);
       var tomorrow = (datefrom.getDate()+1);
       var nextday = datefrom.getFullYear() + '-' +
           (month<10 ? '0' : '') + month + '-' +
           (tomorrow<10 ? '0' : '') + tomorrow;
       $('input[name="returnsto"]').val(nextday);
       $('input[name="returnsto"]').prop('min',function () {
           return nextday;
       });
       return true;
   });
}

function showToDay() {
    if ($('input[name="returnsto"]').val()===''){
        var date = new Date();
        var month = (date.getMonth()+1);
        var tomorrow = (date.getDate()+1);
        var day = date.getFullYear() + '-' +
            (month<10 ? '0' : '') + month + '-' +
            (tomorrow<10 ? '0' : '') + tomorrow;
        $('input[name="returnsto"]').prop('min',function () {
            return day;
        });
    }

    $('input[name="returnsto"]').on('change',function () {
        if ($('input[name="returnsfrom"]').val()===''){
            var dateto = new Date($('input[name="returnsto"]').val());
            var month = (dateto.getMonth()+1);
            var yesterday = (dateto.getDate()-1);
            var previousday = dateto.getFullYear() + '-' +
                (month<10 ? '0' : '') + month + '-' +
                (yesterday<10 ? '0' : '') + yesterday;
            $('input[name="returnsfrom"]').val(previousday);
            $('input[name="returnsfrom"]').prop('min',function () {
                return previousday;
            });
        }
    return true;
    });
}