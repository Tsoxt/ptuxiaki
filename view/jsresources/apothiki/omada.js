//<editor-fold desc="othoni gia Omada">

function selectOmadaCategory(categoryId,categoryName) {
    $('#inventory').modal('hide');
    $('#onoma_category').val(categoryName);
    $('input[name="id_category"]').val(categoryId);
}

function chooseOmadaCateg() {
    var result = callCategories();
    result.then(function (json) {
        var data = JSON.parse(json);
        if (data.response==true){
            $('.flexgroup').empty();
            data.categories.forEach(function (category) {
                var newrow = '<button type="button" class="groupbut" onclick="selectOmadaCategory('+parseInt(category.id)+',\''+category.name+'\')">'+category.name+'</button>';
                $('.flexgroup').append(newrow);
                $('#inventory').modal('show');
            });
        }else{
            bootboxMessage(data.error,1);
        }
    });

}

function callOmadesCateg() {
    var result = callCategories();
    result.then(function (json) {
        var data = JSON.parse(json);
        if (data.response==true){
            $('.flexgroup').empty();
            data.categories.forEach(function (category) {
                var newrow = '<button type="button" class="groupbut" onclick="callOmada('+parseInt(category.id)+',1)">'+category.name+'</button>';
                $('.flexgroup').append(newrow);
                $('#inventory').modal('show');
            });
        }else{
            bootboxMessage(data.error,1);
        }
    });
}

function callOmada(id) {
    var result = callOmades(id);
    result.then(function (json) {
        var data = JSON.parse(json);
        if (data.response){
            $('.flexgroup').empty();
            $('.flexgroup').append('<button type="button" class="groupbut"  onclick="callOmadesCateg()"><i class="icon ion-arrow-return-left"></i></button>');
            data.omades.forEach(function (omada) {
                var newrow = '<button type="button" class="groupbut" onclick="showOmada('+parseInt(omada.id)+',1)">'+omada.name+'</button>';
                $('.flexgroup').append(newrow);
            });
        }else {
            bootboxMessage(data.error,1);
            //toDO hide modal me xronokathisterisi
        }
    });
}

function showOmada(id,type) {
    start(1);
    $("#inventory").modal('hide');
    if(parseInt(type)===1){
        var result=getOmada(id);
        result.then(function (json) {
            var data=JSON.parse(json);
            if(data.response){
                formReset();
                if(data.omada.altID===null){
                    data.omada.altID='';
                }
                if(data.omada.seira===null){
                    data.omada.seira='';
                }
                omada = {id:data.omada.id, altid:data.omada.altID, perigrafi:data.omada.name, id_cat:data.omada.id_category, active:data.omada.active,
                    order:data.omada.seira, from_api:data.omada.api, new:data.omada.new, omada_zugaria:data.omada.omada_zugaria, isozigio:data.omada.isozigio};
                writeOmada();
            } else{
                bootboxMessage(data.error,1);
            }
        });
    }else{
        writeOmada();
    }

}

function writeOmada() {
    formReset();
    $("#groupset3 .itembody").append('<div class="row1"><div class="inlineflex"><span id="checkomada">Ενεργή' +
        '<input onchange="toggleActiveOmada()" type="checkbox"><input name="active" type="hidden" value="'+omada.active+'"></span></div>' +
        '<div class="inlineflex"><span id="checkisozigio">Ισοζύγιο<input onchange="toggleIsozigioOmada()" type="checkbox"><input name="isozigio" type="hidden" value="'+omada.isozigio+'"></span></div>' +
        '<div class="row2"><input class="formrow" name="name" pattern=".{3,}" title="3 χαρακτήρες τουλάχιστον" placeholder="Ομάδα" value="'+omada.perigrafi+'" required autocomplete="off">' +
        '<input name="id" type="hidden" value="'+omada.id+'"><input name="altId" type="hidden" value="'+omada.altid+'">' +
        '<input name="id_category" type="hidden" value="'+omada.id_cat+'"><input name="seira" type="hidden" value="'+omada.order+'"><input name="api" type="hidden" value="'+omada.from_api+'">' +
        '<input name="new" type="hidden" value="'+omada.new+'"></div></div><div class="row1"><input class="formrow" id="onoma_category" placeholder="Επιλέξτε κατηγορία" onfocus="chooseOmadaCateg()" required readonly>' +
        '<input class="formrow" type="number" name="omada_zugaria" min="1" max="99" step="1" placeholder="Κωδικός στη ζυγαριά" data-toggle="tooltip" data-placement="bottom" title="Κωδικός στη ζυγαριά" value="'+omada.omada_zugaria+'"></div>' +
        '<div class="row1"><div class="groupinfo"></div></div><div class="itembuttons"><button class="button4">Αποθήκευση</button></div>');
    if (parseInt(omada.active) === 1){
        $('#checkomada input[type="checkbox"]').prop('checked',true);
    }
    if (parseInt(omada.isozigio)===1){
        $('#checkisozigio input[type="checkbox"]').prop('checked',true);
    }
    if (parseInt(omada.new) === 0) {
        var result = callItems(parseInt(omada.id));
        result.then(function (json) {
            var data = JSON.parse(json);
            var synolo;
            if (data.response){
                synolo = parseInt(data.items.length);
            }else {
                synolo = 0;
            }
            $('.groupinfo').append('Σύνολο προιόντων: <span class="badge">'+synolo+'</span>');
        });
        var omada_name = getCategory(parseInt(omada.id_cat));
        omada_name.then(function (json) {
            var data = JSON.parse(json);
            if (data.response){
                $('#onoma_category').val(data.category.name);
            }else {
                bootboxMessage(data.error,1);
            }
        });
    }
    if (parseInt(omada.from_api)===1){
        $('input[name="name"]').prop('readonly',true);
        $('#onoma_category').prop('disabled',true);
    }
    bindOmadaListeners();
}

function bindOmadaListeners(){
    $('input').off();
    $('select').off();
    $('#insert_omada').off();
    $('[data-toggle="tooltip"]').tooltip();
    $('#insert_omada').on('submit',function (event) {
        event.preventDefault();
        $('#onoma_category').prop('readonly', false);
        if($(this)[0].checkValidity()) {
            var form = $('#insert_omada').serializeArray();
            var data= {};
            form.forEach(function (field) {
                data[field.name]=field.value;
            });
            var action;
            if (parseInt(omada.new)===1) {
                action='insertomada';
            }else{
                action='updateomada';
            }
            var result = editOmada(data,action);
            result.then(function (json) {
                console.log(json);
                var data = JSON.parse(json);
                if(data.response==true){
                    bootboxMessage('Επιτυχής καταχώρηση',2);
                }
                else{
                    bootboxMessage(data.error,1);
                }
                formReset();
                start(1);
            });
            return true;
        }else {
            $(this)[0].reportValidity();
            $('#onoma_category').prop('readonly', true);
        }
    });

}

function toggleActiveOmada() {
    var active = parseInt($("input[name='active']").val());
    if (active===1){
        $("input[name='active']").val(0);
    }else {
        $("input[name='active']").val(1);
    }
}

function toggleIsozigioOmada(){
    var isozigio = parseInt($("input[name='isozigio']").val());
    if (isozigio===1){
        $("input[name='isozigio']").val(0);
    }else {
        $("input[name='isogio']").val(1);
    }
}


//</editor-fold desc="othoni gia Omada">