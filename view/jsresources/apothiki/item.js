//<editor-fold desc="Item Utilities">

//<editor-fold desc="BarcodeModal">

function barcodeModal() {
    $('#bcodeModal').modal('show');
}

//</editor-fold desc="BarcodeModal">

//TODO neo toggle kai antistixo listener
function getPattern(type){//1 zugistiko 0 oxi zugistiko
    var pattern;
    if(type===0){
        pattern='^(?!2)[0-9]{8}$|^(?!2)[0-9]{13}$|^(?!2)[0-9]{15}$|^(?!2)[0-9]{17}$';
    }else{
        var i=0;
        var start;
        Object.keys(specialbcodes).forEach(function (barcode) {
            if(i===0){
                start=barcode;
            }else{
                start+='|'+barcode;
            }
            i++;
        });
        pattern='^('+start+')[0-9]{5}$';
    }
    return pattern;
}

function changeMothercode() {
    if ($('.mothercode_mitrikou').val()!==''){
        var newMothercode=parseInt($('.mothercode_mitrikou').val());
        $('input[name="mothercode"]')
            .val(newMothercode)
            .trigger('change');
        $('.allagi_mothercode input').val('');
        item.mothercode=newMothercode;
        $('a[href="#product_similar"]').trigger('click');
    }
}

function bindItemListeners(){
    $('input').off();
    $('select').off();
    $('#itemShow').off();
    $('[data-toggle="tooltip"]').tooltip();
    $('#checkactive input[type="checkbox"]').on('change',function(){
        toggleActive();
    });
    $('#checktimi input[type="checkbox"]').on('change',function(){
        toggleFlagTimi();
    });
    $('#checkTimiAgoras input[type="checkbox"]').on('change',function(){
        toggleFlagTimiAgoras();
    });
    $('a[href="#product_similar"]').click(function () {
        $('#product_similar tbody').empty();
        $('.allagi_mothercode').empty();
        if(!(($('input[name="barcode"]').val().length===0) || (parseInt($('input[name="zigos"]').val()) === 1))) {
            $('.allagi_mothercode').append( '<div class="row1">' +
                '<input type="number" class="formrow" id="anazitisi_mitrikou" placeholder="Αναζήτηση μητρικού.."><input name="onoma_mitrikou" class="formrow" disabled placeholder="Όνομα">' +
                '<input name="siskevasia_mitrikou" class="formrow" disabled placeholder="Συσκευασία"><input name="apothema_mitrikou" class="formrow" disabled placeholder="Απόθεμα">' +
                '<input class="mothercode_mitrikou" hidden><button type="button" class="submitbtn" onclick="changeMothercode()"><i class="fas fa-exchange-alt"></i></button>' +
                '</div>');
            $("#anazitisi_mitrikou").on('keypress', function (event) {
                if (event.keyCode === 13){
                    $(this).blur();
                    event.preventDefault();
                    var barcode = $(this).val();
                    $(this).val('');
                    var action = 'bybarcode';
                    var result = searchItem(barcode,action);
                    result.then(function (json) {
                        console.log(json);
                        var data = JSON.parse(json);
                        if (data.response) {
                            if(parseInt(data.proion.new)===0){
                                $("input[name='onoma_mitrikou']").val(data.proion.perigrafi);
                                $("input[name='siskevasia_mitrikou']").val(data.proion.temaxia);
                                $("input[name='apothema_mitrikou']").val(data.proion.stock);
                                $(".mothercode_mitrikou").val(data.proion.id);
                            }else{
                                bootboxMessage("Το προϊόν δεν υπάρχει στην βάση",1)
                            }
                        }
                        else {
                            bootboxMessage(data.error,1);
                        }
                    });
                }else{
                    return true;
                }
            });
        }
        if (parseInt(item.new)===0){
            var action='similarproducts';
            var mothercode=parseInt(item.mothercode);
            var result=searchItem(mothercode,action);
            result.then(function (json) {
                console.log(json);
                var data=JSON.parse(json);
                if (data.response){
                    var energo;
                    data.proionta.forEach(function (row) {
                        if (row.barcode===null){
                            row.barcode='-';
                        }
                        if (parseInt(row.active)===1){
                            energo='<i class="fas fa-check-square"></i>';
                        }else {
                            energo='<i class="fas fa-window-close"></i>';
                        }
                       if (parseInt(item.mothercode)===parseInt(row.id)){
                           $('#product_similar tbody').prepend('<tr class="info">' +
                               '<td>'+row.perigrafi+'</td>' +
                               '<td>'+row.barcode+'</td>' +
                               '<td>'+row.temaxia+'</td>' +
                               '<td>'+row.stock+'</td>' +
                               '<td>'+energo+'</td>' +
                               '</tr>');
                       }else {
                           $('#product_similar tbody').append('<tr class="active">' +
                               '<td>'+row.perigrafi+'</td>' +
                               '<td>'+row.barcode+'</td>' +
                               '<td>'+row.temaxia+'</td>' +
                               '<td>'+row.stock+'</td>' +
                               '<td>'+energo+'</td>' +
                               '</tr>');
                       }
                    });
                }else {
                    bootboxMessage(data.error,1);
                }
            });
        }else {
            $('#product_similar tbody').append('<div style="text-align: center">-Δεν υπάρχουν σχετικά προϊόντα-</div>')
        }

    });

    $('input[name="package"]').on('change', function(){
        updatePackage();
    });
    $('input[name="temaxia"]').on('change', function(){
        updateStock();
    });
    if(parseInt(item.from_api)===1){
        $('select[name="tmima"]').on('change',function(){
            showFpa();
        });
        if(parseInt(item.new)===0){
            $('input[name="desc"], input[name="desc2"],input[name="price"]').on('change', function () {
                var name=$(this).attr('name');
                handleItemInput(name);
            });
        }
    }else{
        $('select[name="tmima"]').on('change',function(){
            showFpa();
        });
        $('#checkzigos input[type="checkbox"]').on('change',function(){
            toggleZugistiko();
        });
        $('#temaxiako input[type="checkbox"]').on('change', function () {
            toggleTemaxiako();
        });
        $('#zugaria input[type="checkbox"]').on('change', function () {
            toggleZugaria();
        });
        $('input[name="category"]').on('focus', function(){
            showItemCategories();
        });
        $('input[name="omada"]').on('focus', function(){
            showItemOmada();
        });
        if(parseInt(item.new)===0){
            $('input[name="desc"], input[name="desc2"], input[name="barcode"], input[name="price"]').on('change', function () {
                var name=$(this).attr('name');
                handleItemInput(name);
            });
        }else{
            $('input[name="barcode"]').on('change', function () {
                if($(this).val().length===0 && $(this)[0].checkValidity()){
                    $('input[name="mothercode"]').val(item.id).trigger('change');
                }
            });
        }
    }
    if(parseInt(item.new)===1){
        $('input[name="mothercode"]').on('change', function () {
            var name=$(this).attr('name');
            var sunolo=getSumStock($('input[name="mothercode"]').val());
            sunolo.then(function (json) {
                var data=JSON.parse(json);
                var sunolo=parseFloat(data.proion.sunolo);
                var stockDifference;
                if(data.response){
                    if(parseInt($('input[name="zigos"]').val())===0){
                        stockDifference=parseInt((parseInt(stock.package*stock.apothema)+sunolo)-stock.sunolo);
                        stock.sunolo+=stockDifference;
                    }else{
                        stockDifference=parseFloat((parseFloat(stock.package*stock.apothema)+sunolo)-stock.sunolo);
                        stock.sunolo+=stockDifference;
                        stock.sunolo=parseFloat(stock.sunolo).toFixed(3);
                    }
                    $("#sumstock").val(stock.sunolo);
                }else{
                    bootboxMessage(data.error,3);
                }
            });
        });
        $('#itemShow').on('submit', function (event) {
            event.preventDefault();
            $('input[name="omada"], input[name="category"]').prop('readonly', false);
            if(parseInt(item.from_api)===1){
                //$('select[name="tmima"]').prop('disabled', false);
            }
            if($(this)[0].checkValidity()){
                var form = $('#itemShow').serializeArray();
                var data= {};
                form.forEach(function (field) {
                    if (field.value===''){
                        field.value=null;
                    }
                    data[field.name]=field.value;
                });
                var result = insertItem(data);
                result.then(function (json) {
                    console.log(json);
                    var data = JSON.parse(json);
                    if(data.response){
                        bootboxMessage('Επιτυχής καταχώρηση',2);
                    }
                    else{
                        bootboxMessage(data.error,1);
                    }
                    formReset();
                    start(0);
                });
                return true;
            }else{
                $(this)[0].reportValidity();
                $('input[name="omada"], input[name="category"]').prop('readonly', true);
                if(parseInt(item.from_api)===1){
                   // $('select[name="tmima"]').prop('disabled', true);
                }
            }
        });
    }else{
        $('input[name="mothercode"]').on('change', function () {
            var name=$(this).attr('name');
            var sunolo=getSumStock($('input[name="mothercode"]').val());
            sunolo.then(function (json) {
                var data=JSON.parse(json);
                if(data.response){
                    var sunolo=parseFloat(data.proion.sumstock);
                    if(parseInt($('input[name="zigos"]').val())===0){
                        stock.sunolo=parseInt(sunolo);
                    }else{
                        stock.sunolo=parseFloat(sunolo).toFixed(3);
                    }
                    $("#sumstock").val(stock.sunolo);
                    handleItemInput(name);
                }else{
                    bootboxMessage(data.error,1);
                }
            });
        });

        $('input[name="epistrofi"],input[name="alert"],input[name="recommend"],input[name="agora"]').on('change',function () {
            var name=$(this).attr('name');
            handleItemInput(name);
        });
    }

    //<editor-fold desc="Mothercode Listeners">

    //</editor-fold>

}

function showItemCategories() {
    var result = callCategories();
    result.then(function (json) {
        var data = JSON.parse(json);
        if (data.response==true){
            $('.flexgroup').empty();
            data.categories.forEach(function (category) {
                var newrow = '<button type="button" class="groupbut" onclick="selectItemCategory('+parseInt(category.id)+',\''+category.name+'\')">'+category.name+'</button>';
                $('.flexgroup').append(newrow);
                $('#inventory').modal('show');
            });
        }else{
            bootboxMessage(data.error,1);
        }
    });
}

function showItemOmada() {
    if ($("input[name='category']").val()==''){
        bootboxMessage('Πρέπει να συμπληρώσετε πρώτα την κατηγορία',3);
    }else {
        var id = parseInt($("input[name='id_cat']").val());
        var result = callOmades(id);
        result.then(function (json) {
            var data = JSON.parse(json);
            if (data.response==true){
                $('.flexgroup').empty();
                data.omades.forEach(function (omada) {
                    var newrow = '<button type="button" class="groupbut" onclick="selectItemOmada('+parseInt(omada.id)+',\''+omada.name+'\')">'+omada.name+'</button>';
                    $('.flexgroup').append(newrow);
                    $('#inventory').modal('show');
                });
            }else{
                bootboxMessage(data.error,1);
            }
        });
    }
}

//</editor-fold>


//<editor-fold desc="search Screen">

function showcategories(){
    var result = callCategories();
    result.then(function (json) {
        var data = JSON.parse(json);
        if (data.response){
            $('.flexgroup').empty();
            data.categories.forEach(function (category) {
                var newrow = '<button type="button" class="groupbut" onclick="showomades('+parseInt(category.id)+')">'+category.name+'</button>';
                $('.flexgroup').append(newrow);
            });
            $('#inventory').modal('show');
        } else {
            bootboxMessage(data.error,1);
        }
    });
}

function showomades(id) {
    var result = callOmades(id);
    result.then(function (json) {
        var data = JSON.parse(json);
        if (data.response){
            $('.flexgroup').empty();
            $('.flexgroup').append('<button type="button" class="groupbut"  onclick="showcategories()"><i class="icon ion-arrow-return-left"></i></button>');
            data.omades.forEach(function (omada) {
                var newrow = '<button type="button" class="groupbut" onclick="showItems('+parseInt(omada.id)+', '+id+')">'+omada.name+'</button>';
                $('.flexgroup').append(newrow);
            });
        }else {
            bootboxMessage(data.error,1);
            //toDO hide modal me xronokathisterisi
        }
    });
}

function showItems(omadaId, categoryId) {
    var result = callItems(omadaId);
    result.then(function (json) {
        var data = JSON.parse(json);
        console.log(data);
        if (data.response==true){
            $('.flexgroup').empty();
            $('.flexgroup').append('<button type="button" class="groupbut"  onclick="showomades('+categoryId+')"><i class="icon ion-arrow-return-left"></i></button>');
            data.items.forEach(function (proion) {
                var newrow = '<button type="button" class="groupbut" onclick="callItem('+parseInt(proion.id)+',1)">'+proion.perigrafi+'</button>';
                $('.flexgroup').append(newrow);
            });
        }else {
            bootboxMessage(data.error,1);
            //toDo hide me xronokathisterisi
        }
    });
}

//</editor-fold>


//<editor-fold desc="call Item">

function chooseSameProion(id) {
    $('#choose_item').modal('hide');
    callItem(id,1);
}

function callItem(id, type) {//type=0 bybarcode && type=1 byid && type=3 new item
    start(0);
    var action;
    if (parseInt(type)===0){
        action = 'bybarcode';
        var checktwo = id.substring(0,2);
        if (!(specialbcodes[checktwo] === undefined)){//ean anikei sta specialbarcodes
            id=id.substring(0,7);
        }
    }else if(parseInt(type)===1){
        $("#inventory").modal('hide');
        action = 'byid';
    }
    if(!(parseInt(type)===3)){
        var result=searchItem(id,action);
        result.then(function (json) {
            var data=JSON.parse(json);
            if(data.response){
                formReset();
                if (typeof data.same_bcode !== 'undefined'){//ean yparxoun proionta me to idio barcode
                    $('#choose_item .sameBarcodeGroup').empty();
                    data.same_bcode.forEach(function (field) {
                        $('#choose_item .sameBarcodeGroup').append('<button class="sameBarcodeProion" onclick="chooseSameProion('+field.id+')">'+field.perigrafi+' '+field.timi+' €</button>');
                    });
                    $('#choose_item').modal('show');
                }else {
                    if(data.proion.barcode===null){
                        data.proion.barcode='';//toDO pattern i keno i 13 i 7 me 21
                    }
                    if (data.proion.epistrofi===null){
                        data.proion.epistrofi='';
                    }
                    //toDO CHristos φτιάξτα ένα ένα
                    item = {id:parseInt(data.proion.id), barcode:data.proion.barcode, mothercode:data.proion.mothercode, perigrafi:data.proion.perigrafi, perigrafi2:data.proion.perigrafi2,
                        id_om:data.proion.id_omadas, timi:data.proion.timi, timi_agoras:data.proion.timi_agoras, flag_timi:data.proion.flag_timi, tmima:data.proion.tmima, apothema:data.proion.stock,
                        alert:data.proion.stock_alert,recommend:data.proion.stock_recommend,siskevasia:data.proion.temaxia, energo:data.proion.active, new:data.proion.new, seira:data.proion.seira, epistrofi:data.proion.epistrofi,
                        zugistiko:data.proion.zugistiko, om_name:data.proion.om_desc, cat_name:data.proion.cat_desc, id_cat:data.proion.id_category, from_api:data.proion.api,
                        zugaria:data.proion.zugaria,temaxiako:data.proion.temaxiako, alt_id:data.proion.alt_id};
                    writeItem();
                }
            } else{
                bootboxMessage(data.error,1);
                if (parseInt(type)===0){
                    item.barcode=parseInt(id);
                    writeItem();
                }
            }
        });
    }else{
        writeItem();
    }
}

function writeItem(){
    formReset();
    var itembody='' +
        '<ul class="nav nav-tabs" role="tablist">' +
        '<li role="presentation" class="active"><a href="#product_info" aria-controls="product_info" role="tab" data-toggle="tab">Γενικά</a></li>' +
        '<li role="presentation"><a href="#product_code" aria-controls="product_code" role="tab" data-toggle="tab">Κωδικός</a></li>' +
        '<li role="presentation"><a href="#product_stock" aria-controls="product_stock" role="tab" data-toggle="tab">Απόθεμα</a></li>' +
        '<li role="presentation"><a href="#product_similar" aria-controls="product_similar" role="tab" data-toggle="tab">Σχετικά Προϊόντα</a></li>' +
        '</ul>' +
        '<div class="tab-content">' +
        '<div role="tabpanel" class="tab-pane fade in active" id="product_info">' +
        '<div class="tabcontent">' +
        '<div class="row1"><div class="inlineflex"><span id="checkactive">Ενεργό<input type="checkbox"><input name="active" type="hidden"></span></div>' +
        '<div class="row2"><input class="formrow" name="desc" placeholder="Περιγραφή" autocomplete="off" required>' +
        '<input class="formrow" name="desc2" placeholder="Άλλη Περιγραφή" autocomplete="off"></div></div>' +
        '<div class="row1"><input class="formrow" name="category" placeholder="Κατηγορία" required readonly autocomplete="off">' +
        '<input class="formrow" name="omada" placeholder="Ομάδα" required readonly autocomplete="off">' +
        '<input name="id_om" type="hidden"><input name="id_cat" type="hidden"></div>' +
        '<div class="row1"><div class="inlineflex"><span id="checktimi">Σταθερή τιμή<input type="checkbox"><input name="flag_timi" type="hidden"></span></div>' +
        '<div class="row2" id="price"><input class="formrow" type="number" min="0.01" step="0.01" name="price" placeholder="Τιμή" required>' +
        '<select name="tmima" class="formrow" required><option selected value="">Επιλέξτε τμήμα</option></select>' +
        '<input class="formrow" name="fpa" value="" placeholder="ΦΠΑ" readonly></div></div>' +
        '<div class="row1"><div class="inlineflex"><span id="checkTimiAgoras">Ενημέρωση Τιμής Αγοράς Σχετικών Προϊόντων<input type="checkbox"><input name="flag_timi_agoras" value="0" type="hidden"></span></div>' +
        '<div class="row2" id="buy_price"><input class="formrow" type="number" min="0" step="0.01" name="agora" placeholder="Τιμή Αγοράς/τμχ-kg"></div></div>' +
        '<div class="row1"><div class="inlineflex"><span>Ημερομηνία Λήξεως-Επιστροφής</span></div><div class="row2"><input class="formrow" name="epistrofi" type="date"></div></div>' +
        '</div>' +
        '</div>' +
        '<div role="tabpanel" class="tab-pane fade" id="product_code">' +
        '<div class="tabcontent">' +
        '<div class="row1"><div class="inlineflex"><span id="checkzigos">Ζυγιστικό<input type="checkbox"><input name="zigos" type="hidden"></span></div>' +
        '<div class="inlineflex"><span id="temaxiako">Τεμαχιακό<input type="checkbox"><input name="temaxiako" type="hidden"></span></div>' +
        '<div class="inlineflex"><span id="zugaria">Ενημέρωση Ζυγαριάς<input type="checkbox"><input name="zugaria" type="hidden"></span></div>' +
        '<div class="row2" id="flag"><input class="formrow" name="barcode" placeholder="Barcode" autocomplete="off"></div></div>' +
        '</div>' +
        '</div>' +
        '<div role="tabpanel" class="tab-pane fade" id="product_stock">' +
        '<div class="tabcontent">' +
        '<div class="row1"><div class="inlineflex"><span>Συσκευασία</span></div><input class="formrow" name="package" type="number" min="1" step="1" placeholder="Συσκευασία" data-toggle="tooltip" data-placement="bottom"  title="Συσκευασία" required></div> ' +
        '<div class="row1"><div class="inlineflex"><span>Προσθήκη</span></div><input class="formrow" name="temaxia" type="number"placeholder="Τεμάχια/kg">' +
        '<div class="inlineflex"><span>Απόθεμα</span></div><input class="formrow" id="stock" type="number" placeholder="Απόθεμα" data-toggle="tooltip" data-placement="bottom"  title="Απόθεμα"  readonly></div>' +
        '<div class="row1"><div class="inlineflex"><span>Συνολικό απόθεμα σχετικών προϊόντων</span></div><input class="formrow" id="sumstock" type="number" placeholder="Συνολικό Απόθεμα" data-toggle="tooltip" data-placement="bottom"  title="Συνολικό απόθεμα" readonly></div>' +
        '<div class="row1"><div class="inlineflex"><span>Ειδοποίηση Αποθέματος</span></div><input class="formrow" name="alert" type="number" min="1" step="1" placeholder="Ειδοποίηση ποσότητας" data-toggle="tooltip" data-placement="bottom"  title="Ειδοποίηση αποθέματος">' +
        '<div class="inlineflex"><span>Προτεινόμενη Αγορά</span></div><input class="formrow" name="recommend" type="number" min="1" step="1" placeholder="Προτεινόμενη ποσότητα" data-toggle="tooltip" data-placement="bottom"  title="Προτεινόμενη ποσότητα αγοράς"></div>' +
        '</div>' +
        '</div>' +
        '<div role="tabpanel" class="tab-pane fade" id="product_similar">' +
        '<div class="tabcontent">' +
        '<div class="allagi_mothercode">' +
        '</div>' +
        '<div class="table-responsive">' +
        '<table class="table">' +
        '<thead>' +
        '<tr>' +
        '<th>Όνομα</th>' +
        '<th>Barcode</th>' +
        '<th>Συσκευασία</th>' +
        '<th>Απόθεμα</th>' +
        '<th>Ενεργό</th>' +
        '</tr></thead><tbody></tbody></table></div> ' +
        '<div class="row1"><input name="mothercode" hidden>' +
        '<input name="api" hidden><input name="id" hidden>' +
        '<input name="alt_id" type="hidden"></div>' +
        '</div>' +
        '</div>' +
        '</div>';
    $('#groupset1 .itembody').append(itembody);
    $('input[name="desc"]').val(item.perigrafi);
    $('input[name="desc2"]').val(item.perigrafi2);
    $('input[name="category"]').val(item.cat_name);
    $('input[name="omada"]').val(item.om_name);
    $('input[name="id_cat"]').val(item.id_cat);
    $('input[name="id_om"]').val(item.id_om);
    $('input[name="id"]').val(item.timi);
    $('input[name="api"]').val(item.from_api);
    $('input[name="active"]').val(item.energo);
    $('input[name="flag_timi"]').val(item.flag_timi);
    $('input[name="zigos"]').val(item.zugistiko);
    $('input[name="mothercode"]').val(item.mothercode);
    $('input[name="zugaria"]').val(item.zugaria);
    $('input[name="temaxiako"]').val(item.temaxiako);
    $('input[name="alt_id"]').val(item.alt_id);
    $('input[name="agora"]').val(item.timi_agoras);
    $('input[name="epistrofi"]').val(item.epistrofi);
    if(parseInt(item.energo)===1){
        $('#checkactive input[type="checkbox"]').prop('checked',true);
    }
    if(parseInt(item.zugistiko)===1){
        $('#checkzigos input[type="checkbox"]').prop('checked', true);
        $('input[name="barcode"]').val(item.barcode).prop('pattern', getPattern(parseInt(item.zugistiko))).prop('required', true);
        $('input[name="package"]').val(1).prop('readonly', true);
        $('#stock').val(parseFloat(item.apothema).toFixed(3));
        $('input[name="alert"]')
            .val(parseFloat(item.alert).toFixed(3))
            .prop('step', '0.001');
        $('input[name="recommend"]')
            .val(parseFloat(item.recommend).toFixed(3))
            .prop('step', '0.001');
        $('input[name="temaxia"]').prop('step', '0.001');
        if(parseInt(item.temaxiako)===1){
            $('#temaxiako input[type="checkbox"]').prop('checked',true);
        }
        if (parseInt(item.zugaria)===1){
            $('#zugaria input[type="checkbox"]').prop('checked',true);
        }
    }else{
        $('input[name="barcode"]').val(item.barcode).prop('pattern', getPattern(parseInt(item.zugistiko)));
        $('input[name="package"]').val(parseInt(item.siskevasia));
        $('#stock').val(parseInt(item.apothema));
        $('input[name="alert"]')
            .val(parseInt(item.alert))
            .prop('step', '1');
        $('input[name="recommend"]')
            .val(parseInt(item.recommend))
            .prop('step', '1');
        $('input[name="temaxia"]').prop('step', '1');
        $('#temaxiako input[type="checkbox"]').prop('disabled',true);
        $('#zugaria input[type="checkbox"]').prop('disabled',true);
    }
    if(parseInt(item.flag_timi)===0){
        $('input[name="price"]').val(parseFloat(0).toFixed(2)).prop('readonly', true);
    }else{
        $('input[name="price"]').val(parseFloat(item.timi).toFixed(2));
        $('#checktimi input[type="checkbox"]').prop('checked', true);
    }
    if(parseInt(item.from_api)===1){
        $('input[name="barcode"]').prop('readonly', true);
        //$('select[name="tmima"]').prop('disabled', true);
        $('#checkzigos input[type="checkbox"]').prop('disabled', true);
        $('#zugaria input[type="checkbox"]').prop('disabled',true);
    }
    if ((!(item.alt_id==null)) && (item.alt_id!=='')){
        if (item.barcode){
            $('input[name="barcode"]').prop('readonly',true);
        } else {
            $('input[name="barcode"]').prop('readonly',false);
        }
        $('input[name="temaxia"]').prop('readonly', true);
        $('input[name="package"]').prop('readonly', true);
        $('input[name="epistrofi"]').prop('readonly', true);
        $('input[name="desc"], input[name="desc2"], input[name="price"]').prop('readonly', true);
        //$('select[name="tmima"]').prop('disabled', true);
        $('#checkTimiAgoras input[type="checkbox"]').prop('disabled', true);
        $('#checkzigos input[type="checkbox"]').prop('disabled', true);
        $('#zugaria input[type="checkbox"]').prop('disabled',true);
        $('#checktimi input[type="checkbox"]').prop('disabled',true);
    }
    if (parseInt(item.new) === 1) {
        $('#product_info .tabcontent').append('<div class="itembuttons"><button class="button2" type="button" onclick="callItem(0,3)"><i class="fas fa-redo-alt"></i></button>' +
            '<button class="button3"><i class="fas fa-save"></i></button></div>');
    }
    bindItemListeners();
    var tmimata = callFpa('calltmimata',0);
    tmimata.then(function (json) {
        var tmima = JSON.parse(json);
        tmima.tmimata.forEach(function (fpa) {
            aksia[fpa.id]=fpa.price;
            if (parseInt(fpa.id)===parseInt(item.tmima)){
                $("select[name='tmima']").append('<option selected value="'+fpa.id+'">'+fpa.name+'</option>');
                $('input[name="fpa"]').val(parseFloat(fpa.price)*100+'%');
            } else {
                $("select[name='tmima']").append('<option value="'+fpa.id+'">'+fpa.name+'</option>');
            }
        });
        var sunolo=getSumStock(item.mothercode);
        sunolo.then(function (json) {
            var data=JSON.parse(json);
            if(data.response){
                if(parseInt(item.zugistiko)===1){
                    stock.package=1;
                    stock.apothema=parseFloat(item.apothema).toFixed(3);
                    stock.sunolo=parseFloat(data.proion.sumstock).toFixed(3);
                }else{
                    stock.package=parseInt(item.siskevasia);
                    stock.apothema=parseInt(item.apothema);
                    stock.sunolo=parseInt(data.proion.sumstock);
                }
                $("#sumstock").val(stock.sunolo);
            }else{
                bootboxMessage(data.error,1);
            }
        });
    });
}

//</editor-fold>


//<editor-fold desc="Updates">

function handleItemInput(name) {
    var action,
        data,
        pedio = $('input[name="'+name+'"]');
    var id = parseInt(item.id);
    if (!(pedio[0].checkValidity())){
        pedio[0].reportValidity();
    }
    else {
        if (name==='price'){
            action = 'updateprice';
            data = {id:id, ftimi: $("input[name='flag_timi']").val(), price:$("input[name='price']").val()};
        }else if (name==='desc'){
            action = 'updatedescription';
            data = {id:id, description: $("input[name='desc']").val()};
        }else if (name==='desc2'){
            action = 'updatealtdescription';
            data = {id:id, description2: $("input[name='desc2']").val()};
        } else if (name==='barcode') {
            action = 'updatebarcode';
            data = {id: id, barcode: $("input[name='barcode']").val(), zugistiko: $("input[name='zigos']").val(), zugaria: $("input[name='zugaria']").val(), temaxiako:$('input[name="temaxiako"]').val()};
            if(pedio.val().length===0){
                $('input[name="mothercode"]').val(item.id).trigger('change');
            }
        }else if(name==='mothercode') {
            action = 'updatemcode';
            data = {id: id, mcode: $('input[name="mothercode"]').val()};
        }else if(name==="omada") {
            action = 'updateitemomada';
            data = {id: id, omada: $('input[name="id_om"]').val()};
        }else if(name==='temaxia') {
            action = 'updatestock';
            data = {id: id, stock: $('input[name="temaxia"]').val()};
            $('input[name="temaxia"]').val('');
        }else if(name==='package') {
            action = 'updatepackage';
            data = {id: id, package: stock.package};
        }else if(name==='active'){
            action='updateactive';
            data={id:id,active:$('input[name="active"]').val()};
        }else if (name==='zugaria'){
            action='updatezugaria';
            data={id:id,zugaria: $('input[name="zugaria"]').val()};
        }else if(name==='epistrofi'){
            action='updateEpistrofi';
            data={id:id,epistrofi:pedio.val()};
        }else if (name==='alert'){
            action='updatestockalert';
            data={id:id,alert:pedio.val()};
        }else if (name==='recommend'){
            action='updatestockrecommend';
            data={id:id,recommend:pedio.val()};
        }else if (name==='agora') {
            action = 'updatetimiagoras';
            data = {id: id, timi_agoras: pedio.val(),flag_agoras:$("input[name='flag_timi_agoras']").val(),mothercode:$("input[name='mothercode']").val()};
        }else{
            return true;
        }
        var result = updateItem(action,data);
        result.then(function (json) {
            console.log(json);
            var data = JSON.parse(json);
            if (data.response){
                pedio[0].reportValidity();//TODO check if working
            } else{
                bootboxMessage(data.error,1);
            }
        });
    }
}


function selectItemCategory(categoryId,categoryName){
    $('#inventory').modal('hide');
    $('input[name="category"]').val(categoryName);
    $('input[name="id_cat"]').val(categoryId);
    $('input[name="omada"]').val('');
    $('input[name="id_om"]').val('');
}

function selectItemOmada(omadaId,omadaName){
    $('#inventory').modal('hide');
    $('input[name="omada"]').val(omadaName);
    $('input[name="id_om"]').val(omadaId);
    if(parseInt(item.new)===0){
        handleItemInput('omada');
    }
}

function updateStock() {
    if(!((parseFloat($("input[name='temaxia']").val())===0) || $("input[name='temaxia']").val().length===0)){
        if ($("input[name='temaxia']")[0].checkValidity()) {
            var temaxia = parseFloat($("input[name='temaxia']").val());
            if(parseInt($('input[name="zigos"]').val())===1){
                stock.apothema = parseFloat(stock.apothema)+ temaxia;
                stock.sunolo = parseFloat(stock.sunolo)+ parseFloat(temaxia * stock.package);
                stock.sunolo=parseFloat(stock.sunolo).toFixed(3);
                stock.apothema=parseFloat(stock.apothema).toFixed(3);
            }else{
                stock.apothema += temaxia;
                stock.sunolo += (temaxia * stock.package);
                stock.apothema=parseInt(stock.apothema);
                stock.sunolo=parseInt(stock.sunolo);
            }
            $("#stock").val(stock.apothema);
            $("#sumstock").val(stock.sunolo);
            if(parseInt(item.new)===0){
                handleItemInput('temaxia')
            }
        } else {
            $("input[name='temaxia']")[0].reportValidity();
        }
    }
}

function updatePackage() {
    if($('input[name="package"]')[0].checkValidity()){
        var Package=parseInt($("input[name='package']").val());
        var stockDifference=Package-stock.package;
        stock.package=Package;
        stock.sunolo+=parseInt(stockDifference*stock.apothema);
        $("#sumstock").val(stock.sunolo);
        if(parseInt(item.new)===0) {
            handleItemInput('package');
        }
    }else{
        $('input[name="package"]')[0].reportValidity();
    }
}

function showFpa() {
    var tmima = $("select[name='tmima']").val();
    var id = parseInt(item.id);
    if(tmima.length!==0){
        if(parseInt(item.new)===0){
            var action = 'updatetmima';
            var data = {id: id, tmima: parseInt(tmima)};
            var result=updateItem(action,data);
            result.then(function (json) {
                var data=JSON.parse(json)
                if(data.response){
                    var fpa = parseFloat(aksia[tmima]) * 100;
                    $("input[name='fpa']").val(fpa+'%');
                }else{
                    bootboxMessage(data.error,1);
                }
            });
        }else{
            var fpa = parseFloat(aksia[tmima]) * 100;
            $("input[name='fpa']").val(fpa+'%');
        }
    }else{
        $("input[name='fpa']").val('');
    }
}

//<editor-fold desc="Checkboxes">
function toggleZugaria() {
    var zugaria=parseInt($("input[name='zugaria']").val());
    if (zugaria===1){
        $("input[name='zugaria']").val(0);
        $('#zugaria input[type="checkbox"]').prop('checked',false);
    }else {
        $("input[name='zugaria']").val(1);
        $('#zugaria input[type="checkbox"]').prop('checked',true);
    }
    if (parseInt(item.new)===0){
        handleItemInput('zugaria');
    }
}

function toggleZugistiko() {
    var zugistiko=parseInt($("input[name='zigos']").val());
    if (zugistiko===1){
        zugistiko=0;
        $('#temaxiako input[type="checkbox"]').prop('disabled', true);
        $('#zugaria input[type="checkbox"]').prop('disabled', true);
        $("input[name='zigos']").val(0);
        $('input[name="barcode"]').prop('required', false).prop('pattern', getPattern(0));
        $('input[name="package"]').prop('readonly', false);
        $('input[name="temaxia"]').prop('step', '1');
        $('input[name="alert"]')
            .val(parseInt(item.alert))
            .prop('step', '1');
        $('input[name="recommend"]')
            .val(parseInt(item.recommend))
            .prop('step', '1');
        $("#stock").val(parseInt(stock.apothema));
        $("#sumstock").val(parseInt(stock.sunolo));
    }else {
        zugistiko=1;
        $('#temaxiako input[type="checkbox"]').prop('disabled',false);
        $('#zugaria input[type="checkbox"]').prop('disabled',false);
        $("input[name='zigos']").val(1);
        $('input[name="barcode"]').prop('required', true).prop('pattern', getPattern(1));
        $('input[name="package"]').prop('readonly', true);
        $('input[name="temaxia"]').prop('step', '0.001');
        $('input[name="alert"]')
            .val(parseFloat(item.alert).toFixed(3))
            .prop('step', '0.001');
        $('input[name="recommend"]')
            .val(parseFloat(item.recommend).toFixed(3))
            .prop('step', '0.001');
        $("#stock").val(parseFloat(stock.apothema).toFixed(3));
        $("#sumstock").val(parseFloat(stock.sunolo).toFixed(3));
    }
    bindItemListeners();
    if(zugistiko===parseInt(item.zugistiko)){
        $("#stock").val(stock.apothema);
        $("#sumstock").val(stock.sunolo);
        $('input[name="package"]').val(item.siskevasia).trigger('change');
        $('input[name="mothercode"]').val(item.mothercode).trigger('change');
        $('input[name="temaxiako"]').val(Math.abs(parseInt(item.temaxiako-1)));
        toggleTemaxiako();
        $('input[name="barcode"]').val(item.barcode).trigger('change');
    }else{
        if(zugistiko===1) {
            $('input[name="mothercode"]').val(item.id).trigger('change');
        }else{
            toggleZugaria();
        }
        $('input[name="package"]').val(1).trigger('change');
        $('input[name="temaxiako"]').val(1);
        toggleTemaxiako();
        $('input[name="barcode"]').focus();
    }
}


function toggleTemaxiako(){
    if(parseInt($('input[name="temaxiako"]').val())===1){
        $('input[name="temaxiako"]').val(0);
        $('#temaxiako input[type="checkbox"]').prop('checked',false);
    }else{
        $('input[name="temaxiako"]').val(1);
        $('#temaxiako input[type="checkbox"]').prop('checked',true);
    }
    if($('input[name="barcode"]')[0].checkValidity()){
        $('input[name="barcode"]').trigger('change');
    }else{
        $('input[name="barcode"]')[0].reportValidity();
    }
}

function toggleActive() {
    var active = parseInt($("input[name='active']").val());
    if (active===1){
        $("input[name='active']").val(0);
        //todo activation
    }else {
        $("input[name='active']").val(1);
    }
    if (parseInt(item.new)===0){
       handleItemInput('active');
    }
}

function toggleFlagTimi() {
    var flagtimi = parseInt($("input[name='flag_timi']").val());
    if (flagtimi===1){
        flagtimi=0;
        $("input[name='flag_timi']").val(0);
        $("input[name='price']").prop('readonly', true);
    }else {
        flagtimi=1;
        $("input[name='flag_timi']").val(1);
        $("input[name='price']").prop('readonly', false);
    }
    bindItemListeners();
    if(flagtimi===parseInt(item.flag_timi)){
        $('input[name="price"]').val(parseFloat(item.timi).toFixed(2)).trigger('change');
    }else{
        if(flagtimi===1){
            $("input[name='price']").focus();
        }else{
            $("input[name='price']").val(parseFloat(0).toFixed(2)).trigger('change');
        }
    }
}

function toggleFlagTimiAgoras() {
    var flagtimi = parseInt($("input[name='flag_timi_agoras']").val());
    var agora=parseFloat($("input[name='agora']").val());
    if (flagtimi===1){
        $("input[name='flag_timi_agoras']").val(0);
    } else {
        $("input[name='flag_timi_agoras']").val(1);
        if ((parseInt(item.new)===0) && (agora!==parseFloat(item.timi_agoras))){
            handleItemInput('agora');
        }
    }
}
//</editor-fold>

//</editor-fold>