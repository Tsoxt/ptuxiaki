$.ajaxSetup({
    type: "POST",
    datatype:"json",
    beforeSend: function () {
        $('.ajax-loader').show();
    },
    url:    "../include/resources/dispatcher.php",
    //timeout: 7000,
    cache:false,
    async:true,
    complete: function (xhr){
        xhr.done(function(json){
            var data=JSON.parse(json);
            if(!data['loginStatus']){
                logout();
            }
            $('.ajax-loader').hide();
        });
    },
    error: function(xhr,ajaxOptions,thrownError){
        $('.ajax-loader').hide();
        var msg="Πρόβλημα δικτύου \n http status: "+xhr.status+", σφάλμα: "+thrownError;
        bootboxMessage(msg,1);
    }
});

//<editor-fold desc="calls">

function callCategories() {
    var action = 'callcategories';
    return $.ajax({
        data: {"action":action},
        datatype: "json"
    });
}

function callOmades(id){
    var action = 'callomades';
    var data = {category:id};
    data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function callItems(omadaId){
    var action = 'callitems';
    var data = {omada:omadaId};
    data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

//</editor-fold>


//<editor-fold desc="Categories">

function getCategory(id) {
    var action = 'getcategory';
    var data = {id: id};
    data = JSON.stringify(data);
    return $.ajax({
        data: {"action": action, "data": data},
        datatype: "json"
    });
}


function editCategory(data,action) {
    var data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });

}

//</editor-fold desc="Categories">

//<editor-fold desc="Omades">

function getOmada(id) {
    var action = 'getomada';
    var data = {id: id};
    data = JSON.stringify(data);
    return $.ajax({
        data: {"action": action, "data": data},
        datatype: "json"
    });
}

function editOmada(data,action) {
    var data = JSON.stringify(data);
    return $.ajax({
        type: "POST",
        url: "../include/resources/dispatcher.php",
        data: {"action":action, "data":data},
        datatype: "json"
    });

}

//<editor-fold desc="Omades">


//<editor-fold desc="Items">
function specialBcodes() {
    var action = 'getspecial';
    $.ajax({
        data: {"action": action},
        datatype: "json",
        success: function (json) {
            var data = JSON.parse(json);
            console.log(data);
            if (data.response === true){
                window.specialbcodes = data.body;
            }
        }
    });
}


function getSumStock(mothercode){
    var data={mcode:mothercode};
    data=JSON.stringify(data);
    var action="sunolo";
    return $.ajax({
        data:   {"action":action, "data":data},
        datatype:   "json"
    });
}

function searchItem(id, action) {
    var data = {id:id};
    data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function searchItemByDate(imerominia) {
    var action = 'bydate';
    var data = {date:imerominia};
    data = JSON.stringify(data);
    return $.ajax({
        type: "POST",
        url: "../include/resources/dispatcher.php",
        data: {"action":action, "data":data},
        datatype: "json"
    });

}

function searchItemBetweenDates(fromdate,todate) {
    var action = 'betweendates';
    var data = {fromdate:fromdate,todate:todate};
    data = JSON.stringify(data);
    return $.ajax({
        type: "POST",
        url: "../include/resources/dispatcher.php",
        data: {"action":action, "data":data},
        datatype: "json"
    });

}

function updateItem(action,data) {
    var data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });

}

function insertItem(data) {
    data = JSON.stringify(data);
    var action = 'insertproion';
    return $.ajax({
        data: {"action": action, "data":data},
        datatype: "json"
    });
}

function updateScale(){
    var action = 'updatescale';
    return $.ajax({
        data: {"action": action},
        datatype: "json"
    });
}

//</editor-fold>

//<editor-fold desc="Tmimata">

function callFpa(action,id){
    var data;
    if(action==='calltmimata'){
        data={};
    }else{
        data={id:id};
    }
    data = JSON.stringify(data);
    return $.ajax({
        method: "POST",
        url:"../include/resources/dispatcher.php",
        data: {"action": action, "data":data},
        datatype: "json"
    });
}

function insertTmima(data) {
    var data = JSON.stringify(data);
    var action = 'inserttmima';
    return $.ajax({
        type: "POST",
        url: "../include/resources/dispatcher.php",
        data: {"action": action, "data":data},
        datatype: "json"
    });
}

function updateTmima(data,action) {
    var data = JSON.stringify(data);
    return $.ajax({
        type: "POST",
        url: "../include/resources/dispatcher.php",
        data: {"action": action, "data":data},
        datatype: "json"
    });
}

//</editor-fold desc="Tmimata">