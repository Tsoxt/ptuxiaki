//<editor-fold desc="othoni gia Tmima">
function bindTmimaListeners(){
    $('#insert_tmima').off();
    $("#insert_tmima").submit(function (event) {
        event.preventDefault();
        var form = $("#insert_tmima").serializeArray();
        var data= {};
        form.forEach(function (field) {
            data[field.name]=field.value;
        });
        if ($('input[name="id"]').val()==='') {
            var result = insertTmima(data);
            result.then(function (json) {
                console.log(json);
                var data = JSON.parse(json);
                if(data.response){
                    $("#modalTmimata").modal('hide');
                    if (typeof data.msg){
                        bootboxMessage(data.msg,2);
                    }else{
                        bootboxMessage('Επιτυχής καταχώρηση',2);
                    }
                }
                else{
                    $("#modalTmimata").modal('hide');
                    bootboxMessage(data.error,1);
                }
                showTmimata();
            });
            return true;
        }else{
            var action = 'updatefpa';
            var result = updateTmima(data,action);
            result.then(function (json) {
                console.log(json);
                var data = JSON.parse(json);
                if(data.response){
                    $("#modalTmimata").modal('hide');
                    if (typeof data.msg){
                        bootboxMessage(data.msg,2);
                    }else{
                        bootboxMessage('Επιτυχής ανανέωση',2);
                    }
                } else{
                    $("#modalTmimata").modal('hide');
                    bootboxMessage(data.error,1);
                }
                showTmimata();
            });
            return true;
        }
    });
    $('#modalTmimata').on('shown.bs.modal', function() {
        $('.modal_tmimata input[name="tmima"]').focus();
    });
}

function showTmimata() {
    formReset();
    var action = 'calltmimata';
    var result = callFpa(action,0);
    result.then(function (json) {
        var data = JSON.parse(json);
        if (data.response){
            $('#groupset4 .itembody').append('<div class="table-responsive"><table class="table table-hover"><thead><tr><th>Τμήμα</th><th>Όνομα</th>' +
                '<th>Αξία</th><th>Ενεργό</th><th></th></tr></thead><tbody id="pinakas_tmimaton"></tbody></table></div>');
            data.tmimata.forEach(function (tmima) {
                $("#pinakas_tmimaton").append('<tr><td>'+parseInt(tmima.id)+'</td><td>'+tmima.name+'</td><td>'+tmima.price+'</td>' +
                    '<td id="checktmima'+parseInt(tmima.id)+'"></td><td id="edittmima'+parseInt(tmima.id)+'"></td></tr>');
                if (parseInt(tmima.active)===1){
                    $("#checktmima"+parseInt(tmima.id)).append('<input type="checkbox" onchange="toggleActiveTmima('+parseInt(tmima.id)+')" checked><input name="active'+parseInt(tmima.id)+'" value="1" hidden>');
                } else {
                    $("#checktmima"+parseInt(tmima.id)).append('<input type="checkbox" onchange="toggleActiveTmima('+parseInt(tmima.id)+')"><input name="active'+parseInt(tmima.id)+'" value="0" hidden>');
                }
                if (parseInt(tmima.editable)===1){
                    $("#edittmima"+parseInt(tmima.id)).append('<button type="button" class="edittmimabutt" onclick="editTmima('+parseInt(tmima.id)+',1)"><i class="icon ion-edit"></i></button>');
                }
            });
        }
        else{
            bootboxMessage(data.error,1);
        }
    });
}

function editTmima(id,type) {
    if (parseInt(type)===1){
        var action = 'bytmima';
        var result = callFpa(action,id);
        result.then(function (json) {
            console.log(json);
            var data = JSON.parse(json);
            if (data.response){
               $('input[name="tmima"]').val(data.proion.name);
               $('input[name="price"]').val(data.proion.price);
               $('input[name="id"]').val(id);
               $("#modalTmimata").modal('show');
            }else {
                bootboxMessage(data.error,1);
            }
        });
    } else{
        formReset();
        $('input[name="tmima"]').val('');
        $('input[name="price"]').val('');
        $('input[name="id"]').val('');
        $("#modalTmimata").modal('show');
    }
    bindTmimaListeners();
}

function toggleActiveTmima(id) {
    var active = parseInt($('input[name="active'+id+'"]').val());
    if (active===1){
        $('input[name="active'+id+'"]').val(0);
    }else {
        $('input[name="active'+id+'"]').val(1);
    }
    var allagi = parseInt($('input[name="active'+id+'"]').val());
    var data = {id:id, active:allagi};
    var action = 'updatetmimaactive';
    var result = updateTmima(data,action);
    result.then(function (json) {
        console.log(json);
        var data = JSON.parse(json);
        if (data.response){
            bootboxMessage('Η κατάσταση του τμήματος άλλαξε',2);
        } else{
            bootboxMessage(data.error,1);
        }

    });
}






//<editor-fold desc="othoni gia Tmima">