//<editor-fold desc="othoni gia Category">

function showAllCategories() {
    var result = callCategories();
    result.then(function (json) {
        var data = JSON.parse(json);
        if (data.response==true){
            $('.flexgroup').empty();
            data.categories.forEach(function (category) {
                var newrow = '<button type="button" class="groupbut" onclick="callCategory('+parseInt(category.id)+',1)">'+category.name+'</button>';
                $('.flexgroup').append(newrow);
                $('#inventory').modal('show');
            });
        }else{
            bootboxMessage(data.error,1);
        }
    });
}

function callCategory(id,type){//1 ean einai update, 2 otan einai kainourio
    start(2);
    $("#inventory").modal('hide');
    if(parseInt(type)===1){
        var result=getCategory(id);
        result.then(function (json) {
            var data=JSON.parse(json);
            if(data.response){
                formReset();
                if(data.category.altId===null){
                    data.category.altId='';
                }
                if(data.category.seira===null){
                    data.category.seira='';
                }
                category = {id:data.category.id, altid:data.category.altId, perigrafi:data.category.name, active:data.category.active, order:data.category.seira,
                    from_api:data.category.api, new:data.category.new};
                writeCategory();
            } else{
                bootboxMessage(data.error,1);
            }
        });
    }else{
        writeCategory();
    }
}

function writeCategory() {
    formReset();
    $("#groupset2 .itembody").append('<div class="row1"><div class="inlineflex"><span id="checkcategory">Ενεργή' +
        '<input onchange="toggleActiveCateg()" type="checkbox"><input name="active" value="'+category.active+'" type="hidden"></span></div>' +
        '<div class="row2"><input class="formrow" name="name" pattern=".{3,}" title="3 χαρακτήρες τουλάχιστον" placeholder="Κατηγορία" value="'+category.perigrafi+'" required autocomplete="off">' +
        '<input name="id" type="hidden" value="'+category.id+'"><input name="altId" type="hidden" value="'+category.altid+'">' +
        '<input name="seira" type="hidden" value="'+category.order+'"><input name="api" type="hidden" value="'+category.from_api+'">' +
        '<input name="new" type="hidden" value=""></div></div><div class="row1" id="sum_omades">' +
        '<div class="groupinfo"></div></div><div class="itembuttons"><button class="button4">Αποθήκευση</button></div>');
    if (parseInt(category.active) === 1){
        $('#checkcategory input[type="checkbox"]').prop('checked',true);
    }
    if (parseInt(category.new) === 0) {
        var result = callOmades(parseInt(category.id));
        result.then(function (json) {
            var data = JSON.parse(json);
            var synolo;
            if (data.response){
                synolo = parseInt(data.omades.length);
            }else {
                synolo = 0;
            }
            $('.groupinfo').append('Σύνολο ομάδων: <span class="badge">'+synolo+'</span>');
        });
    }
    if (parseInt(category.from_api)===1){
        $('input[name="name"]').prop('readonly',true);
    }
    bindCategoryListeners();

}

function bindCategoryListeners() {
    $('input').off();
    $('select').off();
    $('#insert_category').off();
    $('#insert_category').on('submit',function (event) {
        event.preventDefault();
        var form = $('#insert_category').serializeArray();
        var data= {};
        form.forEach(function (field) {
            data[field.name]=field.value;
        });
        var action;
        if (parseInt(category.new)===1) {
            action='insertcategory';
        }else{
            action='updatecategory';
        }
        var result = editCategory(data,action);
        result.then(function (json) {
            console.log(json);
            var data = JSON.parse(json);
            if(data.response==true){
                bootboxMessage('Επιτυχής καταχώρηση',2);
            }
            else{
                bootboxMessage(data.error,1);
            }
            formReset();
            start(2);
        });
        return false;
    });
}

function toggleActiveCateg() {
    var active = parseInt($("input[name='active']").val());
    if (active===1){
        $("input[name='active']").val(0);
    }else {
        $("input[name='active']").val(1);
    }
}




//</editor-fold desc="othoni gia Category">