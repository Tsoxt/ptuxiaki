/*function newlineEffect(value){
    setTimeout(function () {
        $(value).css('background', 'rgba(128,204,255,0.5');
    },500);
}*/
$.ajaxSetup({
    type: "POST",
    datatype:"json",
    beforeSend: function () {
        $('.ajax-loader').show();
    },
    url:    "../include/resources/dispatcher.php",
    timeout: 10000,
    cache: false,
    complete: function (xhr){
        xhr.done(function () {
            $('.ajax-loader').hide();
        });
    },
    error: function(xhr,ajaxOptions,thrownError){
        $('.ajax-loader').hide();
        var msg="Πρόβλημα δικτύου \n http status: "+xhr.status+", σφάλμα: "+thrownError;
        bootboxMessage(msg,1);
    }
});

function getCustomerDiscounts(id) {
    var action='getcustomerdiscounts';
    var data={customer:id};
    data=JSON.stringify(data);
    return $.ajax({
        data:   {"action":action, "data":data},
        datatype:   "json"
    });
}

function findCustomer(field,type) {
    var action='searchcustomer';
    var data={search:field,type:type};
    data=JSON.stringify(data);
    return $.ajax({
        data:   {"action":action, "data":data},
        datatype:   "json"
    });

}

function getPaymentMethods() {
    var action='showpaymentmethods';
    return $.ajax({
        data:   {"action":  action},
        datatype:   "json",
        success:    function (json) {
            var data=JSON.parse(json);
            if (data.response){
                $('.paymentrow').empty();
                data.payment_methods.forEach(function (method) {
                    $('.paymentrow').append('<div class="numbutton"><button type="button" onclick="chooseCashout('+method.id+',this)"><i class="'+method.icon+'"></i> '+method.name+'</button></div>');
                });
            }else {
                bootboxMessage(data.error,1);
            }
        }
    });
}

function getSumStock(mothercode){
    var data={mcode:mothercode};
    data=JSON.stringify(data);
    var action="sunolo";
    return $.ajax({
        data:   {"action":action, "data":data},
        datatype:   "json"
    });
}

function sendToCustomerDisplay(data){
    var action='customerinfo';
    data=JSON.stringify(data);
    $.ajax({
        data: {"action":action, "data":data},
        datatype: "json",
        success: function(json){
            var data=JSON.parse(json);
            if(!data.response){
                bootboxMessage(data.error);
            }
        }
    });
}

function delCustomerDisplay(data){
    var action='customerdel';
    data=JSON.stringify(data);
    $.ajax({
        data: {"action":action, "data":data},
        datatype: "json",
        success: function(json){
            var data=JSON.parse(json);
            if(!data.response){
                bootboxMessage(data.error);
            }
        }
    });
}

function updateCustomerDisplay(data){
    var action='customerupdate';
    data=JSON.stringify(data);
    $.ajax({
        data: {"action":action, "data":data},
        datatype:"json",
        success: function(json){
            var data=JSON.parse(json);
            if(!data.response){
                bootboxMessage(data.error);
            }
        }
    });
}

function resetCustomerDisplay(){
    var action='resetdisplay';
    $.ajax({
        data: {"action":action},
        datatype: "json",
        success: function(json){
            var data=JSON.parse(json);
            if(!data.response){
                bootboxMessage(data.error);
            }
        }
    });
}


function toggleCashier() {
    var action = 'togglecashier';
    return $.ajax({
        data:   {"action":  action},
        datatype:   "json"
    });
}

function getAltPerigrafi() {
    var action='getaltperigrafi';
    $.ajax({
        data:   {"action":  action},
        datatype:   "json",
        success: function (json) {
            console.log(json);
            var data=JSON.parse(json);
            if(data.response===true){
                altPerigrafi=true;
            }else {
                altPerigrafi=false;
            }
            changeColorDesc();
        }
    });

}

function changeColorDesc(){
    if (altPerigrafi){
        $('#change_perigrafi').removeClass('primary_desc').addClass('alternative_desc');
        $('#change_perigrafi i').removeClass('ion-toggle').addClass('ion-toggle-filled');

    } else {
        $('#change_perigrafi').removeClass('alternative_desc').addClass('primary_desc');
        $('#change_perigrafi i').removeClass('ion-toggle-filled').addClass('ion-toggle');
    }
}

function changePerigrafi() {
    var action='changeperigrafi';
    return $.ajax({
        data:   {"action":  action},
        datatype:   "json"
    });
}

function userInfo(){
    var action='userinfo';
    return $.ajax({
        data:   {"action":  action},
        datatype:   "json"
    });
}

function getTmimata(){
    var action='calltmimata';
    return $.ajax({
        data:   {"action":  action},
        datatype:   "json",
        success:    function (json) {
            var data = JSON.parse(json);
            if (data.response === true) {
                $("#tmimata").empty();
                data.tmimata.forEach(function (tmima) {
                    if(parseInt(tmima.active)===1){
                        var newdiv;
                        var id = tmima.id;
                        var perigrafi = tmima.name;
                        var newdiv = '<div class="rowbutton"><button type="button" value="' + id + ')">' + perigrafi + '</button></div>'
                        $("#tmimata").append(newdiv);
                    }
                });
            }else{
                bootboxMessage(data.error,1);
            }
        }
    });
}

function getItems(){
    var data = {omada: null};
    data=JSON.stringify(data);
    var action='callitems';
    return $.ajax({
        data:   {"action":  action, "data":data},
        datatype:   "json",
        success: function(json){
            var data = JSON.parse(json);
            if (data.response === true) {
                $("#items").empty();
                data.items.forEach(function (item) {
                    var newdiv;
                    var id = item.id;
                    var perigrafi = item.perigrafi;
                    newdiv = $('<button type="button" value="' + id + '">' + perigrafi + '</button>');
                    $("#items").append(newdiv);
                });
            } else {
                bootboxMessage(data.error,1);
            }
        }
    });
}

function payYpoloipo(data) {
    var data=JSON.stringify(data);
    var action='payCustomerYpoloipo';
    return $.ajax({
        data:   {"action":action, "data":data},
        datatype:   "json"
    });

}

function callCustomerDiscounts() {
    var id=parseInt(customer.id);
    var result=getCustomerDiscounts(id);
    result.then(function (json) {
        console.log(json);
        var data = JSON.parse(json);
        if (data.response) {
            data.body.forEach(function (field) {
                var row={proion:parseInt(field.id_proion),discount:parseFloat(field.discount),type:parseInt(field.type)};
                customer.discounts.push(row);
            });
            updateTameioPrices();
        }
    });
}

function bootboxMessage(msg,type) {
    var msgclass;
    if (parseInt(type)===1){
        msgclass='errormsg';
    } else if (parseInt(type)===2){
        msgclass='successmsg';
    } else{
        msgclass='infomsg';
    }
    bootbox.alert({
       size:"small",
       title:"Μήνυμα",
       message:'<div class="'+msgclass+'">'+msg+'</div>',
        callback: function () {
            setTimeout(function () {
                $('#barcode').focus();
            },50);
        }
    });

}

function number(value){
    var number=value;
    var screen=$("#numbers").val();
    if (number==='b'){
        screen=screen.substring(0,(screen.length)-1);
        $("#numbers").val(screen);
    }else if (number==='c'){
       $('#numbers').val('');
    }else{
        if(screen.length<6) {
            $("#numbers").val(screen + number);
        }
    }
    $("#barcode").focus();
    return false;
}


function updateSum(value){
    value=parseFloat(value);
    var sum=parseFloat($("#sumary").val()).toFixed(2);
    sum=parseFloat(sum);
    sum += parseFloat(value);
    sum = parseFloat(sum).toFixed(2);
    $("#sumary").val(sum);
    $("#sunolo").html(sum);
}

function calculateBarcode(price,flag,bcode){
    price=parseFloat(price).toFixed(2);
    var timi,quantity;
    if (parseInt(flag) === 1){//to barcode einai vasi aksias
        if (bcode.length===26){//ean einai databar expanded
            quantity=bcode.substring(10,15);
            quantity=(parseFloat(quantity)/1000).toFixed(3);
            quantity=parseFloat(quantity);
            price=bcode.substring(20);
            price=parseFloat(price)/100;
            price=parseFloat(price).toFixed(2);
            pinakas.timi=price;
            timi=parseFloat(price)*parseFloat(quantity);
            timi=timi.toFixed(3);
        }else{
            timi=(parseInt(bcode)/100).toFixed(2);
            timi=parseFloat(timi);
            quantity = (timi/parseFloat(price)).toFixed(3);
        }
    }else if (parseInt(flag) ===0) {//to barcode einai vasi kilwn
        quantity = (parseFloat(bcode) / 1000).toFixed(3);
        quantity=parseFloat(quantity);
        timi = (quantity * parseFloat(price)).toFixed(2);
    }else{
        quantity=1;
        timi=(quantity*parseFloat(price)).toFixed(2);
    }
    var data={quantity:quantity,timi:timi};
    return data;
}

function callitems() {
    $("#items").empty();
    var data = {omada: null};
    data=JSON.stringify(data);
    var items = getItems(data);
    items.then(function (json) {
        var data = JSON.parse(json);
        if (data.response === true) {
            data.items.forEach(function (item) {
                var newdiv;
                var id = item.id;
                var perigrafi = item.perigrafi;
                newdiv = $('<button type="button" value="' + id + '">' + perigrafi + '</button>');
                $("#items").append(newdiv);
            });
        } else {
            bootboxMessage(data.error,1);
        }
    });
}

function callproion(id,type){//TODO global
    unbindItemCallListeners();
    pinakas={id:"",perigrafi: "",timi: "",flag_timi:"",tmima:"",zugistiko:0,quantity:"",sunolo:"",siskevasia:1,mothercode:"",
        new:1,api:0,id_om:900000000,id_cat:900000,barcode:""};
    var posotita=parseFloat(numbercheck()).toFixed(3);
    var action;
    if (parseInt(type)===0){//ean einai barcode
        action = 'bybarcode';
        var checktwo = id.substring(0,2);
        if (!(specialcodes[checktwo] === undefined)){//ean ta 2 prota psifia tou barcode exoun aksia h varos
            tail =id.substring(7,12);
            flagbit = specialcodes[checktwo];
            var id=id.substring(0,7);
        }else if(id.length===26){//ean einai databar expanded
            flagbit=1;
            tail=id;
            id=id.substring(3,10);
        }else{
            flagbit=2;
            tail='';
        }
        isTimi=false;
        var result=findproion(id,action);
        result.then(function (json) {
           var item=JSON.parse(json);
           if(item.response){
               pinakas.id = parseInt(item.proion.id);
               pinakas.alt_id=item.proion.alt_id;
               if (altPerigrafi===true){
                   pinakas.perigrafi = item.proion.perigrafi2;
               } else{
                   pinakas.perigrafi = item.proion.perigrafi;
               }
               pinakas.tmima = item.proion.tmima;
               pinakas.zugistiko = item.proion.zugistiko;
               pinakas.siskevasia=item.proion.temaxia;
               pinakas.mothercode=item.proion.mothercode;
               pinakas.energo=item.proion.active;
               pinakas.new=item.proion.new;
               pinakas.api=item.proion.api;
               pinakas.id_om=item.proion.id_omadas;
               pinakas.id_cat=item.proion.id_category;
               pinakas.barcode=item.proion.barcode;
               pinakas.flag_timi=item.proion.flag_timi;
               var result=getSumStock(pinakas.mothercode);
               result.then(function (data) {
                   var summary=JSON.parse(data);
                   if(summary.response) {
                       pinakas.stock = summary.proion.sumstock;
                   }else {
                       pinakas.stock = 0;
                   }
                   if(parseInt(item.proion.flag_timi)===0){
                       if (tail.length===26){//ean einai databar expanded barcode
                           var data=calculateBarcode(0,flagbit,tail);
                           pinakas.quantity=data.quantity;
                           pinakas.sunolo=data.timi;
                           writeproion();
                       } else {
                           isTimi=true;
                           isbarcode=true;
                           numpadShow();
                       }
                   }else{
                       var data=calculateBarcode(item.proion.timi,flagbit, tail);
                       if (tail.length!==26){
                           pinakas.timi=parseFloat(item.proion.timi).toFixed(2);
                       }
                       pinakas.quantity=data.quantity;
                       pinakas.sunolo=data.timi;
                       writeproion();
                   }
                   bindItemCallListeners();
               });
           }else{
               bootboxMessage(item.error,1);
               bindItemCallListeners();
           }
        });
    }else if(parseInt(type)===1){//ean einai id
        action = 'byid';
        var id=id;
        pinakas.zugistiko = 0;
        pinakas.quantity = posotita;
        var result=findproion(id, action);
        isTimi=false;
        result.then(function (json) {
           var item=JSON.parse(json);
            pinakas.id = parseInt(item.proion.id);
            pinakas.alt_id="";
            if (altPerigrafi===true){
                pinakas.perigrafi = item.proion.perigrafi2;
            } else{
                pinakas.perigrafi = item.proion.perigrafi;
            }
            pinakas.tmima = item.proion.tmima;
            pinakas.zugistiko = item.proion.zugistiko;
            pinakas.siskevasia=item.proion.temaxia;
            pinakas.mothercode=item.proion.mothercode;
            pinakas.new=item.proion.new;
            pinakas.api=item.proion.api;
            pinakas.id_om=item.proion.id_omadas;
            pinakas.id_cat=item.proion.id_category;
            pinakas.barcode=item.proion.barcode;
            var flag_timi=parseInt(item.proion.flag_timi);
            var result=getSumStock(pinakas.mothercode);
            result.then(function (data) {
                var summary=JSON.parse(data);
                if(summary.response) {
                    pinakas.stock = summary.proion.sumstock;
                }else {
                    pinakas.stock=0;
                }
                if(parseInt(flag_timi)===0){
                    isTimi=true;
                    if($("#mymodal").is(':visible')){
                        $('#items').removeClass('numpad-open');
                        $(".savebutton").show();
                    }else{
                        numpadShow();
                    }
                }else{
                    if($("#myModal").is(':visible')){
                        numpadClose();
                    }
                    pinakas.quantity=parseFloat(pinakas.quantity);
                    pinakas.timi=parseFloat(item.proion.timi).toFixed(2);
                    pinakas.sunolo=(parseFloat(pinakas.timi)*pinakas.quantity).toFixed(2);
                    writeproion();
                }
                bindItemCallListeners();
            });

        });
    }else{//ean einai tmima
        var id = id;
        action = 'bytmima';
        pinakas.zugistiko = 0;
        pinakas.quantity = 1;
        pinakas.mothercode=1500000000;
        pinakas.new=0;
        pinakas.stock=0;
        var result = findproion(id,action);
        result.then(function (json) {
            var tmima=JSON.parse(json);
            pinakas.id=1500000000;
            pinakas.tmima=tmima.proion.id;
            pinakas.perigrafi = tmima.proion.name;
            isTimi=true;
            numpadShow();
            bindItemCallListeners();
        });
    }

}

function findproion(id,action){
    var data = {id:id};
    data = JSON.stringify(data);
    return $.ajax({
        type: "POST",
        url: "../include/resources/dispatcher.php",
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function afairesi(line) {
    var posotita=parseInt($('#line'+line+' input[name="temaxia[]"]').val());
    if (posotita===1){
        del(line);
    } else {
        var aksia=parseFloat($('#line'+line+' input[name="price[]"]').val());
        var sinolo=parseFloat($('#line'+line+' input[name="sunolo[]"]').val());
        var old_aksia=parseFloat($('#line'+line+' input[name="pre_price[]"]').val());
        var old_sunolo=parseFloat($('#line'+line+' input[name="pre_sunolo[]"]').val());
        var new_value=sinolo-aksia;
        var old_new_value=old_sunolo-old_aksia;
        var new_posotita=posotita-1;
        var perigrafi=$('#line'+line+' input[name="desc[]"]').val();
        var send={"timi":aksia, "temaxia":new_posotita, "id":line, "description":perigrafi};
        var stock=parseFloat($('#line'+line+' input[name="stock[]"]').val())+1;
        $('#line'+line+' input[name="sunolo[]"]').val(new_value.toFixed(2));
        $('#line'+line+' input[name="pre_sunolo[]"]').val(old_new_value.toFixed(2));
        $('#line'+line+' input[name="temaxia[]"]').val(new_posotita.toFixed(3));
        updateSum(-Math.abs(aksia));
        $('#line'+line+' input[name="stock[]"]').val(stock.toFixed(3));
        updateCustomerDisplay(send);
    }
}

function prosthiki(line) {
    var aksia=parseFloat($('#line'+line+' input[name="price[]"]').val());
    var sinolo=parseFloat($('#line'+line+' input[name="sunolo[]"]').val());
    var old_aksia=parseFloat($('#line'+line+' input[name="pre_price[]"]').val());
    var old_sunolo=parseFloat($('#line'+line+' input[name="pre_sunolo[]"]').val());
    var posotita=parseInt($('#line'+line+' input[name="temaxia[]"]').val());
    var new_value=sinolo+aksia;
    var old_new_value=old_aksia+old_sunolo;
    var new_posotita=posotita+1;
    var perigrafi=$('#line'+line+' input[name="desc[]"]').val();
    var send={"timi":aksia, "temaxia":new_posotita, "id":line, "description":perigrafi};
    var stock=parseFloat($('#line'+line+' input[name="stock[]"]').val())-1;
    $('#line'+line+' input[name="sunolo[]"]').val(new_value.toFixed(2));
    $('#line'+line+' input[name="pre_sunolo[]"]').val(old_new_value.toFixed(2));
    $('#line'+line+' input[name="temaxia[]"]').val(new_posotita.toFixed(3));
    updateSum(aksia);
    $('#line'+line+' input[name="stock[]"]').val(stock.toFixed(3));
    updateCustomerDisplay(send);
}

function writeproion(){
    //customer display
    var send={"description":pinakas.perigrafi,"timi":pinakas.timi,"temaxia":pinakas.quantity,"id":counter};
    sendToCustomerDisplay(send);
    //show the data
    var newdiv = $('<div class="line" id="line' + counter + '">\n' +
        '                    <div class="perigrafi">\n' +
        '                        <input type="text" name="desc[]" readonly value="' + pinakas.perigrafi + '">\n' +
        '                    </div>\n' +
        '                    <div class="temaxia">\n' +
        '                        <div class="text">\n' +
        '                            <input type="text" name="temaxia[]" readonly value="' + parseFloat(pinakas.quantity).toFixed(3) + '" > \n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                    <div class="stock" >\n'+
        '                       <input type="text" name="stock[]" readonly value="'+ parseFloat(pinakas.stock-pinakas.quantity).toFixed(3) +'">\n'+
        '                    </div>\n'+
        '                    <div class="text-desc">kg/τμχ</div>\n' +
        '                    <div class="price">\n' +
        '                       <div class="pre_sunolo" hidden><input type="text" style="text-decoration: line-through red" name="pre_sunolo[]" value="'+ parseFloat(pinakas.sunolo).toFixed(2)+'" readonly></div>\n' +
        '                       <div class="input"><input type="text" name="sunolo[]" value="'+ parseFloat(pinakas.sunolo).toFixed(2)+'" readonly></div>\n' +
        '                       <div class="eurosunolo"><i class="ion-social-euro"></i></div>\n' +
        '                    </div>\n' +
        '                    <div class="delete">\n' +
        '                        <button type="button" onclick="del(\'' + counter + '\')"><i class="ion-ios-trash"></i></button>\n' +
        '                        <input type="hidden" name="price[]" value="'+pinakas.timi+'">\n'+
        '                        <input type="hidden" name="pre_price[]" value="'+pinakas.timi+'">\n'+
        '                        <input type="hidden" name="tmima[]" readonly value="' + pinakas.tmima + '">\n' +
        '                        <input type="hidden" name="id[]" value="' + pinakas.id + '">\n' +
        '                        <input type="hidden" name="operator[]" value="'+user.id+'">\n' +
        '                        <input type="hidden" name="tameio[]" value="1">\n' +
        '                        <input type="hidden" name="id_om[]"  value="'+pinakas.id_om+'"> \n'+
        '                        <input type="hidden" name="id_cat[]" value="'+pinakas.id_cat+'">\n'+
        '                        <input type="hidden" name="zigos[]" value="'+pinakas.zugistiko+'">\n'+
        '                        <input type="hidden" name="flag_timi[]" value="'+pinakas.flag_timi+'">\n'+
        '                        <input type="hidden" name="mothercode[]" value="'+pinakas.mothercode+'">\n'+
        '                        <input type="hidden" name="package[]" value="'+pinakas.siskevasia+'">\n'+
        '                        <input type="hidden" name="api[]" value="'+pinakas.api+'">\n'+
        '                        <input type="hidden" name="new[]" value="'+pinakas.new+'">\n' +
        '                        <input type="hidden" name="barcode[]" value="'+pinakas.barcode+'">\n'+
        '                    </div>\n' +
        '                </div>');
    $("#itemsdisplay").append(newdiv);
    if (parseFloat(pinakas.quantity)===parseInt(pinakas.quantity)){
        $('#line'+counter+' .temaxia').prepend('<div class="minus"><button type="button" onclick="afairesi(\''+ counter + '\')"><i class="ion-minus-circled"></i></button></div>');
        $('#line'+counter+' .temaxia').append('<div class="plus"><button type="button" onclick="prosthiki(\''+ counter + '\')"><i class="ion-plus-circled"></i></button></div>');
    }
    if (customer.discounts.length>0) {
        var id=parseInt(pinakas.id);
        var index=customer.discounts.findIndex(item => item.proion===id);
        if (index!==-1){//ean yparxei to proion mesa ston pinaka ekptoseon
            var timi=parseFloat(pinakas.timi);
            var ekptosi=parseFloat(customer.discounts[index].discount);
            if (customer.discounts[index].type===0){
                var new_timi=(timi-(timi*ekptosi)).toFixed(2);
            }  else {
                var new_timi=(timi-ekptosi).toFixed(2);
            }
            var new_sunolo=(new_timi*pinakas.quantity).toFixed(2);
            $('#line'+counter+' input[name="price[]"]').val(new_timi);
            $('#line'+counter+' input[name="sunolo[]"]').val(new_sunolo);
            $('#line'+counter+' .pre_sunolo').show();
        }
        
    }
    $("#itemsdisplay").scrollTop(0);
    if (typeof new_sunolo !=='undefined'){
        updateSum(new_sunolo);
    }else {
        updateSum(pinakas.sunolo);
    }
    counter++;
}

function resetTameioPrices() {
    var i;
    var timi;
    var discount_timi;
    var sunolo;
    var discount_sunolo;
    var diafora;
    if (!($.trim($("#itemsdisplay").html())=='')){//ean den einai keno to tameio
        for (i=0;i<counter;i++){
            if ($('#line'+i)) {//ean yparxei i grammi
                timi=$('#line'+i+' input[name="pre_price[]"]').val();
                discount_timi=$('#line'+i+' input[name="price[]"]').val();
                sunolo=$('#line'+i+' input[name="pre_sunolo[]"]').val();
                discount_sunolo=$('#line'+i+' input[name="sunolo[]"]').val();
                if (parseFloat(timi)!==parseFloat(discount_timi)){
                    $('#line'+i+' input[name="price[]"]').val(timi);
                    $('#line'+i+' input[name="sunolo[]"]').val(sunolo);
                    $('#line'+i+' .pre_sunolo').hide();
                    diafora=parseFloat(sunolo)-parseFloat(discount_sunolo);
                    updateSum(diafora);
                }
            }
        }
    }
}

function updateTameioPrices() {
    var i;
    var id;
    var timi;
    var new_timi;
    if (!($.trim($("#itemsdisplay").html())=='')){//ean den einai keno to tameio
        for (i=0;i<counter;i++){
            if ($('#line'+i)){//ean yparxei i grammi
                id=$('#line'+i+' input[name="id[]"]').val();
                id=parseInt(id);
                var index=customer.discounts.findIndex(item => item.proion===id);
                if (index!==-1){//ean yparxei to proion mesa ston pinaka ekptoseon
                    timi=$('#line'+i+' input[name="price[]"]').val();
                    timi=parseFloat(timi);
                    var ekptosi=parseFloat(customer.discounts[index].discount);
                    if (customer.discounts[index].type===0){
                        new_timi=(timi-(timi*ekptosi)).toFixed(2);
                    }  else {
                        new_timi=(timi-ekptosi).toFixed(2);
                    }
                    var quantity=$('#line'+i+' input[name="temaxia[]"]').val();
                    var new_sunolo=(new_timi*quantity).toFixed(2);
                    var sunolo=$('#line'+i+' input[name="pre_sunolo[]"]').val();
                    var diafora=-(parseFloat(sunolo)-parseFloat(new_sunolo));
                    $('#line'+i+' input[name="price[]"]').val(new_timi);
                    $('#line'+i+' input[name="sunolo[]"]').val(new_sunolo);
                    $('#line'+i+' .pre_sunolo').show();
                    updateSum(diafora);
                }
            }
        }
    }
}

function specialBcodes() {
    var action = 'getspecial';
    return $.ajax({
        data: {"action": action},
        datatype: "json",
        success: function (json) {
            var data = JSON.parse(json);
            console.log(data);
            if (data.response === true){
                window.specialcodes = data.body;
            }
        }
    });
}

//<editor-fold desc="CustomerModal">
function searchCustomer(input,type) {
    $('.customer_list').empty();
    $('.customer_form').hide();
    var result=findCustomer(input,type);
    result.then(function (json) {
        console.log(json);
        var data=JSON.parse(json);
        if (data.response){
            if (parseInt(type)===1){//anazitisi me id
                saveCustomer(data.customers[0]);
                $('.customer_form').show();
            } else{//anazitisi me fulltext
                if (data.gsis){
                    //todo apothikeusi prwta
                    saveCustomer(data.customers[0]);
                } else{
                    unbindCustomerListeners();
                    $('.customer_list').append('<div class="table-responsive">' +
                        '<table class="table table-hover">' +
                        '<thead>' +
                        '<tr>' +
                        '<th style="width: 20%">Όνομα</th>' +
                        '<th style="width: 20%">Διεύθυνση</th>' +
                        '<th style="width: 13%">ΑΦΜ</th>' +
                        '<th style="width: 13%">Τηλέφωνο</th>' +
                        '<th style="width: 13%">Κινητό</th>' +
                        '<th style="width: 13%">Υπόλοιπο</th>' +
                        '<th style="width: 8%"></th>' +
                        '</tr></thead><tbody></tbody></table></div>');
                    data.customers.forEach(function (row) {
                        appendCustomerInventory(row);
                    });
                    bindCustomerListeners();
                }
            }
        } else {
            $('.customer_list').append('<p>'+data.error+'</p>');
        }
    });
}

function appendCustomerInventory(row) {
    if (row.body.afm===null){
        row.body.afm='';
    }
    if (row.body.phone===null){
        row.body.phone='';
    }
    if (row.body.mobile===null){
        row.body.mobile='';
    }
    $('.customer_list tbody').append('<tr>' +
        '<td style="width: 20%">'+row.body.name+'</td>' +
        '<td style="width: 20%">'+row.body.address+'</td>' +
        '<td style="width: 13%">'+row.body.afm+'</td>' +
        '<td style="width: 13%">'+row.body.phone+'</td>' +
        '<td style="width: 13%">'+row.body.mobile+'</td>' +
        '<td style="width: 13%">'+row.ypoloipo+'</td>' +
        '<td style="width: 8%"><input value="'+parseInt(row.body.id)+'" hidden><button type="button" class="fas fa-user-check submitbtn"></button></td></tr>');
}

function resetCustomer() {
    $('#searchCustomerModal input').val('');
    customer={id:0,type:0,invoice:0,discounts:[]};
    resetTameioPrices();
    $('#customers > div').html('Πελάτης');
    $('#customers').css('border-color','inherit');
}

function saveCustomer(data) {
    resetCustomer();
    $('input[name="customer"]').val(data.body.name);
    $('input[name="job"]').val(data.body.job);
    $('input[name="customer_id"]').val(parseInt(data.body.id));
    $('input[name="address"]').val(data.body.address);
    $('input[name="tk"]').val(data.body.postcode);
    $('input[name="city"]').val(data.body.city);
    $('input[name="phone"]').val(data.body.phone);
    $('input[name="afm"]').val(data.body.afm);
    $('input[name="doy"]').val(data.body.doy);
    $('input[name="member"]').val(data.body.member);
    $('input[name="ypoloipo"]').val((parseFloat(data.ypoloipo)).toFixed(2));
    $('input[name="pliromi"]').prop({
        "max":parseFloat(data.ypoloipo)
    });
    if (parseInt(data.body.type)===0){
        $('.customer_form select').val(0);
    } else {
        $('.customer_form select').val(1);
    }
    customer.id=parseInt(data.body.id);
    customer.type=parseInt(data.body.type);
    $('#customers > div').html(data.body.name);
    $('#customers').css('border-color','rgb(130, 139, 201)');
    callCustomerDiscounts();
}

function searchCustomerModalShow() {
    $('#searchCustomerModal').modal('show');
}

function addCustomer() {
    if ($('#sumary').val()!=='0'){
        $('.anamoni').trigger('click');
    }
    navigation(6,1);
}

//</editor-fold>

//<editor-fold desc="MerikoModal">

function merikoModalShow(){
    if(parseFloat($('#sumary').val())<=0) {
        bootboxMessage('Δεν υπάρχει κάτι στο ταμείο προς συναλλαγή',3);
    }else {
        if (!($.trim($(".paymentrow").html())=='')) {
            $('#meriko_sunolo').val($('#sumary').val()+' €');
            $('#meriko_rest').val($('#sumary').val()+' €');
            $('#meriko_change').val(parseFloat(0).toFixed(2)+' €');
            $('#merikoModal').modal('show');
        }
    }
}

function merikoModalClose() {
    $('#merikoModal input').val('');
    $('#merikoModal').modal('hide');
    creditsum=[];
}

function discountType(type) {
    if ( $('#merikonumbers').val()){
        var amount=parseFloat($('#merikonumbers').val());
        var ekptwsi;
        var ypoloipo=parseFloat($('#meriko_rest').val());
        var rest;
        $('#merikonumbers').val('');
        if (parseInt(type)===0){//ekptwsi posou
            ekptwsi=amount.toFixed(2);
            ekptwsi=parseFloat(ekptwsi);
            if (ekptwsi+0.01<=ypoloipo){
                rest=(ypoloipo-amount).toFixed(2);
                $('#meriko_rest').val(rest+' €');
                payment={'id':0,'amount':amount};
                creditsum.push(payment);
            }
        }else{//ekptwsi epi tis %
            ektpwsi=((ypoloipo*amount)/100).toFixed(2);
            ektpwsi=parseFloat(ektpwsi);
            if (ektpwsi+0.01<=ypoloipo){
                rest=(ypoloipo-ektpwsi).toFixed(2);
                $('#meriko_rest').val(rest+' €');
                payment={'id':0,'amount':ektpwsi};
                creditsum.push(payment);
            }
        }
        updateLastpaymethod();
    }

}

function pistosi() {
    if (parseInt(customer.id)===0){
        bootboxMessage('Πρέπει να προσθέσετε πελάτη πρώτα',3);
    }else{
        var rest,pistosi,amount;
        var ypoloipo=parseFloat($('#meriko_rest').val());
        if ( $('#merikonumbers').val()){
            amount=parseFloat($('#merikonumbers').val());
            $('#merikonumbers').val('');
            pistosi=amount.toFixed(2);
            pistosi=parseFloat(pistosi);

            if (pistosi<=ypoloipo){
                rest=(ypoloipo-amount).toFixed(2);
                $('#meriko_rest').val(rest+' €');
                payment={'id':1,'amount':amount};
                creditsum.push(payment);
                updateLastpaymethod();
            }
        }else {
            var sunolo=parseFloat($('#meriko_sunolo').val());
            if (sunolo===ypoloipo){
                amount=parseFloat(ypoloipo);
                $('#meriko_change').val(parseFloat(0).toFixed(2)+' €');
                $('#meriko_rest').val(parseFloat(0).toFixed(2)+' €');
                payment={'id':1,'amount':amount};
                creditsum.push(payment);
                updateLastpaymethod();
            }
        }
    }
}

function updateLastpaymethod() {
    if (creditsum.length>0){
        var id=creditsum[creditsum.length-1]['id'];
        if(parseInt(id)===0){
            $(".discountbtn").prop('disabled', false);
        }
        var amount=creditsum[creditsum.length-1]['amount'];
        if (parseInt(id)===0){
            $('#lastpaymethod').val('Έκπτωση: '+parseFloat(amount).toFixed(2)+' €');
        }else if (parseInt(id)===1){
            $('#lastpaymethod').val('Πίστωση: '+parseFloat(amount).toFixed(2)+' €');
        } else if (parseInt(id)===2){
            $('#lastpaymethod').val('Μετρητά: '+parseFloat(amount).toFixed(2)+' €');
        }else {
            $('#lastpaymethod').val('Κάρτα: '+parseFloat(amount).toFixed(2)+' €');
        }
    }else {
        $('#lastpaymethod').val('');
        $(".discountbtn").prop('disabled',false);
    }
}

function deleteLastpaymethod() {
    if (creditsum.length > 0) {
        var amount = parseFloat(creditsum[creditsum.length - 1]['amount']);
        var ypoloipo = parseFloat($('#meriko_rest').val());
        var resta = parseFloat($('#meriko_change').val());
        var difference = resta - amount;
        if (difference >= 0) {
            ypoloipo = parseFloat(0).toFixed(2);
            resta = parseFloat(difference).toFixed(2);
            $("#meriko_rest").val(ypoloipo + ' €');
            $("#meriko_change").val(resta + ' €');
        } else {
            ypoloipo += Math.abs(parseFloat(difference));
            ypoloipo = ypoloipo.toFixed(2);
            resta = parseFloat(0).toFixed(2);
            $("#meriko_rest").val(ypoloipo + ' €');
            $("#meriko_change").val(resta + ' €');
        }
        creditsum.splice(-1, 1);
        updateLastpaymethod();
    }
}

function chooseCashout(id,element) {
    if (($("#merikonumbers").val()) && (parseFloat($("#merikonumbers").val())!==0)){
        $(".discountbtn").prop('disabled',true);
        var payment;
        var ypoloipo;
        var amount=parseFloat($("#merikonumbers").val());
        if(element!==null){
            var description=$(element).text();
        }else{
            var description="";
        }
        $("#merikonumbers").val('');
        if ($('#meriko_rest').val()) {//ean yparxei ypoloipo
            var rest=parseFloat($('#meriko_rest').val());
            ypoloipo=(rest-amount).toFixed(2);
            if (parseFloat(ypoloipo)>0) {
                $('#meriko_change').val(parseFloat(0).toFixed(2)+' €');
                $('#meriko_rest').val(ypoloipo+' €');
                payment={'id':parseInt(id),'amount':amount, 'description':description};
                creditsum.push(payment);
            }else if (parseFloat(ypoloipo)===0) {
                $('#meriko_change').val(parseFloat(0).toFixed(2)+' €');
                $('#meriko_rest').val(parseFloat(0).toFixed(2)+' €');
                payment={'id':parseInt(id),'amount':amount, 'description':description};
                creditsum.push(payment);
            }else{
                if ((parseInt(id)===2)){//ean o tropos plirwmis einai metrita
                    if (($('#meriko_change').val()) && rest===0){
                        var new_change=(amount+parseFloat($('#meriko_change').val())).toFixed(2);
                        $('#meriko_change').val(new_change+' €');
                    } else {
                        ypoloipo=Math.abs(ypoloipo);
                        $('#meriko_change').val(ypoloipo+' €');
                        $('#meriko_rest').val(parseFloat(0).toFixed(2)+' €');
                    }
                    payment={'id':parseInt(id),'amount':amount, 'description':description};
                    creditsum.push(payment);
                }
            }
        }else {//ean den yparxei ypoloipo
            var sunolo=parseFloat($('#meriko_sunolo').val());
            ypoloipo=(sunolo-amount).toFixed(2);
            if (parseFloat(ypoloipo)===0){
                $('#meriko_change').val(parseFloat(0).toFixed(2)+' €');
                $('#meriko_rest').val(parseFloat(0).toFixed(2)+' €');
                payment={'id':parseInt(id),'amount':amount, 'description':description};
                creditsum.push(payment);
            }else if (parseFloat(ypoloipo)>0){
                $('#meriko_change').val(parseFloat(0).toFixed(2)+' €');
                $('#meriko_rest').val(ypoloipo+' €');
                payment={'id':parseInt(id),'amount':amount, 'description':description};
                creditsum.push(payment);
            } else{
                if (parseInt(id)===2){//ean o tropos plirwmis einai metrita
                    ypoloipo=Math.abs(ypoloipo);
                    $('#meriko_change').val(ypoloipo+' €');
                    $('#meriko_rest').val(parseFloat(0).toFixed(2)+' €');
                    payment={'id':parseInt(id),'amount':amount,'description':description};
                    creditsum.push(payment);
                }
            }
        }
        updateLastpaymethod();
    }
}

function cashout(value) {
    $('#merikonumbers').val('');
    merikonumber(value);
    chooseCashout(2,null);
}

function merikonumber(value){
    var number=value;
    var screen=$("#merikonumbers").val();
    var sinolo=$("#meriko_sunolo").val();
    if (number==='b'){
        screen=screen.substring(0,(screen.length)-1);
        $("#merikonumbers").val(screen);
    }else if (number==='c'){
        $('#merikonumbers').val('');
    }else if (number==='all'){
        if ($('#meriko_rest').val()){
            $('#merikonumbers').val(parseFloat($('#meriko_rest').val()));
        }
        else {
            $('#merikonumbers').val(parseFloat(sinolo));
        }
    }else{
        $("#merikonumbers").val(screen + number);
    }
}

function apodeiksi(type) {
    sum=0;
    var cash=0;
    var upoloipo=parseFloat($("#meriko_rest").val());
    if(upoloipo>0) {
        cashout(upoloipo);
    }
    creditsum.forEach(function (array) {
        var amount = parseFloat(array.amount);
        if (parseInt(array.id) < 2) {
            payment_methods[parseInt(array.id)].amount += amount;
            sum += amount;
        } else if (parseInt(array.id) === 2) {
            cash += parseFloat(array.amount);
        } else {
            var card = {'id': parseInt(array.id), 'amount': amount, 'description': array.description};
            payment_methods.push(card);
            sum += amount;
        }
    });
    var sunolo=parseFloat($("#sumary").val());
    if(sum<sunolo){
        payment_methods.push({'id': -1, 'amount': cash, 'description': ''});
    }else{
        payment_methods.push({'id': -1, 'amount': 0, 'description': ''});
    }
    if (parseInt(type) === 0) {//kanoniki apodeiksi
        $('#sumary').trigger('click');
    } else {//xeirografi
        $('#xeirografi').trigger('click');
    }
}

//</editor-fold>


//<editor-fold desc="BarcodeModal">

function barcodeModal() {
    $('#bcodeModal').modal('show');
}

//</editor-fold desc="BarcodeModal">

function numpadShow(){
    $('#myModal').modal('show');
    if (isTimi){
        $('.savebutton').show();
        $('#items').removeClass('numpad-open');
        if (parseInt(pinakas.zugistiko)===1){
            if (parseInt(flagbit)===0){
                $('#numbers').prop('placeholder',"Εισάγετε τιμή");
            }else {
                $('#numbers').prop('placeholder',"Εισάγετε τιμή");
            }
        }else{
            $('#numbers').prop('placeholder',"Εισάγετε τιμή");
        }
    }
    else{
        $('#items').addClass('numpad-open');
        $('#numbers').prop('placeholder',"Εισάγετε ποσότητα");
    }
}

function numpadClose() {
    if (isTimi){
        $('.savebutton').hide();
        isTimi = false;
    }
    else{
        $('#items').removeClass('numpad-open');
    }
    $('#myModal').modal('hide');
    $('#numbers').val('');
}

function numpadSave() {
    var number=numbereval();
    if(!(number===false)){//val number
        if (isbarcode) {
            var data=calculateBarcode(number,flagbit,tail);
            pinakas.sunolo=data.timi;
            pinakas.timi=parseFloat(number).toFixed(2);
            pinakas.quantity=data.quantity;
            isbarcode = false;
        } else {
            pinakas.timi=parseFloat(number).toFixed(2);
            pinakas.timi=parseFloat(pinakas.timi);
           pinakas.sunolo = (pinakas.timi*parseFloat(pinakas.quantity)).toFixed(2);
        }
        numpadClose();
        writeproion();
    }
    return false;
}

function numbereval(){
    var number =$('#numbers').val();
    //TODO var pattern="/.[0-9]{2,}|[0-9]{3,}";
    if(number.length>0){
        if((number.charAt(0)==='.')&&(number.length===1)) {
            number=false;
        }else if((parseInt(number)===0)&&(number.length===1)) {
            number=false;
        }
    }else{
        number=false;
    }
    $('#numbers').val('');
    return number;
}

function numbercheck() {
    var number = $('#numbers').val();
    var posotita;
    if (number.length > 0) {
        if((number.charAt(0)==='.')&&(number.length===1)) {
            posotita = 1;
        }
        else {
            posotita = parseFloat(number);
        }
    }
    else{
        posotita = 1;
    }
    $('#numbers').val('');
    return posotita;
}

function del(value) {
    var timi=$('#line'+value+' input[name="sunolo[]"').val();
    timi=parseFloat(timi);
    $('#line'+value).remove();
    var sum=$('#sumary').val();
    sum=parseFloat(sum);
    sum=sum-timi;
    sum=parseFloat(sum).toFixed(2);
    $('#sumary').val(sum);
    if(sum!=='0.00') {
        $('#sunolo').html(sum);
    }else{
        $('#sunolo').html('Σύνολο');
        $('#sumary').val(0);
    }
    data={"id":value};
    delCustomerDisplay(data);
}

function checkForStandBy() {
    var data=sessionStorage.getItem('standby');
    if (data){
        keepStandBy();
    } else{
        $('.fa-desktop').css('color','inherit');
    }
}

function keepStandBy() {
    var data=sessionStorage.getItem('standby');
    if (data) {//ean yparxei energi anamoni
        var new_customer={id:customer.id, type:customer.type, discounts:customer.discounts};
        data = JSON.parse(data);
        var  customer_id = parseInt(data.customer.id);
        var discounts = data.customer.discounts;
        sessionStorage.removeItem('standby');
        if ($('#sumary').val() !== '0') {
            saveStandBy(new_customer);
        }else {
            $('.fa-desktop').css('color','inherit');
        }
        if (parseInt(customer_id)!==0){
            searchCustomer(parseInt(customer_id),1);
        }else{
            customer.id=0;
            customer.type=0;
        }
        customer.discounts=discounts;
        data.receipt.forEach(function (row) {
            pinakas = {
                id: parseInt(row.id),
                perigrafi: row.desc,
                timi: parseFloat(row.pre_price).toFixed(2),
                flag_timi: row.flag_timi,
                tmima: row.tmima,
                zugistiko: parseInt(row.zigos),
                quantity: row.temaxia,
                stock: (parseFloat(row.temaxia) + parseFloat(row.stock)).toFixed(3),
                sunolo: parseFloat(row.pre_sunolo).toFixed(2),
                siskevasia: parseInt(row.package),
                mothercode: row.mothercode,
                new: parseInt(row.new),
                api: parseInt(row.api),
                id_om: row.id_om,
                id_cat: row.id_cat,
                barcode: row.barcode
            };
            user.id=parseInt(row.operator);
            writeproion();
        });
    }else {//den yparxei alli anamoni
        if ($('#sumary').val() !== '0') {
            saveStandBy(customer);
        }else {
            bootboxMessage('Δεν υπάρχει κάτι στο ταμείο για να προσθέσετε στην αναμονή',3);
        }
    }
}

function saveStandBy(pelatis) {
    $('.wrapper').fadeTo("fast",0.2).delay(500).fadeTo("fast",1);
    var input = $("#tameio").serializeArray();
    var i=0;
    var receipt=[];
    input.forEach(function (field) {
        if(field.name==="desc[]"){
            receipt[i]={};
        }
        receipt[i][field.name.substring(0,field.name.length-2)]=field.value;
        if(field.name==="barcode[]"){
            i++;
        }
    });
    var anamoni={receipt:'',customer:''};
    anamoni.receipt=receipt;
    var new_customer={id:parseInt(pelatis.id),type:parseInt(pelatis.type),discounts:pelatis.discounts};
    anamoni.customer=new_customer;
    sessionStorage.setItem('standby',JSON.stringify(anamoni));
    start();
    $('.fa-desktop').css('color','#82e566');
}

function invoice() {
    if (parseInt(customer.type)===1){
        customer.invoice=1;
        apodeiksi(0);
    }else {
        if (parseInt(customer.id)===0){
            bootboxMessage('Πρέπει να προσθέσετε πελάτη πρώτα',3);
        } else{
            bootboxMessage('Δεν μπορεί να κοπεί τιμολόγιο για πελάτες λιανικής',1);
        }

    }
}

function checkTameio(id) {
    if($('#sumary').val()!=='0'){
        bootboxMessage('Έχετε ανοικτό ταμείο. Παρακαλώ ολοκληρώστε τη συναλλαγή ή χρησιμοποιήστε την αναμονή',3);
    }else {
        navigation(id,0);
    }
}

function start() {
    $('#merikoModal').modal('hide');
    window.isTimi = false;
    window.isbarcode = false;
    window.openCashier = true;
    window.tail ='';
    window.flagbit = '';
    window.counter=0;
    sum=0;
    var data = {id:"",perigrafi: "",timi: "",flag_timi:"",tmima:"",zugistiko:0,quantity:"",sunolo:"",siskevasia:1,mothercode:"",
        new:1,api:0,id_om:900000000,id_cat:900000,barcode:""};
    window.pinakas = data;//global pinakas
    window.altPerigrafi = false;
    payment_methods=[];
    var i;for (i=0;i<3;i++){
        payment_methods[i]={'id':i, 'amount':0, 'description':""};
    }
    window.discount=[];
    timer=0;
    resetCustomer();
    getAltPerigrafi();
    $('#itemsdisplay').empty();
    $('#creditsum').empty();
    $('#numbers').val('');
    $("#sumary").val('0');
    $("#sunolo").html('Σύνολο');
    $("#barcode").focus();
    $('#merikoModal input').val('');
    resetCustomerDisplay();
}


$(document).ready(function () {
    initCss();
    menuCss();
    appendHead();
    start();//start barcode keno kai global
    $.when(
        specialBcodes(),
        getItems(),
        getTmimata(),
        getPaymentMethods()
    ).then(function () {
        bindItemCallListeners();
    });
    window.user={uname:"", id:""};

    setInterval(function(){
        var action="reloadSession";
        $.ajax({
            data:{"action":action}
        });
    },1200000);

    checkForStandBy();

    $('#searchCustomerModal').on('shown.bs.modal',function () {
        $('#barcode').prop("disabled",true);
        bindCustomerListeners();
    });

    $('#searchCustomerModal').on('hidden.bs.modal',function () {
        unbindCustomerListeners();
        $('#barcode')
            .prop("disabled",false)
            .focus();
        $('#customer_search').val('');
        $('.customer_list').empty();
        $('.customer_form').show();
    });


    $('#merikoModal').on('show.bs.modal', function () {
        $(".discountbtn").prop('disabled',false);
        creditsum=[];
        $("#lastpaymethod").val('');
    });

    $('#merikoModal').on('hidden.bs.modal', function () {
        $('#barcode').focus();
    });

    $('#myModal').on('hidden.bs.modal', function () {
        $('#barcode').focus();
    });

    $('#bcodeModal').on('shown.bs.modal', function () {
        $('#barcode').prop("disabled",true);
        $('#bcodenumbers').focus();
    });

    $('#bcodeModal').on('hidden.bs.modal',function () {
        $('#bcodenumbers').val('');
        $('#barcode')
            .prop("disabled",false)
            .focus();
    });

    $('#choose_item').on('hidden.bs.modal',function () {
        unbindItemCallListeners();
        bindItemCallListeners();
        $('#barcode').focus();
    });

    $(document).on('click',function () {
        $("#barcode").focus();
    });

    $('#reset').click(function () {
        start();
    });

    $('#xeirografi').click(function () {
        if ($('#sumary').val()==='0'){
            return false;
        } else {
            bootbox.confirm({
                message: "<div class='infomsg'>Με αυτή την λειτουργία δεν θα κοπεί απόδειξη απο την μηχανή. Είστε σίγουρος;</div>",
                buttons: {
                    confirm: {
                        label: 'Ναι, θέλω να εκδόσω χειρόγραφη',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'Όχι',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result){
                        var result=toggleCashier();
                        result.then(function (json) {
                            console.log(json);
                            var data=JSON.parse(json);
                            if (data.response){
                                openCashier=false;
                                $('#sumary').trigger('click');
                            } else {
                                bootboxMessage(data.error,1);
                            }
                        });
                    } else{
                        $('#sumary').trigger('click');
                    }
                }
            });
        }


    });

    $('#change_perigrafi').click(function () {
        var result =changePerigrafi();
        result.then(function (json) {
           console.log(json);
           var data = JSON.parse(json);
           if (data.response){
               getAltPerigrafi();
           } else{
               bootboxMessage(data.error,1);
           }

        });

    });

    $('#sumary').click(function () {
        payment_methods[2].amount=parseFloat($('#sumary').val())-parseFloat(sum);
       $('#tameio').trigger('submit');
    });

    $('#tameio').submit(function (event) {
        event.preventDefault();
        if($('#sumary').val()==='0'){
            return false;
        }else {
            if ($('#meriko_change').val()===''){
                $('#meriko_change').val(0);
            }
            var action='submit';
            var input = $("#tameio").serializeArray();
            var i=0;
            var receipt=[];
            input.forEach(function (field) {
                if(field.name==="desc[]"){
                    receipt[i]={};
                }
                receipt[i][field.name.substring(0,field.name.length-2)]=field.value;
                if(field.name==="barcode[]"){
                    i++;
                }
            });
            var data={'receipt':receipt, 'payments':payment_methods, 'customer':customer.id, 'invoice':customer.invoice};
            data=JSON.stringify(data);
            $.ajax({
                data: {"action": action, "data": data},
                datatype: "json",
                success: function (json) {
                    var data = JSON.parse(json);
                    if (data.response === true) {
                        if(typeof data.url !== 'undefined'){
                            try{
                                Android.printInvoice(data.url);
                            }catch (e){
                                window.open(data.url);
                            }
                        }
                        if (!openCashier){
                            var togglecashier=toggleCashier();
                            togglecashier.then(function (json) {
                                var data=JSON.parse(json);
                                if (data.response){
                                    var info=bootbox.alert({
                                        size:"small",
                                        title:"Μήνυμα",
                                        message:'<div class="infomsg">' +
                                            '<p>Πληροφορίες Απόδειξης</p>' +
                                            '<p><b>ΣΥΝΟΛΟ:</br> '+parseFloat($('#sumary').val()).toFixed(2)+'€</p>' +
                                            '<p><b>ΡΕΣΤΑ:</br> '+parseFloat($('#meriko_change').val()).toFixed(2)+'€</p></div>',
                                        callback: function () {
                                            setTimeout(function () {
                                                $('#barcode').focus();
                                            },50);
                                        }
                                    });
                                    setTimeout(function () {
                                        info.modal('hide');
                                    },2000);
                                    start();
                                }else{
                                    bootboxMessage(data.error,1);
                                }

                            });
                        } else {
                            var info=bootbox.alert({
                                size:"small",
                                title:"Μήνυμα",
                                message:'<div class="infomsg">' +
                                    '<p>Πληροφορίες Απόδειξης</p>' +
                                    '<p><b>ΣΥΝΟΛΟ:</b> '+parseFloat($('#sumary').val()).toFixed(2)+' €</p>' +
                                    '<p><b>ΡΕΣΤΑ:</b> '+parseFloat($('#meriko_change').val()).toFixed(2)+' €</p></div>',
                                callback: function () {
                                    setTimeout(function () {
                                        $('#barcode').focus();
                                    },50);
                                }
                            });
                            setTimeout(function () {
                                info.modal('hide');
                                $("#barcode").focus();
                            },2000);
                            start();
                        }

                    } else {
                        payment_methods=[];
                        for (var i =0; i<3; i++){
                            payment_methods[i]={'id':i, 'amount':0, 'description':""};
                        }
                        bootboxMessage(data.error,1);
                        return false;
                    }
                }
            });
        }
    });

    var userinfo=userInfo();
    userinfo.then(function (json) {
        var data=JSON.parse(json);
        if(data.response){
            user.name=data.user.name;
            user.id=parseInt(data.user.user_id);
        }else{
            window.location.replace('../index.php');
        }
    });

});

