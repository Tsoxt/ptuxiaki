$.ajaxSetup({
    type: "POST",
    datatype:"json",
    beforeSend: function () {
        $('.ajax-loader').show();
    },
    url:    "../include/resources/dispatcher.php",
    timeout: 7000,
    cache:false,
    complete: function (xhr){
        xhr.done(function(json){
            var data=JSON.parse(json);
            if(!data['loginStatus']){
                logout();
            }
            $('.ajax-loader').hide();
        });
    },
    error: function(xhr,ajaxOptions,thrownError){
        $('.ajax-loader').hide();
        var msg="Πρόβλημα δικτύου \n http status: "+xhr.status+", σφάλμα: "+thrownError;
        bootboxMessage(msg,1);
    }
});

function storeIsozigio(diff,zigaria,minas) {
    var action='storeisozigio';
    var data={temaxia:diff,id_om:zigaria,month:minas};
    data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function callKiniseis(imera_from,imera_to,action) {
    var data = {datefrom:imera_from, dateto:imera_to};
    data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });

}

function getLastZita() {
    var action='getlastzita';
    return $.ajax({
        data: {"action":action},
        datatype: "json"
    });

}

function updateZlog() {
    var action='insertLog';
    return $.ajax({
        data: {"action":action, "type":2},
        datatype: "json"
    });
}

function printTable(month,year) {
    var action='printisozigio';
    var data = {datefrom:month, dateto:year};
    data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}
/*function LineChart(){
    var ctx = $('#kiniseis');
    var lineChart = new Chart(ctx, {
        type: 'line',
        responsive: 'true',
        data: {
            labels: [],
            datasets: [{
                data: [],
                borderColor: 'rgba(255, 99, 132, 0.2)',
                borderWidth: '2'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
    return lineChart;
}*/

function reports(value){
    var action;
    if(parseInt(value)===7){
        action='xreport';
    }else{
        action='zreport';
    }
    if ((parseInt(value)===1) && (parseInt(access)>2)){
            bootboxMessage('Δεν έχετε τα δικαιώματα για έκδοση Ζ. Επικοινωνήστε με κάποιον προϊστάμενο.',3);
    }else {
        $.ajax({
            data: {"action": action},
            datatype: "json",
            success:function (json) {
                console.log(json);
                var data=JSON.parse(json);
                if(data.response){
                    if (typeof data.msg){
                        bootboxMessage(data.msg,2);
                    } else {
                        bootboxMessage('Επιτυχές, ελέγξτε τη μηχανή',2);
                    }
                }else{
                    bootboxMessage(data.error,1);
                }
            }
        });
    }
}

function bootboxMessage(msg,type) {
    if (parseInt(type)===1) {
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="errormsg">'+msg+'</div>',
        });
    }else if (parseInt(type)===2){
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="successmsg">'+msg+'</div>',
        });
    }else {
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="infomsg">'+msg+'</div>',
        });
    }
}

function updateChart(i,zigaria) {
    var new_val=$("#omada"+i).val();
    var month=$('#month_report').val();
    if (!(parseFloat(old_val)===parseFloat(new_val))) {
        var diff=parseFloat(old_val)-parseFloat(new_val);
        diff=diff.toFixed(3);
        var result=storeIsozigio(diff,zigaria,month);
        result.then(function (json) {
            console.log(json);
            var data=JSON.parse(json);
            if (data.response){
                return true;
            } else {
                bootboxMessage(data.error(),1);
            }
        });
    }
    $(':input[type="number"]').prop('readonly',true);

}

function editInput(i) {
    if (parseInt(access)<=1){
        old_val=$("#omada"+i).val();
        $(':input[type="number"]').prop('readonly',true);
        $("#omada"+i).prop('readonly',false);
        $("#omada"+i).focus();
    }
}

function sendPrint(month,year) {
    var result=printTable(month,year);
    result.then(function (json) {
        console.log(json);
        var data=JSON.parse(json);
        if (data.response){
            bootboxMessage('Επιτυχής εκτύπωση',2);
        }else {
            bootboxMessage(data.error,1);
        }

    });

}

function lastZitaReport() {
    if (parseInt(access)>1){
        bootboxMessage('Δεν έχετε τα δικαιώματα για να δείτε αυτές τις αναφορές',3);
    } else{
        $('.stats').empty();
        $('#kiniseis').remove();
        var result=getLastZita();
        result.then(function (json) {
            var data=JSON.parse(json);
            if (data.response) {
                var lastZitaDate= data.lastLog.lastDate;
                showReport(type,lastZitaDate);
            }else{
                bootboxMessage(data.error,1);
                return false;
            }
        });
    }
}

function showReport(type,lastzitadate) {//0 button = search between dates, 1 button = search from last zita
    var oneDay = 24 * 60 * 60 * 1000;
    var imerominia, date1, date2, month,day, year, curmonth,curday, curyear, curhours, curminutes, curseconds;
    imerominia = $('#date_from').val();
    date1 = new Date(imerominia);
    month = date1.getMonth() + 1;
    day = date1.getDate();
    year = date1.getFullYear();
    var imera_from = year + '-' +
        (month < 10 ? '0' : '') + month + '-' +
        (day < 10 ? '0' : '') + day + ' 00:00:00';
    imerominia = $('#date_to').val();
    date2 = new Date(imerominia);
    month = date2.getMonth() + 1;
    day = date2.getDate();
    year = date2.getFullYear();
    var imera_to = year + '-' +
        (month < 10 ? '0' : '') + month + '-' +
        (day < 10 ? '0' : '') + day + ' 23:59:59';
    var diffDays = Math.round(Math.abs((date1.getTime() - date2.getTime()) / (oneDay))) + 1;
    var curdate=new Date();
    curmonth = curdate.getMonth() + 1;
    curday = curdate.getDate();
    curyear = curdate.getFullYear();
    curhours = curdate.getHours();
    curminutes = curdate.getMinutes();
    curseconds = curdate.getSeconds();
    var current_date = curyear + '-' +
        (curmonth < 10 ? '0' : '') + curmonth + '-' +
        (curday < 10 ? '0' : '') + curday + ' ' + curhours + ':' + curminutes + ':' +curseconds;
    $('.canvas_container').append('<canvas id="kiniseis"></canvas>');
    var coloR = [];
    var dynamicColors = function () {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgba(" + r + "," + g + "," + b + ",0.8)";
    };

    var ctx = $('#kiniseis');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        responsive: 'true',
        data: {
            labels: [],
            datasets: [{
                data: [],
                backgroundColor: coloR,
                borderColor: 'rgba(26, 26, 26, 0.6)',
                hoverBorderColor: 'rgba(26, 26, 26, 1)',
                hoverBorderWidth: '3',
                borderWidth: '2'
            }]
        }
    });
    if (parseInt(type) === 1) {
        var action = 'callstatsbyuser';
        if (typeof lastzitadate !='undefined') {
            var result = callKiniseis(lastzitadate, current_date, action);
        }else{
            var result = callKiniseis(imera_from, imera_to, action);
        }
        result.then(function (json) {
            console.log(json);
            var data = JSON.parse(json);
            console.log(data);
            if (data.response) {
                $('.stats').append('<button type="button" class="printbut" onclick="" hidden>Print</button>' +
                    '<h3><i>Πίνακας Στατιστικών</i></h3><div class="table-responsive"><table class="table"><thead><tr><th>Χρήστης</th>' +
                    '<th>Σύνολο</th><th></th><th>Σύνολο Πελατών</th><th>Μ.Ο /Πελάτη</th></tr></thead><tbody></tbody></table></div>');
                data.userstats.forEach(function (field) {
                    coloR.push(dynamicColors());
                    myChart.data.labels.push(field.name);
                    myChart.data.datasets.forEach((dataset) => {
                        dataset.data.push(field.sunolo);
                    });
                    $('.table tbody').append('<tr><td>' + field.name + '</td><td><input type="number" class="sinola" readonly value="' + parseFloat(field.sunolo).toFixed(2) + '"></td><td>€</td>' +
                        '<td>' + parseInt(field.receipt) + '</td><td>' + (parseFloat(field.sunolo).toFixed(2) / parseInt(field.receipt)).toFixed(2) + ' €</td></tr>');

                });
                myChart.update();
                $('#stats').show();
            } else {
                bootboxMessage(data.error,1);
            }


        });

    } else if (parseInt(type) === 2) {
        var action = 'callstatsbytmima';
        if (typeof lastzitadate !='undefined') {
            var result = callKiniseis(lastzitadate, current_date, action);
        }else{
            var result = callKiniseis(imera_from, imera_to, action);
        }
        result.then(function (json) {
            console.log(json);
            var data = JSON.parse(json);
            if (data.response) {
                $('.stats').append('<button type="button" class="printbut" onclick="" hidden>Print</button>' +
                    '<h3><i>Πίνακας Στατιστικών</i></h3><div class="table-responsive"><table class="table"><thead><tr><th>Τμήμα</th>' +
                    '<th>Σύνολο</th><th></th></tr></thead><tbody></tbody></table></div>');
                data.tmimastats.forEach(function (field) {
                    coloR.push(dynamicColors());
                    myChart.data.labels.push(field.name);
                    myChart.data.datasets.forEach((dataset) => {
                        dataset.data.push(field.sunolo);
                    });
                    $('.table tbody').append('<tr><td>' + field.name + '</td><td><input type="number" class="sinola" readonly value="' + parseFloat(field.sunolo).toFixed(2) + '"></td><td>€</td></tr>');
                });
                myChart.update();
                $('#stats').show();
            } else {
                bootboxMessage(data.error,1);
            }
        });
    } else if (parseInt(type) === 3) {
        var action = 'callstatsbyomada';
        var result = callKiniseis(imera_from, imera_to, action);
        result.then(function (json) {
            console.log(json);
            var data = JSON.parse(json);
            if (data.response) {
                $('.stats').append('<button type="button" class="printbut" onclick="" hidden>Print</button>' +
                    '<h3><i>Πίνακας Στατιστικών</i></h3><div class="table-responsive"><table class="table"><thead><tr><th>Ομάδα</th><th>Ποσότητα</th>' +
                    '<th>Σύνολο</th><th></th></tr></thead><tbody></tbody></table></div>');
                data.omadastats.forEach(function (group) {
                    coloR.push(dynamicColors());
                    myChart.data.labels.push(group.name);
                    myChart.data.datasets.forEach((dataset) => {
                        dataset.data.push(group.sunolo);
                    });
                    $('.table tbody').append('<tr><td>' + group.name + '</td><td>' + parseFloat(group.quantity).toFixed(3) +
                        '</td><td><input type="number" class="sinola" readonly required value="' + parseFloat(group.sunolo).toFixed(2) + '"></td>' +
                        '<td>€</td></tr>');
                });
                myChart.update();
                $('#stats').show();
            } else {
                bootboxMessage(data.error,1);
            }

        });

    } else if (parseInt(type) === 4) {
        var action = 'callstatsbyhour';
        var result = callKiniseis(imera_from, imera_to, action);
        result.then(function (json) {
            console.log(json);
            var data = JSON.parse(json);
            if (data.response) {
                $('.stats').append('<button type="button" class="printbut" onclick="" hidden>Print</button>' +
                    '<h3><i>Πίνακας Στατιστικών</i></h3><div class="table-responsive"><table class="table"><thead><tr><th>Ώρες</th><th>Σύνολο</th><th>Μ.Ο</th><th>Σύνολο Αποδείξεων</th></tr></thead><tbody></tbody></table></div>');
                data.statsperhour.forEach(function (group) {
                    var next = parseInt(group.hour) + 1 + ":00";
                    coloR.push(dynamicColors());
                    myChart.data.labels.push(group.hour + ':00 - ' + next);
                    myChart.data.datasets.forEach((dataset) => {
                        dataset.data.push(group.sunolo);
                    });
                    $('.table tbody').append('<tr><td>' + group.hour + ':00 - ' + next + '</td><td>'+parseFloat(group.sunolo)+' €</td><td>' + (parseFloat(group.sunolo).toFixed(2) / diffDays).toFixed(2) + ' €</td>' +
                        '<td>' + group.receipt + '</td></tr>');
                });
                myChart.update();
                $('#stats').show();
            } else {
                bootboxMessage(data.error,1);
            }
        });
    }else if (parseInt(type)===5){
        var action = 'callstatsbypaymentmethods';
        var result = callKiniseis(imera_from, imera_to, action);
        result.then(function (json) {
            console.log(json);
            var data = JSON.parse(json);
            if (data.response) {
                $('.stats').append('<button type="button" class="printbut" onclick="" hidden>Print</button>' +
                    '<h3><i>Πίνακας Στατιστικών</i></h3><div class="table-responsive"><table class="table"><thead><tr><th>Τρόπος Πληρωμής</th><th>Σύνολο</th>' +
                    '<th></th></tr></thead><tbody></tbody></table></div>');
                data.paymentmethodsstats.forEach(function (group) {
                    coloR.push(dynamicColors());
                    myChart.data.labels.push(group.name);
                    myChart.data.datasets.forEach((dataset) => {
                        dataset.data.push(group.sunolo);
                    });
                    $('.table tbody').append('<tr><td>' + group.name + '</td><td><input type="number" class="sinola" readonly required value="' + parseFloat(group.sunolo).toFixed(2) + '"></td>' +
                        '<td>€</td></tr>');
                });
                myChart.update();
                $('#stats').show();
            } else {
                bootboxMessage(data.error,1);
            }

        });
    } else if (parseInt(type)===6) {
        var iso_month=($('#month_report').val()).substring(5);
        var iso_year=($('#month_report').val()).substring(0,4);
        var action = 'callisozigio';
        var result = callKiniseis(iso_month, iso_year, action);
        result.then(function (json) {
            console.log(json);
            var data = JSON.parse(json);
            if (data.response) {
                $('.stats').append('<button type="button" class="printbut" onclick="sendPrint('+iso_month+','+iso_year+')" hidden>Print</button>' +
                    '<h3><i>Πίνακας Στατιστικών</i></h3><div class="table-responsive"><table class="table"><thead><tr><th>Ομάδα</th><th>Κωδικός Ζυγαριάς</th>' +
                    '<th>Ποσότητα</th><th>Μήνας</th><th>Έτος</th></tr></thead><tbody></tbody></table></div>');
                var i = 0;
                data.omadesisozigio.forEach(function (group) {
                    coloR.push(dynamicColors());
                    myChart.data.labels.push(group.name);
                    myChart.data.datasets.forEach((dataset) => {
                        dataset.data.push((0-parseFloat(group.sunolo)).toFixed(3));
                    });
                    var posotita=(0-parseFloat(group.sunolo)).toFixed(3);
                    $('.table tbody').append('<tr><td>' + group.name + '</td><td>' + parseInt(group.id_omadas) + '</td><td><input type="number" id="omada' + i + '" class="sinola" readonly required min="0" value="' + posotita + '" ondblclick="editInput(' + i + ')" onchange="updateChart(' + i + ','+group.id_omadas+')"></td>' +
                        '<td>' + iso_month + '</td><td>' + iso_year + '</td></tr>');
                    i++;
                });
                myChart.update();
                $('#stats').show();
            } else {
                bootboxMessage(data.error,1);
            }

        });
    }

}

function setSidemenuDefault() {
    $('.sidebuts').css({
        "background-color":"#f6f6f6",
        "color":"#665e5e"
    });
}

function bindReportListeners() {
    $('#date_from').on('change',function () {
        var datefrom = new Date($('#date_from').val());
        var month = (datefrom.getMonth()+1);
        var day = datefrom.getDate();
        var dateto = datefrom.getFullYear() + '-' +
            (month<10 ? '0' : '') + month + '-' +
            (day<10 ? '0' : '') + day;
        $('#date_to').val(dateto);
        $('#date_to').prop('min',function () {
            return dateto;
        });
    });
    $('#date_to').on('change',function () {
        if ($('#date_from').val()===''){
            var dateto = new Date($('#date_from').val());
            var month = (dateto.getMonth()+1);
            var day = dateto.getDate();
            var datefrom = dateto.getFullYear() + '-' +
                (month<10 ? '0' : '') + month + '-' +
                (day<10 ? '0' : '') + day;
            $('#date_from').val(datefrom);
            $('#date_from').prop('min',function () {
                return datefrom;
            });
        }

    });
    $('#choose_date .datesearch').on('click',function () {
        $('.stats').empty();
        $('#kiniseis').remove();
        if (parseInt(access)>2){
            bootboxMessage('Δεν έχετε τα δικαιώματα για να δείτε αυτές τις αναφορές',3);
        }else{
            showReport(type,undefined);
        }
    });
    $('#choose_month .datesearch').on('click',function () {
        $('.stats').empty();
        $('#kiniseis').remove();
        if ($('#month_report')[0].checkValidity()){
            if (parseInt(access)>2){
                bootboxMessage('Δεν έχετε τα δικαιώματα για να δείτε αυτές τις αναφορές',3);
            }else {
                showReport(type,undefined);
            }
        } else {
            $('#month_report')[0].reportValidity();
        }


    });
    $('.stats').hover(function () {
        $('.stats button').show(500);
    },function () {
        $('.stats button').hide(500);
    });
}


function sideButtonsListeners() {
    $(".sidebuts").click(function () {
        var id=$(this).attr('id');
        if (id==="collapseSidemenu"){
            $('.sidebuts span').toggle();
            $('#collapseSidemenu i').toggle();
        } else {
            id=id.substring(3);
            start();
            setSidemenuDefault();
            $(this).css({
                "background-color":"#665e5e",
                "color":"#f6f6f6"
            });
            $('.sidebuts span').hide();
            var newrow;
            if (parseInt(id)===6){
                newrow='<div class="formTab">' +
                    '<div class="date_options">' +
                    '<div id="choose_month" class="date_fields">\n' +
                    '<span>Επιλέξτε μήνα: </span>\n' +
                    '<input class="report_date" id="month_report" type="month" pattern="[0-9]{4}-[0-9]{2}" required>\n' +
                    '<button class="datesearch"><i class="icon ion-search"></i></button>\n' +
                    '</div>' +
                    '</div>' +
                    ' <div class="statistics">\n' +
                    '<div class="canvas_container">\n' +
                    '<canvas id="kiniseis"></canvas>\n' +
                    '</div>\n' +
                    '<div class="stats"></div>\n' +
                    '</div>' +
                    '</div>';
            }else if (parseInt(id)===7 || parseInt(id)===8){//anafores x kai z
                reports(id);
            } else if(parseInt(id)===1 || parseInt(id)===2){
                newrow='<div class="formTab">' +
                    '<div class="date_options">' +
                    '<div id="choose_date" class="ch_date">' +
                    '<button class="lastZita" onclick="lastZitaReport()">Απο τελευταίο Ζ</button>\n' +
                    '<div class="date_fields">\n' +
                    '<span>Από: </span>\n' +
                    '<input class="report_date" id="date_from" type="date">\n' +
                    '<span>εώς:</span>\n' +
                    '<input class="report_date" id="date_to" type="date">\n' +
                    '<button class="datesearch"><i class="icon ion-search"></i></button>\n' +
                    '</div>\n' +
                    '</div>' +
                    '</div>' +
                    ' <div class="statistics">\n' +
                    '<div class="canvas_container">\n' +
                    '<canvas id="kiniseis"></canvas>\n' +
                    '</div>\n' +
                    '<div class="stats"></div>\n' +
                    '</div>' +
                    '</div>';
            } else {
                newrow='<div class="formTab">' +
                    '<div class="date_options">' +
                    '<div id="choose_date" class="ch_date">\n' +
                    '<div class="date_fields">\n' +
                    '<span>Από: </span>\n' +
                    '<input class="report_date" id="date_from" type="date">\n' +
                    '<span>εώς:</span>\n' +
                    '<input class="report_date" id="date_to" type="date">\n' +
                    '<button class="datesearch"><i class="icon ion-search"></i></button>\n' +
                    '</div>\n' +
                    '</div>' +
                    '</div>' +
                    ' <div class="statistics">\n' +
                    '<div class="canvas_container">\n' +
                    '<canvas id="kiniseis"></canvas>\n' +
                    '</div>\n' +
                    '<div class="stats"></div>\n' +
                    '</div>' +
                    '</div>';
            }
            $('.maincolumn').append(newrow);
            bindReportListeners();
            type=parseInt(id);
        }
    });
}


function start(){
    $('.ajax-loader').show(0).delay(500).hide(0);
    $('.maincolumn').empty();
    var type, old_val;
    window.type=type;//global gia to click
    window.old_val=old_val;
}

$(document).ready(function () {
    access;
    initCss();
    menuCss();
    sidebuts();
    appendHead();
    start();

    sideButtonsListeners();
});