function searchSupplier(input,type) {
    $('#groupset2 .listOfPersons').empty();
    var result=findSupplier(input,type);
    result.then(function (json) {
        console.log(json);
        var data=JSON.parse(json);
        if (data.response){
            clearSupplier();
            if (parseInt(type)===1){//anazitisi me id
                writeSupplier(data.suppliers[0]);
            } else{//anazitisi fulltext
                if (data.gsis){
                    supplier=-1;
                    writeSupplier(data.suppliers[0]);
                } else{
                    unbindSupplierListeners();
                    $('#groupset2 .listOfPersons').append('<div class="table-responsive">' +
                        '<table class="table table-hover">' +
                        '<thead>' +
                        '<tr>' +
                        '<th style="width: 20%">Όνομα</th>' +
                        '<th style="width: 20%">Διεύθυνση</th>' +
                        '<th style="width: 13%">ΑΦΜ</th>' +
                        '<th style="width: 13%">Τηλέφωνο</th>' +
                        '<th style="width: 13%">Κινητό</th>' +
                        '<th style="width: 13%">Υπόλοιπο</th>' +
                        '<th style="width: 8%"></th>' +
                        '</tr></thead><tbody></tbody></table></div>');
                    data.suppliers.forEach(function (row) {
                        appendPersonInventory(row,'groupset2');
                    });
                    supplierListeners();
                }
            }
        } else {
            $('#groupset2 .listOfPersons').append('<p>'+data.error+'</p>');
        }
    });
}

function clearSupplier() {
    $('input[type="date"]').val('');
    $('#supplier_parastatika').empty();
    $('#supplier_products').empty();
    $('.supplier_form').hide();
    $('#addProduct')[0].reset();
    $('#groupset2 .listOfPersons').empty();
    clearSupplierDeltio();
}

function writeSupplier(data) {
    if (data.ypoloipo===null){
        data.ypoloipo=0;
    }
    supplier=parseInt(data.body.id);
    $('#supplier_register input[name="ypoloipo"]').val(parseFloat(data.ypoloipo).toFixed(2));
    $('#supplier_register input[name="name"]').val(data.body.name);
    $('#supplier_register input[name="address"]').val(data.body.address);
    $('#supplier_register input[name="city"]').val(data.body.city);
    $('#supplier_register input[name="tk"]').val(data.body.postcode);
    $('#supplier_register input[name="phone"]').val(data.body.phone);
    $('#supplier_register input[name="mobile"]').val(data.body.mobile);
    $('#supplier_register input[name="job"]').val(data.body.job);
    $('#supplier_register input[name="afm"]').val(data.body.afm);
    $('#supplier_register input[name="doy"]').val(data.body.doy);
    $('#supplier_register input[name="email"]').val(data.body.email);
    $('.formbuttons').hide();
    $('.supplier_form').show();
    callSupplierProducts();
}

function handleSupplier(name){
    var data,
        pedio = $('#supplier_register [name="'+name+'"]');
    if (!(pedio[0].checkValidity())){
        pedio[0].reportValidity();
    }else{
        data={id:parseInt(supplier), name:name, value:pedio.val()};
        var result=updateSupplier(data);
        result.then(function (json) {
            console.log(json);
            var data=JSON.parse(json);
            if (data.response){
                pedio[0].reportValidity();
            } else{
                bootboxMessage(data.error,1);
            }
        });
    }
}

function newSupplier() {
    start();
    clearSupplier();
    unbindSupplierListeners();
    $('#groupset2').show();
    $('.supplier_form').show();
    $('a[href="#supplier_info"]').tab('show');
    supplierListeners();
}

function callSupplierProducts() {
    unbindSupplierListeners();
    $('#supplier_products').empty();
    $('#supplier_products').append('<table class="table table-hover">' +
        '<thead>' +
        '<tr>' +
        '<th style="width: 22%">Περιγραφή</th><th style="width: 15%">Συσκευασία</th>' +
        '<th style="width: 15%">Απόθεμα</th><th style="width: 15%">Τιμή αγοράς</th><th style="width: 15%">Ποσότητα παραγγελίας</th>' +
        '<th style="width: 6%"></th><th style="width: 6%"></th><th style="width: 6%"></th>' +
        '</tr>' +
        '</thead>' +
        '<tbody></tbody>' +
        '</table>');
    var id=parseInt(supplier);
    var result=getSupplierProducts(id);
    result.then(function (json) {
        console.log(json);
        var data = JSON.parse(json);
        if (data.response) {
            data.body.forEach(function (row) {
                appendSupplierProduct(row);
            });
        } else {
            $('#supplier_products').append('<div class="info_msg">-'+data.error+'-</div>');
        }
        supplierListeners();
    });
}

function appendSupplierProduct(field) {
    var color;
    if (parseFloat(field.stock)<=parseFloat(field.stock_alert)){
        color='red';
    }else {
        color='inherit';
    }
    $('#supplier_products tbody').prepend('<tr>' +
        '<td style="width: 22%" class="perigrafi">'+field.perigrafi+'<input name="id" value="'+field.id_proion+'" hidden><input name="tmima" value="'+field.tmima+'" hidden></td>' +
        '<td style="width: 15%" class="siskevasia">'+field.temaxia+'</td>' +
        '<td style="width: 15%;color: '+color+'" class="apothema">'+(parseFloat(field.stock)).toFixed(3)+'</td>' +
        '<td style="width: 15%"><input type="number" name="buy_price" min="0" step="0.01" value="'+(parseFloat(field.timi_agoras)).toFixed(2)+'" required></td>' +
        '<td style="width: 15%"><input type="number" name="quantity" value="'+(parseFloat(field.stock_recommend)).toFixed(3)+'" required min="0.1" step="0.1"></td>' +
        '<td style="width: 6%" class="updateCell"><button type="button" name="updateSupplierProduct" data-toggle="tooltip" data-placement="top" data-trigger="manual" data-title="Ενημερώθηκε επιτυχώς" class="far fa-save submitbtn" style="display: none"></button></td>' +
        '<td style="width: 6%"><button type="button" name="addToWare" data-toggle="tooltip" data-placement="top" data-trigger="manual" data-title="Προστέθηκε στο δελτίο" class="fas fa-cart-plus cartbtn"></button></td>' +
        '<td style="width: 6%"><button type="button" name="deleteSupplierProduct" class="far fa-trash-alt resetbtn"></button></td>' +
        '</tr>');
}

function getSupplierParastatikaBetweenDates(fromDate,toDate) {
    unbindSupplierListeners();
    $('#supplier_parastatika').empty();
    var data={fromdate:fromDate,todate:toDate,supplier:parseInt(supplier)};
    var result=getSupplierParastatika(data);
    result.then(function (json) {
        console.log(json);
        var data=JSON.parse(json);
        if (data.response){
            if (data.parastatika[0].pistosi===null){
                data.parastatika[0].pistosi=0;
            }
            var pistosi=parseFloat(data.parastatika[0].pistosi),
                sinolo=parseFloat(data.parastatika[0].pistosi),
                pliroteo=0,
                pliroteo_tim,
                pattern= /ΑΠΕ/;
            $('#supplier_parastatika').append('<table class="table table-hover"><thead><tr>' +
                '<th style="width: 22%;">Ημ/νια</th>' +
                '<th style="width: 23%;">Παραστατικό</th>' +
                '<th style="width: 13%;">Αξία</th>' +
                '<th style="width: 13%;">Πληρωτέο</th>' +
                '<th style="width: 13%;">Πίστωση</th>' +
                '<th style="width: 8%;"></th>' +
                '<th style="width: 8%;"></th>' +
                '</tr></thead><tbody>' +
                '<tr>' +
                '<td style="width: 22%;"></td>' +
                '<td style="width: 23%;">Από μεταφορά</td>' +
                '<td style="width: 13%">'+(pistosi).toFixed(2)+'</td>' +
                '<td style="width: 13%">'+(parseFloat(0)).toFixed(2)+'</td>' +
                '<td style="width: 13%;">'+(pistosi).toFixed(2)+'</td>' +
                '<td style="width: 8%;"></td>' +
                '<td style="width: 8%;"></td>' +
                '</tr></tbody><tfoot></tfoot></table>');
            data.parastatika.slice(1).forEach(function (row) {
                if (pattern.test(row.arithmos)){//apodeiksi eispraksis
                    row.aksia=0;
                }
                pistosi+=parseFloat(row.pistosi);
                sinolo+=parseFloat(row.aksia);
                pliroteo_tim=parseFloat(row.aksia)-parseFloat(row.pistosi);
                pliroteo+=pliroteo_tim;
                $('#supplier_parastatika tbody')
                    .first()
                    .append('<tr> ' +
                        '<td style="width: 22%;">'+row.imerominia+'</td>' +
                        '<td style="width: 23%;">'+row.arithmos+'<input name="parastatiko_id" value="'+row.timologio+'" hidden></td>' +
                        '<td style="width: 13%;">'+(parseFloat(row.aksia)).toFixed(2)+'</td>' +
                        '<td style="width: 13%;">'+pliroteo_tim.toFixed(2)+'</td>' +
                        '<td class="pistosi" style="width: 13%;">'+(parseFloat(row.pistosi)).toFixed(2)+'</td>' +
                        '<td class="deltio_info" style="width: 8%;"></td>' +
                        '<td class="deltio_pay" style="width: 8%;"></td>' +
                        '</tr>');
                if (parseFloat(row.upoloipo)===0){//ean exei eksoflithei to timologio
                    $('#supplier_parastatika tr')
                        .last()
                        .find('.deltio_pay')
                        .append('<i class="fas fa-money-bill-wave submitbtn"></i>');
                }
                if ((parseFloat(row.aksia)===0 && parseFloat(row.pistosi)===0) || parseFloat(row.aksia)!==0){//ean den einai apodeiksi eispraksis
                    $('#supplier_parastatika tr')
                        .last()
                        .find('.deltio_info')
                        .append('<button type="button" class="fas fa-info-circle cartbtn" aria-expanded="false"></button>');
                    $('#supplier_parastatika tbody')
                        .first()
                        .append('<tr>' +
                            '<td style="width: 100%"><div class="collapse" id="deltioCollapse'+row.timologio+'"><table class="table table-condensed">' +
                            '<thead><tr>' +
                            '<th style="width: 25%">Προϊόν</th>' +
                            '<th style="width: 15%">Ποσότητα</th>' +
                            '<th style="width: 15%">Τιμή</th>' +
                            '<th style="width: 15%">ΦΠΑ</th>' +
                            '<th style="width: 15%">Έκπτωση</th>' +
                            '<th style="width: 15%">Σύνολο</th></tr></thead>' +
                            '<tbody></tbody>' +
                            '<tfoot>' +
                            '<tr>' +
                            '<th style="width: 70%"></th>' +
                            '<th style="width: 15%">Υπόλοιπο:</th>' +
                            '<th style="width: 15%">'+parseFloat(row.upoloipo).toFixed(2)+'</th></tr></tfoot></table></div></td></tr>');
                }
            });
            $('#supplier_parastatika tfoot')
                .last()
                .append('<tr>' +
                    '<th style="width: 22%;">Σύνολο</th>' +
                    '<th style="width: 23%;">'+data.parastatika.length+' παραστατικά</th>' +
                    '<th style="width: 13%;">'+sinolo.toFixed(2)+'</th>' +
                    '<th style="width: 13%;">'+pliroteo.toFixed(2)+'</th>' +
                    '<th style="width: 13%;">'+pistosi.toFixed(2)+'</th>' +
                    '<th style="width: 8%;"></th>' +
                    '<th style="width: 8%;"></th>' +
                    '</tr>');
            if (parseFloat(pistosi)>0){
                $('#supplier_parastatika tfoot')
                    .last()
                    .append('<tr class="eksoflisi">' +
                        '<th style="width: 22%">Εξόφληση</th>' +
                        '<th style="width: 36%"><input name="no_eispraksis" type="text" placeholder="Αριθμός Απόδειξης"></th>' +
                        '<th style="width: 26%"><input name="eksoflisi" type="number" placeholder="Ποσό" min="0.01" step="0.01" max="'+pistosi+'" required></th>' +
                        '<th style="width: 8%"><button type="button" class="fas fa-check-circle submitbtn" data-toggle="tooltip" data-placement="top" data-trigger="manual" data-title="Πληρώθηκε επιτυχώς" onclick="paySupplierYpoloipo()"</button></th>' +
                        '<th style="width: 8%"></th>' +
                        '</tr>');
            }
        }else {
            bootboxMessage(data.error,1);
        }
        supplierListeners();
    });
    return true;
}

function paySupplierYpoloipo() {
    if ($('#supplier_parastatika input[name="eksoflisi"]')[0].checkValidity()){
        var amount=parseFloat($('#supplier_parastatika input[name="eksoflisi"]').val());
        var arithmos=$('#supplier_parastatika input[name="no_eispraksis"]').val();
        var data={supplier:supplier,poso:amount,aa:arithmos};
        var result=eksoflisiYpoloipo(data);
        result.then(function (json) {
            console.log(json);
            var data=JSON.parse(json);
            if (data.response){
                $('#supplier_tab .eksoflisi input').val();
                $('#supplier_parastatika .eksoflisi button').tooltip('show');
                setTimeout(function () {
                    $('#supplier_parastatika .eksoflisi button').tooltip('hide');
                    $('#supplier_tab .etisia').trigger('click');
                },1500);
            } else {
                bootboxMessage(data.error,1);
            }

        })
    } else {
        $('#supplier_parastatika input[name="eksoflisi"]')[0].reportValidity();
    }
}

function unbindSupplierListeners() {
    $('#supplier_register').off();
    $('#addProduct').off();
    $('#groupset2 input').not('#supplierData').off();
    $('#supplier_products tbody tr button').off('click');
    $('#allPersons button').off('click');
    $('#supplier_tab .head_column button').off('click');
}

function supplierListeners() {
    if (parseInt(supplier)===0){//kainourios promitheutis
        $('#supplier_register input[name="afm"]').prop('readonly',false);
        $('#supplier_register .formbuttons').show();
    }else if (parseInt(supplier)===-1){//promitheutis apo gsis
        $('#supplier_register input[name="afm"]').prop('readonly',true);
        $('#supplier_register .formbuttons').show();
    }else {//yparxon promitheutis stin vasi
        if ($('#supplier_register input[name="afm"]').val()===''){
            $('#supplier_register input[name="afm"]').prop('readonly',false);
        } else{
            $('#supplier_register input[name="afm"]').prop('readonly',true);
        }
        $('#supplier_register .formbuttons').hide();
        $('#supplier_register  input').on('change',function () {
            var name=$(this).attr('name');
            handleSupplier(name);
        });
    }

    $('#supplier_register').submit(function (e) {
        e.preventDefault();
        var form=$('#supplier_register').serializeArray();
        var data={};
        form.forEach(function (field) {
            data[field.name]=field.value;
        });
        var result=insertSupplier(data);
        result.then(function (json) {
            console.log(json);
            var data=JSON.parse(json);
            if (data.response){
                bootboxMessage('Επιτυχής καταχώρηση',2);
                start();
            } else {
                bootboxMessage(data.error,1);
            }
        });
    });

    $('#addProduct').submit(function (e) {
        e.preventDefault();
        if (parseInt(supplier)===0){
            bootboxMessage('Πρέπει να προσθέσετε προμηθευτή πρώτα',3);
        } else{
            if (!$('#addProduct input[name="productId"]').val()){
                bootboxMessage('Δεν έχετε επιλέξει κάποιο προίόν',3);
            } else {
                var form=$('#addProduct').serializeArray();
                var data= {};
                data['supplier']=parseInt(supplier);
                form.forEach(function (field) {
                    data[field.name]=field.value;
                });
                var action='addSupplierProduct';
                var result=insertSupplierProduct(data,action);
                result.then(function (json) {
                    console.log(json);
                    var insert=JSON.parse(json);
                    if (insert.response){
                        unbindSupplierListeners();
                        if ($('.info_msg')){
                            $('.info_msg').remove();
                        }
                        var row={id_proion:data.productId, perigrafi: data.productName,temaxia: data.productSiskevasia,tmima:data.productTmima
                            ,timi_agoras: data.productBuyPrice, stock: data.productStock, stock_recommend:data.productStockRecommend, stock_alert:data.productStockAlert};
                        appendSupplierProduct(row);
                        supplierListeners();
                        $('.supplierBarcodeBtn').focus();
                    }else{
                        bootboxMessage(insert.error,1);
                    }
                    $('#addProduct')[0].reset();
                });
            }
        }
    });

    $('#supplier_tab .etisia').click(function () {
        if (parseInt(supplier)!==0){
            var d = new Date();
            var year=d.getFullYear();
            var fromDate= year+'-01-01',
                toDate = year+'-12-01';
            getSupplierParastatikaBetweenDates(fromDate,toDate);
        } else {
            bootboxMessage('Πρέπει να προσθέσετε προμηθευτή πρώτα',3);
        }
    });

    $('#supplier_tab input[name="fromDate"]').on('change',function () {
        var fromDate= $('#supplier_tab input[name="fromDate"]').val(),
            toDate = $('#supplier_tab input[name="toDate"]').val();
        var datefrom = new Date(fromDate);
        var month = (datefrom.getMonth()+1);
        var tomorrow = (datefrom.getDate()+1);
        var nextday = datefrom.getFullYear() + '-' +
            (month<10 ? '0' : '') + month + '-' +
            (tomorrow<10 ? '0' : '') + tomorrow;
        $('#supplier_tab input[name="toDate"]').prop('min',function () {
            return nextday;
        });
        if (toDate!==''){
            if (parseInt(supplier)!==0){
                getSupplierParastatikaBetweenDates(fromDate,toDate);
            } else {
                bootboxMessage('Πρέπει να προσθέσετε προμηθευτή πρώτα',3);
            }

        }
        return true;
    });

    $('#supplier_tab input[name="toDate"]').on('change',function () {
        var fromDate= $('#supplier_tab input[name="fromDate"]').val(),
            toDate = $('#supplier_tab input[name="toDate"]').val();
        var dateto = new Date(toDate);
        var month = (dateto.getMonth()+1);
        var yesterday = (dateto.getDate()-1);
        var previousday = dateto.getFullYear() + '-' +
            (month<10 ? '0' : '') + month + '-' +
            (yesterday<10 ? '0' : '') + yesterday;
        $('#supplier_tab input[name="fromDate"]').prop('max',function () {
            return previousday;
        });
        if (fromDate!==''){
            if (parseInt(supplier)!==0){
                getSupplierParastatikaBetweenDates(fromDate,toDate);
            } else {
                bootboxMessage('Πρέπει να προσθέσετε προμηθευτή πρώτα',3);
            }
        }
        return true;
    });

    $('#supplier_products tbody tr input[name="buy_price"]').on('change',function () {
        var row=$(this).closest('tr');
        $(row).find('.submitbtn').show();
    });

    $('#supplier_products tbody tr button').on('click',function () {
        var action=$(this).attr('name');
        var row=$(this).closest('tr');
        var pedio=row.find('input[name="buy_price"]');
        var timi_agoras=$(pedio).val();
        var id=$(row).find('input[name="id"]').val();
        if (action==="addToWare"){
            var name=$(row).find('.perigrafi').text();
            var siskevasia=$(row).find('.siskevasia').text();
            var tmima=$(row).find('input[name="tmima"]').val();
            var posotita=$(row).find('input[name="quantity"]');
            var stock=$(row).find('.apothema').text();
            if ($(posotita)[0].checkValidity()){
                posotita=posotita.val();
                var data=
                    {
                        name:name,
                        id:id,
                        siskevasia:siskevasia,
                        tmima:tmima,
                        posotita:posotita,
                        timi_agoras:timi_agoras,
                        apothema:stock
                    };
                $('.ajax-loader').show(0).delay(100).hide(0);
                appendDeltioProduct(data);
                $(row).find('button[name="addToWare"]').tooltip('show');
                setTimeout(function () {
                    $(row).find('button[name="addToWare"]').tooltip('hide');
                },1500);
            } else {
                $(posotita)[0].reportValidity();
            }

        } else{
            if ((action==='deleteSupplierProduct')||((action==='updateSupplierProduct')&&($(pedio)[0].checkValidity()))){
                var data={supplier:parseInt(supplier),proion:parseInt(id),timi_agoras:parseFloat(timi_agoras)};
                var result=handleSupplierProduct(data,action);
                result.then(function (json) {
                    console.log(json);
                    var data=JSON.parse(json);
                    if (data.response){
                        if (action==='deleteSupplierProduct'){
                            $(row).remove();
                        }else{
                            $(row).find('button[name="updateSupplierProduct"]').tooltip('show');
                            setTimeout(function () {
                                $(row).find('button[name="updateSupplierProduct"]').tooltip('hide');
                                $(row).find('.submitbtn').hide();
                            },1500);
                        }
                        $('.supplierBarcodeBtn').focus();
                    } else{
                        bootboxMessage(data.error,1);
                    }

                })
            }else{
                $(pedio)[0].reportValidity();
            }
        }
    });

    $('a[href="#supplier_warehouse"]').on('shown.bs.tab',function () {
        $('.supplierBarcodeBtn').focus();
        $('#addProduct')[0].reset();
    });

    $('#supplier_parastatika .deltio_info button').click(function () {
        var row=$(this).closest('tr');
        var id=$(row).find('input[name="parastatiko_id"]').val();
        var result=getSupplierParastatikoInfo(id);
        result.then(function (json) {
            console.log(json);
            var data=JSON.parse(json);
            if (data.response){
                $('#deltioCollapse'+id+' tbody').empty();
                data.details.forEach(function (proion) {
                    $('#deltioCollapse'+id+' tbody').append('<tr>' +
                        '<td style="width: 25%">'+proion.perigrafi+'</td>' +
                        '<td style="width: 15%">'+parseFloat(proion.quantity).toFixed(3)+'</td>' +
                        '<td style="width: 15%">'+parseFloat(proion.timi_agoras).toFixed(2)+'</td>' +
                        '<td style="width: 15%">'+(parseFloat(proion.fpa)*100)+' %</td>' +
                        '<td style="width: 15%">'+parseFloat(proion.discount).toFixed(2)+'</td>' +
                        '<td style="width: 15%">'+(parseFloat(proion.final_sum)).toFixed(2)+'</td>' +
                        '</tr>');
                });
                $('#deltioCollapse'+id).collapse('toggle');
            }
        });
    });

    $('#groupset2 .listOfPersons tbody button').click(function () {
        var td=$(this).closest('td');
        var id=$(td).find('input').val();
        searchSupplier(id,1);
        $('#supplierData').val('');
    });

}