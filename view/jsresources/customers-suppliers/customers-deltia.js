function clearCustomerDeltio() {
    $('#addToCustomerDeltio input').val('');
    $('#customer_deltio_form tbody').empty();
    $('#customer_deltio_form tfoot input').val('0.00');
    $('#customer_deltio_form textarea[name="matching_parastatiko"]').text('');
    $('#customer_deltio_type')
        .val(1)
        .trigger('change')
        .prop('disabled',false);
    $('#customer_parastatiko_type').prop('disabled',false);
    $('#customerTicket .update_stock').prop({'checked':true});
}

function customerDeltioModal() {
    if (parseInt(customer)===0){
        bootboxMessage('Πρέπει να επιλέξετε πελάτη πρώτα',3);
    } else {
        $('#customerTicket').modal('show');
    }
}

function changeCustomerDeltioInputAttrs() {
    if ($('#customer_parastatiko_type').val()!==''){//den einai kamia epilogi
        var id=parseInt($('#customer_parastatiko_type').val());
        var parastatiko=parseInt(parastatikaInfo[id]['update_flag']);
        if (parastatiko===0){//enimerosi apothematos
            $('#customer_deltio_submit input[name="ekptosi_proion"]').prop('readonly',true);
        } else {
            $('#customer_deltio_submit input[name="ekptosi_proion"]').prop('readonly',false);
        }
    }
}

function appendCustomerDeltioProduct(data) {
    var result=getFpa(parseInt(data.tmima));
    result.then(function (json) {
        console.log(json);
        var fpa=JSON.parse(json);
        if (fpa.response){
            unbindCustomerDeltioListeners();
            var sinolo=(parseInt(data.posotita)*parseFloat(data.timi)).toFixed(2);
            var teliko_sinolo=sinolo;
            $('#customerTicket tbody').prepend('<tr>' +
                '<td>'+data.name+'<input name="id" value="'+data.id+'" hidden>' +
                '<input name="desc" value="'+data.name+'" hidden><input name="tmima" value="'+parseInt(data.tmima)+'" hidden></td>' +
                '<td>'+parseInt(data.siskevasia)+'<input name="package" value="'+parseInt(data.siskevasia)+'" hidden></td>' +
                '<td>'+parseFloat(data.apothema)+'<input name="stock" value="'+parseFloat(data.apothema)+'" hidden></td>' +
                '<td><input name="temaxia" type="number" required min="0.1" step="0.01" value="'+(parseFloat(data.posotita)).toFixed(3)+'">' +
                '<input name="rest_quantity" value="'+(parseFloat(data.posotita)).toFixed(3)+'" hidden></td>' +
                '<td>'+parseFloat(fpa.proion.price)*100+' %</td>' +
                '<td><input name="price" type="number" required min="0" step="0.01" value="'+(parseFloat(data.timi)).toFixed(2)+'"></td>' +
                '<td><input name="sinolo_proion" type="number" disabled value="'+sinolo+'"><input name="sinolo_me_ekptosi" disabled hidden value="'+teliko_sinolo+'"></td>' +
                '<td style="display: inline-flex"><select style="width: 40%" name="choose_discount"><option selected value="0">%</option><option value="1">€</option> </select>' +
                '<input style="width: 60%" name="ekptosi_proion" type="number" value="0" min="0" required  max="99" step="1"></td>' +
                '<td><input name="mothercode" value="'+data.mothercode+'" hidden><input name="barcode" value="'+data.barcode+'" hidden>' +
                '<input name="id_om" value="'+data.id_om+'" hidden><input name="zigos" value="'+data.zigos+'" hidden>' +
                '<input name="operator" value="'+user.id+'" hidden>' +
                '<input name="tameio" value="1" hidden><input name="sunolo" type="number" readonly value="'+teliko_sinolo+'"></td>' +
                '<td><button type="button" class="far fa-trash-alt resetbtn"></button></td>' +
                '</tr>');
            $('#customerTicket tbody tr').first().find('select[name="fpa_proion"]').val(parseFloat(fpa.proion.price));
            if (parseFloat(sinolo)!==0){
                updateCustomerDeltioSums(sinolo,0,teliko_sinolo);
            }
            changeCustomerDeltioInputAttrs();
            bindCustomerDeltioListeners();
        }else {
            bootboxMessage('Υπήρξε κάποιο πρόβλημα με το ΦΠΑ του προϊόντος',1);
        }
    });
}

function updateCustomerDeltioSums(sinolo,ekptosi,teliko) {
    var sinolo_deltiou=parseFloat($('#customerTicket input[name="sinolo_deltiou"]').val());
    var ekptosi_deltiou=parseFloat($('#customerTicket input[name="ekptosi_deltiou"]').val());
    var teliko_sinolo_deltiou=parseFloat($('#customerTicket input[name="teliko_sinolo_deltiou"]').val());
    sinolo_deltiou+=parseFloat(sinolo);
    sinolo_deltiou=sinolo_deltiou.toFixed(2);
    ekptosi_deltiou+=parseFloat(ekptosi);
    ekptosi_deltiou=ekptosi_deltiou.toFixed(2);
    teliko_sinolo_deltiou+=parseFloat(teliko);
    teliko_sinolo_deltiou=teliko_sinolo_deltiou.toFixed(2);
    $('#customerTicket input[name="sinolo_deltiou"]').val(sinolo_deltiou);
    $('#customerTicket input[name="ekptosi_deltiou"]').val(ekptosi_deltiou);
    $('#customerTicket input[name="teliko_sinolo_deltiou"]').val(teliko_sinolo_deltiou);
    $('#customerTicket input[name="pliroteo_deltiou"]').val(teliko_sinolo_deltiou).prop({
        "max":teliko_sinolo_deltiou
    });
    return false;
}

function unbindCustomerDeltioListeners() {
    $('#customer_deltio_submit').off();
    $('#customer_deltio_form select').off();
    $('#customer_deltio_form button').off('click');
    $('#customer_deltio_form textarea').off();
    $('#customer_deltio_form select').off();
    $('#customerParastatikaInventory tbody button').off('click');
}

function bindCustomerDeltioListeners() {
    $('#customer_deltio_type').change(function () {
        var d = new Date();
        var today = d.getFullYear() + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" + ("0"+d.getDate()).slice(-2);
        var deltio=parseInt($(this).val());
        $('#customer_deltio_form .parastatiko_extra').empty();
        $('#customer_deltio_submit .eispraxthen').hide();
        $('input[name="apodeiksi_eispraksis"]').val((0).toFixed(2));
        $('#customerTicket .update_stock').prop({'checked':true});
        unbindCustomerDeltioListeners();
        if (deltio===1) {//forma polisis
            $('#customer_deltio_form .parastatiko_extra').append('<select id="customer_parastatiko_type" required>' +
                '<option value="">-επιλέξτε παραστατικό-</option>' +
                '</select>');
            $.each(parastatikaInfo,function (index,value) {
                if (parseInt(value.flow_flag)===1){
                    $('#customer_parastatiko_type').append('<option value="'+index+'">'+value.name+'</option>');
                }
            });
            $('#customer_deltio_submit .pliromi_paralavis').show();
        }else if (deltio===2) {//forma epistrofis
            $('#customer_deltio_form .parastatiko_extra').append('<select id="customer_parastatiko_type" required>' +
                '<option value="">-επιλέξτε παραστατικό-</option>' +
                '</select>' +
                '<input name="parastatiko" placeholder="Αριθμός παραστατικού">' +
                '<div class="parastatiko_date">' +
                '<input type="date" value="'+today+'"></div>');
            $.each(parastatikaInfo,function (index,value) {
                if (parseInt(value.flow_flag)===2){
                    $('#customer_parastatiko_type').append('<option value="'+index+'">'+value.name+'</option>');
                }
            });
            $('#customer_deltio_submit .pliromi_paralavis').hide();
        }else {
            //todo akirotiko
        }
        if ($.trim($('#deltio_submit tbody').html())!==''){
            changeCustomerDeltioInputAttrs();
        }
        bindCustomerDeltioListeners();
    });

    $('#customer_parastatiko_type').change(function () {
        var id=parseInt($(this).val());
        var parastatiko=parseInt(parastatikaInfo[id]['update_flag']);
        $('#customerTicket .update_stock').prop('disabled',false);
        $('#customer_deltio_form .search_parastatiko').show();
        $('#customer_deltio_submit .eispraxthen').hide();
        $('#customerTicket .update_stock').prop({'checked':true});
        $('input[name="apodeiksi_eispraksis"]').val((0).toFixed(2));
        if (parastatiko===0){//enimerosi apothematos
            $('#customer_deltio_submit .pliromi_paralavis').hide();
            $('#customerTicket .update_stock').prop('checked',true);
        } else if (parastatiko===1){//enimerosi aksias
            if (id===4){//apodeiksi eispraksis
                $('#customer_deltio_submit .eispraxthen').show();
                $('#customer_deltio_submit .pliromi_paralavis').hide();
            }else {
                $('#customer_deltio_submit .pliromi_paralavis').show();
            }
            $('#customerTicket .update_stock').prop({'checked':false,'disabled':true});
        }else {//enimerosi apothematos kai aksias
            $('#customer_deltio_form .search_parastatiko').hide();
            $('#customer_deltio_submit .pliromi_paralavis').show();
        }
        if ($.trim($('#customer_deltio_submit tbody').html())!==''){
            changeCustomerDeltioInputAttrs();

        }
    });

    $('#addToCustomerDeltio .submitbtn').click(function () {
        if (!$('#addToCustomerDeltio input[name="productId"]').val()) {
            bootboxMessage('Δεν έχετε επιλέξει κάποιο προίόν', 3);
        } else {
            if ($('#addToCustomerDeltio input[name="productQuantity"]')[0].checkValidity()) {
                var data = {
                    "name": $('#addToCustomerDeltio input[name="productName"]').val(),
                    "siskevasia": $('#addToCustomerDeltio input[name="productSiskevasia"]').val(),
                    "id": $('#addToCustomerDeltio input[name="productId"]').val(),
                    "posotita": $('#addToCustomerDeltio input[name="productQuantity"]').val(),
                    "timi": $('#addToCustomerDeltio input[name="productPrice"]').val(),
                    "tmima": $('#addToCustomerDeltio input[name="productTmima"]').val(),
                    "apothema": $('#addToCustomerDeltio input[name="productStock"]').val(),
                    "mothercode": $('#addToCustomerDeltio input[name="productMcode"]').val(),
                    "barcode": $('#addToCustomerDeltio input[name="productBcode"]').val(),
                    "id_om": $('#addToCustomerDeltio input[name="productIdOm"]').val(),
                    "zigos": $('#addToCustomerDeltio input[name="productZigos"]').val()
                };
                appendCustomerDeltioProduct(data);
                $('#addToCustomerDeltio input').val('');
                $('#addToCustomerDeltio .customerBarcodeBtn').focus();
            } else {
                $('#addToCustomerDeltio input[name="productQuantity"]')[0].reportValidity();
            }
        }
    });

    $('#customer_deltio_form tbody select[name="choose_discount"]').change(function () {
        var row = $(this).closest('tr');
        var sinolo_proion = $(row).find('input[name="sinolo_proion"]');
        var ekptosi_proion = $(row).find('input[name="ekptosi_proion"]');
        ekptosi_proion.val(0).trigger('change');
        sinolo_proion .css({
            "text-decoration":"none",
        });
        $(row).find('input[name="sinolo_me_ekptosi"]')
            .val(sinolo_proion.val())
            .hide();
        if (parseInt($(this).val())===1){//aksiaki
            ekptosi_proion.prop({
                'step':0.01,
                'max':sinolo_proion.val()
            });
        } else {//epi tis %
            ekptosi_proion.prop({
                'step':1,
                'max':99
            });
        }
    });

    $('#customer_deltio_form tbody input').change(function () {
        if ($(this).val()===''){
            $(this).val(0);
        }
        var neo_teliko_sinolo,neo_sinolo,old_ekptosi,nea_ekptosi;
        var diafora_sinolou=0;
        var diafora_ekptosis=0;
        var diafora_telikou=0;
        var row = $(this).closest('tr');
        var name=$(this).attr('name');
        var sinolo_proion = $(row).find('input[name="sinolo_proion"]');
        var ekptosi_proion = $(row).find('input[name="ekptosi_proion"]');
        var teliko_sinolo_proion = $(row).find('input[name="sunolo"]');
        var sinolo_me_ekptosi = $(row).find('input[name="sinolo_me_ekptosi"]');
        var type_of_discount = $(row).find('select[name="choose_discount"]').val();
        type_of_discount=parseInt(type_of_discount);
        if (name==="price" || name==="temaxia"){
            var old_sinolo = parseFloat(sinolo_proion.val());
            var posotita = $(row).find('input[name="temaxia"]').val();
            var timi = $(row).find('input[name="price"]').val();
            neo_sinolo = parseFloat(posotita) * parseFloat(timi);
            diafora_ekptosis=-(parseFloat(sinolo_proion.val())-parseFloat(sinolo_me_ekptosi.val()));
            ekptosi_proion.val(0);
            neo_teliko_sinolo=neo_sinolo;
            diafora_telikou=neo_teliko_sinolo-(teliko_sinolo_proion.val());
            teliko_sinolo_proion.val(neo_teliko_sinolo.toFixed(2));
            if (type_of_discount===0){//aksiaki
                ekptosi_proion.prop({
                    'max':neo_sinolo
                });
            }
            neo_sinolo = neo_sinolo.toFixed(2);
            sinolo_proion
                .css({
                    "text-decoration":"none",
                }).val(neo_sinolo);
            sinolo_me_ekptosi
                .val(neo_sinolo)
                .hide();
            neo_sinolo = parseFloat(neo_sinolo);
            if (old_sinolo !== neo_sinolo) {
                diafora_sinolou = neo_sinolo - old_sinolo;
            }
        }else{
            if (ekptosi_proion[0].checkValidity()) {
                old_ekptosi=parseFloat(sinolo_proion.val())-parseFloat(sinolo_me_ekptosi.val());
                if (type_of_discount===1){//aksiaki
                    nea_ekptosi=parseFloat(ekptosi_proion.val());
                } else {
                    nea_ekptosi=(parseInt(ekptosi_proion.val())/100)*parseFloat(sinolo_proion.val());
                }
                sinolo_me_ekptosi.val((parseFloat(sinolo_proion.val())-nea_ekptosi).toFixed(2));
                neo_teliko_sinolo=parseFloat(sinolo_me_ekptosi.val());
                diafora_telikou=neo_teliko_sinolo-parseFloat(teliko_sinolo_proion.val());
                old_ekptosi=old_ekptosi.toFixed(2);
                diafora_ekptosis=nea_ekptosi-parseFloat(old_ekptosi);
                teliko_sinolo_proion.val(neo_teliko_sinolo.toFixed(2));
                diafora_ekptosis=diafora_ekptosis.toFixed(2);
                diafora_telikou=diafora_telikou.toFixed(2);
                if (parseFloat(ekptosi_proion.val())>0){
                    sinolo_me_ekptosi.show();
                    sinolo_proion.css({
                        "text-decoration":"line-through",
                        "text-decoration-color":"red"
                    });
                } else {
                    sinolo_me_ekptosi.hide();
                    sinolo_proion.css({
                        "text-decoration":"none",
                    });
                }
            }else {
                ekptosi_proion[0].reportValidity();
                return false;
            }
        }
        updateCustomerDeltioSums(diafora_sinolou,diafora_ekptosis,diafora_telikou);
    });

    $('#customer_deltio_form tbody button').click(function () {
        var row = $(this).closest('tr');
        var sinolo_proion = $(row).find('input[name="sinolo_proion"]').val();
        var ekptosi_proion = $(row).find('input[name="ekptosi_proion"]').val();
        var teliko_sinolo_proion = $(row).find('input[name="sunolo"]').val();
        sinolo_proion = -parseFloat(sinolo_proion);
        ekptosi_proion=-parseFloat(ekptosi_proion);
        teliko_sinolo_proion=-parseFloat(teliko_sinolo_proion);
        updateCustomerDeltioSums(sinolo_proion,ekptosi_proion,teliko_sinolo_proion);
        $(row).remove();
    });

    $('#customer_deltio_form textarea[name="matching_parastatiko"]').click(function () {
        if ($('#customer_parastatiko_type')[0].checkValidity()){
            var par_id=parseInt($('#customer_parastatiko_type').val());
            var flowflag=parastatikaInfo[par_id]['flow_flag'];
            var updateflag=parastatikaInfo[par_id]['update_flag'];
            if (par_id===4){//ean einai i apodeiksi eispraksis
                flowflag=100;
            }
            var data={'customer':customer,'flow_flag':flowflag,'update_flag':updateflag};
            var action='matchCustomerParastatika';
            var result=matchParastatika(data,action);
            result.then(function (json) {
                console.log(json);
                var data=JSON.parse(json);
                if (data.response){
                    $('#customerTicket').modal('hide');
                    var deltioType,id;
                    $('#customerParastatikaInventory .table-responsive').empty();
                    $('#customerParastatikaInventory .table-responsive').append('<table class="table table-hover">' +
                        '<thead>' +
                        '<tr>' +
                        '<th style="width: 2%">#</th>' +
                        '<th style="width: 18%">Πελάτης</th>' +
                        '<th style="width: 18%">Δελτίο</th>' +
                        '<th style="width: 18%">Παραστατικό</th>' +
                        '<th style="width: 18%">Α/α</th>' +
                        '<th style="width: 18%">Ημερομηνία Καταχώρησης</th>' +
                        '<th style="width: 8%"></th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody></tbody></table>');
                    data.body.forEach(function (row) {
                        id=parseInt(row.type);
                        if (parseInt(parastatikaInfo[id]['flow_flag'])===1){
                            deltioType='Πώλησης';
                        } else {
                            deltioType='Επιστροφής';
                        }
                        $('#customerParastatikaInventory tbody').append('<tr>' +
                            '<td style="width: 2%"><input name="id" hidden value="'+row.id+'">' +
                            '<input name="par_id" value="'+par_id+'" hidden>' +
                            '<input name="id_customer" hidden value="'+row.customer_id+'"></td>' +
                            '<td style="width: 18%">'+row.name+'</td>' +
                            '<td style="width: 18%">'+deltioType+'</td>' +
                            '<td style="width: 18%">'+row.shortname+'</td>' +
                            '<td style="width: 18%">'+row.aa+'</td>' +
                            '<td style="width: 18%">'+row.date+'</td>' +
                            '<td style="width: 8%"><button name="relativeDeltio" type="button" class="fas fa-clipboard-check submitbtn" data-toggle="tooltip" data-placement="top" data-trigger="manual" data-title="Προστέθηκε στο δελτίο"></button></td>' +
                            '</tr>');
                    });
                    $('#customerParastatikaInventory').modal('show');
                }else {
                    bootboxMessage('Δεν υπάρχουν παραστατικά που να σχετίζονται με αυτό το δελτίο',3);
                }
            });
        } else {
            $('#customer_parastatiko_type')[0].reportValidity();
        }
    });

    $('#customerParastatikaInventory tbody button[name="relativeDeltio"]').click(function () {
        var row=$(this).closest('tr');
        var pelatis=$(row).find('input[name="id_customer"]').val();
        var par_id=$(row).find('input[name="par_id"]').val();
        var deltio_type=parseInt(parastatikaInfo[par_id]['flow_flag']);
        var id=$(row).find('input[name="id"]').val();
        var result=getCustomerParastatikoInfo(id);
        result.then(function (json) {
            console.log(json);
            var data=JSON.parse(json);
            if (data.response){
                $(row).find('button[name="relativeDeltio"]').tooltip('show');
                setTimeout(function () {
                    $(row).find('button[name="relativeDeltio"]').tooltip('hide');
                },1500);
                customer=parseInt(pelatis);
                $('#customer_deltio_type')
                    .val(deltio_type)
                    .prop('disabled',true)
                    .trigger('change');
                $('#customer_parastatiko_type')
                    .val(par_id)
                    .prop('disabled',true)
                    .trigger('change');
                data['parastatiko']['receipt'].forEach(function (row) {
                    var product={
                        'name':row.description,
                        'id':row.id,
                        'siskevasia':row.temaxia,
                        'tmima':row.tmima,
                        'posotita':Math.abs(row.quantity),
                        'timi':row.timi,
                        'apothema':row.stock,
                        'mothercode':row.mothercode,
                        'barcode':row.barcode,
                        'id_om':row.id_om,
                        'zigos':row.zigos
                    };
                    appendCustomerDeltioProduct(product);
                });
                var matching_par=$('#customer_deltio_form textarea[name="matching_parastatiko"]').text();
                if (matching_par===""){
                    matching_par=matching_par+id;
                } else {
                    matching_par=matching_par+'&#13;&#10;'+id;
                }
                $('#customer_deltio_form textarea[name="matching_parastatiko"]').html(matching_par);
            }else {
                bootboxMessage(data.error,2);
            }
        });
    });

    $('#customer_deltio_submit').submit(function (e) {
        e.preventDefault();
        if ($.trim($('#customer_deltio_submit tbody').html())!=='' || parseFloat($('#customer_deltio_form input[name="apodeiksi_eispraksis"]').val())>0){
            var form = $('#customer_deltio_submit').serializeArray();
            var data = {};
            data['receipt'] = {};
            var i = 0;
            var action,pliroteo,pistosi,id,ekptosi;
            var deltio=parseInt($('#customer_deltio_form select').val());
            if ($('#customer_deltio_form textarea[name="matching_parastatiko"]').text()!==''){
                data['matched']=$('#customer_deltio_form textarea[name="matching_parastatiko"]').text();
                data['matched']=data['matched'].split(/\r\n/);
            }
            if (deltio===1) {//deltio polisis
                action = 'submit';
                data['arithmos_parastatikou'] = '';
                data['date']='NULL';
                if ($('#customer_parastatiko_type')[0].checkValidity()){
                    id=parseInt($('#customer_parastatiko_type').val());
                    data['parastatiko_type']=id;
                    if (id===3){//apodeiksi lianikis polisis
                        data['invoice']=0;
                    }else {
                        data['invoice']=1;
                    }
                    if (parseInt(parastatikaInfo[id]['update_flag'])!==0){//ean den einai deltio apostolis
                        pliroteo = $('#customer_deltio_form input[name="pliroteo_deltiou"]').val();
                        if (pliroteo===''){
                            pliroteo=0;
                        }
                        pliroteo=parseFloat(pliroteo);
                        pistosi = (parseFloat($('#customer_deltio_form input[name="teliko_sinolo_deltiou"]').val()))-pliroteo;
                        ekptosi = parseFloat($('#customer_deltio_form input[name="ekptosi_deltiou"]').val());
                    }else {//deltio apostolis
                        pliroteo=0;
                        pistosi=0;
                        ekptosi=0;
                    }
                    data['payments'] = [
                        {'amount': ekptosi, 'id': 0, 'description':''},
                        {'amount': pistosi, 'id': 1, 'description':''},
                        {'amount': pliroteo, 'id': 2, 'description':''}
                    ];
                    if ($('#customerTicket .update_stock').is(':checked')) {
                        data['update_stock'] = 1;
                    } else {
                        data['update_stock'] = 0;
                    }
                }else {
                    $('#customer_parastatiko_type')[0].reportValidity();
                    return false;
                }
                form.forEach(function (field) {
                    if (field.name === "pliroteo_deltiou" || field.name === "apodeiksi_eispraksis") {
                        return true;
                    }
                    if (field.name === "id") {
                        data['receipt'][i] = {};
                    }
                    data['receipt'][i][field.name] = field.value;
                    if (field.name === "sunolo") {
                        i++;
                    }
                });
            }else if (deltio===2) {//deltio epistrofis
                action = 'submit';
                data['arithmos_parastatikou'] = $('#customer_deltio_form input[name="parastatiko"]').val();
                data['date']=$('#customer_deltio_form input[type="date"]').val();
                data['invoice']=1;
                if ($('#customer_parastatiko_type')[0].checkValidity()){
                    id=parseInt($('#customer_parastatiko_type').val());
                    data['parastatiko_type']=id;
                    if (parseInt(parastatikaInfo[id]['update_flag'])!==0){//ean den einai deltio apostolis
                        if (id===4){//apodeiksi eispraksis
                            action='payCustomerYpoloipo';
                            pliroteo=0;
                            pistosi= parseFloat($('#customer_deltio_form input[name="apodeiksi_eispraksis"]').val())*(-1);
                            if ($.trim($('#customer_deltio_submit tbody').html())===''){//ean einai keno to deltio
                                data['receipt'][0]={};
                                data['receipt'][0]['operator']=parseInt(user.id);
                                data['receipt'][0]['tameio']=1;
                            }
                        }else {
                            pliroteo = parseFloat($('#customer_deltio_form input[name="pliroteo_deltiou"]').val());
                            if (pliroteo===''){
                                pliroteo=0;
                            }
                            pistosi = (parseFloat($('#customer_deltio_form input[name="teliko_sinolo_deltiou"]').val()))-pliroteo;
                            if (pistosi!==0){
                                pistosi=pistosi*(-1);
                            }
                            if (pliroteo!==0){
                                pliroteo=pliroteo*(-1);
                            }
                        }
                    }else {//deltio apostolis
                        pliroteo=0;
                        pistosi=0;
                    }
                    data['parastatiko_type']=id;
                    data['payments'] = [
                        {'amount': 0, 'id': 0, 'description':''},
                        {'amount': pistosi, 'id': 1, 'description':''},
                        {'amount': pliroteo, 'id': 2, 'description':''}
                    ];
                    if ($('#customerTicket .update_stock').is(':checked')) {
                        data['update_stock'] = 2;
                    } else {
                        data['update_stock'] = 0;
                    }
                }else {
                    $('#customer_parastatiko_type')[0].reportValidity();
                    return false;
                }
                form.forEach(function (field) {
                    if (field.name === "pliroteo_deltiou" || field.name === "apodeiksi_eispraksis") {
                        return true;
                    }
                    if (field.name === "id") {
                        data['receipt'][i] = {};
                    }
                    data['receipt'][i][field.name] = field.value;
                    if (field.name === "sunolo") {
                        i++;
                    }
                });
            }else {//akirotiko deltio
                //todo akirotiko
            }
            if ($('#customerTicket .print_invo').is(':not(:checked)')) {
                data['invoice'] = 2;
            }
            var pinakas = {'customer': customer, 'receipt': data['receipt'], 'invoice':data['invoice'], 'aa': data['arithmos_parastatikou'], 'date':data['date'],
                'payments':data['payments'], 'update_stock':data['update_stock'], 'par_type':data['parastatiko_type'],'matched':data['matched']};
            var result = insertParastatiko(pinakas, action);
            result.then(function (json) {
                console.log(json);
                var data = JSON.parse(json);
                if (data.response) {
                    $('#customerTicket').modal('hide');
                    setTimeout(function () {
                        if (typeof data.msg!=='undefined'){
                            bootboxMessage(data.msg,3);
                        } else {
                            bootboxMessage('Το παραστατικό κατοχυρώθηκε επιτυχώς!', 2);
                        }
                    },500);
                    if(typeof data.url !== 'undefined'){
                        try{
                            Android.printInvoice(data.url);
                        }catch (e){
                            window.open(data.url);
                        }
                    }
                } else {
                    bootboxMessage(data.error, 1);
                }
            });
        } else {
            bootboxMessage('Το δελτίο δεν μπορεί να είναι κενό',3);
        }
    });
}