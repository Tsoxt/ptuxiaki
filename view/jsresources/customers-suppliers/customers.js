function searchCustomer(input,type) {
    $('#groupset1 .listOfPersons').empty();
    var result=findCustomer(input,type);
    result.then(function (json) {
        console.log(json);
        var data=JSON.parse(json);
        if (data.response){
            clearCustomer();
            if (parseInt(type)===1){//anazitisi me id
                writeCustomer(data.customers[0]);
            } else{//anazitisi me fulltext
                if (data.gsis){
                    customer=-1;
                    writeCustomer(data.customers[0]);
                } else{
                    unbindCustomerListeners();
                    $('#groupset1 .listOfPersons').append('<div class="table-responsive">' +
                        '<table class="table table-hover">' +
                        '<thead>' +
                        '<tr>' +
                        '<th style="width: 20%">Όνομα</th>' +
                        '<th style="width: 20%">Διεύθυνση</th>' +
                        '<th style="width: 13%">ΑΦΜ</th>' +
                        '<th style="width: 13%">Τηλέφωνο</th>' +
                        '<th style="width: 13%">Κινητό</th>' +
                        '<th style="width: 13%">Υπόλοιπο</th>' +
                        '<th style="width: 8%"></th>' +
                        '</tr></thead><tbody></tbody></table></div>');
                    data.customers.forEach(function (row) {
                        appendPersonInventory(row,'groupset1');
                    });
                    customerListeners();
                }
            }
        } else {
            $('#groupset1 .listOfPersons').append('<p>'+data.error+'</p>');
        }
    });
}

function clearCustomer() {
    $('input[type="date"]').val('');
    $('#customer_parastatika').empty();
    $('#customer_products').empty();
    $('.customer_form').hide();
    $('#addDiscount')[0].reset();
    $('#groupset1 .listOfPersons').empty();
}

function writeCustomer(data) {
    customer=parseInt(data.body.id);
    if (parseInt(data.body.type)===0){
        $('#customer_register select').val(0);
    }else {
        $('#customer_register select').val(1);
    }
    $('#customer_register input[name="ypoloipo"]').val(parseFloat(data.ypoloipo).toFixed(2));
    $('#customer_register input[name="points"]').val(data.body.points);
    $('#customer_register input[name="member"]').val(data.body.member);
    $('#customer_register input[name="name"]').val(data.body.name);
    $('#customer_register input[name="address"]').val(data.body.address);
    $('#customer_register input[name="city"]').val(data.body.city);
    $('#customer_register input[name="tk"]').val(data.body.postcode);
    $('#customer_register input[name="phone"]').val(data.body.phone);
    $('#customer_register input[name="mobile"]').val(data.body.mobile);
    $('#customer_register input[name="job"]').val(data.body.job);
    $('#customer_register input[name="afm"]').val(data.body.afm);
    $('#customer_register input[name="doy"]').val(data.body.doy);
    $('.formbuttons').hide();
    $('.customer_form').show();
    bindCustomerDeltioListeners();
    $('#customer_deltio_type').trigger('change');
    unbindCustomerListeners();
    callCustomerDiscounts();
}

function handleCustomer(name){
    var data,
        pedio = $('#customer_register [name="'+name+'"]');
    if (!(pedio[0].checkValidity())){
        pedio[0].reportValidity();
    }else{
        data={id:parseInt(customer), name:name, value:pedio.val()};
        var result=updateCustomer(data);
        result.then(function (json) {
            console.log(json);
            var data=JSON.parse(json);
            if (data.response){
                pedio[0].reportValidity();
            } else{
                bootboxMessage(data.error,1);
            }
        });
    }
}

function newCustomer() {
    start();
    clearCustomer();
    unbindCustomerListeners();
    $('#groupset1').show();
    $('.customer_form').show();
    $('a[href="#customer_info"]').tab('show');
    customerListeners();
}

function callCustomerDiscounts() {
    unbindCustomerListeners();
    $('#customer_products').empty();
    $('#customer_products').append('<table class="table table-hover">' +
        '<thead>' +
        '<tr>' +
        '<th style="width: 40%">Περιγραφή</th><th style="width: 10%">Τιμή</th>' +
        '<th style="width: 20%">Έκπτωση</th><th style="width: 10%">Αξία</th>' +
        '<th style="width: 10%"></th><th style="width: 10%"></th>' +
        '</tr>' +
        '</thead>' +
        '<tbody></tbody>' +
        '</table>');
    var id=parseInt(customer);
    var result=getCustomerDiscounts(id);
    result.then(function (json) {
        console.log(json);
        var data = JSON.parse(json);
        if (data.response) {
            data.body.forEach(function (row) {
                appendCustomerDiscount(row);
            });
        } else {
            $('#customer_products').append('<div class="info_msg">-'+data.error+'-</div>');
        }
        customerListeners();
    });
}

function appendCustomerDiscount(field) {
    var discount,tr;
    $('#customer_products tbody').prepend('<tr>' +
        '<td style="width: 40%">'+field.perigrafi+'<input name="proion_id" value="'+field.id_proion+'" hidden></td><td style="width: 10%" class="proion_price">'+field.timi+' €</td>' +
        '<td style="width: 20%">' +
        '<select name="product_discount"><option value="0">Έκπτωση %</option>' +
        '<option value="1">Έκπτωση</option></select>' +
        '</td>' +
        '<td style="width: 10%"><input type="number" name="discount_price"></td>' +
        '<td style="width: 10%" class="updateCell"><button type="button" name="updateDiscount" class="far fa-save submitbtn" style="display: none"></button></td>' +
        '<td style="width: 10%"><button type="button" name="deleteDiscount" class="far fa-trash-alt resetbtn"></button></td>' +
        '</tr>');
    tr=$('#customer_products tbody tr').first();
    if (parseInt(field.type)===0){//ekptosi epis tis %
        discount=parseFloat(field.discount)*100;
        discount=parseInt(discount);
        $(tr).find('select').val(0);
        $(tr).find('input[name="discount_price"]').prop({
            "min":1,
            "max":99,
            "step":1
        });
    } else{//ekptosi aksiaki
        discount=parseFloat(field.discount);
        discount=discount.toFixed(2);
        $(tr).find('select').val(1);
        var max_ekptosi=parseFloat(field.timi)-0.01;
        max_ekptosi=max_ekptosi.toFixed(2);
        $(tr).find('input[name="discount_price"]').prop({
            "min":0.01,
            "max":parseFloat(max_ekptosi),
            "step":0.01
        });
    }
    $(tr).find('input[name="discount_price"]').val(discount);
    if (parseFloat(field.timi)===0){
        $(tr).find('select').prop('disabled',true);
    }
}

function changeDiscountAttrs() {
    if ($('#customer_discounts input[name="productPrice"]').val()) {
        var price=$('#customer_discounts input[name="productPrice"]').val();
        var value=$('#customer_discounts select[name="productDiscType"]').val();
        if (parseInt(value)===0){//ekptwsi epi tis %
            $('#customer_discounts input[name="productDiscPrice"]').prop({
                "min":1,
                "max":99,
                "step":1
            });
        }else{//aksiaki ekptwsi
            var max_ekptosi=parseFloat(price)-0.01;
            max_ekptosi=max_ekptosi.toFixed(2);
            $('#customer_discounts input[name="productDiscPrice"]').prop({
                "min":0.01,
                "max":parseFloat(max_ekptosi),
                "step":0.01
            });
        }
    }

}

function getCustomerParastatikaBetweenDates(fromDate,toDate) {
    unbindCustomerListeners();
    $('#customer_parastatika').empty();
    var data={fromdate:fromDate,todate:toDate,customer:parseInt(customer)};
    var result=getCustomerParastatika(data);
    result.then(function (json) {
        console.log(json);
        var data=JSON.parse(json);
        if (data.response){
            $('#customer_parastatika').append('<table class="table table-hover"><thead><tr>' +
                '<th style="width: 22%">Ημ/νια</th>' +
                '<th style="width: 23%">Παραστατικό</th>' +
                '<th style="width: 13%">Αξία</th>' +
                '<th style="width: 13%">Πληρωτέο</th>' +
                '<th style="width: 13%">Πίστωση</th>' +
                '<th style="width: 8%"></th>' +
                '<th style="width: 8%"></th>' +
                '</tr></thead>' +
                '<tbody>' +
                '<tr>' +
                '<td style="width: 22%"></td>' +
                '<td style="width: 23%">Από μεταφορά</td>' +
                '<td style="width: 13%">'+(data.apometafora).toFixed(2)+'</td>' +
                '<td style="width: 13%">'+(parseFloat(0)).toFixed(2)+'</td>' +
                '<td style="width: 13%">'+(data.apometafora).toFixed(2)+'</td>' +
                '<td style="width: 8%"></td>' +
                '<td style="width: 8%"></td>' +
                '</tr>' +
                '</tbody>' +
                '<tfoot></tfoot></table>');
            var info,
                reprint,
                pliroteo_tim,
                pistosi_tim,
                pliroteo=0,
                i=0,
                sinolo=parseFloat(data.apometafora),
                pistosi=parseFloat(data.apometafora);
            data.parastatika.forEach(function (row) {
                var sum_tim=0,
                    discount=0;
                if (row.info.invoData['shortname']==='ΑΠΕ'){//ean einai apodeiksi eispraksis
                    info='';
                    reprint='';
                }else {
                    info='<button type="button" class="fas fa-info-circle cartbtn" data-toggle="collapse" data-target="#parastatikoPelatiCollapse'+i+'" aria-expanded="false"></button>';
                    reprint='<button type="button" class="fas fa-print printbtn"></button>';
                }
                row.payments.forEach(function (payment) {
                    if (parseInt(payment.id)!==0){//ean dn einai ekptosi
                        sum_tim+=parseFloat(payment.amount);
                        if (parseInt(payment.id)===1){//ean einai pistosi
                            pistosi_tim=parseFloat(payment.amount);
                        }
                    }else {
                        discount+=parseFloat(payment.amount);
                    }
                });
                pliroteo_tim=sum_tim-pistosi_tim;
                pliroteo_tim=parseFloat(pliroteo_tim);
                $('#customer_parastatika tbody').first()
                    .append('<tr>' +
                        '<td style="width: 22%">'+row.info['date']+'</td>' +
                        '<td style="width: 23%">'+row.info.invoData['shortname']+' '+row.info.invoData['lastNumber']+'<input name="parastatiko_id" value="'+row.info.invoData['id']+'" hidden></td>' +
                        '<td style="width: 13%">'+sum_tim.toFixed(2)+'</td>' +
                        '<td style="width: 13%">'+pliroteo_tim.toFixed(2)+'</td>' +
                        '<td style="width: 13%">'+pistosi_tim.toFixed(2)+'</td>' +
                        '<td style="width: 8%">'+info+'</td>' +
                        '<td style="width: 8%">'+reprint+'</td></tr>');
                sinolo+=sum_tim;
                pliroteo+=pliroteo_tim;
                pistosi+=pistosi_tim;
                if (row.receipt.length!==0){
                    $('#customer_parastatika tbody')
                        .first()
                        .append('<tr>' +
                            '<td style="width: 100%"><div class="collapse" id="parastatikoPelatiCollapse'+i+'"><table class="table table-condensed">' +
                            '<thead><tr>' +
                            '<th style="width: 40%">Προϊόν</th>' +
                            '<th style="width: 20%">Ποσότητα</th>' +
                            '<th style="width: 20%">Τιμή</th>' +
                            '<th style="width: 20%">Σύνολο</th></tr></thead>' +
                            '<tbody></tbody>' +
                            '<tfoot><tr>' +
                            '<th style="width: 40%"></th>' +
                            '<th style="width: 20%"></th>' +
                            '<th style="width: 20%">Σύνολο:</th>' +
                            '<td class="sum_product" style="width: 20%"></td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th style="width: 40%"></th>' +
                            '<th style="width: 20%"></th>' +
                            '<th style="width: 20%">Έκπτωση:</th>' +
                            '<td style="width: 20%">'+discount.toFixed(2)+'</td>' +
                            '</tr></tfoot></table></div></td></tr>');
                    var sum_proion=0;
                    row.receipt.forEach(function (proion) {
                        $('#parastatikoPelatiCollapse'+i+' tbody').append('<tr>' +
                            '<td style="width: 40%">'+proion.description+'</td>' +
                            '<td style="width: 20%">'+parseFloat(proion.temaxia).toFixed(3)+'</td>' +
                            '<td style="width: 20%">'+parseFloat(proion.timi).toFixed(2)+'</td>' +
                            '<td style="width: 20%">'+(parseFloat(proion.temaxia)*parseFloat(proion.timi)).toFixed(2)+'</td>' +
                            '</tr>');
                        sum_proion+=(parseFloat(proion.temaxia)*parseFloat(proion.timi));
                    });
                    $('#parastatikoPelatiCollapse'+i+' .sum_product').text(sum_proion.toFixed(2));
                    i++;
                }
            });
            $('#customer_parastatika tfoot')
                .last()
                .append('<tr>' +
                    '<th style="width: 22%">Σύνολο:</th>' +
                    '<th style="width: 23%">'+data.parastatika.length+' παραστατικά</th>' +
                    '<th style="width: 13%">'+sinolo.toFixed(2)+' €</th>' +
                    '<th style="width: 13%">'+pliroteo.toFixed(2)+' €</th>' +
                    '<th style="width: 13%">'+pistosi.toFixed(2)+' €</th>' +
                    '<th style="width: 8%"></th>' +
                    '<th style="width: 8%"></th>' +
                    '</tr>');
        }else {
            bootboxMessage(data.error,1);
        }
        customerListeners();
    });
    return true;
}

function unbindCustomerListeners() {
    $('#customer_register').off();
    $('#addDiscount').off();
    $('#groupset1 input').not('#customerData').off();
    $('select').off();
    $('#customer_products tbody tr button').off('click');
    $('#customer_tab .head_column button').off('click');

}

function customerListeners() {
    if (parseInt(customer)===0){//kainourios pelatis
        $('#customer_register input[name="points"]').prop('readonly',false);
        $('#customer_register input[name="afm"]').prop('readonly',false);
        $('#customer_register .formbuttons').show();
    }else if (parseInt(customer)===-1){//pelatis apo gsis
        $('#customer_register input[name="points"]').prop('readonly',false);
        $('#customer_register input[name="afm"]').prop('readonly',true);
        $('#customer_register .formbuttons').show();
    }else {//yparxon pelatis stin vasi
        if ($('#customer_register input[name="afm"]').val()===''){
            $('#customer_register input[name="afm"]').prop('readonly',false);
        } else{
            $('#customer_register input[name="afm"]').prop('readonly',true);
        }
        $('#customer_register input[name="points"]').prop('readonly',true);
        $('#customer_register .formbuttons').hide();
        $('#customer_register  input, #customer_register select').on('change',function () {
            var name=$(this).attr('name');
            handleCustomer(name);
        });
    }
    $('#customer_register').submit(function (e) {
        e.preventDefault();
        var form=$('#customer_register').serializeArray();
        var data={};
        form.forEach(function (field) {
            data[field.name]=field.value;
        });
        var result=insertCustomer(data);
        result.then(function (json) {
            console.log(json);
            var data=JSON.parse(json);
            if (data.response){
                bootboxMessage('Επιτυχής καταχώρηση',2);
                start();
            } else {
                bootboxMessage(data.error,1);
            }
        });
    });

    $('#addDiscount').submit(function (e) {
        e.preventDefault();
        if (parseInt(customer)===0){
            bootboxMessage('Πρέπει να προσθέσετε πελάτη πρώτα',3);
        } else{
            if (!$('#addDiscount input[name="productId"]').val()){
                bootboxMessage('Δεν έχετε επιλέξει κάποιο προίόν',3);
            } else {
                var form=$('#addDiscount').serializeArray();
                var data= {};
                data['customer']=parseInt(customer);
                form.forEach(function (field) {
                    data[field.name]=field.value;
                });
                if (parseInt(data['productPrice'])===0){
                    data['productDiscType']=$('#addDiscount select[name="productDiscType"]').val();
                }
                if (parseInt(data['productDiscType'])===0){
                    data['productDiscPrice']=parseFloat(data['productDiscPrice'])/100;
                }
                var action='addDiscount';
                var result=insertCustomerDiscount(data,action);
                result.then(function (json) {
                    console.log(json);
                    var insert=JSON.parse(json);
                    if (insert.response){
                        unbindCustomerListeners();
                        if ($('.info_msg')){
                            $('.info_msg').remove();
                        }
                        var row={id_proion:data.productId, perigrafi: data.productName,timi: data.productPrice
                            ,discount: data.productDiscPrice,type: data.productDiscType};
                        appendCustomerDiscount(row);
                        customerListeners();
                        $('.customerBarcodeBtn').focus();
                    }else{
                        bootboxMessage(insert.error,1);
                    }
                    $('#addDiscount')[0].reset();
                });
            }
        }
    });

    $('#customer_tab .head_column button').click(function () {
        if (parseInt(customer)!==0){
            var d = new Date();
            var year=d.getFullYear();
            var fromDate= year+'-01-01',
                toDate = year+'-12-01';
            getCustomerParastatikaBetweenDates(fromDate,toDate);
        } else {
            bootboxMessage('Πρέπει να προσθέσετε πελάτη πρώτα',3);
        }
    });

    $('#customer_tab input[name="fromDate"]').on('change',function () {
        var fromDate= $('#customer_tab input[name="fromDate"]').val(),
            toDate = $('#customer_tab input[name="toDate"]').val();
        var datefrom = new Date(fromDate);
        var month = (datefrom.getMonth()+1);
        var tomorrow = (datefrom.getDate()+1);
        var nextday = datefrom.getFullYear() + '-' +
            (month<10 ? '0' : '') + month + '-' +
            (tomorrow<10 ? '0' : '') + tomorrow;
        $('#customer_tab input[name="toDate"]').prop('min',function () {
            return nextday;
        });
        if (toDate!==''){
            if (parseInt(customer)!==0){
                getCustomerParastatikaBetweenDates(fromDate,toDate);
            } else {
                bootboxMessage('Πρέπει να προσθέσετε πελάτη πρώτα',3);
            }

        }
        return true;
    });

    $('#customer_tab input[name="toDate"]').on('change',function () {
        var fromDate= $('#customer_tab input[name="fromDate"]').val(),
            toDate = $('#customer_tab input[name="toDate"]').val();
        var dateto = new Date(toDate);
        var month = (dateto.getMonth()+1);
        var yesterday = (dateto.getDate()-1);
        var previousday = dateto.getFullYear() + '-' +
            (month<10 ? '0' : '') + month + '-' +
            (yesterday<10 ? '0' : '') + yesterday;
        $('#customer_tab input[name="fromDate"]').prop('max',function () {
            return previousday;
        });
        if (fromDate!==''){
            if (parseInt(customer)!==0){
                getCustomerParastatikaBetweenDates(fromDate,toDate);
            } else {
                bootboxMessage('Πρέπει να προσθέσετε πελάτη πρώτα',3);
            }
        }
        return true;
    });

    $('#customer_products tbody tr input').on('change',function () {
        var row=$(this).closest('tr');
        $(row).find('.submitbtn').show();
    });

    $('#customer_products tbody tr select').on('change',function () {
        var value=$(this).val();
        var row=$(this).closest('tr');
        $(row).find('input[name="discount_price"]').val('');
        $(row).find('.submitbtn').show();
        var price=$(row).find('.proion_price').text();
        if (parseInt(value)===0){//ekptwsi epi tis %
            $(row).find('input[name="discount_price"]').prop({
                "min":1,
                "max":99,
                "step":1
            });
        } else{//aksiaki ekptwsi
            var max_ekptosi=parseFloat(price)-0.01;
            max_ekptosi=max_ekptosi.toFixed(2);
            $(row).find('input[name="discount_price"]').prop({
                "min":0.01,
                "max":parseFloat(max_ekptosi),
                "step":0.01
            });
        }
        $(row).find(' input[name="discount_price"]').focus();
    });

    $('#customer_products tbody tr button').on('click',function () {
        var action=$(this).attr('name');
        var row=$(this).closest('tr');
        var discount;
        discount=$(row).find('input[name="discount_price"]');
        if ((action==='deleteDiscount')||((action==='updateDiscount') && ($(discount)[0].checkValidity()))){
            var discountPrice=$(discount).val();
            var discountType=$(row).find(' select[name="product_discount"]').val();
            var id=$(row).find(' input[name="proion_id"]').val();
            var data={customer:parseInt(customer),proion:parseInt(id),type:parseInt(discountType),discount:parseFloat(discountPrice)};
            var result=handleCustomerDiscount(data,action);
            result.then(function (json) {
                console.log(json);
                var data=JSON.parse(json);
                if (data.response){
                    if (action==='deleteDiscount'){
                        $(row).remove();
                    }else{
                        $(row).find('.submitbtn').hide();
                        $(row).find('.updateCell').append('<i class="fas fa-check-double quickmessage"></i>');
                        setTimeout(function () {
                            $(row).find('.updateCell i').remove();
                        },1500);
                    }
                    $('.customerBarcodeBtn').focus();
                } else{
                    bootboxMessage(data.error,1);
                }

            });
        }else{
            $(discount)[0].reportValidity();
        }

    });

    $('a[href="#customer_discounts"]').on('shown.bs.tab',function () {
        $('.customerBarcodeBtn').focus();
        $('#addDiscount')[0].reset();
    });

    $('#customer_parastatika .printbtn').click(function () {
        var row=$(this).closest('tr');
        var id=$(row).find('input[name="parastatiko_id"]').val();
        var result=reprintTimologio(id);
        result.then(function (json) {
            var data=JSON.parse(json);
            if (data.response){
                if(typeof data.url !== 'undefined'){
                    try{
                        Android.printInvoice(data.url);
                    }catch (e){
                        window.open(data.url);
                    }
                }
            } else {
                bootboxMessage(data.error,1);
            }
        });
    });

    $('#groupset1 .listOfPersons tbody button').click(function () {
        var td=$(this).closest('td');
        var id=$(td).find('input').val();
        searchCustomer(id,1);
        $('#customerData').val('');
    });

}