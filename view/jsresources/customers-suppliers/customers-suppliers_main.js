function bootboxMessage(msg,type) {
    if (parseInt(type)===1) {
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="errormsg">'+msg+'</div>'
        });
    }else if (parseInt(type)===2){
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="successmsg">'+msg+'</div>'
        });
    }else {
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="infomsg">'+msg+'</div>'
        });
    }
}

//<editor-fold desc="search Product Screen">

function showcategories(type){
    var result = callCategories();
    result.then(function (json) {
        var data = JSON.parse(json);
        if (data.response){
            $('.flexgroup').empty();
            data.categories.forEach(function (category) {
                var newrow = '<button type="button" class="groupbut" onclick="showomades('+parseInt(category.id)+','+type+')">'+category.name+'</button>';
                $('.flexgroup').append(newrow);
            });
            $('#inventory').modal('show');
        } else {
            bootboxMessage(data.error,1);
        }
    });
}

function showomades(id,type) {
    var result = callOmades(id);
    result.then(function (json) {
        var data = JSON.parse(json);
        if (data.response){
            $('.flexgroup').empty();
            $('.flexgroup').append('<button type="button" class="groupbut"  onclick="showcategories('+type+')"><i class="icon ion-arrow-return-left"></i></button>');
            data.omades.forEach(function (omada) {
                var newrow = '<button type="button" class="groupbut" onclick="showItems('+parseInt(omada.id)+', '+id+','+type+')">'+omada.name+'</button>';
                $('.flexgroup').append(newrow);
            });
        }else {
            bootboxMessage(data.error,1);
            //toDo hide me xronokathisterisi
        }
    });
}

function showItems(omadaId, categoryId,type) {
    var result = callItems(omadaId);
    result.then(function (json) {
        var data = JSON.parse(json);
        console.log(data);
        if (data.response==true){
            $('.flexgroup').empty();
            $('.flexgroup').append('<button type="button" class="groupbut"  onclick="showomades('+categoryId+','+type+')"><i class="icon ion-arrow-return-left"></i></button>');
            data.items.forEach(function (proion) {
                var newrow = '<button type="button" class="groupbut" onclick="callItem('+parseInt(proion.id)+','+type+')">'+proion.perigrafi+'</button>';
                $('.flexgroup').append(newrow);
            });
        }else {
            bootboxMessage(data.error,1);
            //toDo hide me xronokathisterisi
        }
    });
}

function callItem(id,type) {
    $("#inventory").modal('hide');
    var action= 'byid';
    getProduct(id,action,type);
}

function getProduct(value,action,type) {
    var result=searchItem(value,action);
    result.then(function (json) {
        console.log(json);
        var data=JSON.parse(json);
        if (data.response){
            if (parseInt(type)===1){//pelatis
                if (parseFloat(data.proion.timi)===0){
                    bootboxMessage('Το προίόν που επιλέξατε είναι ανοικτής τιμής. Μπορείτε να εφαρμόσετε μόνο έκπτωση επί τοις %',3);
                    $('select[name="productDiscType"]').val(0);
                    $('select[name="productDiscType"]').prop('disabled',true);
                }else{
                    $('select[name="productDiscType"]').prop('disabled',false);
                }
                $('#addDiscount input[name="productName"]').val(data.proion.perigrafi);
                $('#addDiscount input[name="productPrice"]').val(data.proion.timi);
                $('#addDiscount input[name="productId"]').val(data.proion.id);
                changeDiscountAttrs();
            }else if (parseInt(type)===2) {//promitheutis add product
                $('#addProduct input[name="productName"]').val(data.proion.perigrafi);
                $('#addProduct input[name="productSiskevasia"]').val(data.proion.temaxia);
                $('#addProduct input[name="productStock"]').val(data.proion.stock);
                $('#addProduct input[name="productStockRecommend"]').val(data.proion.stock_recommend);
                $('#addProduct input[name="productStockAlert"]').val(data.proion.stock_alert);
                $('#addProduct input[name="productId"]').val(data.proion.id);
                $('#addProduct input[name="productTmima"]').val(data.proion.tmima);
                $('#addProduct input[name="productMcode"]').val(data.proion.mothercode);
                $('#addProduct input[name="productBcode"]').val(data.proion.barcode);
                $('#addProduct input[name="productIdOm"]').val(data.proion.id_omadas);
                $('#addProduct input[name="productZigos"]').val(data.proion.zugistiko);
            }else if (parseInt(type)===3) {//promitheutis deltio
                $('#addToDeltio input[name="productName"]').val(data.proion.perigrafi);
                $('#addToDeltio input[name="productSiskevasia"]').val(data.proion.temaxia);
                $('#addToDeltio input[name="productStock"]').val(data.proion.stock);
                $('#addToDeltio input[name="productStockRecommend"]').val(data.proion.stock_recommend);
                $('#addToDeltio input[name="productStockAlert"]').val(data.proion.stock_alert);
                $('#addToDeltio input[name="productId"]').val(data.proion.id);
                $('#addToDeltio input[name="productTmima"]').val(data.proion.tmima);
                $('#addToDeltio input[name="productMcode"]').val(data.proion.mothercode);
                $('#addToDeltio input[name="productBcode"]').val(data.proion.barcode);
                $('#addToDeltio input[name="productIdOm"]').val(data.proion.id_omadas);
                $('#addToDeltio input[name="productZigos"]').val(data.proion.zugistiko);
                var timi_agoras;
                if ($.trim($('#supplier_products tbody').html())!==''){
                    var tr=$('#supplier_products input[name="id"]').filter(function () {
                        return $(this).val()===data.proion.id;
                    }).closest('tr');
                    if (tr.length>0){
                        timi_agoras=$(tr).find('input[name="buy_price"]');
                        timi_agoras=timi_agoras.val();
                    }else {
                        timi_agoras=data.proion.timi_agoras;
                    }
                }else {
                    timi_agoras=data.proion.timi_agoras;
                }
                $('#addToDeltio input[name="productBuyPrice"]').val(timi_agoras);
            }else{//pelatis deltio
                $('#addToCustomerDeltio input[name="productName"]').val(data.proion.perigrafi);
                $('#addToCustomerDeltio input[name="productSiskevasia"]').val(data.proion.temaxia);
                $('#addToCustomerDeltio input[name="productStock"]').val(data.proion.stock);
                $('#addToCustomerDeltio input[name="productId"]').val(data.proion.id);
                $('#addToCustomerDeltio input[name="productTmima"]').val(data.proion.tmima);
                $('#addToCustomerDeltio input[name="productMcode"]').val(data.proion.mothercode);
                $('#addToCustomerDeltio input[name="productBcode"]').val(data.proion.barcode);
                $('#addToCustomerDeltio input[name="productIdOm"]').val(data.proion.id_omadas);
                $('#addToCustomerDeltio input[name="productZigos"]').val(data.proion.zugistiko);
                var discount_type,discount_value,timi;
                if ($.trim($('#customer_products tbody').html())!==''){
                    var tr=$('#customer_products input[name="proion_id"]').filter(function () {
                        return $(this).val()===data.proion.id;
                    }).closest('tr');
                    if (tr.length>0){
                        discount_type=$(tr).find('select[name="product_discount"]');
                        discount_type=parseInt(discount_type.val());
                        discount_value=$(tr).find('input[name="discount_price"]');
                        discount_value=parseFloat(discount_value.val());
                        if (discount_type===0){//epi tis %
                            timi=parseFloat(data.proion.timi)-(parseFloat(data.proion.timi)*(discount_value/100));
                        }else {
                            timi=parseFloat(data.proion.timi)-discount_value;
                        }
                        timi=(timi).toFixed(2);
                    }else {
                        timi=data.proion.timi;
                    }
                }else {
                    timi=data.proion.timi;
                }
                $('#addToCustomerDeltio input[name="productPrice"]').val(timi);
            }
        }else {
            bootboxMessage(data.error,1);
        }

    });
}

//</editor-fold>

//<editor-fold desc="search Person Screen">

function appendPersonInventory(row,groupset) {
    if (row.body.afm===null){
        row.body.afm='';
    }
    if (row.body.phone===null){
        row.body.phone='';
    }
    if (row.body.mobile===null){
        row.body.mobile='';
    }
    if (row.ypoloipo===null){
        row.ypoloipo=0;
    }
    $('#'+groupset+' .listOfPersons tbody').append('<tr>' +
        '<td style="width: 20%">'+row.body.name+'</td>' +
        '<td style="width: 20%">'+row.body.address+'</td>' +
        '<td style="width: 13%">'+row.body.afm+'</td>' +
        '<td style="width: 13%">'+row.body.phone+'</td>' +
        '<td style="width: 13%">'+row.body.mobile+'</td>' +
        '<td style="width: 13%">'+parseFloat(row.ypoloipo).toFixed(2)+'</td>' +
        '<td style="width: 8%"><input value="'+parseInt(row.body.id)+'" hidden><button class="fas fa-user-check submitbtn"></button></td></tr>');
}

function setSidemenuDefault() {
    $('.sidebuts').css({
        "background-color":"#f6f6f6",
        "color":"#665e5e"
    });
}

function sideButtonsListeners() {
    $(".sidebuts").click(function () {
        var id=$(this).attr('id');
        if (id==="collapseSidemenu"){
            $('.sidebuts span').toggle();
            $('#collapseSidemenu i').toggle();
        }else{
            var i;
            for (i=1;i<3;i++){
                $('#groupset'+i).hide();
            }
            $('.ajax-loader').show(0).delay(500).hide(0);
            if(parseInt(access)>1){
                bootboxMessage('Δεν έχετε τα δικαιώματα για να δείτε αυτές τις ρυθμίσεις');
            }else{
                setSidemenuDefault();
                $(this).css({
                    "background-color":"#665e5e",
                    "color":"#f6f6f6"
                });
                $('.sidebuts span').hide();
                $('.listOfPersons').empty();
                switch (id){
                    case "btn1":
                        $('#customerData').val('');
                        $('.customer_form').hide();
                        $('#customer_parastatika').empty();
                        $('#groupset1').show();
                        break;
                    case "btn2":
                        clearSupplierDeltio();
                        $('#supplierData').val('');
                        $('.supplier_form').hide();
                        $('#supplier_parastatika').empty();
                        $('#groupset2').show();
                        break;
                }
            }
        }
    });
}

function getAllParastatika() {
    var result=getTableParastatikaInfo();
    result.then(function (json) {
        console.log(json);
        var data=JSON.parse(json);
        if (data.response) {
            parastatikaInfo=data.body;
        }else {
            bootboxMessage(data.error,1);
        }

    });
}

function start() {
    $('.ajax-loader').show(0).delay(500).hide(0);
    $('form').trigger('reset');
    $('#addDiscount')[0].reset();
    $('input[type="date"]').val('');
    $('#customer_products').empty();
    $('#supplier_products').empty();
    $('#groupset1').hide();
    $('#groupset2').hide();
    $('#supplierData').val('');
    $('#customerData').val('');
    customer=0;
    supplier=0;
    timer=0;
    user={};
}

function getSessionMode(){
    var data=sessionStorage.getItem('mode');
    if (parseInt(data)===1){
        $('#btn1').trigger('click');
        setTimeout(function () {
            $('#newCustomerBtn').trigger('click');
        },1000);
    }
    sessionStorage.removeItem('mode');
}

$(document).ready(function () {
    initCss();
    menuCss();
    sidebuts();
    appendHead();
    access;
    start();
    sideButtonsListeners();
    getSessionMode();
    getAllParastatika();

    $('.customerBarcodeBtn').keypress(function (e) {
        var rowId=$(this).closest('form').attr('id');
        if(e.keyCode===13) {
            e.preventDefault();
            var value=$('#'+rowId+' .customerBarcode').val();
            $('#'+rowId+' .customerBarcode').val('');
            var action='bybarcode';
            var type;
            if (rowId==='addDiscount'){//customers add discount
                type=1;
            } else {//customers deltio
                type=4;
            }
            getProduct(value,action,type);
        }else {
            var input=$('#'+rowId+' .customerBarcode').val();
            var key=e.key;
            $('#'+rowId+' .customerBarcode').val(input+key);
        }
    });

    $('.supplierBarcodeBtn').keypress(function (e) {
        var rowId=$(this).closest('form').attr('id');
        if(e.keyCode===13) {
            e.preventDefault();
            var value=$('#'+rowId+' .supplierBarcode').val();
            $('#'+rowId+' .supplierBarcode').val('');
            var action='bybarcode';
            var type;
            if (rowId==='addProduct'){//suppliers add product
                type=2;
            } else {//suppliers deltio
                type=3;
            }
            getProduct(value,action,type);
        }else {
            var input=$('#'+rowId+' .supplierBarcode').val();
            var key=e.key;
            $('#'+rowId+' .supplierBarcode').val(input+key);
        }
    });
    
    $('.customerBarcodeBtn').on('focus',function () {
        $('.customerBarcodeBtn').css('border','2px solid green').css('border-radius','5px');
    });

    $('.customerBarcodeBtn').on('focusout',function () {
        $('.customerBarcodeBtn').css('border','none').css('border-radius','unset');
    });

    $('.supplierBarcodeBtn').on('focus',function () {
        $('.supplierBarcodeBtn').css('border','2px solid green').css('border-radius','5px');
    });

    $('.supplierBarcodeBtn').on('focusout',function () {
        $('.supplierBarcodeBtn').css('border','none').css('border-radius','unset');
    });

    $('#customerData').keyup(function () {
        clearTimeout(timer);
        timer=0;
        var input=this.value;
        if (input.length>=3) {
            timer=setTimeout(function () {
                searchCustomer(input,2);
            },1000);
        }else {
            $('#groupset1 .listOfPersons').empty();
        }
    });

    $('#supplierData').keyup(function () {
        clearTimeout(timer);
        timer=0;
        var input=this.value;
        if (input.length>=3) {
            timer=setTimeout(function () {
                searchSupplier(input,2);
            },1000);
        }else {
            $('#groupset2 .listOfPersons').empty();
        }
    });

    $('#customerTicket')
        .on('shown.bs.modal',function () {
            unbindCustomerDeltioListeners();
            bindCustomerDeltioListeners();
        }).on('hidden.bs.modal',function () {
        clearCustomerDeltio();
        unbindCustomerDeltioListeners();
    });

    $('#supplierTicket')
        .on('shown.bs.modal',function () {
            unbindSupplierDeltioListeners();
            bindSupplierDeltioListeners();
        }).on('hidden.bs.modal',function () {
        clearSupplierDeltio();
        unbindSupplierDeltioListeners();
    });

    $('#parastatikaInventory').on('shown.bs.modal',function () {
        unbindSupplierDeltioListeners();
        bindSupplierDeltioListeners();
    }).on('hidden.bs.modal',function () {
       $('#parastatikaInventory .table-responsive').empty();
        unbindSupplierDeltioListeners();
    });

    $('#customerParastatikaInventory').on('shown.bs.modal',function () {
        unbindCustomerDeltioListeners();
        bindCustomerDeltioListeners();
    }).on('hidden.bs.modal',function () {
        $('#customerParastatikaInventory .table-responsive').empty();
        unbindCustomerDeltioListeners();
    });

    $('#ordersInProgress').on('shown.bs.modal',function () {
        //bindSupplierDeltioListeners();
    }).on('hidden.bs.modal',function () {
         unbindSupplierDeltioListeners();
    });

    var userinfo=userInfo();
    userinfo.then(function (json) {
        var data=JSON.parse(json);
        if(data.response){
            user.name=data.user.name;
            user.id=parseInt(data.user.user_id);
        }else{
            window.location.replace('../index.php');
        }
    });

});