function clearSupplierDeltio() {
    $('#addToDeltio input').val('');
    $('#deltio_form tbody').empty();
    $('#deltio_form tfoot input').val('0.00');
    $('#deltio_form textarea[name="matching_parastatiko"]').text('');
    $('#deltio_type')
        .val(0)
        .trigger('change')
        .prop('disabled',false);
    $('#parastatiko_type').prop('disabled',false);
}

function supplierDeltioModal() {
    if (parseInt(supplier)===0){
        bootboxMessage('Πρέπει να επιλέξετε προμηθευτή πρώτα',3);
    } else {
        $('#supplierTicket').modal('show');
    }
}

function appendDeltioProduct(data) {
    var result=getFpa(parseInt(data.tmima));
    result.then(function (json) {
        console.log(json);
        var fpa=JSON.parse(json);
        if (fpa.response){
            unbindSupplierDeltioListeners();
            var input_posotitas;
            if (typeof data.timi_agoras==='undefined'){
                data.timi_agoras=0;
            }
            if (typeof data.order_id==='undefined'){
                data.order_id=0;
                input_posotitas='<input name="temaxia" type="number" required min="0" step="0.01" value="'+(parseFloat(data.posotita)).toFixed(3)+'">';
            }else {
                input_posotitas='<input name="temaxia" type="number" required min="0" max="'+(parseFloat(data.posotita)).toFixed(3)+'" step="0.01" value="'+(parseFloat(data.posotita)).toFixed(3)+'">';
            }
            var sinolo=(parseInt(data.posotita)*parseFloat(data.timi_agoras)).toFixed(2);
            var aksiaFpa=parseFloat(sinolo)*parseFloat(fpa.proion.price);
            var teliko_sinolo=(parseFloat(sinolo)+parseFloat(aksiaFpa)).toFixed(2);
            $('#supplierTicket tbody').prepend('<tr>' +
                '<td>'+data.name+'<input name="id" value="'+data.id+'" hidden><input name="order_no" value="'+data.order_id+'" hidden>' +
                '<input name="desc" value="'+data.name+'" hidden><input name="tmima" value="'+parseInt(data.tmima)+'" hidden></td>' +
                '<td>'+parseInt(data.siskevasia)+'<input name="package" value="'+parseInt(data.siskevasia)+'" hidden></td>' +
                '<td>'+parseFloat(data.apothema)+'<input name="stock" value="'+parseFloat(data.apothema)+'" hidden></td>' +
                '<td>'+input_posotitas+'<input name="rest_quantity" value="'+(parseFloat(data.posotita)).toFixed(3)+'" hidden></td>' +
                '<td><input name="price" type="number" required min="0" step="0.01" value="'+(parseFloat(data.timi_agoras)).toFixed(2)+'"></td>' +
                '<td><input name="sinolo_proion" type="number" disabled value="'+sinolo+'"><input name="sinolo_me_ekptosi" disabled hidden value="'+sinolo+'"></td>' +
                '<td style="display: inline-flex"><select style="width: 40%" name="choose_discount"><option selected value="0">€</option><option value="1">%</option> </select>' +
                '<input style="width: 60%" name="ekptosi_proion" type="number" value="'+parseFloat(0).toFixed(2)+'" min="0" required  max="'+teliko_sinolo+'" step="0.01">' +
                '<input name="mothercode" value="'+data.mothercode+'" hidden><input name="barcode" value="'+data.barcode+'" hidden>' +
                '<input name="id_om" value="'+data.id_om+'" hidden><input name="zigos" value="'+data.zigos+'" hidden>' +
                '<input name="operator" value="'+user.id+'" hidden><input name="tameio" value="1" hidden></td>' +
                '<td><select name="fpa_proion">' +
                '<option value="0.06" selected>6%</option>' +
                '<option value="0.13">13%</option>' +
                '<option value="0.24">24%</option>' +
                '<option value="0">0%</option>' +
                '</select><input name="aksia_fpa_proion" value="'+aksiaFpa.toFixed(2)+'" disabled hidden></td>' +
                '<td><input name="sunolo" type="number" readonly value="'+teliko_sinolo+'"></td>' +
                '<td><button type="button" class="far fa-trash-alt resetbtn"></button></td>' +
                '</tr>');
            $('#supplierTicket tbody tr').first().find('select[name="fpa_proion"]').val(parseFloat(fpa.proion.price));
            if (parseFloat(sinolo)!==0){
                updateDeltioSums(sinolo,0,aksiaFpa,teliko_sinolo);
            }
            changeDeltioInputAttrs();
            bindSupplierDeltioListeners();
        }else {
            bootboxMessage('Υπήρξε κάποιο πρόβλημα με το ΦΠΑ του προϊόντος',1);
        }
    });
}

function changeDeltioInputAttrs() {
    var deltio=parseInt($('#deltio_type').val());
    if (deltio===0){//forma paraggelias
        $('#deltio_submit input[name="ekptosi_proion"]').val(0.00).prop('readonly',true);
    }else{//forma paralavis h forma epistrofis
        if ($('#parastatiko_type').val()!==''){//kamia epilogi
            var id=parseInt($('#parastatiko_type').val());
            var parastatiko=parseInt(parastatikaInfo[id]['update_flag']);
            if (parastatiko===0){//enimerosi apothematos
                $('#deltio_submit input[name="ekptosi_proion"]').prop('readonly',true);
            } else {
                $('#deltio_submit input[name="ekptosi_proion"]').prop('readonly',false);
            }
        }
    }
}

function updateDeltioSums(sinolo,ekptosi,fpa,teliko) {
    var sinolo_deltiou=parseFloat($('#supplierTicket input[name="sinolo_deltiou"]').val());
    var ekptosi_deltiou=parseFloat($('#supplierTicket input[name="ekptosi_deltiou"]').val());
    var fpa_deltiou=parseFloat($('#supplierTicket input[name="fpa_deltiou"]').val());
    var teliko_sinolo_deltiou=parseFloat($('#supplierTicket input[name="teliko_sinolo_deltiou"]').val());
    sinolo_deltiou+=parseFloat(sinolo);
    sinolo_deltiou=sinolo_deltiou.toFixed(2);
    ekptosi_deltiou+=parseFloat(ekptosi);
    ekptosi_deltiou=ekptosi_deltiou.toFixed(2);
    fpa_deltiou+=parseFloat(fpa);
    fpa_deltiou=fpa_deltiou.toFixed(2);
    teliko_sinolo_deltiou+=parseFloat(teliko);
    teliko_sinolo_deltiou=teliko_sinolo_deltiou.toFixed(2);
    $('#supplierTicket input[name="sinolo_deltiou"]').val(sinolo_deltiou);
    $('#supplierTicket input[name="ekptosi_deltiou"]').val(ekptosi_deltiou);
    $('#supplierTicket input[name="fpa_deltiou"]').val(fpa_deltiou);
    $('#supplierTicket input[name="teliko_sinolo_deltiou"]').val(teliko_sinolo_deltiou);
    $('#supplierTicket input[name="pliroteo_deltiou"]').val(teliko_sinolo_deltiou).prop({
        "max":teliko_sinolo_deltiou
    });
    return false;
}

function showSupplierOrders() {
    var data={'supplier':supplier};
    var result=getOrders(data);
    result.then(function (json) {
        console.log(json);
        var data=JSON.parse(json);
        if (data.response){
            unbindSupplierDeltioListeners();
            $('#ordersInProgress .table-responsive').empty();
            $('#ordersInProgress .table-responsive').append('<table class="table table-hover">' +
                '<thead>' +
                '<tr>' +
                '<th style="width: 4%">#</th>' +
                '<th style="width: 30%">Προμηθευτής</th>' +
                '<th style="width: 20%">Α/α</th>' +
                '<th style="width: 30%">Ημερομηνία Παραγγελίας</th>' +
                '<th style="width: 8%"></th>' +
                '<th style="width: 8%"></th>' +
                '</tr>' +
                '</thead>' +
                '<tbody></tbody></table>');
            data.body.forEach(function (row) {
                $('#ordersInProgress tbody').append('<tr>' +
                    '<td style="width: 4%"><input name="id_order" hidden value="'+row.id+'">' +
                    '<input name="id_supplier" hidden value="'+row.supplier_id+'"></td>' +
                    '<td style="width: 30%">'+row.name+'</td>' +
                    '<td style="width: 20%">'+row.id+'</td>' +
                    '<td style="width: 30%">'+row.date+'</td>' +
                    '<td style="width: 8%"><button type="button" name="addToDeltio" class="fas fa-truck submitbtn" data-toggle="tooltip" data-placement="top" data-trigger="manual" data-title="Προστέθηκε στο δελτίο"></button></td>' +
                    '<td style="width: 8%"><button type="button" name="removeOrder" class="far fa-calendar-times resetbtn"></button></td>' +
                    '</tr>');
            });
            $('#ordersInProgress').modal('show');
            bindSupplierDeltioListeners();
        }else {
            bootboxMessage('Δεν βρέθηκαν παραγγελίες σε εξέλιξη',3);
        }
    });

}

function unbindSupplierDeltioListeners() {
    $('#deltio_submit').off();
    $('#deltio_form button').off('click');
    $('#deltio_form textarea').off();
    $('#deltio_form select').off();
    $('#ordersInProgress tbody button').off('click');
    $('#parastatikaInventory tbody button').off('click');
}

function bindSupplierDeltioListeners() {
    $('#parastatikaInventory tbody button[name="relativeDeltio"]').click(function () {
        var row=$(this).closest('tr');
        var promitheutis=$(row).find('input[name="id_supplier"]').val();
        var par_id=$(row).find('input[name="par_id"]').val();
        var deltio_type=parseInt(parastatikaInfo[par_id]['flow_flag']);
        var id=$(row).find('input[name="id"]').val();
        var result=getSupplierParastatikoInfo(id);
        result.then(function (json) {
            console.log(json);
            var data=JSON.parse(json);
            if (data.response){
                $(row).find('button[name="relativeDeltio"]').tooltip('show');
                setTimeout(function () {
                    $(row).find('button[name="relativeDeltio"]').tooltip('hide');
                },1500);
                supplier=parseInt(promitheutis);
                $('#deltio_type')
                    .val(deltio_type)
                    .prop('disabled',true)
                    .trigger('change');
                $('#parastatiko_type')
                    .val(par_id)
                    .prop('disabled',true)
                    .trigger('change');
                data.details.forEach(function (row) {
                    var product={
                        'name':row.perigrafi,
                        'id':row.proion_id,
                        'siskevasia':row.temaxia,
                        'tmima':row.tmima,
                        'posotita':row.quantity,
                        'timi_agoras':row.timi_agoras,
                        'apothema':row.stock,
                        'mothercode':row.mothercode,
                        'barcode':row.barcode,
                        'id_om':row.id_omadas,
                        'zigos':row.zigos
                    };
                    appendDeltioProduct(product);
                });
                var matching_par=$('#deltio_form textarea[name="matching_parastatiko"]').text();
                if (matching_par===""){
                    matching_par=matching_par+id;
                } else {
                    matching_par=matching_par+'&#13;&#10;'+id;
                }
                $('#deltio_form textarea[name="matching_parastatiko"]').html(matching_par);
            }else {
                bootboxMessage(data.error,2);
            }
        });
    });

    $('#ordersInProgress tbody button').click(function () {
        var name=$(this).attr('name');
        var row=$(this).closest('tr');
        var promitheutis=$(row).find('input[name="id_supplier"]').val();
        var order=$(row).find('input[name="id_order"]').val();
        if (name==='removeOrder'){
            var result=cancelOrder(order,name);
            result.then(function (json) {
                console.log(json);
                var data=JSON.parse(json);
                if (data.response){
                    $(row).remove();
                }else {
                    bootboxMessage(data.error,2);
                }
            });
        } else {
            var result=getOrderInfo(order);
            result.then(function (json) {
                console.log(json);
                var data=JSON.parse(json);
                if (data.response){
                    supplier=parseInt(promitheutis);
                    data.orders.forEach(function (row) {
                        var product={
                            'name':row.perigrafi,
                            'id':row.proion_id,
                            'siskevasia':row.temaxia,
                            'tmima':row.tmima,
                            'posotita':row.quantity,
                            'timi_agoras':row.timi_agoras,
                            'order_id':order,
                            'apothema':row.stock,
                            'mothercode':row.mothercode,
                            'barcode':row.barcode,
                            'id_om':row.id_omadas,
                            'zigos':row.zigos
                        };
                        appendDeltioProduct(product);
                    });
                    $(row).find('button[name="addToDeltio"]').tooltip('show');
                    setTimeout(function () {
                        $(row).find('button[name="addToDeltio"]').tooltip('hide');
                    },1500);
                    $('#deltio_type')
                        .val(1)
                        .prop('disabled',true)
                        .trigger('change');
                }else {
                    bootboxMessage(data.error,2);
                }
            });
        }


    });

    $('#deltio_type').change(function () {
        var d = new Date();
        var today = d.getFullYear() + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" + ("0"+d.getDate()).slice(-2);
        var deltio=parseInt($(this).val());
        $('#deltio_form .parastatiko_extra').empty();
        $('#deltio_form .search_parastatiko').show();
        $('#deltio_submit .pliromi_paralavis').hide();
        $('#deltio_submit .orderBtn').hide();
        $('#deltio_submit .form_paralavis').show();
        unbindSupplierDeltioListeners();
        if (deltio===0){//forma paraggelias
            $('#deltio_submit .form_paralavis').hide();
            $('#deltio_submit .orderBtn').show();
            $('#deltio_form .search_parastatiko').hide();
        }else if (deltio===1) {//forma paralavis
            $('#deltio_form .parastatiko_extra').append('<select id="parastatiko_type" required>' +
                '<option value="">-επιλέξτε παραστατικό-</option>' +
                '</select>' +
                '<input name="parastatiko" placeholder="Αριθμός παραστατικού">' +
                '<div class="parastatiko_date">' +
                '<input type="date" value="'+today+'"></div>');
            $.each(parastatikaInfo,function (index,value) {
                if (parseInt(value.flow_flag)===1){
                    $('#parastatiko_type').append('<option value="'+index+'">'+value.name+'</option>');
                }
            });
        }else {//forma epistrofis
            $('#deltio_form .parastatiko_extra').append('<select id="parastatiko_type" required>' +
                '<option value="">-επιλέξτε παραστατικό-</option>' +
                '</select>' +
                '<input name="parastatiko" placeholder="Αριθμός παραστατικού">' +
                '<div class="parastatiko_date">' +
                '<input type="date" value="'+today+'"></div>');
            $.each(parastatikaInfo,function (index,value) {
                if (parseInt(value.flow_flag)===2){
                    $('#parastatiko_type').append('<option value="'+index+'">'+value.name+'</option>');
                }
            });
        }
        if ($.trim($('#deltio_submit tbody').html())!==''){
            changeDeltioInputAttrs();
        }
        bindSupplierDeltioListeners();
    });

    $('#parastatiko_type').change(function () {
        var id=parseInt($(this).val());
        var parastatiko=parseInt(parastatikaInfo[id]['update_flag']);
        $('#supplierTicket .update_stock').prop('disabled',false);
        $('#deltio_form .search_parastatiko').show();
        if (parastatiko===0){//enimerosi apothematos
            $('#deltio_submit .pliromi_paralavis').hide();
            $('#supplierTicket .update_stock').prop('checked',true);
        } else if (parastatiko===1){//enimerosi aksias
            $('#deltio_submit .pliromi_paralavis').show();
            $('#supplierTicket .update_stock').prop({'checked':false,'disabled':true});
        }else {//enimerosi apothematos kai aksias
            $('#deltio_form .search_parastatiko').hide();
            $('#deltio_submit .pliromi_paralavis').show();
        }
        if ($.trim($('#deltio_submit tbody').html())!==''){
            changeDeltioInputAttrs();

        }
    });

    $('#addToDeltio .submitbtn').click(function () {
        if (!$('#addToDeltio input[name="productId"]').val()) {
            bootboxMessage('Δεν έχετε επιλέξει κάποιο προίόν', 3);
        } else {
            if ($('#addToDeltio input[name="productStockRecommend"]')[0].checkValidity()) {
                var data = {
                    "name": $('#addToDeltio input[name="productName"]').val(),
                    "siskevasia": $('#addToDeltio input[name="productSiskevasia"]').val(),
                    "id": $('#addToDeltio input[name="productId"]').val(),
                    "posotita": $('#addToDeltio input[name="productStockRecommend"]').val(),
                    "timi_agoras": $('#addToDeltio input[name="productBuyPrice"]').val(),
                    "tmima": $('#addToDeltio input[name="productTmima"]').val(),
                    "apothema": $('#addToDeltio input[name="productStock"]').val(),
                    "mothercode": $('#addToDeltio input[name="productMcode"]').val(),
                    "barcode": $('#addToDeltio input[name="productBcode"]').val(),
                    "id_om": $('#addToDeltio input[name="productIdOm"]').val(),
                    "zigos": $('#addToDeltio input[name="productZigos"]').val()
                };
                appendDeltioProduct(data);
                $('#addToDeltio input').val('');
                $('#addToDeltio .supplierBarcodeBtn').focus();
            } else {
                $('#addToDeltio input[name="productStockRecommend"]')[0].reportValidity();
            }
        }
    });

    $('#deltio_form tbody select[name="choose_discount"]').change(function () {
        var row = $(this).closest('tr');
        var sinolo_proion = $(row).find('input[name="sinolo_proion"]');
        var ekptosi_proion = $(row).find('input[name="ekptosi_proion"]');
        ekptosi_proion.val(0).trigger('change');
        sinolo_proion .css({
            "text-decoration":"none",
        });
        $(row).find('input[name="sinolo_me_ekptosi"]')
            .val(sinolo_proion.val())
            .hide();
        if (parseInt($(this).val())===0){//aksiaki
            ekptosi_proion.prop({
                'step':0.01,
                'max':sinolo_proion.val()
            });
        } else {//epi tis %
            ekptosi_proion.prop({
                'step':1,
                'max':99
            });
        }
    });

    $('#deltio_form tbody input, #deltio_form tbody select[name="fpa_proion"]').change(function () {
        if ($(this).val()===''){
            $(this).val(0);
        }
        var nea_aksia_fpa,neo_teliko_sinolo,neo_sinolo,old_ekptosi,nea_ekptosi;
        var diafora_sinolou=0;
        var diafora_ekptosis=0;
        var diafora_fpa=0;
        var diafora_telikou=0;
        var row = $(this).closest('tr');
        var name=$(this).attr('name');
        var sinolo_proion = $(row).find('input[name="sinolo_proion"]');
        var ekptosi_proion = $(row).find('input[name="ekptosi_proion"]');
        var fpa_proion = $(row).find('select[name="fpa_proion"]');
        var aksia_fpa_proion = $(row).find('input[name="aksia_fpa_proion"]');
        var teliko_sinolo_proion = $(row).find('input[name="sunolo"]');
        var sinolo_me_ekptosi = $(row).find('input[name="sinolo_me_ekptosi"]');
        var type_of_discount = $(row).find('select[name="choose_discount"]').val();
        type_of_discount=parseInt(type_of_discount);
        if (name==="price" || name==="temaxia"){
            var old_sinolo = parseFloat(sinolo_proion.val());
            var posotita = $(row).find('input[name="temaxia"]').val();
            var timi = $(row).find('input[name="price"]').val();
            neo_sinolo = parseFloat(posotita) * parseFloat(timi);
            diafora_ekptosis=-(parseFloat(sinolo_proion.val())-parseFloat(sinolo_me_ekptosi.val()));
            ekptosi_proion.val(0);
            nea_aksia_fpa=neo_sinolo*(parseFloat(fpa_proion.val()));
            diafora_fpa=nea_aksia_fpa-(aksia_fpa_proion.val());
            diafora_fpa=diafora_fpa.toFixed(2);
            aksia_fpa_proion.val(nea_aksia_fpa.toFixed(2));
            neo_teliko_sinolo=neo_sinolo+nea_aksia_fpa;
            diafora_telikou=neo_teliko_sinolo-(teliko_sinolo_proion.val());
            teliko_sinolo_proion.val(neo_teliko_sinolo.toFixed(2));
            if (type_of_discount===0){//aksiaki
                ekptosi_proion.prop({
                    'max':neo_sinolo
                });
            }
            neo_sinolo = neo_sinolo.toFixed(2);
            sinolo_proion
                .css({
                "text-decoration":"none",
            }).val(neo_sinolo);
            sinolo_me_ekptosi
                .val(neo_sinolo)
                .hide();
            neo_sinolo = parseFloat(neo_sinolo);
            if (old_sinolo !== neo_sinolo) {
                diafora_sinolou = neo_sinolo - old_sinolo;
            }
        }else{
            if (name==="fpa_proion"){
                nea_aksia_fpa=(parseFloat(sinolo_me_ekptosi.val())*parseFloat(fpa_proion.val())).toFixed(2);
                nea_aksia_fpa=parseFloat(nea_aksia_fpa);
                diafora_fpa=nea_aksia_fpa-(aksia_fpa_proion.val());
                aksia_fpa_proion.val(nea_aksia_fpa);
                neo_teliko_sinolo=parseFloat(sinolo_me_ekptosi.val())+nea_aksia_fpa;
                diafora_telikou=neo_teliko_sinolo-(teliko_sinolo_proion.val());
                teliko_sinolo_proion.val(neo_teliko_sinolo.toFixed(2));
            }else {
                if (ekptosi_proion[0].checkValidity()) {
                    old_ekptosi=parseFloat(sinolo_proion.val())-parseFloat(sinolo_me_ekptosi.val());
                    if (type_of_discount===0){//aksiaki
                        nea_ekptosi=parseFloat(ekptosi_proion.val());
                    } else {
                        nea_ekptosi=(parseInt(ekptosi_proion.val())/100)*parseFloat(sinolo_proion.val());
                    }
                    sinolo_me_ekptosi.val((parseFloat(sinolo_proion.val())-nea_ekptosi).toFixed(2));
                    nea_aksia_fpa=(parseFloat(sinolo_me_ekptosi.val())*parseFloat(fpa_proion.val())).toFixed(2);
                    nea_aksia_fpa=parseFloat(nea_aksia_fpa);
                    diafora_fpa=nea_aksia_fpa-(aksia_fpa_proion.val());
                    aksia_fpa_proion.val(nea_aksia_fpa);
                    neo_teliko_sinolo=parseFloat(sinolo_me_ekptosi.val())+parseFloat(aksia_fpa_proion.val());
                    diafora_telikou=neo_teliko_sinolo-parseFloat(teliko_sinolo_proion.val());
                    old_ekptosi=old_ekptosi.toFixed(2);
                    diafora_ekptosis=nea_ekptosi-parseFloat(old_ekptosi);
                    teliko_sinolo_proion.val(neo_teliko_sinolo.toFixed(2));
                    diafora_ekptosis=diafora_ekptosis.toFixed(2);
                    diafora_telikou=diafora_telikou.toFixed(2);
                    if (parseFloat(ekptosi_proion.val())>0){
                        sinolo_me_ekptosi.show();
                        sinolo_proion.css({
                            "text-decoration":"line-through",
                            "text-decoration-color":"red"
                        });
                    } else {
                        sinolo_me_ekptosi.hide();
                        sinolo_proion.css({
                            "text-decoration":"none",
                        });
                    }
                }else {
                    ekptosi_proion[0].reportValidity();
                    return false;
                }
            }
        }
        updateDeltioSums(diafora_sinolou,diafora_ekptosis,diafora_fpa,diafora_telikou);
    });

    $('#deltio_form tbody button').click(function () {
        var row = $(this).closest('tr');
        var sinolo_proion = $(row).find('input[name="sinolo_proion"]').val();
        var ekptosi_proion = $(row).find('input[name="ekptosi_proion"]').val();
        var aksia_fpa_proion = $(row).find('input[name="aksia_fpa_proion"]').val();
        var teliko_sinolo_proion = $(row).find('input[name="sunolo"]').val();
        sinolo_proion = -parseFloat(sinolo_proion);
        ekptosi_proion=-parseFloat(ekptosi_proion);
        aksia_fpa_proion=-parseFloat(aksia_fpa_proion);
        teliko_sinolo_proion=-parseFloat(teliko_sinolo_proion);
        updateDeltioSums(sinolo_proion,ekptosi_proion,aksia_fpa_proion,teliko_sinolo_proion);
        $(row).remove();
    });

   $('#deltio_form textarea[name="matching_parastatiko"]').click(function () {
       if ($('#parastatiko_type')[0].checkValidity()){
           var par_id=parseInt($('#parastatiko_type').val());
           var flowflag=parastatikaInfo[par_id]['flow_flag'];
           var updateflag=parastatikaInfo[par_id]['update_flag'];
           var data={'supplier':supplier,'flow_flag':flowflag,'update_flag':updateflag};
           var action='matchSupplierParastatiko';
           var result=matchParastatika(data,action);
           result.then(function (json) {
               console.log(json);
               var data=JSON.parse(json);
               if (data.response){
                   $('#supplierTicket').modal('hide');
                   var deltioType,id;
                   $('#parastatikaInventory .table-responsive').empty();
                   $('#parastatikaInventory .table-responsive').append('<table class="table table-hover">' +
                       '<thead>' +
                       '<tr>' +
                       '<th style="width: 2%">#</th>' +
                       '<th style="width: 18%">Προμηθευτής</th>' +
                       '<th style="width: 18%">Δελτίο</th>' +
                       '<th style="width: 18%">Παραστατικό</th>' +
                       '<th style="width: 18%">Α/α</th>' +
                       '<th style="width: 18%">Ημερομηνία Καταχώρησης</th>' +
                       '<th style="width: 8%"></th>' +
                       '</tr>' +
                       '</thead>' +
                       '<tbody></tbody></table>');
                   data.body.forEach(function (row) {
                       id=parseInt(row.type);
                       if (parseInt(parastatikaInfo[id]['flow_flag'])===1){
                           deltioType='Παραλαβής';
                       } else {
                           deltioType='Επιστροφής';
                       }
                       $('#parastatikaInventory tbody').append('<tr>' +
                           '<td style="width: 2%"><input name="id" hidden value="'+row.id+'">' +
                           '<input name="par_id" value="'+par_id+'" hidden>' +
                           '<input name="id_supplier" hidden value="'+row.supplier_id+'"></td>' +
                           '<td style="width: 18%">'+row.name+'</td>' +
                           '<td style="width: 18%">'+deltioType+'</td>' +
                           '<td style="width: 18%">'+row.shortname+'</td>' +
                           '<td style="width: 18%">'+row.aa+'</td>' +
                           '<td style="width: 18%">'+row.parastatiko_date+'</td>' +
                           '<td style="width: 8%"><button name="relativeDeltio" type="button" class="fas fa-clipboard-check submitbtn" data-toggle="tooltip" data-placement="top" data-trigger="manual" data-title="Προστέθηκε στο δελτίο"></button></td>' +
                           '</tr>');
                   });
                   $('#parastatikaInventory').modal('show');
               }else {
                   bootboxMessage('Δεν υπάρχουν παραστατικά που να σχετίζονται με αυτό το δελτίο',3);
               }
           });
       } else {
           $('#parastatiko_type')[0].reportValidity();
       }
    });

    $('#deltio_submit').submit(function (e) {
        e.preventDefault();
        if ($.trim($('#deltio_submit tbody').html())!==''){
            var form = $('#deltio_submit').serializeArray();
            var data = {};
            data['receipt'] = {};
            var i = 0;
            var action,pliroteo,pistosi,id;
            var deltio=parseInt($('#deltio_form select').val());
            if ($('#deltio_form textarea[name="matching_parastatiko"]').text()!==''){
                data['matched']=$('#deltio_form textarea[name="matching_parastatiko"]').text();
                data['matched']=data['matched'].split(/\r\n/);
            }else {
                data['matched']='NULL';
            }
            if (deltio=== 0) {// deltio paraggelias
                action = 'insertDeltioParaggelias';
                data['arithmos_parastatikou'] = 'NULL';
                data['date']='NULL';
                data['invoice']=1;
                data['parastatiko_type']=0;
                data['payments'] = [
                    {'amount': 0, 'id': 1,'description':''},
                    {'amount': 0, 'id': 2,'description':''},
                    {'amount': 0, 'id': 3,'description':''}
                ];
            } else if (deltio===1) {//deltio paralavis
                action = 'insertDeltioParalavis';
                data['arithmos_parastatikou'] = $('#deltio_form input[name="parastatiko"]').val();
                data['date']=$('#deltio_form input[type="date"]').val();
                if ($('#parastatiko_type')[0].checkValidity()){
                    id=parseInt($('#parastatiko_type').val());
                    if (id===3){//apodeiksi lianikis polisis
                        data['invoice']=0;
                    }else {
                        data['invoice']=1;
                    }
                    if (parseInt(parastatikaInfo[id]['update_flag'])!==0){//ean den einai deltio apostolis
                        pliroteo = parseFloat($('#deltio_form input[name="pliroteo_deltiou"]').val());
                        if (pliroteo===''){
                            pliroteo=0;
                        }
                        pistosi = (parseFloat($('#deltio_form input[name="teliko_sinolo_deltiou"]').val()))-pliroteo;
                    }else {//deltio apostolis
                        pliroteo=0;
                        pistosi=0;
                    }
                    data['parastatiko_type']=id;
                    data['payments'] = [
                        {'amount': pistosi, 'id': 1,'description':''},
                        {'amount': pliroteo, 'id': 2,'description':''},
                        {'amount': 0, 'id': 3,'description':''}
                    ];
                    if ($('#supplierTicket .update_price').is(':checked')) {
                        data['update_price'] = 1;
                    } else {
                        data['update_price'] = 0;
                    }
                    if ($('#supplierTicket .update_stock').is(':checked')) {
                        data['update_stock'] = 1;
                    } else {
                        data['update_stock'] = 0;
                    }
                }else {
                    $('#parastatiko_type')[0].reportValidity();
                    return false;
                }
            }else {//deltio epistrofis
                action = 'insertDeltioEpistrofis';
                data['arithmos_parastatikou'] = '';
                data['date']='NULL';
                data['invoice']=1;
                if ($('#parastatiko_type')[0].checkValidity()){
                    id=parseInt($('#parastatiko_type').val());
                    if (parseInt(parastatikaInfo[id]['update_flag'])!==0){//ean den einai deltio apostolis
                        pliroteo = parseFloat($('#deltio_form input[name="pliroteo_deltiou"]').val());
                        if (pliroteo===''){
                            pliroteo=0;
                        }
                        pistosi = (parseFloat($('#deltio_form input[name="teliko_sinolo_deltiou"]').val()))-pliroteo;
                        if (pistosi!==0){
                            pistosi=pistosi*(-1);
                        }
                        if (pliroteo!==0){
                            pliroteo=pliroteo*(-1);
                        }
                    }else {//deltio apostolis
                        pliroteo=0;
                        pistosi=0;
                    }
                    data['parastatiko_type']=id;
                    data['payments'] = [
                        {'amount': pistosi, 'id': 1,'description':''},
                        {'amount': pliroteo, 'id': 2,'description':''},
                        {'amount': 0, 'id': 3,'description':''}
                    ];
                    if ($('#supplierTicket .update_price').is(':checked')) {
                        data['update_price'] = 1;
                    } else {
                        data['update_price'] = 0;
                    }
                    if ($('#supplierTicket .update_stock').is(':checked')) {
                        data['update_stock'] = 1;
                    } else {
                        data['update_stock'] = 0;
                    }
                }else {
                    $('#parastatiko_type')[0].reportValidity();
                    return false;
                }
            }
            form.forEach(function (field) {
                if (field.name === "pliroteo_deltiou") {
                    return true;
                }
                if (field.name === "id") {
                    data['receipt'][i] = {};
                }
                data['receipt'][i][field.name] = field.value;
                if (field.name === "sunolo") {
                    i++;
                }
            });
            var j=0;
            var reorder={};
            $.each(data['receipt'],function (index,proion) {
                if (parseInt(proion.order_no)!==0){
                    if (parseFloat(proion.temaxia)!==parseFloat(proion.rest_quantity)){
                        var new_posotita=parseFloat(proion.rest_quantity)-parseFloat(proion.temaxia);
                        reorder[j]={
                            'name':proion.desc,
                            'id':proion.id_proion,
                            'siskevasia':proion.package,
                            'tmima':proion.tmima,
                            'posotita':new_posotita,
                            'timi_agoras':proion.price,
                            'apothema':proion.stock,
                            'mothercode':proion.mothercode,
                            'barcode':proion.barcode,
                            'id_om':proion.id_om,
                            'zigos':proion.zigos
                        };
                        j++;
                    }
                }
            });
            if ($('#supplierTicket .print_invo').is(':not(:checked)')) {
                data['invoice'] = 2;
            }
            var pinakas = {'supplier': supplier, 'deltio': data, 'aa': data['arithmos_parastatikou'], 'date':data['date']};
            var result = insertParastatiko(pinakas, action);
            result.then(function (json) {
                console.log(json);
                var data = JSON.parse(json);
                if (data.response) {
                    $('#supplierTicket').modal('hide');
                    if (!(jQuery.isEmptyObject(reorder))){
                        bootbox.confirm({
                            message: "Το παραστατικό αποθηκέυτηκε! Σε κάποια προιόντα παραλάβατε λιγότερα απ'όσα παραγγείλατε. Θέλετε να εκδόσετε νέα παραγγελία με τις διαφορές?",
                            buttons: {
                                confirm: {
                                    label: 'Ναι',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'Όχι',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function (result) {
                                if (result){
                                    $.each(reorder,function (index,proion) {
                                        appendDeltioProduct(proion);
                                    });
                                    $('#supplierTicket').modal('show');
                                }
                            }
                        });
                    }else {
                        bootboxMessage('Το παραστατικό κατοχυρώθηκε επιτυχώς!', 2);
                    }
                } else {
                    bootboxMessage(data.error, 1);
                }
            });
        } else {
            bootboxMessage('Το δελτίο είναι κενό',3);
        }

    });

}