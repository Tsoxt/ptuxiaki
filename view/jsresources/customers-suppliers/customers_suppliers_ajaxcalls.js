$.ajaxSetup({
    type: "POST",
    datatype:"json",
    beforeSend: function () {
        $('.ajax-loader').show();
    },
    url:    "../include/resources/dispatcher.php",
    //timeout: 7000,
    cache:false,
    async:true,
    complete: function (xhr){
        xhr.done(function(json){
            var data=JSON.parse(json);
            if(!data['loginStatus']){
                logout();
            }
            $('.ajax-loader').hide();
        });
    },
    error: function(xhr,ajaxOptions,thrownError){
        $('.ajax-loader').hide();
        var msg="Πρόβλημα δικτύου \n http status: "+xhr.status+", σφάλμα: "+thrownError;
        bootboxMessage(msg,1);
    }
});

function userInfo(){
    var action='userinfo';
    return $.ajax({
        data:   {"action":  action},
        datatype:   "json"
    });
}

//<editor-fold desc="Products-calls">
function callCategories() {
    var action = 'callcategories';
    return $.ajax({
        data: {"action":action},
        datatype: "json"
    });
}

function callOmades(id){
    var action = 'callomades';
    var data = {category:id};
    data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function callItems(omadaId){
    var action = 'callitems';
    var data = {omada:omadaId};
    data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function searchItem(id, action) {
    var data = {id:id};
    data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function getFpa(id) {
    var data = {id:id};
    var action='bytmima';
    data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });

}
//</editor-fold>

//<editor-fold desc="Parastatika-calls">
function getTableParastatikaInfo() {
    var action='getallparastatika';
    return $.ajax({
        data: {"action":action},
        datatype: "json"
    });
}
//</editor-fold>

//<editor-fold desc="Suppliers-calls">
function getOrderInfo(order) {
    var action='getOrderDetails';
    var data={order_id:order};
    var data=JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function insertParastatiko(data,action) {
    var data=JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function cancelOrder(order,name) {
    var data={order_id:order};
    var data=JSON.stringify(data);
    return $.ajax({
        data: {"action":name, "data":data},
        datatype: "json"
    });
}

function getOrders(data) {
    var action='getSuppliersOrders';
    var data=JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function matchParastatika(data,action) {
    var data=JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function findSupplier(input,type) {
    var data={search:input,type:type};
    data = JSON.stringify(data);
    var action = 'searchSupplier';
    return $.ajax({
        data: {"action": action, "data":data},
        datatype: "json"
    });
}

function updateSupplier(data) {
    var action='updateSupplier';
    var data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function insertSupplier(data) {
    var data=JSON.stringify(data);
    var action='insertSupplier';
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function getSupplierParastatikoInfo(parastatiko) {
    var data={id:parseInt(parastatiko)};
    data=JSON.stringify(data);
    var action='supplierParastatikoInfo';
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function getSupplierParastatika(data) {
    var data=JSON.stringify(data);
    var action='getSupplierParastatika';
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function getSupplierProducts(id) {
    var action='getSupplierProducts';
    var data={supplier:parseInt(id)};
    data=JSON.stringify(data);
    return $.ajax({
        data:   {"action":action, "data":data},
        datatype:   "json"
    });
}

function insertSupplierProduct(data,action) {
    var data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function handleSupplierProduct(data,action) {
    var data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function eksoflisiYpoloipo(data) {
    var action='paySupplierYpoloipo';
    var data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });

}
//</editor-fold>

//<editor-fold desc="Customers-calls">
function getCustomerParastatikoInfo(parastatiko) {
    var data={id:parseInt(parastatiko)};
    data=JSON.stringify(data);
    var action='customerParastatikoInfo';
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function reprintTimologio(id) {
    var data={id:id};
    var data=JSON.stringify(data);
    var action='reprintInvoice';
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function findCustomer(input,type) {
    var data={search:input,type:type};
    data = JSON.stringify(data);
    var action = 'searchcustomer';
    return $.ajax({
        data: {"action": action, "data":data},
        datatype: "json"
    });
}

function updateCustomer(data) {
    var action='updatecustomer';
    var data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function insertCustomer(data) {
    var data=JSON.stringify(data);
    var action='insertcustomer';
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function getCustomerParastatika(data) {
    var data=JSON.stringify(data);
    var action='getcustomerparastatika';
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });

}

function getCustomerDiscounts(id) {
    var action='getcustomerdiscounts';
    var data={customer:parseInt(id)};
    data=JSON.stringify(data);
    return $.ajax({
        data:   {"action":action, "data":data},
        datatype:   "json"
    });
}

function insertCustomerDiscount(data, action) {
    var data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function handleCustomerDiscount(data, action) {
    var data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}
//</editor-fold>