function bindItemCallListeners(){
    $('.items button').on('click',function () {
        var id=$(this).val();
        callproion(id,1);
        return true;
    });

    $('.rowbutton button').on('click',function(){
        var id=$(this).val();
        callproion(id,2);
        return true;
    });

    $('#barcode').keypress(function (e) {
        if(e.keyCode===13) {
            var value=$('#barcode_search').val();
            $('#barcode_search').val('');
            if (value.length===8 || value.length===13 || value.length===15 || value.length===26){
                callproion(value, 0);
            }
            return true;
        }else{
            var input=$('#barcode_search').val();
            var key=e.key;
            $('#barcode_search').val(input+key);
        }
    });

    $('#bcodenumbers').keypress(function (e) {
        if(e.keyCode===13) {
            var value=$('#bcodenumbers').val();
            if ((value.length===8) || (value.length===13) || (value.length===15) || (value.length>=17)){
                callproion(value,0);
                $('#bcodeModal').modal('hide');
            }
            return true;
        }
    });
}

function unbindItemCallListeners(){
    $('.items button').off();
    $('.rowbutton button').off();
    $('#barcode').off();
    $('#bcodenumbers').off();
}

function bindCustomerListeners() {
    $('#customer_search').keyup(function () {
        clearTimeout(timer);
        timer=0;
        var input=this.value;
        if (input.length>=3){
            timer=setTimeout(function () {
                searchCustomer(input,2);
            },1000);
        } else {
            $('.customer_list').empty();
            $('.customer_form').show();
        }
    });

    $('.customer_list tbody button').click(function () {
        var td=$(this).closest('td');
        var id=$(td).find('input').val();
        searchCustomer(id,1);
        $('#customer_search')
            .val('')
            .blur();
    });

    $('.eispraksi').click(function () {
        var pliromi=$('input[name="pliromi"]').val();
        if ((parseInt(customer.id)!==0) && (pliromi!=='')){
            if (parseFloat(pliromi)>0){
                if ($('input[name="pliromi"]')[0].checkValidity()){
                    pliromi=Math.abs(pliromi)*-1;
                    var data={customer:parseInt(customer.id),amount:parseFloat(pliromi),operator:parseInt(user.id)};
                    var result=payYpoloipo(data);
                    result.then(function (json) {
                        console.log(json);
                        var data=JSON.parse(json);
                        if (data.response){
                            $('.eispraksi').tooltip('show');
                            setTimeout(function () {
                                $('.eispraksi').tooltip('hide');
                                searchCustomer(customer.id,1);
                            },1500);
                        }else {
                            bootboxMessage(data.error,1);
                        }
                    });

                } else {
                    $('input[name="pliromi"]')[0].reportValidity();
                }

            }
        }

    })
}

function unbindCustomerListeners() {
    $('.customer_list tbody button').off('click');
    $('#customer_search').off('keyup');
    $('.eispraksi').off('click');
}