function getActiveUsers() {
    var action='showactiveusers';
    return $.ajax({
        method: "POST",
        url: '../include/resources/dispatcher.php',
        data:   {"action":  action},
        datatype:   "json"
    });
}

function showActiveUsers(){
    var result=getActiveUsers();
    result.then(function (json) {
        console.log(json);
        var data=JSON.parse(json);
        if (data.response){
            data.users.forEach(function (user) {
                $('#loginform select').append('<option value="'+user.username+'">'+user.name+'</option>');
            });
            $('#loginform select').append('<option value="someone_else">Άλλος Χρήστης</option>');
            $('#loginform input[name="username"]').remove();
        }else {
            bootboxMessage(data.error,1);
            $('#loginform select').remove();
        }
    });
}

function checkAnotherUser() {
    $('#loginform input[name="passwd"]').val('');
    var user=$('#loginform select').val();
    if (user==='someone_else'){
        $('#loginform').prepend('<input type="text" id="username" name="username" class="form-control" placeholder="Όνομα χρήστη" required>');
        $('#loginform select').remove();
    }

}

function bootboxMessage(msg,type) {
    if (parseInt(type)===1) {
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="errormsg">'+msg+'</div>',
        });
    }else if (parseInt(type)===2){
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="successmsg">'+msg+'</div>',
        });
    }else {
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="infomsg">'+msg+'</div>',
        });
    }
}

$(document).ready(function () {
    showActiveUsers();
    $("#error").hide();

    $("#loginform").submit(function (event) {
        event.preventDefault();
        var form_pre = $("#loginform").serializeArray();
        var form = JSON.stringify(form_pre);
        var action = $("#login").val();
        $.ajax({
            method: "POST",
            url: '../include/resources/dispatcher.php',
            data: {"action": action, "data": form},
            datatype: "json",
            success: function (json) {
                console.log(json);
                var data = JSON.parse(json);
                if (data.response) {
                    if (typeof data.msg !=='undefined'){
                        bootbox.alert({ message:'<div class="infomsg">'+data.msg+'</div>', callback: function() {
                                sessionStorage.removeItem('standby');
                                window.location.replace('../index.php');
                            } });
                    }else {
                        sessionStorage.removeItem('standby');
                        window.location.replace('../index.php');
                    }
                } else {
                    if (typeof data.msg !=='undefined'){
                        bootbox.alert('<div class="errormsg">'+data.msg+'</div>');
                        if(data.lock){
                            window.location.replace('./serialValidation.php');
                        }
                    }else{
                        $("#error").show();
                    }

                }
            }
        });
    });
});