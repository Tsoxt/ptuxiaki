$.ajaxSetup({
    type: "POST",
    datatype:"json",
    beforeSend: function () {
        $('.ajax-loader').show();
    },
    url:    "../include/resources/dispatcher.php",
    timeout: 10000,
    cache: false,
    complete: function (xhr){
        xhr.done(function () {
            $('.ajax-loader').hide();
        });
    },
    error: function(xhr,ajaxOptions,thrownError){
        $('.ajax-loader').hide();
        var msg="Πρόβλημα δικτύου \n http status: "+xhr.status+", σφάλμα: "+thrownError;
        bootboxMessage(msg,1);
    }
});

function getProgramInfo() {
    var action='systeminfo';
    return $.ajax({
        data:   {"action":  action},
        datatype:   "json"
    });

}

function checkKeyValidity(key) {
    var action='checkValidation';
    return $.ajax({
        data:   {"action":action, "data":key},
        datatype:   "json"
    });
}

function steps(id) {
    $('.container').empty();
    var newdiv;
    if (parseInt(id)===1){//1 step
        newdiv='<div class="form-step">\n' +
            '        <div class="step-header">Βήμα 1</div>\n' +
            '        <div class="step-details">Επικοινωνήστε μαζί μας στο 2108140000 ή στείλτε μας με email στο ' +
            '<a href="mailto:licence@ecs.dummy?Subject=Ενεργοποίηση ECS-Αίτηση κωδικού ενεργοποίησης" target="_top">licence@epos.com.gr</a>' +
            ' το κλειδί σας για να σας στείλουμε τον κωδικό ενεργοποίησης. To κλειδί σας είναι: <b>'+key+'</b></div>\n' +
            '        <div class="step-actions"><button class="btn btn-primary" onclick="steps(2)" type="button">Επομένο ></button></div>\n' +
            '    </div>';
        $('.container').append(newdiv);

    }else if (parseInt(id)===2){//2 step
        newdiv='<form class="form-step" id="validationForm">\n' +
            '        <div class="step-header">Βήμα 2</div>\n' +
            '        <div class="step-details">\n' +
            '            <p>Εισάγετε το κλειδί ενεργοποίησης στα πεδία όπως ορίζονται</p>\n' +
            '            <input name="key[]" id="key1" placeholder="1ο μέρος" size="16" pattern="^.{16}$" required> -\n' +
            '            <input name="key[]" id="key2" placeholder="2ο μέρος" size="16" pattern="^.{16}$" required> -\n' +
            '            <input name="key[]" id="key3" placeholder="3ο μέρος" size="16" pattern="^.{16}$" required> -\n' +
            '            <input name="key[]" id="key4" placeholder="4ο μέρος" size="16" pattern="^.{16}$" required> -\n' +
            '            <input name="key[]" id="key5" placeholder="5ο μέρος" size="16" pattern="^.{16}$" required> -\n' +
            '            <input name="key[]" id="key6" placeholder="6ο μέρος" size="8" pattern="^.{8}$" required>\n' +
            '        </div>\n' +
            '        <div class="step-actions"><button class="btn btn-primary" type="button" onclick="steps(1)">< Πίσω</button><button class="btn btn-primary" type="submit">Επομένο ></button></div>\n' +
            '    </form>';
        $('.container').append(newdiv);
        bindListeners();
    }else if (parseInt(id)===3){//3 step
        newdiv='<div class="form-step">\n' +
            '        <div class="step-header">Βήμα 3</div>\n' +
            '        <div class="step-details">' +
            '<p>Η διαδικασία ενεργοποίησης ολοκληρώθηκε επιτυχώς! Σύντομα θα ανακατευθυνθείτε στην αρχική σελίδα...</p></div>\n' +
            '        <div class="step-actions"><button class="btn btn-success" onclick="" type="button">Πατήστε εδώ εάν δεν έγινε αυτόματη ανακατεύθυνση</button></div>\n' +
            '    </div>';
        $('.container').append(newdiv);
    }
}

function bindListeners() {
    $('#validationForm input').on('input',function (e) {
        var t = $( e.target );
        var maxlength;
        if (t.attr("id") !== "key6"){
            maxlength=16;
            if (e.target.value.length === maxlength) {
                t.next().focus();
            }
        }else {
            maxlength=8;
            if (e.target.value.length === maxlength) {
                t.blur();
            }
        }
        if (e.target.value.length > maxlength) {
            e.target.value = e.target.value.substr(0,maxlength);
        }
    }).on('paste',function (e) {
        e.preventDefault();
        var serial=e.originalEvent.clipboardData.getData('text/plain');
        serial=serial.replace(/(\r\n|\n|\r)/gm,"");
        for (var i=1;i<6;i++){
            var keyPart=serial.substring(0,16);
            $('#key'+i).val(keyPart);
            serial=serial.substring(17);
        }
        $('#key6').val(serial);
    });

    $('#validationForm').submit(function (e) {
        e.preventDefault();
        var form = $('#validationForm').serializeArray();
        var wholekey='';
        form.forEach(function (field) {
            wholekey+=field.value;
        });
        var result=checkKeyValidity(wholekey);
        result.then(function (json) {
            console.log(json);
            var data=JSON.parse(json);
            if (data.response){
                steps(3);
                setTimeout(function () {
                    window.location.replace('./login.php');
                },5000);
            } else {
                alert('Ο κωδικός που πληκτρολογήσατε δεν είναι έγκυρος');
            }

        })
    });
}


function start(){
    var result=getProgramInfo();
    result.then(function (json) {
        console.log(json);
        var data=JSON.parse(json);
        if (data.response){
            key=data.body.serial;
        }else {
            key='-Δεν είναι διαθέσιμο-'
        }
        steps(1);
    });
}

$(document).ready(function () {
    start();
});