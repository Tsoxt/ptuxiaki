function logout(){
    var action="logout";
    $.ajax({
        method: "POST",
        url: '../include/resources/dispatcher.php',
        data: {"action": action},
        datatype: "json",
        success: function () {
            window.location.replace('../index.php');
        }
    });
}

function navigation(nav,mode) {
    sessionStorage.setItem('mode',mode);
    if(nav===1){
        window.location.replace('./main.php');
    }else if(nav===2){
        window.location.replace('./apothiki.php');
    }else if(nav===3){
        window.location.replace('./reports.php');
    }else if(nav===4){
        window.location.replace('./settings.php');
    }
    else if(nav===5){
        logout();
    }else if (nav===6){
        window.location.replace('./customers-suppliers.php');
    }
    else if (nav===7){
        window.location.replace('./about.php');
    }
}