   $.ajaxSetup({
    type: "POST",
    datatype:"json",
    beforeSend: function () {
        $('.ajax-loader').show();
    },
    url:    "../include/resources/dispatcher.php",
    timeout: 7000,
    cache:false,
    complete: function (xhr){
        xhr.done(function(json){
            var data=JSON.parse(json);
            if(!data['loginStatus']){
                logout();
            }
            $('.ajax-loader').hide();
        });
    },
    error: function(xhr,ajaxOptions,thrownError){
        $('.ajax-loader').hide();
        var msg="Πρόβλημα δικτύου \n http status: "+xhr.status+", σφάλμα: "+thrownError;
        bootboxMessage(msg,1);
    }
});

function insertNewUser(data) {
    data = JSON.stringify(data);
    var action = 'insertnewuser';
    return $.ajax({
        data: {"action": action, "data":data},
        datatype: "json"
    });

}

function newScale(scale) {
    var data={group:scale};
    data = JSON.stringify(data);
    var action = 'insertsetting';
    return $.ajax({
        data: {"action": action, "data":data},
        datatype: "json"
    });
}

function updateUserField(user,value,action) {
    var data= {id: user,value:value};
    data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function showAllUsers() {
    var action='getallusers';
    return $.ajax({
        data: {"action":action},
        datatype: "json"
    });

}

function showGeneralSettings(data) {
    var action='getSettings';
    var data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action,"data":data},
        datatype: "json"
    });

}

function updateSettingInput(value, id) {
    var data = {value:value,id:id};
    var action = 'updatesetting';
    data = JSON.stringify(data);
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });
}

function getSpecialBarcodes() {
    var action='getallspecial';
    return $.ajax({
        data: {"action":action},
        datatype: "json"
    });
}

function updateSpecial(data) {
    var data=JSON.stringify(data);
    var action='updatespecial';
    return $.ajax({
        data: {"action":action, "data":data},
        datatype: "json"
    });

}


function bootboxMessage(msg,type) {
    if (parseInt(type)===1) {
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="errormsg">'+msg+'</div>'
        });
    }else if (parseInt(type)===2){
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="successmsg">'+msg+'</div>'
        });
    }else {
        bootbox.alert({
            size:"small",
            title:"Μήνυμα",
            message:'<div class="infomsg">'+msg+'</div>'
        });
    }
}

function updateSettingRow(value){
    var name=$('#row'+value+' [name="value"]').val();
    var id=$('#row'+value+' input[name="id"]').val();
    $('form').submit(function (event) {
        event.preventDefault();
        var result=updateSettingInput(name,id);
        result.then(function (json) {
            var data=JSON.parse(json);
            if (data.response){
                $('#row'+value+' button').hide();
                $('#row'+value+' .setbtn').append('<p class="quickmessage">Αποθηκέυτηκε!</p>');
                setTimeout(function () {
                    $('#row'+value+' .setbtn p').remove();
                },1500);
                $('form').off();
            } else{
                bootboxMessage(data.error,1);
            }
            return false;
        });
    });

}

function saveScale() {
    $('#scale_insert').submit(function (e) {
        e.preventDefault();
        var form=$('#scale_insert').serializeArray();
        var data= [];
        var i=0;
        form.forEach(function (field) {
            if (field.name==="value"){
                data[i]={};
            }
            data[i][field.name]=field.value;
            if (field.name==="name"){
                i++;
            }
        });
        var result=newScale(data);
        result.then(function (json) {
            console.log(json);
            var data=JSON.parse(json);
            if (data.response){
                bootboxMessage('Επιτυχής καταχώρηση',2);
                start();
            } else {
                bootboxMessage(data.error,1);
            }
        });
    });
}


function showButton(value) {
   $('#row'+value+' .inputval').bind('change',function () {
        $('#row'+value+' button').show();
    });
}

function scalesHandler(value){
    if (parseInt(value) === 1) {
        if (!$('#btn3').length) {
            $('#btn2').after('<button class="sidebuts" id="btn3" type="button"><i class="fas fa-balance-scale"></i>' +
                '<span>Ζυγαριές</span></button>');
            unbindSideButtonListeners();
        }
    } else {
        $('#btn3').remove();
        $('#scale_settings').empty();
    }
}

function toggleSetting() {
    var id=$(this).parent().parent().attr('id');
    var value=$('#'+id+' input[name="value"]').val();
    var toggleId=parseInt($('#'+id+' input[name="id"]').val());
    if (parseInt(value)===1){
        value=0;
    } else{
        value=1;
    }
    var result=updateSettingInput(value,toggleId);
    result.then(function (json) {
        console.log(json);
        var data=JSON.parse(json);
        if (data.response){
            $('#'+id+' .togglesettingbut i').toggle();
            $('#'+id+' input[name="value"]').val(value);
            if (toggleId===21){//ean einai i ennalagi stin zigaria
                scalesHandler(value);
            }else if(toggleId===20){//ean einai ta special barcodes
                specialBarcodes();
            }
        } else {
            bootboxMessage(data.error,1);
        }
    });
}

function specialBarcodes() {
    $('#special_codes').empty();
    var result=getSpecialBarcodes();
    result.then(function (json) {
        console.log(json);
        var data=JSON.parse(json);
        if (data.response){
            $('#special_codes').append('<table class="table"><caption style="text-align: center">Προθέματα Barcode</caption><thead><tr><th></th><th>Βάρος</th><th>Αξία</th>' +
                '<th>Ενεργό</th><th></th></tr></thead><tbody></tbody></table>');
            data.body.forEach(function (group) {
                var row=''+
                    '<tr id="special'+group.start+'">\n' +
                    '<td class="col-sm-3">'+group.start+'</td>\n' +
                    '<td class="weight_radio col-sm-2"><input type="radio" name="radio'+group.start+'" value="0"></td>\n' +
                    '<td class="price_radio col-sm-2"><input type="radio" name="radio'+group.start+'" value="1"></td>\n' +
                    '<td class="col-sm-2"><input type="checkbox" name="active"  value="'+parseInt(group.active)+'"></td>\n' +
                    '<td class="quick_msg col-sm-3"></td>\n' +
                    '</tr>';
                $('#special_codes tbody').append(row);
                if (parseInt(group.type)===0){//varos
                    $('.weight_radio input[name="radio'+group.start+'"]').prop('checked',true);
                }else {//aksia
                    $('.price_radio input[name="radio'+group.start+'"]').prop('checked',true);
                }
                if (parseInt(group.active)===1){
                    $('#special'+group.start+' input[name="active"]').prop('checked',true);
                }
            });
            if (data.pretyped){
                $('#special26').addClass("danger");
                $('#special26 input').prop('disabled',true);
                $('#special27').addClass("danger");
                $('#special27 input').prop('disabled',true);
                $('#special29').addClass("danger");
                $('#special29 input').prop('disabled',true);
            }
        }else {
            bootboxMessage(data.error,1);
        }
        bindSpecialCodesListeners();
    });

}

function scales() {
    var result=showGeneralSettings(100);
    result.then(function (json) {
        var data=JSON.parse(json);
        if (data.response){
            $('.maincolumn').append('<div class="formTab" id="groupset3">' +
                '<ul class="nav nav-tabs" role="tablist"></ul>' +
                '<div class="tab-content"></div> ' +
                '</div>');
            $.each(data.body,function (group,settings) {
                $('#groupset3 ul').append('<li role="presentation"><a href="#scales'+group+'" aria-controls="scales'+group+'" role="tab" data-toggle="tab">' + settings.groupname.description + '</a></li>');
                $('#groupset3 .tab-content').append('<div role="tabpanel" class="tab-pane fade" id="scales'+group+'">' +
                    '<div class="tabcontent">' +
                    '<form class="formset" method="post" action=""></form>' +
                    '</div>' +
                    '</div>');
                $.each(settings, function (setting, attribute) {
                    var formdata;
                    if (setting === 'groupname') {
                        formdata = '' +
                            '<div class="rowform" id="row' + attribute.id + '">\n' +
                            '   <span class="spanval">Κατάσταση :</span>\n' +
                            '   <div class="togglesettingbut"><button type="button"></button></div>\n' +
                            '   <input name="value" value="' + attribute.value + '" type="hidden">\n' +
                            '   <input name="id" value="' + attribute.id + '" hidden>\n' +
                            '   <div class="setbtn"></div>\n' +
                            '</div>';
                        $('#scales' + group + ' form').prepend(formdata);
                        if (parseInt(attribute.value) === 1) {
                            $('#row' + attribute.id + ' .togglesettingbut button').append('<i style="color: green" class="ion-toggle-filled"></i><i hidden style="color: gray" class="ion-toggle"></i>');
                        } else {
                            $('#row' + attribute.id + ' .togglesettingbut button').append('<i style="color: gray" class="ion-toggle"></i><i hidden style="color: green" class="ion-toggle-filled"></i>');
                        }
                    } else {
                        formdata = '' +
                            '<div class="rowform" id="row' + attribute.id + '">\n' +
                            '  <span class="spanval">' + attribute.description + ' :</span>\n' +
                            '  <input class="inputval" name="value" value="' + attribute.value + '" onfocus="showButton(' + attribute.id + ')">\n' +
                            '  <input name="id" value="' + attribute.id + '" type="hidden">\n' +
                            '  <div class="setbtn"><button onclick="updateSettingRow(' + attribute.id + ')" hidden><i class="fas fa-save"></i></button></div>\n' +
                            '</div>';
                        $('#scales' + group + ' form').append(formdata);
                        //todo many ip patterns and diferrent input settings
                    }
                    if (setting.name === 'scaleIp') {
                        $('#row' + setting.id + ' input[name="value"]').attr({
                            'pattern': '^(?!0)(?!.*\\.$)((1?\\d?\\d|25[0-5]|2[0-4]\\d)(\\.|$)){4}$',
                            'title': 'Πρέπει να είναι της μορφής XXX.XXX.X.XXX'
                        })
                    }
                });
            });
            bindToggleListeners();
        }else{
            bootboxMessage(data.error,1);
        }
    });
}

function showPrinters() {
    var result=showGeneralSettings(200);
    result.then(function (json) {
        var data=JSON.parse(json);
        if (data.response){
            $('.maincolumn').append('<div class="formTab" id="groupset4">' +
                '<ul class="nav nav-tabs" role="tablist"></ul>' +
                '<div class="tab-content"></div> ' +
                '</div>');
            $.each(data.body,function (group,settings) {
                $('#groupset4 ul').append('<li role="presentation"><a href="#printers'+group+'" aria-controls="scales'+group+'" role="tab" data-toggle="tab">' + settings.groupname.description + '</a></li>');
                $('#groupset4 .tab-content').append('<div role="tabpanel" class="tab-pane fade" id="printers'+group+'">' +
                    '<div class="tabcontent">' +
                    '<form class="formset" method="post" action=""></form>' +
                    '</div>' +
                    '</div>');
                $.each(settings, function (setting, attribute) {
                    var formdata;
                    if (setting === 'groupname') {
                        formdata = '' +
                            '<div class="rowform" id="row' + attribute.id + '">\n' +
                            '   <span class="spanval">Κατάσταση :</span>\n' +
                            '   <div class="togglesettingbut"><button type="button"></button></div>\n' +
                            '   <input name="value" value="' + attribute.value + '" type="hidden">\n' +
                            '   <input name="id" value="' + attribute.id + '" hidden>\n' +
                            '   <div class="setbtn"></div>\n' +
                            '</div>';
                        $('#printers' + group + ' form').prepend(formdata);
                        if (parseInt(attribute.value) === 1) {
                            $('#row' + attribute.id + ' .togglesettingbut button').append('<i style="color: green" class="ion-toggle-filled"></i><i hidden style="color: gray" class="ion-toggle"></i>');
                        } else {
                            $('#row' + attribute.id + ' .togglesettingbut button').append('<i style="color: gray" class="ion-toggle"></i><i hidden style="color: green" class="ion-toggle-filled"></i>');
                        }
                    } else {
                        formdata = '' +
                            '<div class="rowform" id="row' + attribute.id + '">\n' +
                            '  <span class="spanval">' + attribute.description + ' :</span>\n' +
                            '  <input class="inputval" name="value" value="' + attribute.value + '" onfocus="showButton(' + attribute.id + ')">\n' +
                            '  <input name="id" value="' + attribute.id + '" type="hidden">\n' +
                            '  <div class="setbtn"><button onclick="updateSettingRow(' + attribute.id + ')" hidden><i class="fas fa-save"></i></button></div>\n' +
                            '</div>';
                        $('#printers' + group + ' form').append(formdata);
                    }
                    if (setting === 'printIp') {
                        $('#row' + setting.id + ' input[name="value"]').attr({
                            'pattern': '^(?!0)(?!.*\\.$)((1?\\d?\\d|25[0-5]|2[0-4]\\d)(\\.|$)){4}$',
                            'title': 'Πρέπει να είναι της μορφής XXX.XXX.X.XXX'
                        })
                    }
                });
            });
            bindToggleListeners();
        }else{
            bootboxMessage(data.error,1);
        }
    });
}

function generalSettings() {
    var result=showGeneralSettings('all');
    result.then(function (json) {
        var data=JSON.parse(json);
        if (data.response){
            $('.maincolumn').append('<div class="formTab" id="groupset1">' +
                '<ul class="nav nav-tabs" role="tablist"></ul>' +
                '<div class="tab-content"></div> ' +
                '</div>');
            $.each(data.body,function (group,settings) {
                    $('#groupset1 ul').append('<li role="presentation"><a href="#general'+group+'" aria-controls="general'+group+'" role="tab" data-toggle="tab">' + settings.groupname.description + '</a></li>');
                    $('#groupset1 .tab-content').append('<div role="tabpanel" class="tab-pane fade" id="general'+group+'">' +
                        '<div class="tabcontent">' +
                        '<form class="formset" method="post" action=""></form>' +
                        '</div>' +
                        '</div>');
                    if (parseInt(group) === 3) {//Μεμονομένες Ρυθμίσεις Ζυγαριάς
                        $.each(settings,function (setting,attribute) {
                            var formdata = '' +
                                '<div class="rowform" id="row' + attribute.id + '">\n' +
                                '   <span class="spanval">Κατάσταση :</span>\n' +
                                '   <div class="togglesettingbut"><button type="button"></button></div>\n' +
                                '   <input name="value" value="' + attribute.value + '" type="hidden">\n' +
                                '   <input name="id" value="' + attribute.id + '" hidden>\n' +
                                '   <div class="setbtn"></div>\n' +
                                '</div>';
                            if(setting==='groupname'){
                                $('#general' + group + ' form').prepend(formdata);
                                scalesHandler(attribute.value);
                            }else if(setting==='pretypedBarcodes'){
                                $('#general' + group + ' form').append(formdata).append('<div class="table-responsive" id="special_codes"></div>');
                                $('#row'+attribute.id+' .spanval').html(attribute.description+' :');
                                specialBarcodes();
                            }else{
                                return true;
                            }
                            if (parseInt(attribute.value)===1){
                                $('#row'+attribute.id+' .togglesettingbut button').append('<i style="color: green" class="ion-toggle-filled"></i><i hidden style="color: gray" class="ion-toggle"></i>');
                            }else {
                                $('#row' +attribute.id + ' .togglesettingbut button').append('<i style="color: gray" class="ion-toggle"></i><i hidden style="color: green" class="ion-toggle-filled"></i>');
                            }

                        });
                    }else if (parseInt(group)===5){//ektiposeis
                        $.each(settings, function (setting, attribute) {
                            var formdata;
                            if (setting === 'groupname') {
                                formdata = '' +
                                    '<div class="rowform" id="row' + attribute.id + '">\n' +
                                    '   <span class="spanval">Κατάσταση :</span>\n' +
                                    '   <div class="togglesettingbut"><button type="button"></button></div>\n' +
                                    '   <input name="value" value="' + attribute.value + '" type="hidden">\n' +
                                    '   <input name="id" value="' + attribute.id + '" hidden>\n' +
                                    '   <div class="setbtn"></div>\n' +
                                    '</div>';
                                $('#general' + group + ' form').prepend(formdata);
                                if (parseInt(attribute.value) === 1) {
                                    $('#row' + attribute.id + ' .togglesettingbut button').append('<i style="color: green" class="ion-toggle-filled"></i><i hidden style="color: gray" class="ion-toggle"></i>');
                                } else {
                                    $('#row' + attribute.id + ' .togglesettingbut button').append('<i style="color: gray" class="ion-toggle"></i><i hidden style="color: green" class="ion-toggle-filled"></i>');
                                }
                            } else {
                                formdata = '' +
                                    '<div class="rowform" id="row' + attribute.id + '">\n' +
                                    '  <span class="spanval">' + attribute.description + ' :</span>\n' +
                                    '  <select name="value" class="inputval" onfocus="showButton(' + attribute.id + ')"></select>\n' +
                                    '  <input name="id" value="' + attribute.id + '" type="hidden">\n' +
                                    '  <div class="setbtn"><button onclick="updateSettingRow(' + attribute.id + ')" hidden><i class="fas fa-save"></i></button></div>\n' +
                                    '</div>';
                                $('#general' + group + ' form').append(formdata);
                                if (attribute.description==='Αποδείξεις'){
                                    $.each(data.printers['apodeikseis'],function (index,value) {
                                        if (parseInt(attribute.value)===parseInt(index)){
                                            $('#row'+attribute.id+' select').append('<option selected value="'+parseInt(index)+'">'+value.name+'</option>');
                                        }else {
                                            $('#row'+attribute.id+' select').append('<option value="'+parseInt(index)+'">'+value.name+'</option>');
                                        }
                                    });
                                }else {
                                    $.each(data.printers['ektyposeis'],function (index,value) {
                                        if (parseInt(attribute.value)===parseInt(index)){
                                            $('#row'+attribute.id+' select').append('<option selected value="'+parseInt(index)+'">'+value.name+'</option>');
                                        }else {
                                            $('#row'+attribute.id+' select').append('<option value="'+parseInt(index)+'">'+value.name+'</option>');
                                        }
                                    });
                                }
                            }
                        });
                    }else {
                        $.each(settings, function (setting, attribute) {
                            var formdata;
                            if (setting === 'groupname') {
                                formdata = '' +
                                    '<div class="rowform" id="row' + attribute.id + '">\n' +
                                    '   <span class="spanval">Κατάσταση :</span>\n' +
                                    '   <div class="togglesettingbut"><button type="button"></button></div>\n' +
                                    '   <input name="value" value="' + attribute.value + '" type="hidden">\n' +
                                    '   <input name="id" value="' + attribute.id + '" hidden>\n' +
                                    '   <div class="setbtn"></div>\n' +
                                    '</div>';
                                $('#general' + group + ' form').prepend(formdata);
                                if (parseInt(attribute.value) === 1) {
                                    $('#row' + attribute.id + ' .togglesettingbut button').append('<i style="color: green" class="ion-toggle-filled"></i><i hidden style="color: gray" class="ion-toggle"></i>');
                                } else {
                                    $('#row' + attribute.id + ' .togglesettingbut button').append('<i style="color: gray" class="ion-toggle"></i><i hidden style="color: green" class="ion-toggle-filled"></i>');
                                }
                            } else {
                                formdata = '' +
                                    '<div class="rowform" id="row' + attribute.id + '">\n' +
                                    '  <span class="spanval">' + attribute.description + ' :</span>\n' +
                                    '  <input class="inputval" name="value" value="' + attribute.value + '" onfocus="showButton(' + attribute.id + ')">\n' +
                                    '  <input name="id" value="' + attribute.id + '" type="hidden">\n' +
                                    '  <div class="setbtn"><button onclick="updateSettingRow(' + attribute.id + ')" hidden><i class="fas fa-save"></i></button></div>\n' +
                                    '</div>';
                                $('#general' + group + ' form').append(formdata);
                                //todo many ip patterns and diferrent input settings
                            }
                            if (setting === 'cashierIp') {
                                $('#row' + setting.id + ' input[name="value"]').attr({
                                    'pattern': '^(?!0)(?!.*\\.$)((1?\\d?\\d|25[0-5]|2[0-4]\\d)(\\.|$)){4}$',
                                    'title': 'Πρέπει να είναι της μορφής XXX.XXX.X.XXX'
                                })
                            }
                        });
                    }
            });
            bindToggleListeners();
        }else{
            bootboxMessage(data.error,1);
        }
        bindSpecialCodesListeners();
    });

}

function bindToggleListeners(){
    $('.togglesettingbut button').off().on('click',toggleSetting);
}


function showSubmitBut(user) {
    $('#user'+user+' input').bind('change',function () {
        $('#user'+user+' .submitbtn').show();
    });
}

function updateUserPass(user) {
    var action='updateuserpass';
    var value=parseInt($('#user'+user+' input[name="password"]').val());
    $('form').submit(function (event) {
        event.preventDefault();
        var result=updateUserField(user,value,action);
        result.then(function (json) {
            console.log(json);
            var data=JSON.parse(json);
            if (data.response){
                $('#user'+user+' .user_submit button').hide();
                $('#user'+user+' .user_submit').append('<p class="quickmessage">Αποθηκέυτηκε!</p>');
                setTimeout(function () {
                    $('#user'+user+' .user_submit p').remove();
                },1500);
                $('form').off();
            }else{
                bootboxMessage(data.error,1);
            }

        });

    });


}

function updateUserStatus(user) {
    var action='updateuseractive';
    var value=parseInt($('#user'+user+' input[name="active"]').val());
    if (value===0){
        value=1;
    }else{
        value=0;
    }
    var result=updateUserField(user,value,action);
    result.then(function (json) {
        console.log(json);
        var data=JSON.parse(json);
        if (data.response){
            $('#user'+user+' input[name="active"]').val(value);
            $('#user'+user+' .disable_user i').toggle();
            $('#user'+user+' .user_submit button').hide();
            $('#user'+user+' .user_submit').append('<p class="quickmessage">Αποθηκέυτηκε!</p>');
            setTimeout(function () {
                $('#user'+user+' .user_submit p').remove();
            },1500);
        }else{
            bootboxMessage(data.error,1);
        }

    });
}

function updateUserUac(user){
    var action='updateuseruac';
    var value=parseInt($('#user'+user+' select').val());
    var result=updateUserField(user,value,action);
    result.then(function (json) {
        console.log(json);
        var data=JSON.parse(json);
        if (data.response){
            $('#user'+user+' .user_submit').append('<p class="quickmessage">Αποθηκέυτηκε!</p>');
            setTimeout(function () {
                $('#user'+user+' .user_submit p').remove();
            },1500);
        }else{
            bootboxMessage(data.error,1);
        }

    });
}

function userSettings() {
    var result=showAllUsers();
    result.then(function (json) {
        console.log(json);
        var data=JSON.parse(json);
        if (data.response){
            var i=1;
            var newrow='<div class="formTab" id="groupset1">' +
                '<ul class="nav nav-tabs" role="tablist">' +
                '<li role="presentation" class="active"><a href="#users_info" aria-controls="users_info" role="tab" data-toggle="tab">Επεξεργασία χειριστών</a></li>' +
                '<li role="presentation"><a href="#new_user" aria-controls="new_user" role="tab" data-toggle="tab">Προσθήκη νέου</a></li>' +
                '</ul>' +
                '<div class="tab-content">' +
                '   <div role="tabpanel" class="tab-pane fade in active" id="users_info">' +
                '       <div class="tabcontent">' +
                '           <form class="formset" action="" method="post">' +
                '               <div class="table-responsive" id="edit_users">' +
                '                   <table class="table"><thead><tr><th>#</th><th>Όνομα</th><th>Username</th><th>Password</th>' +
                '                    <th>Δικαιώματα</th><th></th><th></th></tr></thead><tbody></tbody></table>' +
                '               </div>' +
                '           </form>' +
                        '</div>' +
                '   </div>' +
                '   <div role="tabpanel" class="tab-pane fade" id="new_user">' +
                '       <div class="tabcontent">' +
                '           <form class="formset" id="user_register" method="post" action="">\n' +
                '                                    <div class="rowform">\n' +
                '                                        <span class="spanval">Όνομα: </span>\n' +
                '                                        <input class="inputval" name="name" pattern=".{4,}" autocomplete="off" required title="Τουλάχιστον 4 χαρακτήρες">\n' +
                '                                        <div class="setbtn"></div>\n' +
                '                                    </div>\n' +
                '                                    <div class="rowform">\n' +
                '                                        <span class="spanval">Username: </span>\n' +
                '                                        <input class="inputval" name="username" pattern=".{4,}" autocomplete="off" required title="Τουλάχιστον 4 χαρακτήρες">\n' +
                '                                        <div class="setbtn"></div>\n' +
                '                                    </div>\n' +
                '                                    <div class="rowform">\n' +
                '                                        <span class="spanval">Κωδικός: </span>\n' +
                '                                        <input type="password" class="inputval" name="password" pattern=".{4,}" autocomplete="off" required title="Τουλάχιστον 4 χαρακτήρες">\n' +
                '                                        <div class="setbtn"></div>\n' +
                '                                    </div>\n' +
                '                                    <div class="rowform">\n' +
                '                                        <span class="spanval">Επιβεβαίωση κωδικού: </span>\n' +
                '                                        <input type="password" class="inputval" name="password2" pattern=".{4,}" autocomplete="off" required title="Τουλάχιστον 4 χαρακτήρες">\n' +
                '                                        <div class="setbtn"></div>\n' +
                '                                    </div>\n' +
                '                                    <div class="rowform">\n' +
                '                                        <span class="spanval">Δικαιώματα: </span>\n' +
                '                                        <select name="uac" class="inputval">\n' +
                '                                            <option value="1">Admin</option>\n' +
                '                                            <option value="2">Super User</option>\n' +
                '                                            <option selected value="3">User</option>\n' +
                '                                        </select>\n' +
                '                                        <div class="setbtn"></div>\n' +
                '                                    </div>\n' +
                '                                    <div class="formbuttons">\n' +
                '                                        <button class="ion-refresh resetbtn" type="reset"></button>\n' +
                '                                        <button class="fas fa-save submitbtn" type="submit"></button>\n' +
                '                                    </div>\n' +
                '           </form>' +
                '        </div>' +
                '    </div>' +
                '   </div> ' +
                '</div>';
            $('.maincolumn').append(newrow);
            data.users.forEach(function (user) {
                var id=parseInt(user.user_id);
                $('#edit_users tbody').append('<tr id="user'+id+'">' +
                    '<td class="col-sm-1">'+i+'</td>' +
                    '<td class="col-sm-2">'+user.name+'</td>' +
                    '<td class="col-sm-2">'+user.username+'</td>' +
                    '<td class="col-sm-2"><input name="password" class="input_pass" type="password"  onclick="showSubmitBut('+id+')" value="'+user.password+'">' +
                    '<input name="id" value="'+id+'" type="hidden"><input name="active" value="'+user.active+'" type="hidden"></td>' +
                    '<td class="col-sm-2"><select class="user_rights" onchange="updateUserUac('+id+')"><option value="1">Admin</option><option value="2">Super User</option><option value="3">User</option></select></td>' +
                    '<td class="disable_user col-sm-1"><button type="button" class="status" onclick="updateUserStatus('+id+')"></button></td>' +
                    '<td class="user_submit col-sm-2"><button class="submitbtn" onclick="updateUserPass('+id+')" hidden><i class="fas fa-save"></i></button></td>' +
                    '</tr>');
                if (parseInt(user.uac)===1){//admin
                  $('#user'+id+'').addClass('success');
                    $('#user'+id+' select').val('1');
                }else if (parseInt(user.uac)===2){//super user
                    $('#user'+id+'').addClass('warning');
                    $('#user'+id+' select').val('2');
                } else{//user
                    $('#user'+id+'').addClass('active');
                    $('#user'+id+' select').val('3');
                }
                if (parseInt(user.active)===1){
                    $('#user'+id+' .disable_user button').append('<i class="ion-toggle-filled"></i><i class="ion-toggle" hidden></i>');
                } else{
                    $('#user'+id+' .disable_user button').append('<i class="ion-toggle"></i><i class="ion-toggle-filled" hidden></i>');
                }
                i++;
            });
            bindUserListeners();
        }else{
            bootboxMessage(data.error,1);
        }

    });
}

function bindUserListeners() {
    $('#user_register').submit(function (e) {
        e.preventDefault();
        var pass1=$('#user_register input[name="password"]').val();
        var pass2=$('#user_register input[name="password2"]').val();
        if (pass1===pass2){
            var form=$('#user_register').serializeArray();
            var data= {};
            form.forEach(function (field) {
                data[field.name]=field.value;
            });
            var result=insertNewUser(data);
            result.then(function (json) {
                console.log(json);
                var data=JSON.parse(json);
                if (data.response){
                    bootboxMessage('Επιτυχής καταχώρηση',2);
                    start();

                } else{
                    bootboxMessage(data.error,1);
                }

            });
        }else {
            bootboxMessage('Παρακαλώ πληκτρολογήστε τον ίδιο κωδικό και στα δύο πεδία',1);
        }
    });
}

function bindSpecialCodesListeners() {

    $('#special_codes input').on('click',function () {
        var value=parseInt(this.value);
        var number=$(this).closest('tr');
        number=(number[0].id).substring(7);
        number=parseInt(number);
        var group;
        if (this.type==="radio"){
            group='type';
        } else{
            group='active';
            value=Math.abs(value-1);
        }
        var data={start:number,value:value,group:group};
        var result=updateSpecial(data);
        result.then(function (json) {
            console.log(json);
            var data=JSON.parse(json);
            if (data.response){
                if (this.type==="checkbox"){
                    $('#special'+number+' input[name="active"]').val(value);
                }
                $('#special'+number+' .quick_msg').append('<p class="quickmessage">Αποθηκέυτηκε!</p>');
                setTimeout(function () {
                    $('#special'+number+' .quick_msg p').remove();
                },1500);
            }else {
                bootboxMessage(data.error,1);
            }
        });
    });

}

function showScales() {
    $('#scale_settings').empty();
    $('form').trigger('reset');
    scales();
}

function setSidemenuDefault() {
    $('.sidebuts').css({
        "background-color":"#f6f6f6",
        "color":"#665e5e"
    });
}

function sideButtonsListeners() {
    $(".sidebuts").click(function () {
        var id=$(this).attr('id');
        unbindSideButtonListeners();
        if (id==="collapseSidemenu"){
            $('.sidebuts span').toggle();
            $('#collapseSidemenu i').toggle();
        }else {
            $('.maincolumn').empty();
            if(parseInt(access)>1){
                bootboxMessage('Δεν έχετε τα δικαιώματα για να δείτε αυτές τις ρυθμίσεις');
            }else{
                setSidemenuDefault();
                $(this).css({
                    "background-color":"#665e5e",
                    "color":"#f6f6f6"
                });
                $('.sidebuts span').hide();
                switch (id){
                    case "btn1":
                        generalSettings();
                        break;
                    case "btn2":
                        userSettings();
                        break;
                    case "btn3":
                        showScales();
                        break;
                    case "btn4":
                        showPrinters();
                        break;
                }
            }
        }

    });
}

function unbindSideButtonListeners() {
    $('.sidebuts').off();
    setTimeout(function () {
        sideButtonsListeners();
    },500);
}

function start() {
    $('.ajax-loader').show(0).delay(500).hide(0);
    $('form').trigger('reset');
    $('.maincolumn').empty();
    if (parseInt(access)<=1){
        generalSettings();
    }
}


$(document).ready(function () {
    access;
    initCss();
    menuCss();
    sidebuts();
    appendHead();
    sideButtonsListeners();
    start();

});