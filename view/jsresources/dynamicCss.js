function initCss(){
    height=Math.max(document.documentElement.clientHeight, window.innerHeight || 0)/100;
    css='';
}

function menuCss(){
    css+='.wrapper{min-height:'+100*height+'px}\n'+
        '.navbarFixed{min-height:'+10*height+'px}\n'+
        '.navbar-buttons{min-height:'+10*height+'px}\n'+
        '.container{min-height:'+90*height+'px;margin-top:'+10*height+'px}\n'+
        '.navbar-buttons i{font-size:'+5*height+'px}\n';
}

function sidebuts() {
    css+='.sidebuts{min-height:'+10*height+'px}\n';
}

function apothikiCss() {
    css+='.itemhead button{min-height:'+8*height+'px}\n'+
        'input{min-height:'+8*height+'px}\n'+
        'input[type="checkbox"]{min-height:'+3*height+'px}\n';
}

function settingsCss(){

}

function appendHead(){
    var style='<style type="text/css">\n'+
        css+
        '</style>';
    $('head').append(style);
}
