<?php
require '../include/resources/header.php';
?>

<!DOCTYPE html>
<html lang="el">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="author" content="tsoxt">
    <!-- scripts-->
    <script src="<?php echo $paths->_jquery;?>" ></script>
    <script src="<?php echo $paths->_bootstrap_js;?>" ></script>
    <script src="<?php echo $paths->_bootbox_js;?>" ></script>
    <script src="jsresources/apothiki/ajaxcalls.js"></script>
    <script src="jsresources/apothiki/item.js"></script>
    <script src="jsresources/apothiki/category.js"></script>
    <script src="jsresources/apothiki/omada.js"></script>
    <script src="jsresources/apothiki/tmima.js"></script>
    <script src="jsresources/apothiki/epistrofes.js"></script>
    <script src="jsresources/dynamicCss.js"></script>
    <script src="jsresources/apothiki.js"></script>
    <script type="text/javascript">
        access=<?php echo $user['access']; ?>;
    </script>
    <script src="jsresources/menu.js"></script>
    <title>Αποθήκη</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php   echo $paths->_bootstrap_css; ?> " rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/apothiki.css" rel="stylesheet">
    <link href="css/menu.css" rel="stylesheet">
    <link href="css/fontawesome/css/all.css" rel="stylesheet">
    <link href="css/ionicons.min.css" rel="stylesheet">
</head>

<body>
<div class="ajax-loader">
    <img src="img/ajax-loader.gif" class="img-responsive" />
</div>
<div class="wrapper">
    <div class="navbarFixed">
        <div class="menu">
            <button class="navbar-buttons" onclick="navigation(1,0)"><i class="fas fa-home"></i></button>
            <button class="navbar-buttons" onclick="#"><i class="ion-cube" style="color: white;"></i></button>
            <button class="navbar-buttons" onclick="navigation(6,0)"><i class="fas fa-users"></i></button>
            <button class="navbar-buttons" onclick="navigation(3,0)"><i class="ion-stats-bars"></i></button>
            <button class="navbar-buttons" onclick="navigation(4,0)"><i class="ion-ios-settings-strong"></i></button>
            <button class="navbar-buttons" onclick="navigation(7,0)"><i class="fas fa-info-circle"></i></button>
        </div>
        <div class="utilities"></div>
        <div class="info">
            <div class="navbar-buttons date"><?php echo $date;  ?></div>
            <button class="navbar-buttons user" onclick="navigation(5,0)">
                <span class="username"><?php echo $user['name'];?></span>
                <i class="ion-power" style="color: #f25050"></i>
            </button>
        </div>
    </div>
    <div class="container">
        <div class="columns">
            <button class="sidebuts" type="button" id="btn1">
                <i class="fas fa-boxes"></i><span>Προϊόντα</span>
            </button>
            <button class="sidebuts" type="button"  id="btn2">
                <i class="fas fa-indent"></i><span>Κατηγορίες</span>
            </button>
            <button class="sidebuts" type="button" id="btn3">
                <i class="fas fa-stream"></i><span>Ομάδες</span>
            </button>
            <button class="sidebuts" type="button" id="btn4">
                <i class="fas fa-bullhorn"></i><span>Τμήματα</span>
            </button>
            <button class="sidebuts" type="button" id="btn5">
                <i class="fas fa-calendar"></i><span>Επιστροφές</span>
            </button>
            <button class="sidebuts" type="button" id="btn6">
                <i class="fas fa-balance-scale"></i><span>Ενημερώση ζυγαριάς</span>
            </button>
            <button class="sidebuts" type="button" id="collapseSidemenu">
                <i style="display: none"  class="fas fa-caret-square-right"></i><i class="fas fa-caret-square-left"></i><span>Σύμπτυξη μενού</span>
            </button>
        </div>
        <div class="maincolumn">

            <!--Modal choose_bcode-->
            <div id="choose_item" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="choose_itemLabel" data-backdrop="static">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title" id="choose_itemLabel">Επιλογή Προϊόντος</h3>
                        </div>
                        <div class="modal-body">
                            <div class="sameBarcodeGroup">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of modal choose_bcode-->

            <!--Inventory modal-->
            <div id="inventory" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="inventoryLabel" data-backdrop="static">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="inventoryLabel">Ευρετήριο</h2>
                        </div>
                        <div class="modal-body">
                            <div class="flexgroup">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of inventory modal-->

            <!--BarcodeModal-->

            <div class="modal fade" id="bcodeModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="search-group">
                                <input type="number" id="bcodenumbers" placeholder="Αναζήτηση barcode.." min="8" value="" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--End of BarcodeModal-->

            <!--Product insert-->
            <div id="groupset1" class="formTab">
                <form class="formcontainer" id="itemShow" action="" method="post">
                    <div class="itemhead">
                        <input id="barcode_search" hidden>
                        <button type="button" id="barcode" class="barcode_search fas fa-barcode" ondblclick="barcodeModal()"></button>
                        <button type="button" class="headrowRbutton" onclick="showcategories()"><i class="icon fas fa-search"></i> Αναζήτηση</button>
                        <button type="button" class="headrowLbutton" onclick="callItem(0,3)"><i class="icon fas fa-plus"></i> Νέο</button>
                    </div>
                    <div class="itembody"></div>
                </form>

            </div>
            <!--End Product insert-->

            <!--Category insert-->
            <div id="groupset2" class="formTab">
                <form class="formcontainer" id="insert_category" action="" method="post">
                    <div class="itemhead">
                        <button type="button" class="headrowRbutton" onclick="showAllCategories()"><i class="icon fas fa-search"></i> Αναζήτηση</button>
                        <button type="button" class="headrowLbutton" onclick="callCategory(0,2)"><i class="icon fas fa-plus"></i> Νέο</button>
                    </div>
                    <div class="itembody"></div>
                </form>
            </div>
            <!--End Category insert-->

            <!--Omada insert-->
            <div id="groupset3" class="formTab">
                <form class="formcontainer" id="insert_omada" action="" method="post">
                    <div class="itemhead">
                        <button type="button" class="headrowRbutton" onclick="callOmadesCateg()"><i class="icon fas fa-search"></i> Αναζήτηση</button>
                        <button type="button" class="headrowLbutton" onclick="showOmada(0,2)"><i class="icon fas fa-plus"></i> Νέο</button>
                    </div>
                    <div class="itembody"></div>
                </form>
            </div>
            <!--End Omada insert-->

            <!--Tmimata insert-->
            <div id="groupset4" class="formTab">
                <div class="formcontainer">
                    <div class="itemhead">
                        <button type="button" class="headrowRbutton" onclick="showTmimata()"><i class="icon fas fa-search"></i> Αναζήτηση</button>
                        <button type="button" class="headrowLbutton" onclick="editTmima(0,2)"><i class="icon fas fa-plus"></i> Νέο</button>
                    </div>
                    <div class="itembody"></div>
                </div>
                <div id="modalTmimata" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalTmimataLabel" data-backdrop="static">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <form id="insert_tmima">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h2 class="modal-title" id="modalTmimataLabel">Τμήματα ΦΠΑ</h2>
                                </div>
                                <div class="modal-body">
                                    <div class="modal_tmimata">
                                        <input type="text" name="tmima" placeholder="Όνομα" pattern=".{3,}" required autocomplete="off" autofocus>
                                        <input type="number" min="0.01" step="0.01" max="0.99" name="price" placeholder="Αξία" required>
                                        <input name="id" hidden>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="button2" type="reset"><i class="icon ion-arrow-return-left"></i></button>
                                    <button class="button3" type="submit"><i class="icon ion-checkmark"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Tmimata insert-->

            <!--Epistrofes insert-->
            <div id="groupset5" class="formTab">
                <div class="formcontainer">
                    <div class="itemhead">
                        <div class="dates">
                            <button id="today" type="button" class="daybutton" onclick="searchByDay(event)"><i class="icon far fa-calendar-alt"></i> Σήμερα</button>
                            <button id="tomorrow" type="button" class="daybutton" onclick="searchByDay(event)"> <i class="icon far fa-calendar-alt"></i> Αύριο</button>
                        </div>
                        <div class="input-group">
                            <input name="returnsfrom" type="date" onfocus="showFromDay()" class="form-control" value="">
                            <div class="input-group-addon">εώς</div>
                            <input name="returnsto" type="date" onfocus="showToDay()" class="form-control" value="">
                        </div>
                        <button class="datesearch" onclick="searchBetweenDates()"><i class="icon fas fa-search"></i></button>
                    </div>
                    <div class="itembody">

                    </div>
                </div>
            </div>
            <!--End Epistrofes insert-->

            <!--alt_id insert-->
            <div id="groupset6" class="formTab">
                <div class="formcontainer">
                    <div class="itemhead">
                        <button type="button" class="headrowRbutton" onclick="getTypos()"><i class="icon fas fa-calendar-plus"></i> Παραλαβή</button>
                        <button type="button" class="headrowLbutton" onclick="returnTypos()"><i class="icon fas fa-calendar-minus"></i> Επιστροφή</button>
                    </div>
                    <div class="itembody"></div>
                </div>
            </div>
            <!--End alt_id insert-->

        </div>
    </div>
</div>
</body>
</html>

