<?php
require '../include/resources/header.php';
?>
<!DOCTYPE html>
<html lang="el">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="author" content="tsoxt">
    <!-- scripts-->
    <script src="<?php echo $paths->_jquery;?>" ></script>
    <script src="<?php echo $paths->_bootstrap_js;?>" ></script>
    <script src="<?php echo $paths->_bootbox_js;?>" ></script>
    <script src="jsresources/dynamicCss.js"></script>
    <script src="jsresources/customers-suppliers/customers-suppliers_main.js"></script>
    <script src="jsresources/customers-suppliers/customers.js"></script>
    <script src="jsresources/customers-suppliers/suppliers.js"></script>
    <script src="jsresources/customers-suppliers/suppliers-deltia.js"></script>
    <script src="jsresources/customers-suppliers/customers-deltia.js"></script>
    <script src="jsresources/customers-suppliers/customers_suppliers_ajaxcalls.js"></script>
    <script type="text/javascript">
        access=<?php echo $user['access']; ?>;
    </script>
    <script src="jsresources/menu.js"></script>
    <title>Πελάτες-Προμηθευτές</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php   echo $paths->_bootstrap_css; ?> " rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/customers-suppliers.css" rel="stylesheet">
    <link href="css/menu.css" rel="stylesheet">
    <link href="css/fontawesome/css/all.css" rel="stylesheet">
    <link href="css/ionicons.min.css" rel="stylesheet">
</head>

<body>
<div class="ajax-loader">
    <img src="img/ajax-loader.gif" class="img-responsive" />
</div>
<div class="wrapper">
    <div class="navbarFixed">
        <div class="menu">
            <button class="navbar-buttons" onclick="navigation(1,0)"><i class="fas fa-home"></i></button>
            <button class="navbar-buttons" onclick="navigation(2,0)"><i class="ion-cube"></i></button>
            <button class="navbar-buttons" onclick="#"><i class="fas fa-users" style="color: white;"></i></button>
            <button class="navbar-buttons" onclick="navigation(3,0)"><i class="ion-stats-bars"></i></button>
            <button class="navbar-buttons" onclick="navigation(4,0)"><i class="ion-ios-settings-strong"></i></button>
            <button class="navbar-buttons" onclick="navigation(7,0)"><i class="fas fa-info-circle"></i></button>
        </div>
        <div class="utilities"></div>
        <div class="info">
            <div class="navbar-buttons date"><?php echo $date;  ?></div>
            <button class="navbar-buttons user" onclick="navigation(5,0)">
                <span class="username"><?php echo $user['name'];?></span>
                <i class="ion-power" style="color: #f25050"></i>
            </button>
        </div>
    </div>
    <div class="container">
        <div class="columns">
            <button class="sidebuts" type="button" id="btn1">
                <i class="fas fa-user-tie"></i><span>Πελάτες</span>
            </button>
            <button class="sidebuts" type="button" id="btn2">
                <i class="fas fa-people-carry"></i><span>Προμηθευτές</span>
            </button>
            <button class="sidebuts" type="button" id="collapseSidemenu">
                <i style="display: none"  class="fas fa-caret-square-right"></i><i class="fas fa-caret-square-left"></i><span>Σύμπτυξη μενού</span>
            </button>
        </div>
        <div class="maincolumn">

            <!--Orders in progress modal-->
            <div id="ordersInProgress" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ordersInProgressLabel" data-backdrop="static">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="ordersInProgressLabel">Παραγγελίες</h2>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of Deltia in progress modal-->

            <!--Customers Deltiο modal-->
            <div id="customerTicket" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="customerTicketLabel" data-backdrop="static">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="customerTicketLabel">Φόρμα</h2>
                        </div>
                        <div class="modal-body">
                            <div id="customer_deltio_form" class="deltio_form">
                                <div class="deltio_header">
                                    <div class="parastatiko">
                                        <select id="customer_deltio_type">
                                            <option value="1" selected>Πώλησης</option>
                                            <option value="2">Επιστροφής</option>
                                        </select>
                                        <div class="parastatiko_extra">
                                        </div>
                                    </div>
                                    <div class="search_parastatiko"><textarea name="matching_parastatiko" readonly placeholder="Σχετικά παραστατικά..." rows="3" cols="15" ></textarea></div>
                                    <form id="addToCustomerDeltio" class="rowform">
                                        <button type="button" class="fas fa-barcode customerBarcodeBtn" autofocus></button>
                                        <input class="customerBarcode" hidden>
                                        <input name="productName" class="inputval" placeholder="Προϊόν" readonly onfocus="showcategories(4)">
                                        <input name="productSiskevasia" class="inputval" placeholder="Συσκευασία" readonly>
                                        <input name="productStock" class="inputval" placeholder="Απόθεμα" readonly>
                                        <input name="productId" hidden><input name="productPrice" hidden><input name="productTmima" hidden>
                                        <input name="productDiscountType" hidden><input name="productDiscountValue" hidden>
                                        <input name="productMcode" hidden><input name="productBcode" hidden>
                                        <input name="productIdOm" hidden><input name="productZigos" hidden>
                                        <input name="productQuantity" type="number" class="inputval"
                                               placeholder="Ποσότητα" min="0.1" step="0.01" required>
                                        <button class="fas fa-plus-circle submitbtn" type="button"></button>
                                    </form>
                                </div>
                                <form id="customer_deltio_submit">
                                    <div class="deltio_body">
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Περιγραφή</th>
                                                    <th>Συσκ.</th>
                                                    <th>Απόθεμα</th>
                                                    <th>Ποσότητα</th>
                                                    <th>ΦΠΑ %</th>
                                                    <th>Τιμή</th>
                                                    <th>Σύνολο</th>
                                                    <th>Έκπτωση</th>
                                                    <th>Τελικό Σύνολο</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th style="width: 80%;text-align: right">Σύνολο (προ έκπτωσης):</th>
                                                    <td style="width: 20%"><input type="number" name="sinolo_deltiou"
                                                                                  disabled value="0.00"></td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 80%;text-align: right">Έκπτωση:</th>
                                                    <td style="width: 20%"><input type="number" name="ekptosi_deltiou" disabled value="0.00"></td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 80%;text-align: right">Σύνολο (μετά την έκπτωση):</th>
                                                    <td style="width: 20%"><input type="number" name="teliko_sinolo_deltiou" disabled value="0.00"></td>
                                                </tr>
                                                <tr class="eispraxthen">
                                                    <th style="width: 80%;text-align: right">Εισπραχθέν:</th>
                                                    <td style="width: 20%"><input type="number" step="0.01" name="apodeiksi_eispraksis" min="0"></td>
                                                </tr>
                                                <tr class="pliromi_paralavis">
                                                    <th style="width: 80%;text-align: right">Πληρωτέο:</th>
                                                    <td style="width: 20%"><input type="number" step="0.01" name="pliroteo_deltiou" min="0"></td>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="deltio_footer">
                                        <button type="button" onclick="clearCustomerDeltio()">Καθάρισμα</button>
                                        <div class="form_paralavis">
                                            <!--<div class="inline_checkbox">
                                                Αποστολή με email<input type="checkbox" class="send_email" disabled>
                                            </div>-->
                                            <div class="inline_checkbox">
                                                Εκτύπωση<input type="checkbox" class="print_invo" checked>
                                            </div>
                                            <div class="inline_checkbox">
                                                Ενημέρωση Αποθέματος<input type="checkbox" class="update_stock" checked>
                                            </div>
                                            <button type="submit">Καταχώρηση</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of Customers Deltiο modal-->

            <!--Suppliers Deltiο modal-->
            <div id="supplierTicket" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="supplierTicketLabel" data-backdrop="static">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="supplierTicketLabel">Φόρμα</h2>
                        </div>
                        <div class="modal-body">
                            <div id="deltio_form" class="deltio_form">
                                <div class="deltio_header">
                                    <div class="parastatiko">
                                        <select id="deltio_type">
                                            <option value="0" selected>Παραγγελίας</option>
                                            <option value="1">Παραλαβής</option>
                                            <option value="2">Επιστροφής</option>
                                        </select>
                                        <div class="parastatiko_extra">
                                        </div>
                                    </div>
                                    <div class="search_parastatiko" style="display: none"><textarea name="matching_parastatiko" readonly placeholder="Σχετικά παραστατικά..." rows="3" cols="15" ></textarea></div>
                                    <form id="addToDeltio" class="rowform">
                                        <button type="button" class="fas fa-barcode supplierBarcodeBtn"
                                                autofocus></button>
                                        <input class="supplierBarcode" hidden>
                                        <input name="productName" class="inputval" placeholder="Προϊόν" readonly
                                               onfocus="showcategories(3)">
                                        <input name="productSiskevasia" class="inputval" placeholder="Συσκευασία"
                                               readonly>
                                        <input name="productStock" class="inputval" placeholder="Απόθεμα" readonly>
                                        <input name="productId" hidden><input name="productBuyPrice" hidden>
                                        <input name="productStockAlert" hidden><input name="productTmima" hidden>
                                        <input name="productMcode" hidden><input name="productBcode" hidden>
                                        <input name="productIdOm" hidden><input name="productZigos" hidden>
                                        <input name="productStockRecommend" type="number" class="inputval"
                                               placeholder="Ποσότητα" min="0" step="0.01" required>
                                        <button class="fas fa-plus-circle submitbtn" type="button"></button>
                                    </form>
                                </div>
                                <form id="deltio_submit">
                                    <div class="deltio_body">
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Περιγραφή</th>
                                                    <th>Συσκ.</th>
                                                    <th>Απόθεμα</th>
                                                    <th>Ποσότητα</th>
                                                    <th>Τιμή Αγοράς</th>
                                                    <th>Σύνολο</th>
                                                    <th>Έκπτωση</th>
                                                    <th>ΦΠΑ %</th>
                                                    <th>Τελικό Σύνολο</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th style="width: 80%;text-align: right">Σύνολο (προ έκπτωσης):</th>
                                                    <td style="width: 20%"><input type="number" name="sinolo_deltiou"
                                                                                  disabled value="0.00"></td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 80%;text-align: right">Έκπτωση:</th>
                                                    <td style="width: 20%"><input type="number" name="ekptosi_deltiou" disabled value="0.00"></td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 80%;text-align: right">Σύνολο (μετά την έκπτωση):</th>
                                                    <td style="width: 20%"><input type="number" name="aksia_meta" disabled value="0.00"></td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 80%;text-align: right">ΦΠΑ:</th>
                                                    <td style="width: 20%"><input type="number" name="fpa_deltiou" disabled value="0.00"></td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 80%;text-align: right">Τελικό σύνολο:</th>
                                                    <td style="width: 20%"><input type="number" name="teliko_sinolo_deltiou" disabled value="0.00"></td>
                                                </tr>
                                                <tr class="pliromi_paralavis" style="display: none">
                                                    <th style="width: 80%;text-align: right">Πληρωτέο:</th>
                                                    <td style="width: 20%"><input type="number" step="0.01" name="pliroteo_deltiou" min="0"></td>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="deltio_footer">
                                        <button type="button" onclick="clearSupplierDeltio()">Καθάρισμα</button>
                                        <div class="form_paralavis" style="display: none">
                                            <div class="inline_checkbox">Ενημέρωση Τιμών<input type="checkbox"
                                                                                               class="update_price"></div>
                                            <div class="inline_checkbox">Ενημέρωση Αποθέματος<input type="checkbox"
                                                                                                    class="update_stock"
                                                                                                    checked></div>
                                            <button type="submit">Αποθήκευση</button>
                                        </div>
                                        <button class="orderBtn" type="submit">Αποστολή</button>
                                        <div style="display: flex;justify-content: space-between;align-items: center">
                                            <div class="inline_checkbox">
                                                Εκτύπωση<input type="checkbox" class="print_invo" checked>
                                            </div>
                                            <!--<div class="inline_checkbox">
                                                Αποστολή με email<input type="checkbox" class="send_email" disabled>
                                            </div>-->
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of Suppliers Deltiο modal-->

            <!--Supplier Parastatika Inventory modal-->
            <div id="parastatikaInventory" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="parastatikaInventoryLabel" data-backdrop="static">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="parastatikaInventoryLabel">Παραστατικά</h2>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of Supplier Parastatika Inventory modal-->

            <!--Customer Parastatika Inventory modal-->
            <div id="customerParastatikaInventory" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="customerParastatikaInventoryLabel" data-backdrop="static">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="customerParastatikaInventoryLabel">Παραστατικά</h2>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of Customer Parastatika Inventory modal-->

            <!--Inventory modal-->
            <div id="inventory" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="inventoryLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="inventoryLabel">Ευρετήριο</h2>
                        </div>
                        <div class="modal-body">
                            <div class="flexgroup">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of inventory modal-->

            <div class="formTab" id="groupset1">
                <div class="search_column">
                    <input id="customerData" autocomplete="off" placeholder="Αναζήτηση πελάτη..">
                    <button id="newCustomerBtn" type="button" onclick="newCustomer()"><i class="fas fa-plus"></i></button>
                </div>
                <div class="listOfPersons"></div>
                <div class="customer_form">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#customer_info" aria-controls="customer_info" role="tab" data-toggle="tab">Στοιχεία</a>
                        </li>
                        <li role="presentation">
                            <a href="#customer_tab" aria-controls="customer_tab" role="tab" data-toggle="tab">Καρτέλα</a>
                        </li>
                        <li role="presentation">
                            <a href="#customer_discounts" aria-controls="customer_discounts" role="tab" data-toggle="tab">Εκπτώσεις</a>
                        </li>
                        <li role="presentation">
                            <a href="#customer_tickets" aria-controls="customer_tickets" role="tab" data-toggle="tab">Παραστατικά</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="customer_info">
                            <form class="formset" id="customer_register" method="post" action="">
                                <div class="rowform">
                                    <span class="spanval">Υπόλοιπο: </span>
                                    <input class="inputval" name="ypoloipo" value="0.00" readonly>
                                    <span class="spanval">Πόντοι: </span>
                                    <input class="inputval" name="points" value="0">
                                </div>
                                <div class="rowform">
                                    <select name="type" class="inputval">
                                        <option selected  value="0">Λιανικής</option>
                                        <option value="1">Χονδρικής</option>
                                    </select>
                                    <input class="inputval" name="member" pattern=".{4,}" autocomplete="off" title="Τουλάχιστον 4 χαρακτήρες" placeholder="Κωδικός κάρτας">
                                </div>
                                <div class="rowform">
                                    <input class="inputval" name="name" pattern=".{4,}" autocomplete="off" required title="Τουλάχιστον 4 χαρακτήρες" placeholder="Επωνυμία">
                                </div>
                                <div class="rowform">
                                    <input class="inputval" name="address" pattern=".{4,}" autocomplete="off" required title="Τουλάχιστον 4 χαρακτήρες" placeholder="Διεύθυνση">
                                    <input class="inputval" name="city" pattern=".{4,}" autocomplete="off" required title="Τουλάχιστον 4 χαρακτήρες" placeholder="Πόλη">
                                    <input class="inputval" name="tk" pattern="\d{5}" autocomplete="off" required title="Πρέπει να περιέχει 5 νούμερα" placeholder="Τ.Κ">
                                </div>
                                <div class="rowform">
                                    <input class="inputval" name="phone" pattern="\d{10}" autocomplete="off" title="Πρέπει να περιέχει 10 νούμερα" placeholder="Τηλέφωνο">
                                    <input class="inputval" name="mobile" pattern="\d{10}" autocomplete="off"  title="Πρέπει να περιέχει 10 νούμερα" placeholder="Κινητό">
                                </div>
                                <div class="rowform">
                                    <input class="inputval" name="job" pattern=".{4,}" autocomplete="off" title="Τουλάχιστον 4 χαρακτήρες" placeholder="Επάγγελμα">
                                    <input class="inputval" name="afm" pattern="\d{9}" autocomplete="off" title="Πρέπει να περιέχει 9 νούμερα" placeholder="ΑΦΜ">
                                    <input class="inputval" name="doy" pattern=".{4,}" autocomplete="off" title="Τουλάχιστον 4 χαρακτήρες" placeholder="ΔΟΥ">
                                </div>
                                <div class="formbuttons">
                                    <button class="resetButton" type="reset"><i class="fas fa-redo-alt"></i></button>
                                    <button class="submitButton" type="submit"><i class="fas fa-save"></i></button>
                                </div>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="customer_tab">
                            <div class="head_column">
                                <div class="input-group">
                                    <input name="fromDate" type="date" class="form-control">
                                    <div class="input-group-addon">εώς</div>
                                    <input name="toDate" type="date" class="form-control">
                                </div>
                                <button type="button" class="etisia">ΕΤΗΣΙΑ</button>
                            </div>
                            <div class="table-responsive" id="customer_parastatika">
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="customer_discounts">
                            <form class="formset" method="post" id="addDiscount">
                                <div class="rowform">
                                    <button type="button" class="fas fa-barcode customerBarcodeBtn" autofocus></button>
                                    <input class="customerBarcode" hidden>
                                    <input name="productName" class="inputval" placeholder="Προϊόν" readonly onfocus="showcategories(1)">
                                    <input name="productPrice" class="inputval" placeholder="Τιμή" readonly>
                                    <input name="productId" hidden>
                                    <select name="productDiscType" class="inputval" onchange="changeDiscountAttrs()">
                                        <option value="0">Έκπτωση %</option>
                                        <option value="1">Έκπτωση</option>
                                    </select>
                                    <input name="productDiscPrice" type="number" class="inputval" placeholder="Αξία" required>
                                    <button class="fas fa-plus-circle submitbtn" type="submit"></button>
                                </div>
                            </form>
                            <div class="table-responsive" id="customer_products">
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="customer_tickets">
                            <div class="tickets_forms">
                                <button type="button" style="background-color: #4a8790" onclick="customerDeltioModal()"><i class="fas fa-shopping-cart"></i> Νέο Δελτίο</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="formTab" id="groupset2">
                <div class="search_column">
                    <input id="supplierData" autocomplete="off" placeholder="Αναζήτηση προμηθευτή..">
                    <button id="newSupplierBtn" type="button" onclick="newSupplier()"><i class="fas fa-plus"></i></button>
                </div>
                <div class="listOfPersons"></div>
                <div class="supplier_form">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#supplier_info" aria-controls="supplier_info" role="tab" data-toggle="tab">Στοιχεία</a>
                        </li>
                        <li role="presentation">
                            <a href="#supplier_tab" aria-controls="supplier_tab" role="tab" data-toggle="tab">Καρτέλα</a>
                        </li>
                        <li role="presentation">
                            <a href="#supplier_warehouse" aria-controls="supplier_warehouse" role="tab" data-toggle="tab">Προϊόντα</a>
                        </li>
                        <li role="presentation">
                            <a href="#supplier_tickets" aria-controls="supplier_tickets" role="tab" data-toggle="tab">Παραστατικά</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="supplier_info">
                            <form class="formset" id="supplier_register" method="post" action="">
                                <div class="rowform">
                                    <div class="spacediv"></div>
                                    <span class="spanval">Υπόλοιπο: </span>
                                    <input class="inputval" name="ypoloipo" value="0.00" readonly>
                                </div>
                                <div class="rowform">
                                    <input class="inputval" name="name" pattern=".{4,}" autocomplete="off" required title="Τουλάχιστον 4 χαρακτήρες" placeholder="Επωνυμία">
                                </div>
                                <div class="rowform">
                                    <input class="inputval" name="address" pattern=".{4,}" autocomplete="off" required title="Τουλάχιστον 4 χαρακτήρες" placeholder="Διεύθυνση">
                                    <input class="inputval" name="city" pattern=".{4,}" autocomplete="off" required title="Τουλάχιστον 4 χαρακτήρες" placeholder="Πόλη">
                                    <input class="inputval" name="tk" pattern="\d{5}" autocomplete="off" required title="Πρέπει να περιέχει 5 νούμερα" placeholder="Τ.Κ">
                                </div>
                                <div class="rowform">
                                    <input class="inputval" name="phone" pattern="\d{10}" autocomplete="off" title="Πρέπει να περιέχει 10 νούμερα" placeholder="Τηλέφωνο">
                                    <input class="inputval" name="mobile" pattern="\d{10}" autocomplete="off"  title="Πρέπει να περιέχει 10 νούμερα" placeholder="Κινητό">
                                </div>
                                <div class="rowform">
                                    <input class="inputval" name="job" pattern=".{4,}" autocomplete="off" title="Τουλάχιστον 4 χαρακτήρες" placeholder="Επάγγελμα">
                                    <input class="inputval" name="afm" pattern="\d{9}" autocomplete="off" title="Πρέπει να περιέχει 9 νούμερα" placeholder="ΑΦΜ">
                                    <input class="inputval" name="doy" pattern=".{4,}" autocomplete="off" title="Τουλάχιστον 4 χαρακτήρες" placeholder="ΔΟΥ">
                                </div>
                                <div class="rowform">
                                    <input class="inputval" type="email" name="email"pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" autocomplete="off" title="Πρέπει να είναι έγκυρο email" placeholder="Email">
                                </div>
                                <div class="formbuttons">
                                    <button class="resetButton" type="reset"><i class="fas fa-redo-alt"></i></button>
                                    <button class="submitButton" type="submit"><i class="fas fa-save"></i></button>
                                </div>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="supplier_tab">
                            <div class="head_column">
                                <div class="input-group">
                                    <input name="fromDate" type="date" class="form-control">
                                    <div class="input-group-addon">εώς</div>
                                    <input name="toDate" type="date" class="form-control">
                                </div>
                                <button type="button" class="etisia">ΕΤΗΣΙΑ</button>
                            </div>
                            <div class="table-responsive" id="supplier_parastatika">
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="supplier_warehouse">
                            <form class="formset" method="post" id="addProduct">
                                <div class="rowform">
                                    <button type="button" class="fas fa-barcode supplierBarcodeBtn" autofocus></button>
                                    <input class="supplierBarcode" hidden>
                                    <input name="productName" class="inputval" placeholder="Προϊόν" readonly onfocus="showcategories(2)">
                                    <input name="productSiskevasia" class="inputval" placeholder="Συσκευασία" readonly>
                                    <input name="productStock" class="inputval" placeholder="Απόθεμα" readonly>
                                    <input name="productId" hidden><input name="productStockRecommend" hidden><input name="productStockAlert" hidden>
                                    <input name="productTmima" hidden>
                                    <input name="productBuyPrice" type="number" class="inputval" placeholder="Τιμή αγοράς" min="0" step="0.01" required>
                                    <button class="fas fa-plus-circle submitbtn" type="submit"></button>
                                </div>
                            </form>
                            <div class="table-responsive" id="supplier_products">
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="supplier_tickets">
                            <div class="tickets_forms">
                                <button type="button" style="background-color: #4a8790" onclick="supplierDeltioModal()"><i class="fas fa-shopping-cart"></i> To Δελτίο μου</button>
                                <button type="button" style="background-color: #4a904a" onclick="showSupplierOrders()"><i class="far fa-eye"></i> Σε εξέλιξη</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

