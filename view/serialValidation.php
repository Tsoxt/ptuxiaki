<?php
require '../include/resources/paths.php';
$paths =new paths;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="tsoxt">
    <link rel="icon" href="../../favicon.ico">
    <!-- scripts-->
    <script src="<?php echo $paths->_jquery;?>" ></script>
    <script src="<?php echo $paths->_bootstrap_js;?>" ></script>
    <script src="<?php echo $paths->_bootbox_js;?>" ></script>
    <script type="text/javascript" src="jsresources/serialValidation.js"></script>
    <title>Ενεργοποίηση προγράμματος</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $paths->_bootstrap_css; ?>" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/serialValidation.css" rel="stylesheet">

</head>

<body>

<div class="ajax-loader">
    <img src="img/ajax-loader.gif" class="img-responsive" />
</div>

<div class="container">
    <div class="form-step">
        <div class="step-header">Βήμα 1</div>
        <div class="step-details">Επικοινωνήστε μαζί μας στο 210xxxxxxx ή στείλτε μας με email στο <a href="mailto:licence@ecs.dummy?Subject=Ενεργοποίηση ECSPos-Αίτηση κωδικού ενεργοποίησης" target="_top">licence@ecs.dummy</a> το κλειδί σας για να σας στείλουμε τον κωδικό ενεργοποίησης. To κλειδί σας είναι: <b>B827EB7F7A06</b></div>
        <div class="step-actions"><button class="btn btn-primary" type="button">Επομένο ></button></div>
    </div>
    <form class="form-step">
        <div class="step-header">Βήμα 2</div>
        <div class="step-details">
            <p>Εισάγετε το κλειδί ενεργοποίησης στα πεδία όπως ορίζονται</p>
            <input name="key[]" id="key1" placeholder="1ο μέρος" size="16" pattern="^.{16}$"> -
            <input name="key[]" id="key2" placeholder="2ο μέρος" size="16" pattern="^.{16}$"> -
            <input name="key[]" id="key3" placeholder="3ο μέρος" size="16" pattern="^.{16}$"> -
            <input name="key[]" id="key4" placeholder="4ο μέρος" size="16" pattern="^.{16}$"> -
            <input name="key[]" id="key5" placeholder="5ο μέρος" size="16" pattern="^.{16}$"> -
            <input name="key[]" id="key6" placeholder="6ο μέρος" size="8" pattern="^.{8}$">
        </div>
        <div class="step-actions"><button class="btn btn-primary" type="button" onclick="">< Πίσω</button><button class="btn btn-primary" type="submit">Επομένο ></button></div>
    </form>
</div> <!-- /container -->

</body>
</html>