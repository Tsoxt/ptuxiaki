<?php
require '../include/resources/header.php';
?>
<!DOCTYPE html>
<html lang="el">
<head>
    <meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="author" content="tsoxt">
    <!-- scripts-->
    <script src="<?php echo $paths->_jquery;?>" ></script>
    <script src="<?php echo $paths->_bootstrap_js;?>" ></script>
    <script src="<?php echo $paths->_bootbox_js;?>" ></script>
    <script src="jsresources/tameio/listeners.js"></script>
    <script src="jsresources/dynamicCss.js"></script>
    <script src="jsresources/menu.js"></script>
    <script src="jsresources/main.js"></script>
    <title>Ταμείο</title>
    <!-- Bootstrap core CSS -->
    <link href="<?php   echo $paths->_bootstrap_css; ?> " rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/menu.css" rel="stylesheet">
    <link href="css/ionicons.min.css" rel="stylesheet">
    <link href="css/fontawesome/css/all.css" rel="stylesheet">
</head>
<body>
<div class="ajax-loader">
    <img src="img/ajax-loader.gif" class="img-responsive" />
</div>

<div class="wrapper">
    <div class="navbarFixed">
        <div class="menu">
            <button class="navbar-buttons" onclick="#"><i class="fas fa-home" style="color: white;"></i></button>
            <button class="navbar-buttons" onclick="checkTameio(2)"><i class="ion-cube"></i></button>
            <button class="navbar-buttons" onclick="checkTameio(6)"><i class="fas fa-users"></i></button>
            <button class="navbar-buttons" onclick="checkTameio(3)"><i class="ion-stats-bars"></i></button>
            <button class="navbar-buttons" onclick="checkTameio(4)"><i class="ion-ios-settings-strong"></i></button>
            <button class="navbar-buttons" onclick="checkTameio(7)"><i class="fas fa-info-circle"></i></button>
            <button class="navbar-buttons" onclick="keepStandBy()"><i class="fas fa-desktop"></i> </button>
        </div>
        <div class="utilities"></div>
        <div class="info">
            <div class="navbar-buttons date"><?php echo $date;  ?></div>
            <button class="navbar-buttons user" onclick="navigation(5,0)">
                <span class="username"><?php echo $user['name'];?></span>
                <i class="ion-power" style="color: #f25050"></i>
            </button>
        </div>
    </div>
    <div class="container">
        <div id="itemcontainer" class="itemcontainer">
            <div  id="items" class="items"></div>
            <form class="main_display" id="tameio">
                <div id="itemsdisplay" class="itemsdisplay"></div>
                <div class="screen">
                    <div class="barcode">
                        <button id="barcode" type="button" class="ion-android-funnel" style="cursor: pointer" ondblclick="barcodeModal()"></button>
                        <div class="text">
                            <input type="text" id="barcode_search" hidden>
                        </div>
                    </div>
                    <div class="numbers">
                        <button type="button" onclick="numpadShow()"><i class="ion-calculator"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="tameiocontainer">
            <div id="tmimata" class="tmimata"></div>

            <!--Search Customer Modal-->
            <div class="modal fade" id="searchCustomerModal" data-backdrop="static" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="choose_itemLabel">Πελάτης</h2>
                        </div>
                        <div class="modal-body">
                            <div class="search_column">
                                <input type="text" id="customer_search" placeholder="Αναζήτηση..." required>
                                <button type="button" onclick="addCustomer()"><i class="fas fa-user-plus"></i></button>
                                <button type="button" style="color: #ac4242" onclick="resetCustomer()"><i class="fas fa-user-minus"></i></button>
                            </div>
                            <div class="customer_list"></div>
                            <div class="customer_form">
                                <div class="modal_row">
                                    <input name="customer" type="text" placeholder="Επωνυμία" readonly>
                                    <input name="job" type="text" placeholder="Επάγγελμα" readonly>
                                    <input name="customer_id" hidden>
                                </div>
                                <div class="modal_row">
                                    <input name="address" type="text" placeholder="Διεύθυνση" readonly>
                                    <input name="tk" type="text" placeholder="Τ.Κ." readonly>
                                </div>
                                <div class="modal_row">
                                    <input name="city" type="text" placeholder="Πόλη" readonly>
                                    <input name="phone" type="number" placeholder="Τηλέφωνο" readonly>
                                </div>
                                <div class="modal_row">
                                    <input type="text" name="afm" placeholder="Α.Φ.Μ." readonly>
                                    <input name="doy" type="text" placeholder="ΔΟΥ" readonly>
                                </div>
                                <div class="modal_row">
                                    <select disabled>
                                        <option value="0">Ιδιώτης</option>
                                        <option value="1">Εταιρία</option>
                                    </select>
                                    <div class="inline_row">
                                       <span>Υπόλοιπο:</span>
                                        <input type="number" name="ypoloipo" placeholder="Ποσό" readonly>
                                    </div>
                                </div>
                                <div class="modal_row">
                                    <div style="flex: 1;display: flex"><button type="button" class="closeModalBtn" data-dismiss="modal">Κλείσιμο</button></div>
                                    <div class="inline_row">
                                        <button type="button" class="eispraksi" data-toggle="tooltip" data-placement="top" data-trigger="manual" data-title="Πληρώθηκε επιτυχώς">
                                            <i class="fas fa-wallet"></i>
                                        </button>
                                        <input type="number" name="pliromi" min="0" step="0.01" placeholder="Είσπραξη">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of Search Customer Modal-->

            <!--BarcodeModal-->

            <div class="modal fade" id="bcodeModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="search-group">
                                <input type="number" id="bcodenumbers" placeholder="Αναζήτηση barcode.." min="8" value="" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--End of BarcodeModal-->

            <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="numpad">
                            <div class="num-controls">
                                <div class="numrow">
                                    <div class="closebutton"><button type="button" onclick="numpadClose()"><i class="icon ion-close"></i></button> </div>
                                    <div class="numbutton"><input type="text" id="numbers" maxlength="6" readonly value=""></div>
                                    <div style="display: none" class="savebutton" onclick="numpadSave()"><button type="button"><i class="icon ion-checkmark"></i> </button> </div>
                                </div>
                            </div>
                            <div class="numrow">
                                <div class="numbutton"><button type="button" onclick="number('7')">7</button> </div>
                                <div class="numbutton"><button type="button" onclick="number('8')">8</button> </div>
                                <div class="numbutton"><button type="button" onclick="number('9')">9</button> </div>
                            </div>
                            <div class="numrow">
                                <div class="numbutton"><button type="button" onclick="number('4')">4</button> </div>
                                <div class="numbutton"><button type="button" onclick="number('5')">5</button></div>
                                <div class="numbutton"><button type="button" onclick="number('6')">6</button></div>
                            </div>
                            <div class="numrow">
                                <div class="numbutton"><button type="button" onclick="number('1')">1</button></div>
                                <div class="numbutton"><button type="button" onclick="number('2')">2</button></div>
                                <div class="numbutton"><button type="button" onclick="number('3')">3</button></div>
                            </div>
                            <div class="numrow">
                                <div class="numbutton"><button type="button" onclick="number('b')"><i class="icon ion-backspace"></i> </button></div>
                                <div class="numbutton"><button type="button" onclick="number('.')">,</button></div>
                                <div class="numbutton"><button type="button" onclick="number('0')">0</button></div>
                            </div>
                            <div class="numrow">
                                <div class="numbutton"><button type="button"></button></div>
                                <div class="numbutton"><button type="button" onclick="number('c')">CLEAR</button></div>
                                <div class="numbutton"><button type="button"></button></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="merikoModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="numpad">
                            <div class="num-controls">
                                <div class="numrow">
                                    <div class="closebutton"><button type="button" onclick="merikoModalClose()"><i class="icon ion-close"></i></button> </div>
                                    <div class="numbutton"><input type="text" id="merikonumbers" maxlength="6" placeholder="Εισάγετε ποσό" readonly value=""></div>
                                    <div class="wholesumbutton" onclick="merikonumber('all')"><button type="button">ΥΠΟΛΟΙΠΟ</button></div>
                                    <div class="numbutton"><button class="pistosibtn" type="button" onclick="pistosi()"><i class="fas fa-handshake"></i><span class="text">Πίστωση</span></button></div>
                                </div>
                            </div>
                            <div class="numrow">
                                <div class="numbutton"><button type="button" onclick="merikonumber('7')">7</button> </div>
                                <div class="numbutton"><button type="button" onclick="merikonumber('8')">8</button> </div>
                                <div class="numbutton"><button type="button" onclick="merikonumber('9')">9</button> </div>
                                <div class="numbutton"><button class="discountbtn" type="button" onclick="discountType(0)"><i class="fas fa-tags"></i><span class="text">Έκπτωση</span></button></div>
                            </div>
                            <div class="numrow">
                                <div class="numbutton"><button type="button" onclick="merikonumber('4')">4</button> </div>
                                <div class="numbutton"><button type="button" onclick="merikonumber('5')">5</button></div>
                                <div class="numbutton"><button type="button" onclick="merikonumber('6')">6</button></div>
                                <div class="numbutton"><button class="discountbtn" type="button" onclick="discountType(1)"><i class="fas fa-tags"></i><span class="text">Έκπτωση%</span></button></div>
                            </div>
                            <div class="numrow">
                                <div class="numbutton"><button type="button" onclick="merikonumber('1')">1</button></div>
                                <div class="numbutton"><button type="button" onclick="merikonumber('2')">2</button></div>
                                <div class="numbutton"><button type="button" onclick="merikonumber('3')">3</button></div>
                                <div class="numbutton"><i class="fas fa-undo-alt input_i" onclick="deleteLastpaymethod()"></i><input class="cash_field" type="text" id="lastpaymethod" placeholder="Τελευταία πληρωμή" readonly></div>
                            </div>
                            <div class="numrow">
                                <div class="numbutton"><button type="button" onclick="merikonumber('.')">,</button></div>
                                <div class="numbutton"><button type="button" onclick="merikonumber('c')">C</button></div>
                                <div class="numbutton"><button type="button" onclick="merikonumber('0')">0</button></div>
                                <div class="numbutton"><i class="fas fa-shopping-cart input_i"></i><input class="cash_field" type="text" id="meriko_sunolo" placeholder="Σύνολο" readonly value=""></div>
                            </div>
                            <div class="numrow">
                                <div class="numbutton">
                                    <button class="money" type="button" onclick="cashout('0.01')"><img src="img/moneyIcons/1cent.png"></button>
                                    <button class="money" type="button" onclick="cashout('0.02')"><img src="img/moneyIcons/2cent.png"></button>
                                </div>
                                <div class="numbutton">
                                    <button class="money" type="button" onclick="cashout('0.20')"><img src="img/moneyIcons/20cent.png"></button>
                                    <button class="money" type="button" onclick="cashout('0.50')"><img src="img/moneyIcons/50cent.png"></button>
                                </div>
                                <div class="numbutton">
                                    <button class="money" type="button" onclick="cashout('5')"><img src="img/moneyIcons/5euro.png"></button>
                                    <button class="money" type="button" onclick="cashout('10')"><img src="img/moneyIcons/10euro.png"></button>
                                </div>
                                <div class="numbutton"><i class="fab fa-y-combinator input_i"></i><input class="cash_field" type="text" id="meriko_rest" placeholder="Υπόλοιπο" readonly value=""></div>
                            </div>
                            <div class="numrow">
                                <div class="numbutton">
                                    <button class="money" type="button" onclick="cashout('0.05')"><img src="img/moneyIcons/5cent.png"></button>
                                    <button class="money" type="button" onclick="cashout('0.10')"><img src="img/moneyIcons/10cent.png"></button>
                                </div>
                                <div class="numbutton">
                                    <button class="money" type="button" onclick="cashout('1')"><img src="img/moneyIcons/1euro.png"></button>
                                    <button class="money" type="button" onclick="cashout('2')"><img src="img/moneyIcons/2euro.png"></button>
                                </div>
                                <div class="numbutton">
                                    <button class="money" type="button" onclick="cashout('20')"><img src="img/moneyIcons/20euro.png"></button>
                                    <button class="money" type="button" onclick="cashout('50')"><img src="img/moneyIcons/50euro.png"></button>
                                </div>
                                <div class="numbutton"><i class="fas fa-coins input_i"></i><input class="cash_field" type="text" id="meriko_change" placeholder="Ρέστα" readonly value=""></div>
                            </div>
                            <div class="numrow">
                                <div class="paymentrow">
                                </div>
                                <div class="apodeiksi_btn">
                                    <div class="numbutton"><button class="sunolo" type="button" onclick="apodeiksi(0)"><i class="fas fa-receipt"></i><span class="text">Απόδειξη</span></button></div>
                                    <div class="numbutton"><button class="timologio" type="button" onclick="invoice()"><i class="fas fa-file-invoice"></i><span class="text">Τιμολόγιο</span></button></div>
                                    <div class="numbutton"><button class="xeirografi" type="button" onclick="apodeiksi(1)"><i class="fas fa-pencil-alt"></i><span class="text">Χειρόγραφη Απόδειξη</span></button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="function">
                <div class="functionsbuttons">
                    <div class="function1">
                        <button type="button" id="customers" class="members" onclick="searchCustomerModalShow()" value=""><i class="ion-android-contact"></i> <div class="text">Πελάτης</div></button>
                        <!--<button type="button" id="discount" class="discount" value=""><i class="ion-scissors"></i> <div class="text">Έκπτωση</div></button>-->
                        <button type="button" id="change_perigrafi" class="primary_desc" value=""><i class="ion-toggle"></i> <div class="text">Εναλλαγή</div></button>
                    </div>
                    <div class="function1">
                        <button type="button" class="meriko" onclick="merikoModalShow()"><i class="fas fa-money-check-alt"></i><div class="text">Μερικό</div></button>
                    </div>
                </div>
                <div class="functionsbuttons">
                    <div class="function3">
                        <button type="button" id="reset" class="oliki" value="reset"><i class="fas fa-times-circle"></i><div class="text">Ολική Ακύρωση</div></button>
                        <button type="button" class="allagi" value=""><i class="fas fa-exchange-alt"></i><div class="text">Αλλαγή Είδους</div></button>
                        <button type="button" id="xeirografi" class="xeirografi" value=""><i class="fas fa-receipt"></i><div class="text">Χειρόγραφη Απόδειξη</div></button>
                    </div>
                    <div class="function2">
                        <button type="button" id="sumary" class="sunolo"value=""><i class="ion-social-euro"></i><div class="text" id="sunolo"></div></button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
