<?php
require '../include/resources/header.php';
?>

<!DOCTYPE html>
<html lang="el">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="author" content="tsoxt">
    <!-- scripts-->
    <script src="<?php echo $paths->_jquery;?>" ></script>
    <script src="<?php echo $paths->_bootstrap_js;?>" ></script>
    <script src="<?php echo $paths->_bootbox_js;?>" ></script>
    <script src="jsresources/dynamicCss.js"></script>
    <script src="jsresources/settings.js"></script>
    <script type="text/javascript">
        access=<?php echo $user['access'];?>;
    </script>
    <script src="jsresources/menu.js"></script>
    <title>Ρυθμίσεις</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php   echo $paths->_bootstrap_css; ?> " rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/settings.css" rel="stylesheet">
    <link href="css/menu.css" rel="stylesheet">
    <link href="css/fontawesome/css/all.css" rel="stylesheet">
    <link href="css/ionicons.min.css" rel="stylesheet">
</head>

<body>
<div class="ajax-loader">
    <img src="img/ajax-loader.gif" class="img-responsive" />
</div>
<div class="wrapper">
    <div class="navbarFixed">
        <div class="menu">
            <button class="navbar-buttons" onclick="navigation(1,0)"><i class="fas fa-home"></i></button>
            <button class="navbar-buttons" onclick="navigation(2,0)"><i class="ion-cube"></i></button>
            <button class="navbar-buttons" onclick="navigation(6,0)"><i class="fas fa-users"></i></button>
            <button class="navbar-buttons" onclick="navigation(3,0)"><i class="ion-stats-bars"></i></button>
            <button class="navbar-buttons" onclick="#"><i class="ion-ios-settings-strong" style="color: white;"></i></button>
            <button class="navbar-buttons" onclick="navigation(7,0)"><i class="fas fa-info-circle"></i></button>
        </div>
        <div class="utilities"></div>
        <div class="info">
            <div class="navbar-buttons date"><?php echo $date;  ?></div>
            <button class="navbar-buttons user" onclick="navigation(5,0)">
                <span class="username"><?php echo $user['name'];?></span>
                <i class="ion-power" style="color: #f25050"></i>
            </button>
        </div>
    </div>
    <div class="container">
        <div class="columns">
            <button class="sidebuts" type="button" id="btn1">
                <i class="fas fa-cogs"></i><span>Γενικές</span>
            </button>
            <button class="sidebuts" type="button" id="btn2">
                <i class="fas fa-users-cog"></i><span>Χειριστές</span>
            </button>
            <button class="sidebuts" type="button" id="btn4">
                <i class="fas fa-print"></i><span>Εκτυπωτές</span>
            </button>
            <button class="sidebuts" type="button" id="collapseSidemenu">
                <i style="display: none"  class="fas fa-caret-square-right"></i><i class="fas fa-caret-square-left"></i><span>Σύμπτυξη μενού</span>
            </button>
        </div>
        <div class="maincolumn">
        </div>
    </div>
</div>
</body>
</html>

