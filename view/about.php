<?php
require '../include/resources/header.php';
?>
<!DOCTYPE html>
<html lang="el">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="author" content="tsoxt">
    <!-- scripts-->
    <script src="<?php echo $paths->_jquery;?>" ></script>
    <script src="<?php echo $paths->_bootstrap_js;?>" ></script>
    <script src="<?php echo $paths->_bootbox_js;?>" ></script>
    <script src="jsresources/menu.js"></script>
    <script src="jsresources/dynamicCss.js"></script>
    <script src="jsresources/about.js"></script>
    <script type="text/javascript">
        access=<?php echo $user['access']; ?>;
    </script>
    <title>Πληροφορίες Συστήματος</title>
    <!-- Bootstrap core CSS -->
    <link href="<?php   echo $paths->_bootstrap_css; ?> " rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/about.css" rel="stylesheet">
    <link href="css/menu.css" rel="stylesheet">
    <link href="css/fontawesome/css/all.css" rel="stylesheet">
    <link href="css/ionicons.min.css" rel="stylesheet">
</head>
<body>
<div class="ajax-loader">
    <img src="img/ajax-loader.gif" class="img-responsive" />
</div>

<div class="wrapper">
    <div class="navbarFixed">
        <div class="menu">
            <button class="navbar-buttons" onclick="navigation(1,0)"><i class="fas fa-home"></i></button>
            <button class="navbar-buttons" onclick="navigation(2,0)"><i class="ion-cube"></i></button>
            <button class="navbar-buttons" onclick="navigation(6,0)"><i class="fas fa-users"></i></button>
            <button class="navbar-buttons" onclick="navigation(3,0)"><i class="ion-stats-bars"></i></button>
            <button class="navbar-buttons" onclick="navigation(4,0)"><i class="ion-ios-settings-strong"></i></button>
            <button class="navbar-buttons" onclick="#"><i class="fas fa-info-circle" style="color: white;"></i></button>
        </div>
        <div class="utilities"></div>
        <div class="info">
            <div class="navbar-buttons date"><?php echo $date;  ?></div>
            <button class="navbar-buttons user" onclick="navigation(5,0)">
                <span class="username"><?php echo $user['name'];?></span>
                <i class="ion-power" style="color: #f25050"></i>
            </button>
        </div>
    </div>
    <div class="container">
        <div class="columns">
            <button class="sidebuts" type="button" id="btn1">
                <i class="fas fa-globe"></i><span>Πληροφορίες</span>
            </button>
            <button class="sidebuts" type="button" id="collapseSidemenu">
                <i style="display: none"  class="fas fa-caret-square-right"></i><i class="fas fa-caret-square-left"></i><span>Σύμπτυξη μενού</span>
            </button>
        </div>
        <div class="maincolumn">
            <div id="groupset1" class="formTab">
                <div class="group-info">
                    <div class="col-info">
                        <div class="col-header">Άδεια Χρήσης</div>
                        <div class="col-desc" id="customer"></div>
                    </div>
                    <div class="col-info">
                        <div class="col-header">ΑΦΜ</div>
                        <div class="col-desc" id="afm"></div>
                    </div>
                    <div class="col-info">
                        <div class="col-header">Τηλέφωνο</div>
                        <div class="col-desc" id="phone"></div>
                    </div>
                </div>
                <div class="group-info">
                    <div class="col-info">
                        <div class="col-header">Μεταπωλητής</div>
                        <div class="col-desc" id="seller"></div>
                    </div>
                    <div class="col-info">
                        <div class="col-header">Τηλέφωνο</div>
                        <div class="col-desc" id="seller_phone"></div>
                    </div>
                    <div class="col-info">
                        <div class="col-header">Email</div>
                        <div class="col-desc" id="seller_email"></div>
                    </div>
                </div>
                <div class="group-info">
                    <div class="col-info">
                        <div class="col-header">Όνομα Προγράμματος</div>
                        <div class="col-desc" id="program"></div>
                    </div>
                    <div class="col-info">
                        <div class="col-header">Έκδοση</div>
                        <div class="col-desc" id="version"></div>
                    </div>
                    <div class="col-info">
                        <div class="col-header">Serial Key</div>
                        <div class="col-desc" id="serial"></div>
                    </div>
                    <div class="col-info">
                        <div class="col-header">Ημ/νια Έναρξης</div>
                        <div class="col-desc" id="start_date"></div>
                    </div>
                    <div class="col-info">
                        <div class="col-header">Ημ/νια Λήξης</div>
                        <div class="col-desc" id="exp_date"></div>
                    </div>
                    <div class="col-info">
                        <div class="col-header">IP συντήρησης</div>
                        <div class="col-desc"><?php echo $serverIp; ?></div>
                    </div>
                </div>
                <div class="group-info">
                    <div class="col-update">
                        <button type="button" onclick="checkForUpdates()"><i class="fas fa-cloud-download-alt"></i>
                            Έλεγχος για ενημερώσεις
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
