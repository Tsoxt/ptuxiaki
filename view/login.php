<?php
require '../include/resources/paths.php';
$paths =new paths;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="tsoxt">
    <link rel="icon" href="../../favicon.ico">
    <!-- scripts-->
    <script src="<?php echo $paths->_jquery;?>" ></script>
    <script src="<?php echo $paths->_bootstrap_js;?>" ></script>
    <script src="<?php echo $paths->_bootbox_js;?>" ></script>
    <script type="text/javascript" src="jsresources/login.js"></script>
    <title>Σύνδεση</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $paths->_bootstrap_css; ?>" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/login.css" rel="stylesheet">

</head>

<body>

<div class="container">
    <div class="columns"></div>
    <div class="logo"><img src="/view/img/logo.png" style="width: 50%;margin: 0 25%" align="center"></div>
    <div class="loginbody">
    <form class="form-signin" id="loginform">
        <select name="username" class="form-control" onchange="checkAnotherUser()"></select>
        <input type="text" id="username" name="username" class="form-control" placeholder="Όνομα χρήστη" required>
        <input type="password" id="passwd" name="passwd" class="form-control" placeholder="Συνθηματικό" required>
        <button class="btn btn-lg btn-block" id="login" type="submit" value="login">Σύνδεση</button>
    </form>
        <div id="error" class="error">Λάθος Όνομα Χρήστη ή Συνθηματικό</div>
    </div>
    <div class="columns"></div>
</div> <!-- /container -->

</body>
</html>

