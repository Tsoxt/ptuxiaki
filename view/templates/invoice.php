<?php
/**
 * Created by PhpStorm.
 * User: tsoxt
 * Date: 19/12/2018
 * Time: 2:11 μμ
 */

class invoice
{
    private $receipts;
    private $info;
    private $pliromes;
    private $customer;
    private $sum;
    private $document;
    private $discountPercentage;
    private $summary;
    private $subtotal;

    private function startDocument(){
        $html=<<<EOD
<!DOCTYPE html>
<html lang="el">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <script src="../../vendor/components/jquery/jquery.min.js"></script>
    <script src="../../vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
    <link href="../../vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../view/templates/css/template.css" rel="stylesheet">
</head>
<body>
EOD;
        return $html;

    }

    private function closeDocument(){
        $html=<<<EOD
</body>
</html>
EOD;
        return $html;

    }

    private function getHeader($page,$from,$parastatiko,$date,$reprint){
        if($reprint){
            $type="Ακριβές αντίγραφο εκ πρωτοτύπου";
        }else{
            $type="Πρωτότυπο";
        }
        $html= <<<EOD
<div class="A4">
<div class="container">
    <div class="row">
        <div class="col-xs-4" ><span style="font-weight: bolder">{$type}</span></div>
        <div class="col-xs-8" >Επωνυμία: {$this->info['customer']}</div>
    </div>
    <div class="row">
        <div class="col-xs-4" >Επάγγελμα: {$this->info['epaggelma']}</div>
        <div class="col-xs-4" >ΑΦΜ: {$this->info['afm']}</div>
        <div class="col-xs-4" >ΔΟΥ: {$this->info['doy']}</div>
    </div>
    <div class="row">
        <div class="col-xs-4" >{$this->info['address']}</div>
        <div class="col-xs-4" >{$this->info['TK']} {$this->info['city']}</div>
        <div class="col-xs-4" >ΤΗΛ: {$this->info['phone']}</div>
    </div>
    <div class="row">
        <div class="col-xs-12"><hr></div>
    </div>
    <div class="row">
        <div class="col-xs-5">
            <strong >{$parastatiko['name']}#{$parastatiko['number']}</strong>
        </div>
        <div class="col-xs-5">
             <strong >{$date}</strong>
        </div>
        <div class="col-xs-2" >Σελίδα: {$page}/{$from}</div>
    </div>
    <div class="row">
        <div class="col-xs-12"><hr></div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <strong>Στοιχεία Πελάτη</strong>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4" >{$this->customer['name']}</div>
    <div class="col-xs-4" >ΑΦΜ: {$this->customer['afm']}</div>
        <div class="col-xs-4" >ΔΟΥ: {$this->customer['doy']}</div>
    </div>
    <div class="row">
        <div class="col-xs-4" >{$this->customer['address']}</div>
        <div class="col-xs-4" >{$this->customer['postcode']} {$this->customer['city']}</div>
        <div class="col-xs-4" >ΤΗΛ: {$this->customer['phone']}</div>
    </div>
    <div class="row">
        <div class="col-xs-12" >Επάγγελμα: {$this->customer['job']}</div>
    </div>
    <div class="row">
        <div class="col-xs-5">Τόπος Αποστολής: Έδρα μας</div>
        <div class="col-xs-5 col-xs-offset-2" >Τόπος Προορισμού: Έδρα Πελάτου</div>
    </div>
    <div class="row">
        <div class="col-xs-12"><hr></div>
    </div>
    <div class="row" style="min-height: 31vh">
        <div class="col-xs-12">
            <div class="table-responsive-xs">
                <table class="table table-striped" style="font-size: 0.7em;">
                    <thead>
                    <tr>
                        <th class="center">#</th>
                        <th>Προϊόν</th>
                        <th class="right">Τιμή Μονάδας</th>
                        <th class="center">Ποσότητα</th>
                        <th class="center">Έκπτωση</th>
                        <th class="right">Καθ. Αξία</th>
                        <th class="center">Φ.Π.Α.</th>
                        <th class="right">Αξία Φ.Π.Α.</th>
                    </tr>
                    </thead>
                    <tbody>
EOD;
        return $html;
    }

    private function getBody($lastLine){
        $html='';
        $previousLine=0;
        foreach ($this->receipts as $number=>$line){
            $number+=1;
            $previousLine=$number;
            if($number<=$lastLine){
                continue;
            }
            $vat=((int)$line['fpa']);
            $price=((float)$line['timi'])*(100/(100+$vat));
            $value=((float)$price)*((float)$line['temaxia']);
            if (isset($line['ekptosi'])){
                $discount=(float)$line['ekptosi'];
            }else{
                $discount=($value*$this->discountPercentage/100);
            }
            $subtotal=$value-$discount;
            $vatValue=$subtotal*($vat/100);
            $price=number_format($price,2,".","");
            $temaxia=number_format($line['temaxia'],3,".","");
            $value=number_format($value,2,".","");
            $discount=number_format($discount,2,".","");
            $subtotal=number_format($subtotal,2,".","");
            $vatValue=number_format($vatValue,2,".","");
            $this->sum['value']+=(float)$value;
            $this->sum['discount']=((float)$this->sum['discount'])+((float)$discount);
            $this->sum['subtotal']=((float)$this->sum['subtotal'])+((float)$subtotal);
            $this->sum['fpa']=((float)$this->sum['fpa'])+((float)$vatValue);
            $this->sum['total']=((float)$this->sum['total'])+(((float)$subtotal)+((float)$vatValue));
            if($number===sizeof($this->receipts)){
                $total=number_format($this->sum['total'],2,".","");
                $difference=((float)$total)-((float)$this->subtotal);
                if($difference<>0){
                    $this->sum['value']=((float)$this->sum['value'])-((float)$value);
                    $this->sum['discount']=((float)$this->sum['discount'])-((float)$discount);
                    $this->sum['subtotal']=((float)$this->sum['subtotal'])-((float)$subtotal);
                    $this->sum['fpa']=((float)$this->sum['fpa'])-((float)$vatValue);
                    $this->sum['total']=((float)$this->sum['total'])-(((float)$subtotal)+((float)$vatValue));
                    $vat=((int)$line['fpa']);
                    $price=((float)$line['timi'])*(100/(100+$vat));
                    $value=((float)$price)*((float)$line['temaxia']);
                    $discount=($value*$this->discountPercentage/100);
                    if($this->discountPercentage<>0){
                        $discount=(float)$discount+$difference;
                        $subtotal=$value-$discount;
                        $vatValue=$subtotal*($vat/100);
                        $subtotal=number_format($subtotal,2,".","");
                    }else{
                        $value=(float)$value-$difference;
                        $subtotal=$value-$discount;
                        $subtotal=number_format($subtotal,2,".","");
                        $vatValue=((float)$subtotal)*($vat/100);
                    }
                    $price=number_format($price,2,".","");
                    $temaxia=number_format($line['temaxia'],3,".","");
                    $value=number_format($value,2,".","");
                    $discount=number_format($discount,2,".","");
                    $vatValue=number_format($vatValue,2,".","");
                    $this->sum['value']+=(float)$value;
                    $this->sum['discount']+=(float)$discount;
                    $this->sum['subtotal']=((float)$this->sum['subtotal'])+((float)$subtotal);
                    $this->sum['fpa']=((float)$this->sum['fpa'])+((float)$vatValue);
                    $this->sum['total']=((float)$this->sum['total'])+(((float)$subtotal)+((float)$vatValue));
                }
            }
            $html.=<<<EOD
<tr>
    <td class="center">{$number}</td>
    <td>{$line['description']}</td>
    <td class="right">{$price}€</td>
    <td class="center">{$temaxia}</td>
    <td class="center">{$discount}€</td>
    <td class="right">{$subtotal}€</td>
    <td class="center">{$vat}%</td>
    <td class="right">{$vatValue}€</td>
</tr>
EOD;
            if($number%8===0){
                break;
            }
        }
        $response['lastLine']=$previousLine;
        $response['html']=$html;
        return $response;

    }

    private function ApodeiksiEispraksisBody($parastatika){
        if (sizeof($parastatika)>0){
            $matched='';
            foreach ($parastatika as $par){
                $matched.=$par.", ";
            }
        }else{
            $matched='-';
        }
        $html=<<<EOD
<tr>
    <td class="center" style="width: 30%">Αναφέρεται σε παραστατικά </td>
    <td class="right" style="width: 70%">$matched</td>
</tr>
EOD;
        $response['html']=$html;
        return $response;
    }

    private function getFooter($pages,$from){
        $value="";
        $discount="";
        $subtotal="";
        $vat="";
        $total="";
        $credit="";
        $card="";
        $cash="";
        if($pages==$from){
            $credit=0.00;
            $card=0.00;
            $cash=0.00;
            $value=number_format(((float)$this->sum['value']),2,".","");
            $discount=number_format(((float)$this->sum['discount']),2,".","");
            $subtotal=number_format(((float)$this->sum['subtotal']),2,".","");
            $vat=number_format(((float)$this->sum['fpa']),2,".","");
            $total=number_format(((float)$this->sum['total']),2,".","");
            foreach ($this->pliromes as $pliromi){
                switch ((int)$pliromi['id']){
                    case -1:
                        continue;
                    case 0:
                        continue;
                    case 1:
                        $credit+=((float)$pliromi['amount']);
                        break;
                    case 2:
                        $cash+=((float)$pliromi['amount']);
                        break;
                    default:
                        $card+=((float)$pliromi['amount']);
                        break;
                }
            }
        }
        $credit=number_format($credit,2,".","");
        $cash=number_format($cash,2,".","");
        $card=number_format($card,2,".","");
        $html=<<<EOD
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <table class="table table-clear">
                <tbody>
                <tr>
                    <td class="left">
                        <strong>Αξία</strong>
                    </td>
                    <td class="right">{$value}€</td>
                </tr>
                <tr>
                    <td class="left">
                        <strong>Αξία Έκπτωσης</strong>
                    </td>
                    <td class="right">{$discount}€</td>
                </tr>
                <tr>
                    <td class="left">
                        <strong>Καθαρή Αξία</strong>
                    </td>
                    <td class="right">{$subtotal}€</td>
                </tr>
                <tr>
                    <td class="left">
                        <strong>Αξία Φ.Π.Α.</strong>
                    </td>
                    <td class="right">
                        <strong>{$vat}€</strong>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <strong>Πληρωτέο</strong>
                    </td>
                    <td class="right">
                        <strong>{$total}€</strong>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-xs-4 col-xs-offset-4">
            <table class="table table-clear">
                <tbody>
                <tr>
                    <td class="left">
                        <strong>Πίστωση</strong>
                    </td>
                    <td class="right">{$credit}€</td>
                </tr>
                <tr>
                    <td class="left">
                        <strong>Κάρτα</strong>
                    </td>
                    <td class="right">{$card}€</td>
                </tr>
                <tr>
                    <td class="left">
                        <strong>Μετρητά</strong>
                    </td>
                    <td class="right">{$cash}€</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">Εκδότης</div>
        <div class="col-xs-4 col-xs-offset-4">Πελάτης</div>
    </div>
</div>
</div>
<div class="page-break"></div> 
EOD;
        return $html;
    }

    public function getDocument(){
        try{

            $file='/home/ecs/ECSPos/files/invoices/draft.html';
            file_put_contents($file,$this->document);
            $return['response']=true;
            $return['url']=$_SERVER['SERVER_ADDR'].'/files/invoices/draft.html';
            return $return;
        }catch (Exception $e){
            $response['response']=false;
            $response['error']=$e->getMessage();
            return $response;
        }
    }

    public function __construct($receipt,$invoice,$creditCards,$customer,$reprint=false)
    {
        $this->receipts=$receipt;
        $this->pliromes=$creditCards;
        $this->info=(new Validation())->getProgramInfo()['body'];
        $this->customer=$customer;
        $discount=0.00;
        $this->summary=0.00;
        $this->subtotal=0.00;
        foreach ($this->pliromes as $pliromi){
            switch ((int)$pliromi['id']){
                case -1:
                    continue;
                case 0:
                    $discount=abs((float)$pliromi['amount']);
                    $this->summary+=abs((float)$pliromi['amount']);
                    break;
                default:
                    $this->summary+=abs((float)$pliromi['amount']);
                    $this->subtotal+=abs((float)$pliromi['amount']);
                    break;
            }
        }
        $this->discountPercentage=($discount/$this->summary)*100;
        $this->discountPercentage=number_format($this->discountPercentage,2,".","");
        $this->discountPercentage=((float)$this->discountPercentage);
        $start=$this->startDocument();
        $pages=sizeof($this->receipts)/8;
        if($pages>floor($pages)){
            $pages=floor($pages)+1;
        }else{
            $pages=floor($pages);
        }
        $parastatiko=array("name"=>$invoice['invoData']['name'],"number"=>str_pad($invoice['invoData']['lastNumber'],6,"0",STR_PAD_LEFT));
        $date=$invoice['date'];
        $this->document=$start;
        for($j=0;$j<=1;$j++){
            $this->sum=array("value"=>0.00,"discount"=>0.00,"subtotal"=>0.00,"fpa"=>0.00,"total"=>0.00);
            $lastLine=0;
            for ($i=1; $i<=$pages;$i++){
                $this->document.=$this->getHeader($i,$pages,$parastatiko,$date,$reprint);
                if (isset($this->receipts['apodeiksi_eispraksis'])){
                    $getBody=$this->ApodeiksiEispraksisBody($this->receipts['matched']);
                }else{
                    $getBody=$this->getBody($lastLine);
                    $lastLine=$getBody['lastLine'];
                }
                $this->document.=$getBody['html'];
                $this->document.=$this->getFooter($i,$pages);
            }
            if($reprint){
                break;
            }
            $reprint=true;
        }
        $this->document.=$this->closeDocument();
    }
}