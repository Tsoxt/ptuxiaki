jQuery.fn.updateWithText = function(text, speed)
{
    var dummy = $('<div/>').html(text);

    if ($(this).html() != dummy.html())
    {
        $(this).fadeOut(speed/2, function() {
            $(this).html(text);
            $(this).fadeIn(speed/2, function() {
                //done
            });
        });
    }
}

$.urlParam = function(name, url) {
    if (!url) {
        url = window.location.href;
    }
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(url);
    if (!results) {
        return undefined;
    }
    return results[1] || undefined;
}


jQuery.fn.outerHTML = function(s) {
    return s
        ? this.before(s).remove()
        : jQuery("<p>").append(this.eq(0).clone()).html();
};

function roundVal(temp)
{
	return Math.round(temp * 10) / 10;
}

function kmh2beaufort(kmh)
{
	var speeds = [1, 5, 11, 19, 28, 38, 49, 61, 74, 88, 102, 117, 1000];
	for (var beaufort in speeds) {
		var speed = speeds[beaufort];
		if (speed > kmh) {
			return beaufort;
		}
	}
	return 12;
}

function welcomeMessage(receipt){
    $("#screen").empty();
    $("#sunolo").html('');
    $("#screen").removeClass('receipt');
    var html='<h2>'+receipt[0].line1+'</h2><h2>'+receipt[0].line2+'</h2>';
    $("#screen").append(html);
    var columns='<div id="receipt" class="col-md-center"></div>';
    $("#screen").append(columns);
    sum=0;
    sums=[];
}

jQuery(document).ready(function($) {
	$.ajaxSetup({cache: false});
	previous=0;
    urli="http://localhost/files/CustomerDisplay/";
	var lang = "el";
	var weatherParams 		= {'q':'Athens,GR','units':'metric','lang':lang,'APPID':'b338b6aac100a60cd2953a20d28486ab'};
	var locale = {'q':' - Athens'};
	moment.locale(lang);
   
	
	(function updateImageSrcs()	{
		$.getJSON( urli+"images.json", function( data ) {
			var urls=data;
			$(".carousel-inner").find('*').not('.active').remove();
			var i=0;
			var div;
			urls.urls.forEach(function( url ) {
				if (i<1){
					$(".active").empty();
					div ='<img class="d-block w-100" src="'+url+'" alt="'+(i+1)+'st slide">';
					$('.active').append(div);
				}else{
					div='<div class="position carousel-item">\n' +
							'<img class="d-block w-100" src="'+url+'" alt="'+(i+1)+'st slide">\n' +
						'</div>';
					$('.carousel-inner').append(div);
				}
				i++;
			});
			$('.carousel').carousel({
			  interval:10000
			});
		});
		setTimeout(function() {
			updateImageSrcs();
		}, 86400000 );//24 hours
	})();
	
	
	(function updateTime()	{
		var now = new Date();
		var day = now.getDay();
		var date = now.getDate();
		var month = now.getMonth();
		var year = now.getFullYear();
		var date = moment.weekdays(day) + ', ' + date+' ' + moment.months(month) + ' ' + year;
		$('.date').html(date);
		$('.time').html(now.toTimeString().substring(0,5) + '<span class="sec">'+now.toTimeString().substring(6,8)+'</span>');
		
		setTimeout(function() {
			updateTime();
		}, 1000);//1 second
	})();

	(function updateCurrentWeather() {
		var iconTable = {
			'01d':'wi-day-sunny',
			'02d':'wi-day-cloudy',
			'03d':'wi-cloudy',
			'04d':'wi-cloudy-windy',
			'09d':'wi-showers',
			'10d':'wi-rain',
			'11d':'wi-thunderstorm',
			'13d':'wi-snow',
			'50d':'wi-fog',
			'01n':'wi-night-clear',
			'02n':'wi-night-cloudy',
			'03n':'wi-night-cloudy',
			'04n':'wi-night-cloudy',
			'09n':'wi-night-showers',
			'10n':'wi-night-rain',
			'11n':'wi-night-thunderstorm',
			'13n':'wi-night-snow',
			'50n':'wi-night-alt-cloudy-windy'		
		};
		

		$.getJSON('http://api.openweathermap.org/data/2.5/weather', weatherParams, function(json, textStatus) {

			var temp = roundVal(json.main.temp);

			var wind = roundVal(json.wind.speed);

			var iconClass = iconTable[json.weather[0].icon];
			var icon = $('<span/>').addClass('icon').addClass('dimmed').addClass('wi').addClass(iconClass);
			$('.temp').updateWithText(icon.outerHTML()+temp+'&deg;', 1000);

			var now = new Date();
			var sunrise = new Date(json.sys.sunrise*1000).toTimeString().substring(0,5);
			var sunset = new Date(json.sys.sunset*1000).toTimeString().substring(0,5);

			var windString = '<span class="wi wi-strong-wind xdimmed"></span> ' + kmh2beaufort(wind) ;
			var sunString = '<span class="wi wi-sunrise xdimmed"></span> ' + sunrise;
			if (json.sys.sunrise*1000 < now && json.sys.sunset*1000 > now) {
				sunString = '<span class="wi wi-sunset xdimmed"></span> ' + sunset;
			}

			$('.windsun').updateWithText(windString+' '+sunString+'  '+locale.q, 1000);
		});
			
		setTimeout(function() {
			updateCurrentWeather();
		}, 14400000); //  4 hours
	})();

	(function writeReceipt(){
		$.getJSON(urli+'receipt.json', function(receipt){
			if (receipt.length===1){
               welcomeMessage(receipt);
               previous=1;
			}else{
                if(receipt.length>previous){
                    $("#screen h2").hide();
                    $("#screen").addClass('receipt');
                    var line=receipt[previous];
                    if(parseInt(line.mode)===1){
                        var html='<div id="'+line.id+'" class="row small">'+line.line1+'&nbsp;'+line.line2+'</div>';
                        $("#receipt").prepend(html);
                        sum+=parseFloat(line.ammount);
                        sums[line.id]=line.ammount;
                    }else if(parseInt(line.mode)===0){
                        $("#"+line.id).remove();
                        sum-=sums[line.id];
                    }else {
                        $("#"+line.id).remove();
                        var html='<div id="'+line.id+'" class="row small">'+line.line1+'&nbsp;'+line.line2+'</div>';
                        $("#receipt").prepend(html);
                        sum-=parseFloat(sums[line.id]);
                        sum+=parseFloat(line.ammount);
                        sums[line.id]=line.ammount;
                    }
                    $("#sunolo").html('Σύνολο:&nbsp'+sum.toFixed(2)+'€');
                    previous++;
                    if(parseFloat(sum)<=0){
                        welcomeMessage(receipt)
                    }
				}
			}
		});
		setTimeout(function () {
			writeReceipt();
        },100);//0.1seconds
	})();
});

