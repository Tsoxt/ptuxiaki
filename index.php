<?php
@session_start();
if (isset($_SESSION['user'])){
    header('Location: '.$paths->_root.'view/main.php');
    exit;
}else{
    if(isset($_COOKIE['user'])){
        $_SESSION['username']=$_COOKIE['user'];
        header('Location: '.$paths->_root.'view/main.php');
        exit;
    }else{
        header('Location: '.$paths->_root.'view/login.php');
        exit;
    }
}