<?php
$fileroot="/home/ecs/ECSPos/";
$files=glob($fileroot."include/Database/*.php");
foreach ($files as $file){
    require_once $file;
}
$file=$argv[1];
$dir="/home/ecs/ECSPos/files/keys";
$privateKey=openssl_pkey_new(array(
    'digest_alg'=>'sha1',
    'private_key_bits'=>2048,
    'private_key_type'=>OPENSSL_KEYTYPE_RSA
));
$key=openssl_pkey_get_details($privateKey);
$publicKey=$key['key'];
if(is_writable($dir)){
    openssl_pkey_export_to_file($privateKey,$dir.'/private.key');
    $pkfile=$dir.'/license.key';
    file_put_contents($pkfile,$publicKey);
}
openssl_free_key($privateKey);
$versionFile=file_get_contents("/home/ecs/ECSPos/include/Database/versionDB.json");
$versionFile=json_decode($versionFile,true);
$version=$versionFile['version'];
$licenseData=file_get_contents($file);
$license=json_decode($licenseData,true);
$dbactions=new dbactions;
$query="INSERT INTO systemInfo VALUES (".$dbactions->quote($license['customer']).
    ",".$dbactions->quote($license['epaggelma']).",".$dbactions->quote($license['address']).
    ",".$dbactions->quote($license['city']).",".$dbactions->quote($license['TK']).",".$dbactions->quote($license['afm']).
    ",".$dbactions->quote($license['phone']).",".$dbactions->quote($license['doy']).",".$dbactions->quote($license['email']).
    ",".$dbactions->quote($license['seller']).",".$dbactions->quote($license['seller_phone']).",".$dbactions->quote($license['seller_email']).
    ",".$dbactions->quote($license['program']).",NULL,".$version.",NOW(),".$dbactions->quote($license['serial']).",NULL,NULL,NOW())";
$result=$dbactions->db_query($query);
echo var_dump($result)."\n";
if(file_put_contents($dir."/license",$licenseData)){
    echo "license ok\n";
}else{
    echo "license failed\n";
}



