<?php
$log="/home/ecs/ECSPos/Logs/system/phpErrors.txt";
$directory="/home/ecs/ECSPos/include/Database/";
$versionFile=file_get_contents($directory."versionDB.json");
$config = parse_ini_file('/var/www/html/public/credentials/config.ini');

$versionFile=json_decode($versionFile,true);
$query="SELECT `versionDB` FROM `systemInfo` WHERE `afm` is not NULL;";
$selectQuery="mysql -u '".$config['user']."' -p'".$config['password']."' -D ecs -e ".escapeshellarg($query)." ";
$output=array();
$return['response']=true;
exec($selectQuery,$output,$error);
if($error===0){
    if ((int)$output[1]===(int)$versionFile['version']){
        $return['response']=false;
        $return['msg']="Η βάση σας είναι ενημερωμένη!";
        echo $return['msg'];
    }else{
        //todo backup first
        for($i=(int)$output[1];$i<(int)$versionFile['version'];$i++){
            $scriptFile=$directory."databaseUpdateScripts/update_from_".$i."_to_".($i+1).".sql";
            $scriptFile=file_get_contents($scriptFile);
            $scripts=explode("|",$scriptFile);
            foreach ($scripts as $script){
                $command="mysql -u '".$config['user']."' -p'".$config['password']."' -D ecs -e ".escapeshellarg($script)." ";
                $output=array();
                exec($command,$output,$error);
                if($error!==0){
                    file_put_contents($log,$output."Command failed: ".$command." File: ".$scriptFile."/n",FILE_APPEND);
                    $return['response']=false;
                    $return['error']="Κάτι πήγε στραβά κατά την ενημέρωση της βάσης. Παρακαλώ ελέγξτε το αρχείο λαθών.";
                    echo $return['error'];
                    break 2;
                }
            }
        }
    }
}else{
    file_put_contents($log,$output."Command failed: ".$selectQuery."/n",FILE_APPEND);
    $return['response']=false;
    $return['error']="Κάτι πήγε στραβά στην ενημέρωση της βάσης. Παρακαλώ ελέγξτε το αρχείο λαθών.";
    echo $return['error'];
}
if ($return['response']===true){
    $query="UPDATE `systemInfo` SET `versionDB`= ".(int)$versionFile['version']." WHERE `afm` is not NULL;";
    $selectQuery="mysql -u ".$config['user']." -p".$config['password']." -D ecs -e ".escapeshellarg($query)." ";
    $output=array();
    exec($selectQuery,$output,$error);
    if ($error===0){
        echo "Η βάση ενημερώθηκε με επιτυχία!";
    }else{
        $return['response']=false;
        $return['error']='Δεν ενημερώθηκε η έκδοση της βάσης στον αντίστοιχο πίνακα.';
        echo $return['error'];
    }

}
return $return;


