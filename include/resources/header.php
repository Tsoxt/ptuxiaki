<?php
@session_start();
if(!isset($_SESSION['user'])){
    if(!isset($_COOKIE['user'])){
        header('Location: login.php');
        exit;
    }
}
header('Content-Type: text/html; charset=utf-8');
header(date_default_timezone_set('Europe/Athens'));
require '../include/resources/paths.php';
$paths= new paths;
$date=date('d/m/y');
$user['name']=$_SESSION['user']['name'];
$user['id']=$_SESSION['user']['user_id'];
$user['access']=$_SESSION['user']['uac'];
$serverIp=$_SERVER['SERVER_ADDR'];
?>
