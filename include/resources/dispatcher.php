<?php
error_reporting(E_ALL);
ini_set('display_startup_errors', 'off');
ini_set('display_errors', 'off');
ini_set("log_errors", 1);
ini_set("error_log", $_SERVER['DOCUMENT_ROOT']."/Logs/system/phpErrors.txt");
@session_start();

$files=glob($_SERVER['DOCUMENT_ROOT'].'/include/Database/*.php');
foreach ($files as $file){
    require_once $file;
}
require_once $_SERVER['DOCUMENT_ROOT'].'/include/resources/Settings.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/include/resources/User.php';
$files=glob($_SERVER['DOCUMENT_ROOT'].'/include/objectEntities/*.php');
foreach ($files as $file){
    require_once $file;
}
date_default_timezone_set('Europe/Athens');
$return=array();
if(!isset($_SESSION['user'])){
    if(!isset($_COOKIE['user'])){
        if($_POST['action']==="login") {
            $data = json_decode($_POST['data'], true);
            $user = new User();
            $return=$user->login($data);
            $return['loginStatus']=true;
            unset($user);
        }elseif ($_POST['action']==="showactiveusers") {
            $user = new User();
            $return = $user->showActiveUsers();
            $return['loginStatus'] = true;
            unset($user);
        }elseif($_POST['logout']){
            $user = new User();
            $user->logout();
            unset($user);
        }elseif ($_POST['action']==="systeminfo"){
            $validation=new Validation();
            $return=$validation->getProgramInfo();
            unset($validation);
        }elseif ($_POST['action']==="checkValidation"){
            //$data = json_decode($_POST['data'], true);
            $validation=new Validation();
            $return=$validation->insertLicense($_POST['data']);
            unset($validation);
        }else{
            $return['loginStatus']=false;
        }
        echo json_encode($return);
        exit;
    }
}

if(isset($_POST['data'])){
    $data=json_decode($_POST['data'],true);
}
switch ($_POST['action']){
    //<editor-fold desc="User interaction">
    case 'reloadSession':
        break;
    case 'getallusers':
        $user=new User();
        $return=$user->getAllUsers();
        unset($user);
        break;
    case 'userinfo':
        $user=new User();
        $return=$user->getUser();
        unset($user);
        break;
    case 'insertnewuser':
        $user=new User();
        $return=$user->insertNewUser($data);
        unset($user);
        break;
    case 'logout':
        $user=new User();
        $user->logout();
        break;
    case 'updateuseruac':
        $user=new User();
        $return=$user->updateUserUac($data);
        unset($user);
        break;
    case 'updateuserpass':
        $user=new User();
        $return=$user->updateUserPass($data);
        unset($user);
        break;
    case 'updateuseractive':
        $user=new User();
        $return=$user->updateUserActive($data);
        unset($user);
        break;
    //</editor-fold>
    //<editor-fold desc="categories">
    case 'callcategories':
        $category =new Category();
        $return=$category->getCategories();
        unset($category);
        break;
    case 'getcategory':
        $category=new Category();
        $return=$category->getCategory($data);
        unset($category);
        break;
    case 'insertcategory':
        $category=new Category();
        $return=$category->insertCategory($data);
        unset($category);
        break;
    case 'updatecategory':
        $category=new Category();
        $return=$category->updateCategory($data);
        unset($category);
        break;
    //</editor-fold>
    //<editor-fold desc="omades">
    case 'callomades':
        $omada=new Omada();
        $return=$omada->getOmades($data);
        unset($omada);
        break;
    case 'getomada':
        $omada=new Omada();
        $return=$omada->getOmada($data);
        unset($omada);
        break;
    case 'updateomada':
        $omada=new Omada();
        $return=$omada->updateOmada($data);
        unset($omada);
        break;
    case 'insertomada':
        $omada=new Omada();
        $return=$omada->insertOmada($data);
        unset($omada);
        break;
    //</editor-fold>
    //<editor-fold desc="items">

    //<editor-fold desc="search">
    case 'callitems':
        $proion=new Proion();
        $return=$proion->callItems($data);
        unset($proion);
        break;
    case 'bybarcode':
        $proion=new Proion();
        $return=$proion->findBarcode($data);
        unset($proion);
        break;
    case 'byid':
        $proion=new Proion();
        $return=$proion->findId($data);
        unset($proion);
        break;
    case 'similarproducts':
        $proion=new Proion();
        $return=$proion->findSimilarProducts($data);
        unset($proion);
        break;
    case 'bydate':
        $proion=new Proion();
        $return=$proion->itemsByDate($data);
        unset($proion);
        break;
    case 'betweendates':
        $proion=new Proion();
        $return=$proion->itemsBetweenDates($data);
        unset($proion);
        break;
    case 'sunolo':
        $proion=new Proion();
        $return=$proion->getSumStock($data);
        unset($proion);
        break;
    //</editor-fold>
    //<editor-fold desc="Updates">
    case 'updateEpistrofi':
        $proion=new Proion();
        $return=$proion->updateEpistrofi($data);
        unset($proion);
        break;
    case 'updatetimiagoras':
        $proion=new Proion();
        $return=$proion->updateTimiAgoras($data);
        unset($proion);
        break;
    case 'updatestockrecommend':
        $proion=new Proion();
        $return=$proion->updateStockRecommend($data);
        unset($proion);
        break;
    case 'updatestockalert':
        $proion=new Proion();
        $return=$proion->updateStockAlert($data);
        unset($proion);
        break;
    case 'updatezugaria':
        $proion=new Proion();
        $return=$proion->updateZugaria($data);
        unset($proion);
        break;
    case 'updatedescription':
        $proion=new Proion();
        $return=$proion->setDescription($data);
        unset($proion);
        break;
    case 'updatealtdescription':
        $proion=new Proion();
        $return=$proion->setAltDescription($data);
        unset($proion);
        break;
    case 'updateitemomada':
        $proion=new Proion();
        $return=$proion->setOmada($data);
        unset($proion);
        break;
    case 'updatebarcode':
        $proion=new Proion();
        $return=$proion->setBcode($data);
        unset($proion);
        break;
    case 'updateprice':
        $proion=new Proion();
        $return=$proion->setPrice($data);
        unset($proion);
        break;
    case 'updatetmima':
        $proion=new Proion();
        $return=$proion->setTmima($data);
        unset($proion);
        break;
    case 'updatestock':
        $proion=new Proion();
        $return=$proion->updateStock($data);
        unset($proion);
        break;
    case 'updatepackage':
        $proion=new Proion();
        $return=$proion->setPackage($data);
        unset($proion);
        break;
    case 'updateactive':
        $proion=new Proion();
        $return=$proion->updateActive($data);
        unset($proion);
        break;
    case 'updatemcode':
        $proion=new Proion();
        $return=$proion->setMothercode($data);
        unset($proion);
        break;
    case 'insertproion':
        $proion=new Proion();
        $return=$proion->insertItem($data);
        unset($proion);
        break;
    case 'updatescale':
        $scales=new Scales();
        $return=$scales->updatePLUs();
        unset($scales);
        break;
    case 'getaltperigrafi':
        $scales=new Scales();
        $return['response']=$scales->getAlternativeStatus();
        unset($scales);
        break;
    case 'changeperigrafi':
        $scales=new Scales();
        $return=$scales->updateAlternativeStatus();
        unset($scales);
        break;
    //</editor-fold>

    //</editor-fold>
    //<editor-fold desc="Tmimata">
    case 'bytmima':
        $tmima=new Fpa();
        $return=$tmima->findTmima($data);
        unset($tmima);
        break;
    case 'calltmimata':
        $fpa=new Fpa();
        $return=$fpa->getTmimata();
        unset($fpa);
        break;
    case 'inserttmima':
        $fpa=new Fpa();
        $return=$fpa->insertTmima($data);
        unset($fpa);
        break;
    case 'updatefpa':
        $fpa=new Fpa();
        $return=$fpa->updateTmima($data);
        unset($fpa);
        break;
    case 'updatetmimaactive':
        $fpa=new Fpa();
        $return=$fpa->updateTmimaActive($data);
        unset($fpa);
        break;
    //</editor-fold>
    //<editor-fold desc="Receipt">
    case 'submit':
        $receipt=new Receipt();
        $return=$receipt->handleReceipt($data);
        unset($receipt);
        break;
    case 'storeisozigio':
        $receipt=new Receipt();
        $return=$receipt->storeIsozigio($data);
        unset($receipt);
        break;
    case 'payCustomerYpoloipo':
        $receipt=new Receipt();
        $return=$receipt->payCustomerYpoloipo($data);
        unset($receipt);
        break;
    //</editor-fold>
    //<editor-fold desc="Special Barcodes">
    case 'getallspecial':
        $special = new SpecialBarcodes();
        $return=$special->getAllBarcodes();
        unset($special);
        break;
    case 'getspecial':
        $special = new SpecialBarcodes();
        $return=$special->getBarcodes();
        unset($special);
        break;
    case 'updatespecial':
        $special=new SpecialBarcodes();
        $return=$special->updateBarcodes($data);
        unset($special);
        break;
    //</editor-fold>
    //<editor-fold desc="Settings">
    case 'getSettings':
        $settings=new Settings();
        $return=$settings->getSettings($data);
        unset($settings);
        break;
    case 'updatesetting':
        $settings=new Settings();
        $return=$settings->updateSetting($data);
        unset($settings);
        break;
    case 'insertsetting':
        $settings=new Settings();
        $return=$settings->insertScale($data);
        unset($settings);
        break;
    //</editor-fold>
    //<editor-fold desc="Receipt">
    case 'zreport':
        $cashier=new Cashier();
        $return=$cashier->ZReport();
        unset($cashier);
        break;
    case 'xreport':
        $cashier=new Cashier();
        $return=$cashier->XReport();
        unset($cashier);
        break;
    case 'togglecashier':
        $cashier=new Cashier();
        $return=$cashier->toggleCashier();
        unset($cashier);
        break;
    //</editor-fold>
    //<editor-fold desc="Stats">
    case 'callstatsbyuser':
        $stats=new Stats();
        $return=$stats->getUserStats($data);
        unset($stats);
        break;
    case 'callstatsbytmima':
        $stats=new Stats();
        $return=$stats->getTmimataStats($data);
        unset($stats);
        break;
    case 'callstatsbyomada':
        $stats=new Stats();
        $return=$stats->getOmadesStats($data);
        unset($stats);
        break;
    case 'callisozigio':
        $stats=new Stats();
        $return=$stats->getIsozigio($data);
        unset($stats);
        break;
    case 'callstatsbyhour':
        $stats=new Stats();
        $return=$stats->getStatsPerHour($data);
        unset($stats);
        break;
    case 'callstatsbypaymentmethods':
        $stats=new Stats();
        $return=$stats->getStatsByPaymentMethods($data);
        unset($stats);
        break;
    case 'getlastzita':
            $data['type']=2;
            $data['date']=date("Y-m-d H:i:s");
            $logs=new Logs();
            $return=$logs->getLastLog($data);
            unset($logs);
            break;
    case 'insertlog':
        $logs=new Logs();
        $return=$logs->insertLog($type);
        unset($logs);
        break;
    //</editor-fold>
    //<editor-fold desc="system">
    case 'customerinfo':
        $cdisplay=new customerDisplay();
        $return=$cdisplay->sendLine($data);
        unset($cdisplay);
        break;
    case 'resetdisplay':
        $cdisplay=new customerDisplay();
        $return=$cdisplay->resetDisplay();
        unset($cdisplay);
        break;
    case 'customerdel':
        $cdisplay=new customerDisplay();
        $return=$cdisplay->deleteLine($data);
        unset($cdisplay);
        break;
    case 'customerupdate':
        $cdisplay= new customerDisplay();
        $return=$cdisplay->updateLine($data);
        unset($cdisplay);
        break;
    //</editor-fold>
    //<editor-fold desc="Payments">
    case 'showpaymentmethods':
        $payments=new Payments();
        $return=$payments->getPaymentMethods();
        unset($payments);
        break;
    //</editor-fold>
    //<editor-fold desc="Prints">
    case 'printisozigio':
        $print=new Printer();
        $return=$print->printIsozigio($data);
        unset($print);
        break;
    //</editor-fold>
    //<editor-fold desc="Customers">
    case 'searchcustomer':
        $customers=new Customers();
        $return=$customers->searchCustomers($data);
        unset($customers);
        break;
    case 'insertcustomer':
        $customers=new Customers();
        $return=$customers->insertCustomer($data);
        unset($customers);
        break;
    case 'getcustomerdiscounts':
        $customers=new Customers();
        $return=$customers->getDiscounts($data);
        unset($customers);
        break;
    case 'getcustomerypoloipo':
        $customers=new Customers();
        $return=$customers->getYpoloipo($data);
        unset($customers);
        break;
    case 'updatecustomer':
        $customers=new Customers();
        $return=$customers->updateCustomer($data);
        unset($customers);
        break;
    //</editor-fold>
    //<editor-fold desc="Parastatika">
    case 'getcustomerparastatika':
        $parastatika=new Parastatika();
        $return=$parastatika->getParastatika($data);
        unset($parastatika);
        break;
    case 'reprintInvoice':
        $parastatika=new Parastatika();
        $return=$parastatika->reprintParastatiko($data);
        unset($parastatika);
        break;
    case 'getallparastatika':
        $parastatika=new Parastatika();
        $return=$parastatika->getAllParastatikaInfo($data);
        unset($parastatika);
        break;
    case 'matchCustomerParastatika':
        $parastatika=new Parastatika();
        $return=$parastatika->matchCustomerParastatika($data);
        unset($parastatika);
        break;
    case 'customerParastatikoInfo':
        $parastatika=new Parastatika();
        $return=$parastatika->parastatikaInfo($data['id']);
        unset($parastatika);
        break;
    //</editor-fold>
    //<editor-fold desc="Validation">
    case 'getsysteminfo':
        $validation=new Validation();
        $return=$validation->getProgramInfo();
        unset($validation);
        break;
    //</editor-fold>
    //<editor-fold desc="CustomersDiscounts">
    case 'addDiscount':
        $discounts=new CustomersDiscounts();
        $return=$discounts->addDiscount($data);
        unset($discounts);
        break;
    case 'updateDiscount':
        $discounts=new CustomersDiscounts();
        $return=$discounts->updateDiscount($data);
        unset($discounts);
        break;
    case 'deleteDiscount':
        $discounts=new CustomersDiscounts();
        $return=$discounts->deleteDiscount($data);
        unset($discounts);
        break;
    //</editor-fold>
    //<editor-fold desc="Suppliers">
    case 'searchSupplier':
        $suppliers=new Suppliers();
        $return=$suppliers->searchSupplier($data);
        unset($suppliers);
        break;
    case 'insertSupplier':
        $suppliers=new Suppliers();
        $return=$suppliers->insertSupplier($data);
        unset($suppliers);
        break;
    case 'getSupplierProducts':
        $suppliers=new Suppliers();
        $return=$suppliers->getSupplierProducts($data);
        unset($suppliers);
        break;
    case 'getSupplierYpoloipo':
        $suppliers=new Suppliers();
        $return=$suppliers->getSupplierYpoloipo($data);
        unset($suppliers);
        break;
    case 'updateSupplier':
        $suppliers=new Suppliers();
        $return=$suppliers->updateSupplier($data);
        unset($suppliers);
        break;
    case 'addSupplierProduct':
        $suppliers=new Suppliers();
        $return=$suppliers->addSupplierProduct($data);
        unset($suppliers);
        break;
    case 'updateSupplierProduct':
        $suppliers=new Suppliers();
        $return=$suppliers->updateSupplierProduct($data);
        unset($suppliers);
        break;
    case 'deleteSupplierProduct':
        $suppliers=new Suppliers();
        $return=$suppliers->deleteSupplierProduct($data);
        unset($suppliers);
        break;
    //</editor-fold>
    //<editor-fold desc="SuppliersParastatika">
    case 'insertDeltioParalavis':
        $parastatika=new SuppliersParastatika();
        $return=$parastatika->insertDeltioParalavis($data);
        unset($parastatika);
        break;
    case 'insertDeltioParaggelias':
        $parastatika=new SuppliersParastatika();
        $return=$parastatika->insertDeltioParaggelias($data);
        unset($parastatika);
        break;
    case 'insertDeltioEpistrofis':
        $parastatika=new SuppliersParastatika();
        $return=$parastatika->insertDeltioEpistrofis($data);
        unset($parastatika);
        break;
    case 'matchSupplierParastatiko':
        $parastatika=new SuppliersParastatika();
        $return=$parastatika->matchParastatiko($data);
        unset($parastatika);
        break;
    case 'getSuppliersOrders':
        $parastatika=new SuppliersParastatika();
        $return=$parastatika->getOrders($data);
        unset($parastatika);
        break;
    case 'getOrderDetails':
        $parastatika=new SuppliersParastatika();
        $return=$parastatika->getOrderDetails($data);
        unset($parastatika);
        break;
    case 'removeOrder':
        $parastatika=new SuppliersParastatika();
        $return=$parastatika->removeOrder($data);
        unset($parastatika);
        break;
    case 'getSupplierParastatika':
        $parastatika=new SuppliersParastatika();
        $return=$parastatika->getSupplierParastatika($data);
        unset($parastatika);
        break;
    case 'supplierParastatikoInfo':
        $parastatika=new SuppliersParastatika();
        $return=$parastatika->supplierParastatikaInfo($data);
        unset($parastatika);
        break;
    case 'paySupplierYpoloipo':
        $parastatika=new SuppliersParastatika();
        $return=$parastatika->payYpoloipo($data);
        unset($parastatika);
        break;
    //</editor-fold>

}
$return['loginStatus']=true;
echo json_encode($return);
exit;
