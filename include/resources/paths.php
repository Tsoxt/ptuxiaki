<?php

define("ROOT",$_SERVER['SERVER_ADDR']."/");

class paths
{
    public $_root=ROOT;
    public $_jquery="../vendor/components/jquery/jquery.min.js";
    public $_charts="../vendor/components/charts/Chart.min.js";
    public $_bootstrap_css="../vendor/twbs/bootstrap/dist/css/bootstrap.min.css";
    public $_bootstrap_js="../vendor/twbs/bootstrap/dist/js/bootstrap.min.js";
    public $_bootbox_js="../vendor/components/bootbox/bootbox.min.js";
}