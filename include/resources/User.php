<?php

class User
{
    private $model,$validation,$actions;

    public function __construct()
    {
        $db=new dbmodel;
        $this->model=$db->_users;
        $this->validation=new Validation();
        $this->actions=new dbactions();
    }

    public function login($data){
        $form=array();
        foreach ($data as $result){
            $form[str_replace('"', '', $result['name'])]=str_replace('"', '', $result['value']);
        }
        $query="SELECT * FROM ".$this->model['table']." WHERE ".$this->model['uname']." = ".$this->actions->quote($form['username']).";";
        $return=$this->actions->db_query($query);
        if(mysqli_num_rows($return)>0){
            $result=mysqli_fetch_assoc($return);
            if($result['password']==$form['passwd']){
                $response['response']=true;
                $expiration=$this->validation->checkExpirationDate();
                if ($expiration['response']){
                    $response['lock']=$expiration['lock'];
                    if ($expiration['lock']===true){//ean exei liksei kai i periodos twn extra 15 imerwn
                        if ((int)$result['uac']!==0) {
                            $response['response'] = false;
                            $response['msg']=$expiration['msg'];
                        }else{
                            @session_start();
                            $_SESSION['user']=$result;
                            setcookie("user", $_SESSION['user'],time() + (10 * 365 * 24 * 60 * 60));
                        }
                    }else{
                        @session_start();
                        $_SESSION['user']=$result;
                        setcookie("user", $_SESSION['user'],time() + (10 * 365 * 24 * 60 * 60));
                        if (isset($expiration['msg'])){
                            $response['msg']=$expiration['msg'];
                        }

                    }
                }else{
                    $response['response']=false;
                    $response['error']=$expiration;
                }
            }else{
                $response['response']=false;
            }
        }else{
            $response['response']=false;
        }
        return $response;
    }

    public function getUser(){
        if(isset($_SESSION['user'])){
            @session_start();
            $return['response']=true;
            $return['user']=$_SESSION['user'];
        }else{
            $return['response']=false;
        }
        return $return;
    }

    public function getAllUsers(){
        $this->actions = new dbactions;
        $query="SELECT * FROM ".$this->model['table']." WHERE ".$this->model['access'].">0;";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['users']=array();
            while($row=mysqli_fetch_assoc($result)){
                array_push($return['users'],$row);
            }

        }else{
            $return['response']=false;
            $return['error']="Υπήρξε κάποιο πρόβλημα, παρακαλώ προσπαθήστε ξανά";
        }
        return $return;
    }

    public function showActiveUsers(){
        $this->actions = new dbactions;
        $query="SELECT ".$this->model['uname'].", ".$this->model['name']." FROM ".$this->model['table']." WHERE ".$this->model['access'].">0 AND ".$this->model['active']."=1;";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['users']=array();
            while($row=mysqli_fetch_assoc($result)){
                array_push($return['users'],$row);
            }

        }else{
            $return['response']=false;
            $return['error']="Υπήρξε κάποιο πρόβλημα, παρακαλώ προσπαθήστε ξανά";
        }
        return $return;
    }

    public function insertNewUser($data){
        $this->actions = new dbactions;
        $query = "SELECT * FROM ".$this->model['table']." WHERE ".$this->model['uname']." = ".$this->actions->quote($data['username'])." ;";
        $result=$this->actions->db_query($query);
        if (mysqli_num_rows($result)>0){
            $return['response']=false;
            $return['error']='Το username χρησιμοποιείται ήδη. Παρακαλώ επιλέξτε άλλο';
        }else{
            $query = "INSERT INTO ".$this->model['table']." VALUES ( NULL, ".$this->actions->quote($data['username']).", ".$data['password'].", ".$this->actions->quote($data['name']).", ".(int)$data['uac'].", 1) ;";
            $result=$this->actions->db_query($query);
            if($result===true) {
                $return['response']=$result;
            }else{
                $return['response']=false;
                $return['error']=$result;
            }
        }
        return $return;
    }

    public function logout(){
        session_destroy();
        if(isset($_SESSION['user'])){
            unset($_SESSION['user']);
        }
        $display=new customerDisplay();
        $display->closeDisplay();
    }

    public function updateUserUac($data){
        $this->actions = new dbactions;
        $query="UPDATE ".$this->model['table']." SET ".$this->model['access']." = ".(int)$data['value']." WHERE ".$this->model['id']." = ".$data['id'].";";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=$result;
        }else{
            $return['query']=$query;
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function updateUserActive($data){
        $this->actions = new dbactions;
        $query="UPDATE ".$this->model['table']." SET ".$this->model['active']." = ".(int)$data['value']." WHERE ".$this->model['id']." = ".$data['id'].";";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=$result;
        }else{
            $return['query']=$query;
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function updateUserPass($data){
        $this->actions = new dbactions;
        $query="UPDATE ".$this->model['table']." SET ".$this->model['pass']." = ".$data['value']." WHERE ".$this->model['id']." = ".$data['id'].";";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=$result;
        }else{
            $return['query']=$query;
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }
}