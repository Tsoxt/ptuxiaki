<?php

class Settings
{
    private $settings=array();
    private $model,$actions;

    public function __construct()
    {//settings group value for call int 0 for cashier, 2 for api
        $db= new dbmodel;
        $this->actions=new dbactions();
        $this->model=$db->_settings;
    }

    private function getLatestScaleGroup(){
        $query="select max(".$this->model['group'].") from ".$this->model['table']." WHERE ".$this->model['group']." BETWEEN 100 AND 199;";
        return (int)mysqli_fetch_assoc($this->actions->db_query($query))["max(".$this->model['group'].")"]+1;
    }

    private function getLatestPrinterGroup(){
        $query="select max(".$this->model['group'].") from ".$this->model['table']." WHERE ".$this->model['group']." BETWEEN 200 AND 299;";
        return (int)mysqli_fetch_assoc($this->actions->db_query($query))["max(".$this->model['group'].")"]+1;
    }

    public function getSettings($group){
        if($group==='all'){
            $query="SELECT * FROM ".$this->model['table']." where ".$this->model['group']."<100 order by ".$this->model['group'].";";
        }else if((int)$group>=100&&(int)$group<200) {
            $query = "SELECT * FROM ".$this->model['table']." WHERE ".$this->model['group']." between 100 AND 199".
            " order by ".$this->model['group'].";";
        }else if((int)$group>=200&&(int)$group<300) {
            $query = "SELECT * FROM ".$this->model['table']." WHERE ".$this->model['group']." between 200 AND 299".
                " order by ".$this->model['group'].";";
        }else{
            $query = "SELECT * FROM " . $this->model['table'] . " WHERE " . $this->model['group'] . "=" . (int)$group . " ;";
        }
        $response=$this->actions->db_query($query);
        if(mysqli_num_rows($response)>0){
            $this->settings['response']=true;
            $this->settings['body']=array();
            if ($group==='all'){
                $json=file_get_contents("../ConfigFiles/printers_types.json");
                $this->settings['printers']=json_decode($json,true);
            }
            $i=-1;
            $currentGroup=-1;
            while ($line=mysqli_fetch_assoc($response)){
                if($currentGroup!=(int)$line['setgroup']){
                    $i++;
                    $currentGroup=(int)$line['setgroup'];
                    $this->settings['body'][$currentGroup]=array();
                }
                foreach ($line as $key=>$value){
                    if($key==="setgroup"){
                        continue;
                    }
                    if($key==="name"){
                        $name=$value;
                        continue;
                    }
                    $array[$key]=$value;
                }
                $this->settings['body'][$currentGroup][$name]=$array;
            }
        }else{
            $this->settings['response']=false;
            $this->settings['error']=$response;
        }
        return $this->settings;
    }

    public function updateSetting($data){
        $query="update ".$this->model['table']." set ".$this->model['value']."=".$this->actions->quote($data['value'])." where ".$this->model['id']."=".(int)$data['id'].";";
        $result=$this->actions->db_query($query);
        if($result===true){
            $response['response']=$result;
        }else{
            $response['response']=false;
            $response['error']=$result;
        }
        return $response;
    }

    public function insertScale($data){
        $response['response']=true;
        $data['setgroup']=$this->getLatestScaleGroup();
        foreach ($data['group'] as $field){
            if ($field['name']==="groupname"){
                $field['description']=$field['value'];
                $field['value']=1;
            }
            if ($field['name']==="scaleIp"){
                $field['value']=$this->actions->quote($field['value']);
            }else{
                $field['value']=(int)$field['value'];
            }
            $query='INSERT INTO '.$this->model['table']." VALUES (NULL, ".$this->actions->quote($field['name']).", ".$this->actions->quote($field['description'])
                .", ".$field['value'].", ".$data['setgroup'].");";
            $result=$this->actions->db_query($query);
            if($result===true){
                $response['response']=$result;
            }else{
                $response['response']=false;
                $response['error']=$result;
                break;
            }

        }
        return $response;
    }

    public function insertPrinter($data){
        $response['response']=true;
        $data['setgroup']=$this->getLatestPrinterGroup();
        foreach ($data['group'] as $field){
            if ($field['name']==="groupname"){
                $field['description']=$field['value'];
                $field['value']=1;
            }
            if ($field['name']==="printerPath"){
                $field['value']=$this->actions->quote($field['value']);
            }else{
                $field['value']=(int)$field['value'];
            }
            $query='INSERT INTO '.$this->model['table']." VALUES (NULL, ".$this->actions->quote($field['name']).", ".$this->actions->quote($field['description'])
                .", ".$field['value'].", ".$data['setgroup'].");";
            $result=$this->actions->db_query($query);
            if($result===true){
                $response['response']=$result;
            }else{
                $response['response']=false;
                $response['error']=$result;
                break;
            }
        }
    }
}