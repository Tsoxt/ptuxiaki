-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Φιλοξενητής: localhost
-- Χρόνος δημιουργίας: 23 Ιουλ 2019 στις 12:43:21
-- Έκδοση διακομιστή: 10.0.38-MariaDB-0ubuntu0.16.04.1
-- Έκδοση PHP: 7.0.33-0ubuntu0.16.04.5

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Βάση δεδομένων: `ecs_1-0-0`
--

DELIMITER $$
--
-- Διαδικασίες
--
CREATE DEFINER=`phpmyadmin`@`localhost` PROCEDURE `incrementInvoNum` (IN `id_in` INT)  BEGIN UPDATE parastatikaInfo SET lastNumber=lastNumber+1 WHERE id=id_in; SELECT * FROM parastatikaInfo WHERE id=id_in; END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `category`
--

CREATE TABLE `category` (
  `id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `altID` mediumint(6) UNSIGNED DEFAULT NULL,
  `name` varchar(60) DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `seira` tinyint(2) UNSIGNED DEFAULT NULL,
  `api` tinyint(1) NOT NULL DEFAULT '0',
  `new` smallint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `category`
--

TRUNCATE TABLE `category`;
--
-- Άδειασμα δεδομένων του πίνακα `category`
--

INSERT INTO `category` (`id`, `altID`, `name`, `active`, `seira`, `api`, `new`) VALUES
(900000, NULL, 'Χωρίς Κατηγορία', 1, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `customers`
--

CREATE TABLE `customers` (
  `id` int(11) UNSIGNED ZEROFILL NOT NULL,
  `afm` varchar(9) DEFAULT NULL,
  `name` varchar(70) NOT NULL,
  `address` varchar(45) NOT NULL,
  `city` varchar(20) NOT NULL,
  `postcode` varchar(5) NOT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `mobile` varchar(10) DEFAULT NULL,
  `job` varchar(45) DEFAULT NULL,
  `doy` varchar(45) DEFAULT NULL,
  `member` varchar(13) DEFAULT NULL,
  `type` tinyint(1) NOT NULL,
  `points` mediumint(8) UNSIGNED NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `customers`
--

TRUNCATE TABLE `customers`;
-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `customersDiscounts`
--

CREATE TABLE `customersDiscounts` (
  `id_proion` int(11) NOT NULL,
  `id_customer` int(11) UNSIGNED ZEROFILL NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  `type` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `customersDiscounts`
--

TRUNCATE TABLE `customersDiscounts`;
-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `fpa`
--

CREATE TABLE `fpa` (
  `id` tinyint(3) UNSIGNED ZEROFILL NOT NULL,
  `name` varchar(20) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `editable` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `fpa`
--

TRUNCATE TABLE `fpa`;
--
-- Άδειασμα δεδομένων του πίνακα `fpa`
--

INSERT INTO `fpa` (`id`, `name`, `price`, `editable`, `active`) VALUES
(001, 'ΕΙΔΗ 6%', '0.06', 0, 1),
(002, 'ΕΙΔΗ 13%', '0.13', 0, 1),
(003, 'ΕΙΔΗ 24%', '0.24', 0, 1),
(004, 'ΚΑΡΤΕΣ', '0.00', 0, 0),
(005, 'ΤΣΙΓΑΡΑ', '0.00', 0, 0),
(006, 'ΕΙΣΙΤΗΡΙΑ', '0.00', 0, 0),
(007, 'ΣΑΚΟΥΛΑ', '0.24', 0, 0),
(008, 'ΕΦΗΜΕΡΙΔΑ', '0.00', 0, 0);

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `isozugio`
--

CREATE TABLE `isozugio` (
  `id` int(11) UNSIGNED ZEROFILL NOT NULL,
  `id_omadas` tinyint(2) UNSIGNED ZEROFILL NOT NULL,
  `eidos_id` varchar(20) DEFAULT NULL,
  `quantity` decimal(7,3) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `isozugio`
--

TRUNCATE TABLE `isozugio`;
-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `kiniseis`
--

CREATE TABLE `kiniseis` (
  `id_kinisis` int(10) UNSIGNED ZEROFILL NOT NULL,
  `mothercode` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `barcode` varchar(15) DEFAULT NULL,
  `id_omadas` int(9) UNSIGNED ZEROFILL NOT NULL,
  `quantity` decimal(7,3) NOT NULL,
  `timi` decimal(7,2) UNSIGNED NOT NULL,
  `fpa` tinyint(3) UNSIGNED NOT NULL,
  `sum` decimal(10,2) UNSIGNED NOT NULL,
  `tmima` tinyint(3) UNSIGNED ZEROFILL NOT NULL,
  `temaxia` smallint(6) UNSIGNED NOT NULL,
  `receipt` int(9) UNSIGNED ZEROFILL NOT NULL,
  `uploaded` tinyint(2) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `kiniseis`
--

TRUNCATE TABLE `kiniseis`;
-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `Log`
--

CREATE TABLE `Log` (
  `id` int(4) UNSIGNED ZEROFILL NOT NULL,
  `type` tinyint(1) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `Log`
--

TRUNCATE TABLE `Log`;
-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `omada`
--

CREATE TABLE `omada` (
  `id` int(9) UNSIGNED ZEROFILL NOT NULL,
  `altID` mediumint(8) UNSIGNED DEFAULT NULL,
  `name` varchar(60) NOT NULL,
  `id_category` int(6) UNSIGNED ZEROFILL NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `seira` tinyint(2) UNSIGNED DEFAULT NULL,
  `api` tinyint(1) NOT NULL DEFAULT '0',
  `new` smallint(1) UNSIGNED NOT NULL DEFAULT '0',
  `omada_zugaria` tinyint(2) UNSIGNED ZEROFILL DEFAULT NULL,
  `isozigio` smallint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `omada`
--

TRUNCATE TABLE `omada`;
--
-- Άδειασμα δεδομένων του πίνακα `omada`
--

INSERT INTO `omada` (`id`, `altID`, `name`, `id_category`, `active`, `seira`, `api`, `new`, `omada_zugaria`, `isozigio`) VALUES
(900000000, NULL, 'Χωρίς Ομάδα', 900000, 1, NULL, 0, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `parastatika`
--

CREATE TABLE `parastatika` (
  `id` int(11) UNSIGNED ZEROFILL NOT NULL,
  `customer_id` int(11) UNSIGNED ZEROFILL NOT NULL,
  `receipt_id` int(11) UNSIGNED ZEROFILL NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `aa` int(6) UNSIGNED ZEROFILL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `parastatika`
--

TRUNCATE TABLE `parastatika`;
-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `parastatikaInfo`
--

CREATE TABLE `parastatikaInfo` (
  `id` tinyint(2) NOT NULL,
  `name` varchar(40) NOT NULL,
  `shortname` varchar(20) NOT NULL,
  `lastNumber` mediumint(6) UNSIGNED ZEROFILL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `parastatikaInfo`
--

TRUNCATE TABLE `parastatikaInfo`;
--
-- Άδειασμα δεδομένων του πίνακα `parastatikaInfo`
--

INSERT INTO `parastatikaInfo` (`id`, `name`, `shortname`, `lastNumber`) VALUES
(1, 'Τιμολόγιο Πώλησης-Δελτιο Αποστολής', 'ΤΠ-ΔΑΤ', 000000),
(2, 'Υπόλοιπο από μεταφορά', 'ΥΑΜ', NULL),
(3, 'Απόδειξη λιανικής πώλησης', 'ΑΛΠ', 000000),
(4, 'Απόδειξη είσπραξης', 'ΑΠΕ', NULL),
(5, 'Τιμολόγιο Πώλησης', 'ΤΠ', 000000);

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `paymentMethods`
--

CREATE TABLE `paymentMethods` (
  `id` tinyint(3) UNSIGNED ZEROFILL NOT NULL,
  `name` varchar(45) NOT NULL,
  `icon` varchar(40) DEFAULT 'far fa-credit-card',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `paymentMethods`
--

TRUNCATE TABLE `paymentMethods`;
--
-- Άδειασμα δεδομένων του πίνακα `paymentMethods`
--

INSERT INTO `paymentMethods` (`id`, `name`, `icon`, `active`) VALUES
(000, 'Έκπτωση', 'far fa-credit-card', 0),
(001, 'Πίστωση', 'far fa-credit-card', 0),
(002, 'Μετρητά', 'fas fa-money-bill-wave', 1),
(003, 'MasterCard', 'fab fa-cc-mastercard', 1),
(004, 'Visa', 'fab fa-cc-visa', 1),
(005, 'American Express', 'fab fa-cc-amex', 1);

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED ZEROFILL NOT NULL,
  `receipt` int(9) UNSIGNED ZEROFILL NOT NULL,
  `amount` decimal(10,3) NOT NULL,
  `card` tinyint(3) UNSIGNED ZEROFILL NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `payments`
--

TRUNCATE TABLE `payments`;
-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `proion`
--

CREATE TABLE `proion` (
  `id` int(11) NOT NULL,
  `barcode` varchar(20) DEFAULT NULL,
  `mothercode` int(11) DEFAULT NULL,
  `perigrafi` varchar(60) NOT NULL,
  `perigrafi2` varchar(60) NOT NULL,
  `id_omadas` int(9) UNSIGNED ZEROFILL NOT NULL,
  `timi` decimal(7,2) NOT NULL DEFAULT '0.00',
  `timi_agoras` decimal(7,2) DEFAULT '0.00',
  `flag_timi` tinyint(1) NOT NULL DEFAULT '0',
  `tmima` tinyint(3) UNSIGNED ZEROFILL DEFAULT NULL,
  `stock` decimal(10,3) NOT NULL DEFAULT '0.000',
  `stock_alert` decimal(10,3) NOT NULL DEFAULT '1.000',
  `stock_recommend` decimal(10,3) NOT NULL DEFAULT '1.000',
  `temaxia` smallint(6) NOT NULL DEFAULT '1',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `new` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `seira` tinyint(2) UNSIGNED DEFAULT NULL,
  `epistrofi` date DEFAULT NULL,
  `insertDate` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `zugistiko` tinyint(1) UNSIGNED NOT NULL,
  `api` tinyint(1) NOT NULL DEFAULT '0',
  `zugaria` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `temaxiako` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `alt_id` int(11) DEFAULT NULL,
  `daysToExpire` tinyint(2) UNSIGNED ZEROFILL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `proion`
--

TRUNCATE TABLE `proion`;
--
-- Άδειασμα δεδομένων του πίνακα `proion`
--

INSERT INTO `proion` (`id`, `barcode`, `mothercode`, `perigrafi`, `perigrafi2`, `id_omadas`, `timi`, `timi_agoras`, `flag_timi`, `tmima`, `stock`, `stock_alert`, `stock_recommend`, `temaxia`, `active`, `new`, `seira`, `epistrofi`, `insertDate`, `updated`, `zugistiko`, `api`, `zugaria`, `temaxiako`, `alt_id`, `daysToExpire`) VALUES
(1500000000, '0000000000001', 1500000000, 'ΤΜΗΜΑΤΑ', 'ΤΜΗΜΑΤΑ', 900000000, '0.00', '0.00', 0, NULL, '0.000', '1.000', '1.000', 1, 1, 0, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `receipts`
--

CREATE TABLE `receipts` (
  `id` int(9) UNSIGNED ZEROFILL NOT NULL,
  `id_tameiou` tinyint(3) UNSIGNED ZEROFILL NOT NULL,
  `operator` tinyint(3) UNSIGNED ZEROFILL NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `receipts`
--

TRUNCATE TABLE `receipts`;
-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `settings`
--

CREATE TABLE `settings` (
  `id` mediumint(6) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `value` varchar(40) DEFAULT NULL,
  `setgroup` tinyint(3) UNSIGNED ZEROFILL NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `settings`
--

TRUNCATE TABLE `settings`;
--
-- Άδειασμα δεδομένων του πίνακα `settings`
--

INSERT INTO `settings` (`id`, `name`, `description`, `value`, `setgroup`) VALUES
(1, 'cashierIp', 'IP μηχανής', '', 000),
(2, 'cashierPort', 'Πόρτα', '', 000),
(3, 'cashierProtocol', 'Πρωτόκολλο', '', 000),
(4, 'cashierVendor', 'Τύπος', '', 000),
(5, 'cashierDriverPath', 'Driver Path', '', 000),
(6, 'groupname', 'Ρυθμίσεις μηχανής', '', 000),
(7, 'apiIp', 'IP Api', 'http://tsoxtapi.ddns.net', 001),
(8, 'apiExpire', 'Λήξη ', NULL, 001),
(9, 'apiKey', 'Κλειδί', NULL, 001),
(10, 'groupname', 'Ρυθμίσεις Api', '1', 001),
(11, 'apiVersion', 'Έκδοση Api', 'v1', 001),
(12, 'apiPort', 'Πόρτα Api', '8001', 001),
(17, 'scaleIp', 'IP ζυγαριάς', '', 101),
(18, 'scalePort', 'Πόρτα', '', 101),
(19, 'scaleVendor', 'Τύπος', '', 101),
(20, 'pretypedBarcodes', 'Τυποποιημένα Ζυγιστικά', '1', 003),
(21, 'groupname', 'Ρυθμισεις Ζυγαριάς', '1', 003),
(27, 'groupname', 'Ζυγαριά 1', '0', 101),
(28, 'scaleProtocol', 'Πρωτόκολλο', '0', 101),
(30, 'groupname', 'Οθόνη Πελάτη', '1', 004),
(31, 'line1', 'Γραμμή 1', 'DEMO ΠΕΛΑΤΗΣ', 004),
(32, 'line2', 'Γραμμή 2', 'ΣΑΣ ΕΥΧΑΡΙΣΤΟΥΜΕ ', 004),
(33, 'type', 'Τύπος', '1', 004),
(34, 'path', 'Συσκευή', '/home/ecs/ECSPos/files/CustomerDisplay', 004),
(35, 'encoding', 'Κωδικοποιήση', 'CP737', 004),
(36, 'type', 'Εκπτυπωτής/Μηχανή', '2', 005),
(37, 'ektupotis', 'Εκτυπωτής', '/path/to/printer', 005),
(38, 'groupname', 'Εκτυπώσεις', '1', 005),
(50, 'altDescription', 'Περιγραφή', '1', 003),

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `specialBcodes`
--

CREATE TABLE `specialBcodes` (
  `id` int(10) UNSIGNED NOT NULL,
  `start` varchar(2) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `specialBcodes`
--

TRUNCATE TABLE `specialBcodes`;
--
-- Άδειασμα δεδομένων του πίνακα `specialBcodes`
--

INSERT INTO `specialBcodes` (`id`, `start`, `type`, `active`) VALUES
(1, '21', 1, 1),
(2, '22', 0, 0),
(3, '23', 1, 0),
(4, '24', 0, 0),
(5, '25', 0, 0),
(6, '26', 0, 1),
(7, '27', 1, 1),
(8, '29', 0, 1);

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(11) UNSIGNED ZEROFILL NOT NULL,
  `afm` varchar(9) NOT NULL,
  `name` varchar(70) NOT NULL,
  `address` varchar(45) NOT NULL,
  `city` varchar(20) NOT NULL,
  `postcode` varchar(5) DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `mobile` varchar(10) DEFAULT NULL,
  `job` varchar(45) DEFAULT NULL,
  `doy` varchar(45) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `suppliers`
--

TRUNCATE TABLE `suppliers`;
-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `suppliersKiniseis`
--

CREATE TABLE `suppliersKiniseis` (
  `id` int(10) UNSIGNED ZEROFILL NOT NULL,
  `proion_id` int(11) NOT NULL,
  `quantity` decimal(10,3) NOT NULL,
  `timi_agoras` decimal(7,2) DEFAULT NULL,
  `parastatika_id` int(11) UNSIGNED ZEROFILL DEFAULT NULL,
  `orders_id` int(11) UNSIGNED ZEROFILL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `suppliersKiniseis`
--

TRUNCATE TABLE `suppliersKiniseis`;
-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `suppliersOrders`
--

CREATE TABLE `suppliersOrders` (
  `id` int(11) UNSIGNED ZEROFILL NOT NULL,
  `supplier_id` int(11) UNSIGNED ZEROFILL NOT NULL,
  `date` datetime NOT NULL,
  `received` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `suppliersOrders`
--

TRUNCATE TABLE `suppliersOrders`;
-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `suppliersParastatika`
--

CREATE TABLE `suppliersParastatika` (
  `id` int(11) UNSIGNED ZEROFILL NOT NULL,
  `supplier_id` int(11) UNSIGNED ZEROFILL NOT NULL,
  `type` tinyint(1) NOT NULL,
  `date` datetime NOT NULL,
  `uploaded` tinyint(2) NOT NULL DEFAULT '0',
  `aa` varchar(45) DEFAULT NULL,
  `payoff` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `suppliersParastatika`
--

TRUNCATE TABLE `suppliersParastatika`;
-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `suppliersPayments`
--

CREATE TABLE `suppliersPayments` (
  `id` int(10) UNSIGNED ZEROFILL NOT NULL,
  `parastatiko_id` int(11) UNSIGNED NOT NULL,
  `amount` decimal(10,3) NOT NULL,
  `method` tinyint(3) UNSIGNED ZEROFILL NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `suppliersPayments`
--

TRUNCATE TABLE `suppliersPayments`;
-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `suppliersPayMethods`
--

CREATE TABLE `suppliersPayMethods` (
  `id` tinyint(3) UNSIGNED ZEROFILL NOT NULL,
  `name` varchar(45) NOT NULL,
  `icon` varchar(40) DEFAULT 'fas fa-money-bill-alt',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `suppliersPayMethods`
--

TRUNCATE TABLE `suppliersPayMethods`;
--
-- Άδειασμα δεδομένων του πίνακα `suppliersPayMethods`
--

INSERT INTO `suppliersPayMethods` (`id`, `name`, `icon`, `active`) VALUES
(001, 'Πίστωση', 'far fa-credit-card', 1),
(002, 'Πληρωμή', 'fas fa-money-bill-alt', 1),
(003, 'Έναντι', 'fas fa-money-bill-alt', 1);

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `suppliersProducts`
--

CREATE TABLE `suppliersProducts` (
  `id_proion` int(11) NOT NULL,
  `id_supplier` int(11) UNSIGNED ZEROFILL NOT NULL,
  `timi_agoras` decimal(7,2) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `suppliersProducts`
--

TRUNCATE TABLE `suppliersProducts`;
-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `systemInfo`
--

CREATE TABLE `systemInfo` (
  `customer` varchar(45) NOT NULL,
  `epaggelma` varchar(45) NOT NULL DEFAULT 'none',
  `address` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `TK` varchar(5) NOT NULL,
  `afm` varchar(9) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `doy` varchar(10) NOT NULL,
  `email` varchar(45) NOT NULL,
  `seller` varchar(45) NOT NULL,
  `seller_phone` varchar(10) NOT NULL,
  `seller_email` varchar(45) NOT NULL,
  `program` varchar(20) NOT NULL,
  `version` varchar(20) DEFAULT NULL,
  `versionDB` varchar(20) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `serial` varchar(45) NOT NULL,
  `signature` varchar(70) DEFAULT 'signature',
  `start_date` datetime DEFAULT NULL,
  `exp_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `systemInfo`
--

TRUNCATE TABLE `systemInfo`;
-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `tameio`
--

CREATE TABLE `tameio` (
  `id` tinyint(3) UNSIGNED ZEROFILL NOT NULL,
  `mac` varchar(23) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `tameio`
--

TRUNCATE TABLE `tameio`;
--
-- Άδειασμα δεδομένων του πίνακα `tameio`
--

INSERT INTO `tameio` (`id`, `mac`) VALUES
(001, 'FF:FF:FF:FF:FF');

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `users`
--

CREATE TABLE `users` (
  `user_id` tinyint(3) UNSIGNED ZEROFILL NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `name` varchar(35) NOT NULL,
  `uac` tinyint(1) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Εκκαθάριση του πίνακα πριν την εισαγωγή `users`
--

TRUNCATE TABLE `users`;
--
-- Άδειασμα δεδομένων του πίνακα `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `name`, `uac`, `active`) VALUES
(002, 'Admin', '1234', 'Admin', 1, 1);

--
-- Ευρετήρια για άχρηστους πίνακες
--

--
-- Ευρετήρια για πίνακα `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `altCat` (`altID`) USING BTREE;

--
-- Ευρετήρια για πίνακα `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `afm` (`afm`),
  ADD UNIQUE KEY `member` (`member`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD UNIQUE KEY `mobile` (`mobile`);
ALTER TABLE `customers` ADD FULLTEXT KEY `name` (`name`);
ALTER TABLE `customers` ADD FULLTEXT KEY `afm_2` (`afm`);
ALTER TABLE `customers` ADD FULLTEXT KEY `phone_2` (`phone`);
ALTER TABLE `customers` ADD FULLTEXT KEY `mobile_2` (`mobile`);
ALTER TABLE `customers` ADD FULLTEXT KEY `member_2` (`member`);

--
-- Ευρετήρια για πίνακα `customersDiscounts`
--
ALTER TABLE `customersDiscounts`
  ADD PRIMARY KEY (`id_customer`,`id_proion`) USING BTREE,
  ADD KEY `id_proion` (`id_proion`),
  ADD KEY `id_customer` (`id_customer`);

--
-- Ευρετήρια για πίνακα `fpa`
--
ALTER TABLE `fpa`
  ADD PRIMARY KEY (`id`);

--
-- Ευρετήρια για πίνακα `isozugio`
--
ALTER TABLE `isozugio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_omadas` (`id_omadas`),
  ADD KEY `eidos_id` (`eidos_id`);

--
-- Ευρετήρια για πίνακα `kiniseis`
--
ALTER TABLE `kiniseis`
  ADD PRIMARY KEY (`id_kinisis`),
  ADD KEY `id_omadas` (`id_omadas`),
  ADD KEY `mothercode` (`mothercode`) USING BTREE,
  ADD KEY `barcode` (`barcode`) USING BTREE,
  ADD KEY `receipt` (`receipt`),
  ADD KEY `id` (`id`);

--
-- Ευρετήρια για πίνακα `Log`
--
ALTER TABLE `Log`
  ADD PRIMARY KEY (`id`);

--
-- Ευρετήρια για πίνακα `omada`
--
ALTER TABLE `omada`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `omada_zugaria_2` (`omada_zugaria`),
  ADD KEY `fk_omada_cat_1-N` (`id_category`),
  ADD KEY `altOmada` (`altID`) USING BTREE,
  ADD KEY `omada_zugaria` (`omada_zugaria`);

--
-- Ευρετήρια για πίνακα `parastatika`
--
ALTER TABLE `parastatika`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `receipt_id` (`receipt_id`),
  ADD KEY `type` (`type`);

--
-- Ευρετήρια για πίνακα `parastatikaInfo`
--
ALTER TABLE `parastatikaInfo`
  ADD PRIMARY KEY (`id`);

--
-- Ευρετήρια για πίνακα `paymentMethods`
--
ALTER TABLE `paymentMethods`
  ADD PRIMARY KEY (`id`);

--
-- Ευρετήρια για πίνακα `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `receipt` (`receipt`),
  ADD KEY `card` (`card`);

--
-- Ευρετήρια για πίνακα `proion`
--
ALTER TABLE `proion`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alt_id` (`alt_id`),
  ADD UNIQUE KEY `barcode` (`barcode`) USING BTREE,
  ADD KEY `fk_proion_omada_1-N` (`id_omadas`),
  ADD KEY `mothercode` (`mothercode`),
  ADD KEY `id_omadas` (`id_omadas`);

--
-- Ευρετήρια για πίνακα `receipts`
--
ALTER TABLE `receipts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tameiou` (`id_tameiou`),
  ADD KEY `operator` (`operator`);

--
-- Ευρετήρια για πίνακα `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Ευρετήρια για πίνακα `specialBcodes`
--
ALTER TABLE `specialBcodes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `start` (`start`);

--
-- Ευρετήρια για πίνακα `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `afm` (`afm`),
  ADD UNIQUE KEY `mobile` (`mobile`),
  ADD UNIQUE KEY `phone` (`phone`);
ALTER TABLE `suppliers` ADD FULLTEXT KEY `name` (`name`);
ALTER TABLE `suppliers` ADD FULLTEXT KEY `afm_2` (`afm`);
ALTER TABLE `suppliers` ADD FULLTEXT KEY `phone_2` (`phone`);
ALTER TABLE `suppliers` ADD FULLTEXT KEY `mobile_2` (`mobile`);

--
-- Ευρετήρια για πίνακα `suppliersKiniseis`
--
ALTER TABLE `suppliersKiniseis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proion_id` (`proion_id`),
  ADD KEY `parastatika_id` (`parastatika_id`),
  ADD KEY `orders_id` (`orders_id`);

--
-- Ευρετήρια για πίνακα `suppliersOrders`
--
ALTER TABLE `suppliersOrders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Ευρετήρια για πίνακα `suppliersParastatika`
--
ALTER TABLE `suppliersParastatika`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `type` (`type`);

--
-- Ευρετήρια για πίνακα `suppliersPayments`
--
ALTER TABLE `suppliersPayments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parastatiko_id` (`parastatiko_id`),
  ADD KEY `method` (`method`);

--
-- Ευρετήρια για πίνακα `suppliersPayMethods`
--
ALTER TABLE `suppliersPayMethods`
  ADD PRIMARY KEY (`id`);

--
-- Ευρετήρια για πίνακα `suppliersProducts`
--
ALTER TABLE `suppliersProducts`
  ADD PRIMARY KEY (`id_proion`,`id_supplier`),
  ADD KEY `id_proion` (`id_proion`),
  ADD KEY `id_supplier` (`id_supplier`);

--
-- Ευρετήρια για πίνακα `systemInfo`
--
ALTER TABLE `systemInfo`
  ADD UNIQUE KEY `serial` (`serial`);

--
-- Ευρετήρια για πίνακα `tameio`
--
ALTER TABLE `tameio`
  ADD PRIMARY KEY (`id`);

--
-- Ευρετήρια για πίνακα `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT για άχρηστους πίνακες
--

--
-- AUTO_INCREMENT για πίνακα `category`
--
ALTER TABLE `category`
  MODIFY `id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=900004;
--
-- AUTO_INCREMENT για πίνακα `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT για πίνακα `fpa`
--
ALTER TABLE `fpa`
  MODIFY `id` tinyint(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT για πίνακα `isozugio`
--
ALTER TABLE `isozugio`
  MODIFY `id` int(11) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT για πίνακα `kiniseis`
--
ALTER TABLE `kiniseis`
  MODIFY `id_kinisis` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT για πίνακα `Log`
--
ALTER TABLE `Log`
  MODIFY `id` int(4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT για πίνακα `omada`
--
ALTER TABLE `omada`
  MODIFY `id` int(9) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=900000012;
--
-- AUTO_INCREMENT για πίνακα `parastatika`
--
ALTER TABLE `parastatika`
  MODIFY `id` int(11) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT για πίνακα `parastatikaInfo`
--
ALTER TABLE `parastatikaInfo`
  MODIFY `id` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT για πίνακα `paymentMethods`
--
ALTER TABLE `paymentMethods`
  MODIFY `id` tinyint(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT για πίνακα `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT για πίνακα `proion`
--
ALTER TABLE `proion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1500000001;
--
-- AUTO_INCREMENT για πίνακα `receipts`
--
ALTER TABLE `receipts`
  MODIFY `id` int(9) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT για πίνακα `settings`
--
ALTER TABLE `settings`
  MODIFY `id` mediumint(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT για πίνακα `specialBcodes`
--
ALTER TABLE `specialBcodes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT για πίνακα `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(11) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT για πίνακα `suppliersKiniseis`
--
ALTER TABLE `suppliersKiniseis`
  MODIFY `id` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT για πίνακα `suppliersOrders`
--
ALTER TABLE `suppliersOrders`
  MODIFY `id` int(11) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT για πίνακα `suppliersParastatika`
--
ALTER TABLE `suppliersParastatika`
  MODIFY `id` int(11) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT για πίνακα `suppliersPayments`
--
ALTER TABLE `suppliersPayments`
  MODIFY `id` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT για πίνακα `suppliersPayMethods`
--
ALTER TABLE `suppliersPayMethods`
  MODIFY `id` tinyint(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT για πίνακα `tameio`
--
ALTER TABLE `tameio`
  MODIFY `id` tinyint(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT για πίνακα `users`
--
ALTER TABLE `users`
  MODIFY `user_id` tinyint(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Περιορισμοί για άχρηστους πίνακες
--

--
-- Περιορισμοί για πίνακα `customersDiscounts`
--
ALTER TABLE `customersDiscounts`
  ADD CONSTRAINT `customersDiscounts_ibfk_1` FOREIGN KEY (`id_proion`) REFERENCES `proion` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `customersDiscounts_ibfk_2` FOREIGN KEY (`id_customer`) REFERENCES `customers` (`id`) ON UPDATE CASCADE;

--
-- Περιορισμοί για πίνακα `isozugio`
--
ALTER TABLE `isozugio`
  ADD CONSTRAINT `isozugio_ibfk_1` FOREIGN KEY (`eidos_id`) REFERENCES `proion` (`barcode`) ON UPDATE CASCADE,
  ADD CONSTRAINT `isozugio_ibfk_2` FOREIGN KEY (`id_omadas`) REFERENCES `omada` (`omada_zugaria`) ON UPDATE CASCADE;

--
-- Περιορισμοί για πίνακα `kiniseis`
--
ALTER TABLE `kiniseis`
  ADD CONSTRAINT `fk_kiniseis_proion_1-N` FOREIGN KEY (`mothercode`) REFERENCES `proion` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `kiniseis_ibfk_1` FOREIGN KEY (`receipt`) REFERENCES `receipts` (`id`) ON UPDATE CASCADE;

--
-- Περιορισμοί για πίνακα `omada`
--
ALTER TABLE `omada`
  ADD CONSTRAINT `fk_omada_cat_1` FOREIGN KEY (`id_category`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Περιορισμοί για πίνακα `parastatika`
--
ALTER TABLE `parastatika`
  ADD CONSTRAINT `parastatika_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `parastatika_ibfk_2` FOREIGN KEY (`receipt_id`) REFERENCES `receipts` (`id`),
  ADD CONSTRAINT `parastatika_ibfk_3` FOREIGN KEY (`type`) REFERENCES `parastatikaInfo` (`id`) ON UPDATE CASCADE;

--
-- Περιορισμοί για πίνακα `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`receipt`) REFERENCES `receipts` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `payments_ibfk_2` FOREIGN KEY (`card`) REFERENCES `paymentMethods` (`id`) ON UPDATE CASCADE;

--
-- Περιορισμοί για πίνακα `proion`
--
ALTER TABLE `proion`
  ADD CONSTRAINT `fk_proion_omada_1-N` FOREIGN KEY (`id_omadas`) REFERENCES `omada` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Περιορισμοί για πίνακα `receipts`
--
ALTER TABLE `receipts`
  ADD CONSTRAINT `receipts_ibfk_1` FOREIGN KEY (`id_tameiou`) REFERENCES `tameio` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `receipts_ibfk_2` FOREIGN KEY (`operator`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE;

--
-- Περιορισμοί για πίνακα `suppliersKiniseis`
--
ALTER TABLE `suppliersKiniseis`
  ADD CONSTRAINT `suppliersKiniseis_ibfk_1` FOREIGN KEY (`proion_id`) REFERENCES `proion` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `suppliersKiniseis_ibfk_2` FOREIGN KEY (`parastatika_id`) REFERENCES `suppliersParastatika` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `suppliersKiniseis_ibfk_3` FOREIGN KEY (`orders_id`) REFERENCES `suppliersOrders` (`id`) ON UPDATE CASCADE;

--
-- Περιορισμοί για πίνακα `suppliersOrders`
--
ALTER TABLE `suppliersOrders`
  ADD CONSTRAINT `suppliersOrders_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON UPDATE CASCADE;

--
-- Περιορισμοί για πίνακα `suppliersParastatika`
--
ALTER TABLE `suppliersParastatika`
  ADD CONSTRAINT `suppliersParastatika_ibfk_2` FOREIGN KEY (`type`) REFERENCES `parastatikaInfo` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `suppliersParastatika_ibfk_3` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON UPDATE CASCADE;

--
-- Περιορισμοί για πίνακα `suppliersPayments`
--
ALTER TABLE `suppliersPayments`
  ADD CONSTRAINT `suppliersPayments_ibfk_1` FOREIGN KEY (`parastatiko_id`) REFERENCES `suppliersParastatika` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `suppliersPayments_ibfk_2` FOREIGN KEY (`method`) REFERENCES `suppliersPayMethods` (`id`) ON UPDATE CASCADE;

--
-- Περιορισμοί για πίνακα `suppliersProducts`
--
ALTER TABLE `suppliersProducts`
  ADD CONSTRAINT `suppliersProducts_ibfk_1` FOREIGN KEY (`id_proion`) REFERENCES `proion` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `suppliersProducts_ibfk_2` FOREIGN KEY (`id_supplier`) REFERENCES `suppliers` (`id`) ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
