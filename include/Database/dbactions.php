<?php
$config = parse_ini_file('/var/www/html/public/credentials/config.ini');

define('HOST',$config['host']);
define('USERNAME',$config['user']);
define('PASS',$config['password']);
define('DB',$config['database']);
define('PORT',$config['port']);


class dbactions
{
    private $connection;
    private $error;

    public function __construct()
    {
        static $connection;
        if(!isset($connection)){
            $connection = mysqli_connect(HOST,USERNAME,PASS,DB,PORT);
            $connection->set_charset("utf8");
        }
        if(!$connection){
            $this->error=$connection->connect_error;
        }else{
            $this->connection=$connection;
        }
    }

    public function db_query($query){
        if(isset($this->error)){
            return $this->error;
        }
        $result=$this->connection->query($query);
        if($result===false){
            $result=$this->connection->error;
        }
        return $result;
    }

    public function process($query){
        $return=array();
        if(isset($this->error)){
            array_push($return,array("response"=>false,"error"=>$this->error));
        }else{
            if($this->connection->multi_query($query)) {
                while ($result = $this->connection->use_result()) {
                    if ($result !== false) {
                        array_push($return,array("response"=>true,"result"=>$result->fetch_all(MYSQLI_ASSOC)));
                        $result->close();
                    } else {
                        if ($this->connection->error !== '') {
                            array_push($return, array("response" => false, "error" => $this->connection->error));
                            $result->close();
                        }
                    }
                    $this->connection->next_result();
                }
            }
            else{
                array_push($return,array("response"=>false,"error"=>"sql process error"));
            }
        }
        return $return[0];
    }

    public function quote($value){
        if(isset($this->error)){
            return $this->error;
        }
        return "'".$this->connection->real_escape_string($value)."'";
    }

}