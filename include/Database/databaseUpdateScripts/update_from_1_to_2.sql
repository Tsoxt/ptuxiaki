﻿INSERT INTO `settings` (`id`, `name`, `description`, `value`, `setgroup`) VALUES (NULL, 'scaleDesc', 'Περιγραφή', 'Μεγάλη ζυγαριά', '101'), (NULL, 'scaleDesc', 'Περιγραφή', 'Μικρή ζυγαριά', '102');|INSERT INTO `parastatikaInfo` (`id`, `name`, `shortname`, `lastNumber`) VALUES (NULL, 'Δελτίο Αποστολής', 'ΔΑΤ', 0), (NULL, 'Πιστωτικό Τιμολόγιο Πώλησης', 'ΠΤΠ', 0),(NULL, 'Πιστωτικό Τιμολόγιο-Δελτίο Αποστολής ', 'ΠΤΠ-ΔΑΤ', '0'),(NULL, 'Δελτίο Αποστολής', 'ΔΑΤ', '0');|ALTER TABLE `parastatikaInfo` ADD `flow_flag` TINYINT(2) NOT NULL DEFAULT '0' AFTER `lastNumber`, ADD `update_flag` TINYINT(2) NOT NULL DEFAULT '0' AFTER `flow_flag`;|UPDATE `parastatikaInfo` SET `flow_flag`=0 WHERE `id`=2;|UPDATE `parastatikaInfo` SET `flow_flag`=1 WHERE `id`=1 OR `id`=3 OR `id`=5 OR `id`=6;|UPDATE `parastatikaInfo` SET `flow_flag`=2 WHERE `id`=4 OR `id`=7 OR `id`=8 OR `id`=9;|UPDATE `parastatikaInfo` SET `update_flag`=1 WHERE `id`=4 OR `id`=5 OR `id`=7;|UPDATE `parastatikaInfo` SET `update_flag`=2 WHERE `id`=1 OR `id`=3 OR `id`=8;|ALTER TABLE `suppliersParastatika` ADD `parastatiko_date` DATETIME NOT NULL AFTER `payoff`, ADD `matched` TINYINT(2) NOT NULL DEFAULT '0' AFTER `parastatiko_date`;|ALTER TABLE `parastatika` ADD `matched` TINYINT(2) NOT NULL DEFAULT '0' AFTER `aa`;|ALTER TABLE `suppliersKiniseis` ADD `rest_quantity` DECIMAL(10,3) NOT NULL DEFAULT '0' AFTER `quantity`, ADD `discount` DECIMAL(7,2) NOT NULL DEFAULT '0' AFTER `timi_agoras`, ADD `fpa` DECIMAL(7,2) NOT NULL DEFAULT '0' AFTER `discount`, ADD `final_sum` DECIMAL(7,2) NOT NULL DEFAULT '0' AFTER `fpa`;|ALTER TABLE `settings` CHANGE `setgroup` `setgroup` SMALLINT UNSIGNED ZEROFILL NOT NULL;|INSERT INTO `settings` (`id`, `name`, `description`, `value`, `setgroup`) VALUES (NULL, 'receipts', 'Αποδείξεις', '0', '5'), (NULL, 'rest_invo', 'Λοιπά Παραστατικά', '0', '5'), (NULL, 'isozugio', 'Ισοζύγιο', '1', '5'), (NULL, 'kartela', 'Καρτέλα Πελατών-Προμηθευτών', NULL, '5');|INSERT INTO `settings` (`id`, `name`, `description`, `value`, `setgroup`) VALUES (NULL, 'groupname', 'Εκτυπωτής Α4', '0', '200'), (NULL, 'printName', 'Όνομα', 'HP Black', '200'), (NULL, 'printIp', 'IP Εκτυπωτή', NULL, '200'), (NULL, 'printPort', 'Πόρτα', NULL, '200'), (NULL, 'printVendor', 'Τύπος', NULL, '200'), (NULL, 'printType', 'Μέγεθος Χαρτιού', '0', '200'), (NULL, 'groupname', 'Εκτυπωτής 80mm', '0', '201'), (NULL, 'printName', 'Όνομα', 'IRSPos black', '201'), (NULL, 'printIp', 'IP Εκτυπωτή', NULL, '201'), (NULL, 'printPort', 'Πόρτα', NULL, '201'), (NULL, 'printVendor', 'Τύπος', NULL, '201'), (NULL, 'printType', 'Μέγεθος Χαρτιού', '1', '201');|DELETE FROM `settings` WHERE `settings`.`id` = 36;|DELETE FROM `settings` WHERE `settings`.`id` = 37;|INSERT INTO `parastatikaInfo` (`id`, `name`, `shortname`, `lastNumber`, `flow_flag`, `update_flag`) VALUES (NULL, 'Δελτίο Παραγγελίας', 'ΔΠ', '0', '3', '3');|UPDATE `parastatikaInfo` SET `id`=0 WHERE `flow_flag`=3 AND `update_flag`=3;|CREATE ALGORITHM=UNDEFINED DEFINER=`phpmyadmin`@`% `SQL SECURITY INVOKER VIEW` timologia-suppliers`  AS  select concat(`parastatikaInfo`.`shortname`,' ',`suppliersParastatika`.`aa`) AS `arithmos`,`suppliersParastatika`.`date` AS `imerominia`,`suppliersParastatika`.`id` AS `timologio`,sum(`suppliersPayments`.`amount`) AS `sunolo`,`suppliersParastatika`.`supplier_id` AS `pelatis`,`suppliersPayments`.`method` AS `type` from ((`suppliersParastatika` join `parastatikaInfo` on((`suppliersParastatika`.`type` = `parastatikaInfo`.`id`))) join (`suppliersPayments` join `suppliersPayMethods` on((`suppliersPayments`.`method` = `suppliersPayMethods`.`id`))) on((`suppliersParastatika`.`id` = `suppliersPayments`.`parastatiko_id`))) group by `suppliersParastatika`.`id`,`suppliersPayments`.`method` order by `suppliersParastatika`.`date` ;|DELIMITER $$
CREATE DEFINER=`phpmyadmin`@`localhost` PROCEDURE `suppliersSum`(IN `customer` INT, IN `apo` DATETIME, IN `mexri` DATETIME)
BEGIN
  set @apo=apo;
  set @mexri=mexri;
  set @customer=customer;
  SELECT 'Από Μεταφορά' as arithmos,@apo as imerominia, NULL as timologio, NULL as pelatis, NULL as aksia, SUM(sunolo) as pistosi, NULL as upoloipo
  FROM `timologia-suppliers`
  WHERE imerominia<@apo AND pelatis = @customer AND type=1
  UNION
  SELECT arithmos, imerominia, summary1.timologio as timologio, pelatis, aksia, pistosi,upoloipo
  FROM
    (
        (
            (
              (SELECT
                 arithmos,
                 imerominia,
                 timologio,
                 pelatis,
                 SUM(sunolo) AS upoloipo
               FROM `timologia-suppliers`
               WHERE (imerominia BETWEEN @apo AND @mexri) AND pelatis = @customer AND type != 2
               GROUP BY timologio)
                AS summary1
              )
            INNER JOIN
            (
              (SELECT
                 timologio,
                 SUM(sunolo) AS pistosi
               FROM `timologia-suppliers`
               WHERE (imerominia BETWEEN @apo AND @mexri) AND pelatis = @customer AND type =1
               GROUP BY timologio)
                AS summary2
              ) ON summary1.timologio = summary2.timologio
          )
        INNER JOIN
        (
          (SELECT
             timologio,
             SUM(sunolo) AS aksia
           FROM `timologia-suppliers`
           WHERE (imerominia BETWEEN @apo AND @mexri) AND pelatis = @customer AND type != 3
           GROUP BY timologio)
            AS summary3
          ) ON summary1.timologio = summary3.timologio
    )
  ORDER by imerominia ASC;
END$$
DELIMITER ;|DELIMITER $$
CREATE DEFINER=`phpmyadmin`@`localhost` PROCEDURE `payoffSupplierParastatiko`(IN `id_par` INT, IN `poso` FLOAT)
BEGIN 
UPDATE suppliersParastatika SET suppliersParastatika.payoff=1 WHERE suppliersParastatika.id=id_par; UPDATE suppliersPayments SET suppliersPayments.amount=suppliersPayments.amount+poso WHERE suppliersPayments.method=3 AND suppliersPayments.parastatiko_id=id_par;
END$$
DELIMITER ;

