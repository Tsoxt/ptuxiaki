<?php

class dbmodel
{
    public $_category = [
        'table'     => "`category`",
        'id'        => "`id`",
        'altid'     => "`altId`",
        'perig'     => "`name`",
        'active'    => "`active`",
        'order'     => "`seira`",
        'api'       => "`api`",
        'new'       => "`new`"
    ];

    public $_fpa = [
        'table' => "`fpa`",
        'id'    => "`id`",
        'name'  => "`name`",
        'price' => "`price`",
        'editable'       => "`editable`",
        'active'       => "`active`"
    ];

    public $_kiniseis = [
        'table'     => "`kiniseis`",
        'id'        => "`id_kinisis`",
        'mcode'     => "`mothercode`",
        'itemId'    => "`id`",
        'bcode'     => "`barcode`",
        'omada'     => "`id_omadas`",
        'quan'      => "`quantity`",
        'timi'      => "`timi`",
        'fpa'       => "`fpa`",
        'sum'       => "`sum`",
        'tmima'     => '`tmima`',
        'temaxia'   => "`temaxia`",
        'up'        => "`uploaded`",
        'receipt'   =>  "`receipt`"
    ];

    public $_omada = [
        'table'     => "`omada`",
        'id'        => "`id`",
        'altId'  => "`altId`",
        'perig'     => "`name`",
        'id_cat'    => "`id_category`",
        'active'    => "`active`",
        'order'     => "`seira`",
        'api'       =>  "`api`",
        'new'       =>  "`new`",
        'zugaria'   =>  "`omada_zugaria`",
        'isozigio'  =>  "`isozigio`"
    ];

    public $_proion = [
        'table'     =>  "`proion`",
        'id'        =>  "`id`",
        'code'      =>  "`barcode`",
        'mcode'     =>  "`mothercode`",
        'perig'     =>  "`perigrafi`",
        'perig2'    =>  "`perigrafi2`",
        'id_omad'   =>  "`id_omadas`",
        'timi'      =>  "`timi`",
        'flag'      =>  "`flag_timi`",
        'tmima'     =>  "`tmima`",
        'stock'     =>  "`stock`",
        'units'     =>  "`temaxia`",
        'active'    =>  "`active`",
        'new'       =>  "`new`",
        'order'     =>  "`seira`",
        'day'       =>  "`epistrofi`",
        'insert'    =>  "`insertDate`",
        'updated'   =>  "`updated`",
        'zugos'     =>  "`zugistiko`",
        'api'       =>  "`api`",
        'zugaria'   =>  "`zugaria`",
        'temaxiako' =>  "`temaxiako`",
        'alt_id'     =>  "`alt_id`",
        'days'      =>  "`daysToExpire`",
        'agora'     =>  "`timi_agoras`",
        'alert'     =>  "`stock_alert`",
        'recommend' =>  "`stock_recommend`"
    ];

    public $_users = [
        'table'     => "`users`",
        'id'        => "`user_id`",
        'uname'     => "`username`",
        'pass'      => "`password`",
        'name'      => "`name`",
        'access'    => "`uac`",
        'active'    => "`active`"
    ];

    public $_tameio = [
        'id'    => "`id`",
        'mac'   => "`mac`",
    ];

    public $_settings = [
        'table' => "`settings`",
        'id'    => "`id`",
        'name'  => "`name`",
        'value' => "`value`",
        'desc'  => "`description`",
        'group' => "`setgroup`"
    ];

    public $_barcodes = [
        'table' =>  "`specialBcodes`",
        'id'    =>  "`id`",
        'start' =>  "`start`",
        'type'  =>  "`type`",
        'active'  =>  "`active`"
    ];

    public $_isozigio = [
        'table'     =>  "`isozugio`",
        'id'        =>  "`id`",
        'id_om'     =>  "`id_omadas`",
        'eidos_id'  =>  "`eidos_id`",
        'quantity'  =>  "`quantity`",
        'date'      =>  "`date`"
    ];

    public $_payments = [
        'table'     => "`payments`",
        'id'        => "`id`",
        'receipt'   => "`receipt`",
        'amount'    => "`amount`",
        'card'      => "`card`"
    ];

    public $_payment_methods = [
        'table'  => "`paymentMethods`",
        'id'     => "`id`",
        'name'   => "`name`",
        'icon'   => "`icon`",
        'active' => "`active`"
    ];

    public $_receipts = [
        'table'     => "`receipts`",
        'id'        => "`id`",
        'tameio'    => "`id_tameiou`",
        'operator'  => "`operator`",
        'date'      => "`date`"
    ];

    public $_log = [
        'table'  => "`Log`",
        'id'     => "`id`",
        'type'   => "`type`",
        'date'   => "`date`"
    ];

    public $_customers = [
        'table'     => "`customers`",
        'id'        => "`id`",
        'afm'       => "`afm`",
        'name'      => "`name`",
        'address'   => "`address`",
        'city'      => "`city`",
        'postcode'  => "`postcode`",
        'phone'     => "`phone`",
        'mobile'    => "`mobile`",
        'job'       => "`job`",
        'doy'       => "`doy`",
        'member'    => "`member`",
        'type'      => "`type`",
        'points'    => "`points`",
        'date'      => "`date`"
    ];

    public $_customers_proion = [
        'table'     => "`customersDiscounts`",
        'proion'    => "`id_proion`",
        'customer'  => "`id_customer`",
        'discount'  => "`discount`",
        'type'      => "`type`"
    ];

    public $_parastatika = [
        'table'     => "`parastatika`",
        'id'        => "`id`",
        'customer'  => "`customer_id`",
        'receipt'   => "`receipt_id`",
        'type'      => "`type`",
        'aa'        => "`aa`",
        'matched'   => "`matched`"
    ];

    public $_parastatikaInfo = [
        'table'     => "`parastatikaInfo`",
        'id'        => "`id`",
        'name'      => "`name`",
        'shortname' => "`shortname`",
        'flow'      => "`flow_flag`",
        'update'    => "`update_flag`"
    ];

    public $_programInfo = [
        'table'         => "`systemInfo`",
        'customer'      => "`customer`",
        'address'       => "`address`",
        'city'          => "`city`",
        'afm'           => "`afm`",
        'phone'         => "`phone`",
        'email'         => "`email`",
        'seller'        => "`seller`",
        'seller_phone'  => "`seller_phone`",
        'seller_email'  => "`seller_email`",
        'program'       => "`program`",
        'version'       => "`version`",
        'update'        => "`last_update`",
        'serial'        => "`serial`",
        'sign'          => "`signature`",
        'starting'      => "`start_date`",
        'expire'        => "`exp_date`"
    ];

    public $_suppliers = [
        'table'     => "`suppliers`",
        'id'        => "`id`",
        'afm'       => "`afm`",
        'name'      => "`name`",
        'address'   => "`address`",
        'city'      => "`city`",
        'postcode'  => "`postcode`",
        'phone'     => "`phone`",
        'mobile'    => "`mobile`",
        'job'       => "`job`",
        'doy'       => "`doy`",
        'email'     => "`email`",
        'date'      => "`date`"
    ];

    public $_suppliersProducts = [
        'table'     => "`suppliersProducts`",
        'proion'    => "`id_proion`",
        'supplier'  => "`id_supplier`",
        'price'     => "`timi_agoras`",
        'date'      => "`date`"
    ];

    public $_suppliersParastatika = [
        'table'                 => "`suppliersParastatika`",
        'id'                    => "`id`",
        'supplier'              => "`supplier_id`",
        'type'                  => "`type`",
        'date'                  => "`date`",
        'uploaded'              => "`uploaded`",
        'aa'                    => "`aa`",
        'pay'                   =>  "payoff",
        'parastatiko_date'      =>  "parastatiko_date",
        'matched'               =>  "matched"
    ];

    public $_suppliersKiniseis = [
        'table'         => "`suppliersKiniseis`",
        'proion'        => "`proion_id`",
        'quantity'      => "`quantity`",
        'rest'          => "`rest_quantity`",
        'timi'          => "`timi_agoras`",
        'discount'      => "`discount`",
        'fpa'           => "`fpa`",
        'sum'           => "`final_sum`",
        'parastatiko'   => "`parastatika_id`",
        'order'         => "`orders_id`"
    ];

    public $_suppliersOrders = [
        'table'     => "`suppliersOrders`",
        'id'        => "`id`",
        'supplier'  => "`id_supplier`",
        'date'      => "`date`",
        'received'  =>  "`received`"
    ];

    public $_suppliersPaymethods = [
        'table'  => "`suppliersPayMethods`",
        'id'     => "`id`",
        'name'   => "`name`",
        'icon'   => "`icon`",
        'active' => "`active`"
    ];

    public $_suppliersPayments = [
        'table'       => "`suppliersPayments`",
        'parastatiko' => "`parastatiko_id`",
        'amount'      => "`name`",
        'method'      => "`method`"
    ];

}