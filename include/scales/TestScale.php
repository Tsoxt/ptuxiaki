<?php

foreach (glob($_SERVER['DOCUMENT_ROOT'].'/include/netProtocols/*.php') as $file){
    require_once $file;
}

class TestScale
{
    private $ip;
    private $port;
    private $protocol;
    private $altName;

    public function __construct($ip,$port,$protocol,$altName)
    {
        define("TCP", 0);
        define("UDP", 1);
        $this->ip=$ip;
        $this->port=$port;
        $this->protocol=$protocol;
        $this->altName=$altName;
    }

    private function calculateChecksum(){
        //εδώ υπολογίζουμε το validation bit που χρειάζονται οι περισσότερες δικτυακές ζυγαρίες
        $checksum="";
        return $checksum;
    }

    private function prepareText($description){
        //εδώ μορφοποιούμε το κέιμενο και κάνουμε όποια μετατροπή κριθέι απαραίτητη μεταξύ των διαφορετικώ  encodings
        return;
    }

    private function prepareCommand($data){
        if($this->altName){
            $description=$this->prepareText($data['perigrafi2']);
        }else{
            $description=$this->prepareText($data['perigrafi']);
        }
        $checksum=$this->calculateChecksum();
        //εδώ προετοιμάζουμε την εντολή και τοποθετούμαι όλα τα απαραίτητα στοιχεία στην κατάλληλη σειρά
        $command="";
        return $command;
    }

    public function updatePlus($dataset){
        //Η βασική συνάρτηση για να εκτελέσουμε τις αλλαγές στις ψηφιακές ζυγαριές
        //Μετατρέπουμε την εντολή σε bytestream και την στέλνουμε μέσω tcp socket για κάθε προιόν
        foreach($dataset['items'] as $plu){
            $command=$this->prepareCommand($plu);
            $command=hex2bin($command);
            $socket=new TCPProtocol($this->ip,(int)$this->port);
            if(!$socket->status()['response']){
                return $socket->status();
            }
            $return=$socket->sendData($command);
            if(!$return['response']){
                return $return;
            }
            unset($socket);
        }
        return $return;
    }
}