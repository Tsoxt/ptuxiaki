<?php
/**
 * Created by PhpStorm.
 * User: tsoxt
 * Date: 29/12/2018
 * Time: 12:58 μμ
 */

class TCPProtocol
{
    private $port;
    private $remoteAddress;
    private $socket;
    private $status=true;
    private $error="";

    public function __construct($ip,$port)
    {
        $this->remoteAddress=$ip;
        $this->port=(int)$port;

        if (!($this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP))) {
            $errorcode = socket_last_error();
            $this->error = socket_strerror($errorcode);
            $this->status=false;
        }

        if (!socket_connect($this->socket, $this->remoteAddress, $this->port)){
            $errorcode = socket_last_error();
            $this->error = socket_strerror($errorcode);
            $this->status = false;
        }

        if(!$this->status){
            $this->status;
        }
    }

    public function status(){
        $return['response']=$this->status;
        $return['error']=$this->error;
        return $return;
    }

    public function sendData($data){
        $write=socket_write($this->socket,$data);
        if($write===false){
            $return['response']=false;
            $return['error']=socket_strerror(socket_last_error($this->socket));
            socket_close($this->socket);
            return $return;
        }
        $read=socket_read($this->socket,2048);
        if($read===false){
            $return['response']=false;
            $return['error']=socket_strerror(socket_last_error($this->socket));
        }else{
            $return['response']=true;
        }
        socket_close($this->socket);
        return $return;
    }
}