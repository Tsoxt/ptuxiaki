<?php

class UDPprotocol
{
    private $port;
    private $localeAddress;
    private $remoteAddress;
    private $listener;
    private $sender;
    private $buffer;
    private $status=true;

    public function __construct($ip,$port)
    {
        $this->localeAddress=$_SERVER['SERVER_ADDR'];
        $this->remoteAddress=$ip;
        $this->port=(int)$port;
        if (!($this->listener = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP))) {
            $errorcode = socket_last_error();
            $errormsg = socket_strerror($errorcode);
            $this->status=false;
        }

        if (!socket_bind($this->listener, $this->localeAddress, $this->port)) {
            $errorcode = socket_last_error();
            $errormsg = socket_strerror($errorcode);
            $this->status=false;
        }else{
            $timeout=socket_set_option($this->listener,SOL_SOCKET,SO_RCVTIMEO,array('sec'=>4,'usec'=>0));
        }

        if (!($this->sender = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP))) {
            $errorcode = socket_last_error();
            $errormsg = socket_strerror($errorcode);
            $this->status=false;
        }

        if (!socket_connect($this->sender, $this->remoteAddress, $this->port)){
            $errorcode = socket_last_error();
            $errormsg = socket_strerror($errorcode);
            $this->status = false;
        }

        if(!$this->status){
            $this->status;
        }
    }

    public function listen ($isENQ){
        $this->buffer="";
        $timeout=(float)(8);
        $start=-microtime(true);
        while($start<=$timeout)
        {
            $end=$start;
            //Receive some data
            $receive = socket_recvfrom($this->listener, $this->buffer, 2048, 0, $this->localeAddress, $this->port);
            if($receive===-1){
                $this->status=false;
                break;
            }
            if((substr($this->buffer, -1)===chr(0x03))||(substr($this->buffer, -1)===chr(0x15))||((substr($this->buffer,-1) === chr(0x06))&&$isENQ)){
                break;
            }
            $start+=microtime(true);
        }
        if($end>=$timeout){
            $diff="";
        }
        if($this->buffer===""){
            $this->buffer="C8";
        }
        return $this->buffer;
    }

    public function status(){
        return $this->status;
    }

    public function receiveData(){
        return $this->buffer;
    }

    public function sendData($data){
        socket_sendto($this->sender, $data, strlen($data), 0, $this->remoteAddress, $this->port);
    }

    public function closeSocket(){
        $this->sendData(chr(0x06));
        socket_close($this->listener);
        socket_close($this->sender);
    }
}