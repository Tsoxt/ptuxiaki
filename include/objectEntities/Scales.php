<?php

foreach (glob($_SERVER['DOCUMENT_ROOT'].'/include/scales/*.php') as $file){
    require_once $file;
}

class Scales
{
    private $zugaria= array();
    private $status;
    private $barcodes;
    private $description;

    public function __construct()
    {
        define("TESTSCALE", 0);
        $Sets = new Settings();
        $group=100;
        $temp=$Sets->getSettings($group);
        $settings=array();
        foreach ($temp['body'] as $tempSetting){
            foreach ($tempSetting as $key=>$setting){
                $settings[$key]=$setting['value'];
            }
            $scale['vendor']=(int)$settings['scaleVendor'];
            $scale['ip']=$settings['scaleIp'];
            $scale['port']=$settings['scalePort'];
            $scale['protocol']=$settings['scaleProtocol'];
            $scale['active']=$settings['groupname'];
            array_push($this->zugaria,$scale);
        }
        unset($Sets);
        $Sets=new Settings();
        $group=3;
        $temp=$Sets->getSettings($group);
        $settings=array();
        foreach ($temp['body'][$group] as $key=>$setting){
            $settings[$key]=$setting['value'];
        }
        $this->barcodes=$settings['pretypedBarcodes'];
        $this->status=$settings['groupname'];
        $this->description=$settings['altDescription'];
        unset($Sets);
    }

    public function isActive(){
        $active=array();
        foreach ($this->zugaria as $scale){
            if((int)$scale['active']===1){
                array_push($active,true);
            }else{
                array_push($active,false);
            }
        }
        return $active;
    }

    public function getAlternativeStatus(){
        if((int)$this->description===1){
            return true;
        }else{
            return false;
        }
    }

    public function updateAlternativeStatus(){
        $this->description=abs((int)$this->description-1);
        $Settings = new Settings();
        $data['id']=50;//id in Database Settings of altDescription
        $data['value']=$this->description;
        $return=$Settings->updateSetting($data);
        if($return['response']){
            $return=$this->updatePlUs();
        }
        return $return;
    }

    public function updatePlUs(){
        $proion=new Proion();
        $data=$proion->itemsToScale();
        if($data['response']){
            $return['response']=false;
            $return['error']="Δεν υπάρχει Ζυγαριά";
            $active=$this->isActive();
            $altName=$this->getAlternativeStatus();
            $i=0;
            foreach ($this->zugaria as $scales){
                if($active[$i]&&($this->status==1)){
                    switch ($scales['vendor']){
                        case TESTSCALE:
                            $scale=new TestScale($scales['ip'],$scales['port'],$scales['protocol'],$altName);
                            $return=$scale->updatePlus($data);
                            break;
                        default:
                            break;
                    }
                    unset($scale);
                    if (!$return['response']){
                        break;
                    }
                }
                $i++;
            }
        }else{
            $return=$data;
        }
        return $return;
    }
}