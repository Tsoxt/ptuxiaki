<?php
/**
 * Created by PhpStorm.
 * User: tsoxt
 * Date: 27/12/2018
 * Time: 11:27 πμ
 */
$path='../../vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/view/templates/invoice.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/view/templates/isozugio.php';

class Printer
{
    private $settings=array();
    private $db;
    private $print_types;
    private $allSettings;
    public function __construct()
    {
        $this->allSettings=new Settings();
        $group=5;
        $Sets=$this->allSettings->getSettings($group);
        $printers_types=file_get_contents("../ConfigFiles/printers_types.json");
        $this->print_types=json_decode($printers_types,true);
        foreach ($Sets['body'][$group] as $key=>$setting){
            $this->settings[$key]=$setting['value'];
        }
        define('PRINTER',0);
        define('CASHIER',1);
        define('AIRPRINT',2);
        define('NONE',3);
        $this->db=new dbactions();
    }

    public function isActive(){
        if((int)$this->settings['groupname']===1){
            return true;
        }else{
            return false;
        }
    }

    public function printIsozigio($data){
        if($this->isActive()){
            $stats=new Stats();
            $isozigio=$stats->getIsozigio($data);
            if($isozigio['response']){
                $toPrint['header']="ΙΣΟΖΥΓΙΟ ".$data['datefrom']."-".$data['dateto'];
                $toPrint['lines']=$isozigio['omadesisozigio'];
               //$cashier=new Cashier();
                //$response=$cashier->freePrint($toPrint);
                switch ($this->settings['isozugio']){
                    case PRINTER:
                        //todo print pdf and sent it to printer
                        $response['response']=false;
                        $response['error']="Οι επιλεγμένες ρυθμίσεις εκτυπωτή δεν υποστιρίζονται ακόμα";
                        break;
                    case CASHIER:
                        $cashier=new Cashier();
                        $response=$cashier->freePrint($toPrint);
                        break;
                    case AIRPRINT:
                        //todo print pdf and sent it to printer
                        $response['response']=false;
                        $response['error']="Οι επιλεγμένες ρυθμίσεις εκτυπωτή δεν υποστιρίζονται ακόμα";
                        break;
                    case NONE:
                        $response['response']=false;
                        $response['error']="Δεν έχετε επιλέξει κάποιον εκτυπωτή από τις ρυθμίσεις";
                        break;
                    default:
                        $cashier=new Cashier();
                        $response=$cashier->freePrint($toPrint);
                }
            }else{
                $response=$isozigio;
            }
        }else{
            $response['response']=false;
            $response['error']="Δεν υπάρχει συνδεδεμένος εκτυπωτής";
        }
        return $response;
    }

    public function printInvoice($receipt,$creditCards,$invoice){
        if($this->isActive()){
            if (isset($invoice['par_type'])){
                $parastatiko_type=(int)$invoice['par_type'];
            }else{
                $parastatiko_type=1;
            }
            switch ($this->settings['rest_invo']){
                case CASHIER:
                    if(!isset($invoice['invoData'])){
                        $cashier=new Cashier();
                        $return=$cashier->sendInvoice($receipt,$creditCards,$invoice);
                        if($return['response']){
                            $query="CALL incrementInvoNum(5)";
                            $result=$this->db->process($query);
                            if($result['response']){
                                $return['invoData']=$result['result'][0];
                            }else{
                                return $result;
                            }
                        }
                    }
                    break;
                case PRINTER:
                    if(!isset($invoice['invoData'])){
                        $query="CALL incrementInvoNum(".$parastatiko_type.")";
                        $result=$this->db->process($query);
                        if($result['response']){
                            $invoice['invoData']=$result['result'][0];
                        }else{
                            return $result;
                        }
                        $invoice['date']=date('d/m/Y H:i:s');
                        $reprint=false;
                    }else{
                        $reprint=true;
                    }
                    if (isset($invoice['customer'])){//pelatis
                        $customer=(new Customers())->fetchCustomerData(array('id'=>$invoice['customer']));
                    }else{//promitheutis
                        $customer=(new Suppliers())->fetchSupplierData(array('id'=>$invoice['supplier']));
                    }
                    if(!$customer['response']){
                        $return=$customer;
                    }else{
                        $print=new invoice($receipt,$invoice,$creditCards,$customer['body'],$reprint);
                        $invoiceHtml=$print->getDocument();
                        if ($invoiceHtml['response']){
                            $return['response']=true;
                            $printer=$this->print_types['ektyposeis'][$this->settings['rest_invo']];
                            $printerGroup=$this->allSettings->getSettings($printer['group']);
                            if ($printerGroup['response']){
                               $printname='';
                               foreach ($printerGroup['body'] as $group){
                                   if ((int)$group['groupname']['value']===1 && (int)$group['printType']['value']===(int)$printer['type']){
                                       $printname=$group['printName']['value'];
                                       break;
                                   }
                               }
                               if ($printname!==''){
                                   $command=exec('xvfb-run wkhtmltopdf /home/ecs/ECSPos/files/invoices/draft.html /home/ECSPos/ECSPos/files/invoices/draft.pdf');
                                   if (preg_match('/Done/',$command)){
                                       $command=exec('lp -d '.$printname.' -n 1 -o media=a4 /home/ecs/ECSPos/files/invoices/draft.pdf');
                                       if ($command===''){
                                           $return['response']=false;
                                           $return['error']='Μήνυμα: Κάτι πήγε στραβά με την εκτύπωση. Παρακαλώ ελέγξτε τις ρυθμίσεις του εκτυπωτή';
                                       }
                                   }
                               }else{
                                   $return['response']=false;
                                   $return['error']="Μήνυμα: Δεν υπάρχει ενεργός εκτυπωτής. Παρακαλώ ελέγξτε τις ρυθμίσεις";
                               }
                            }
                        }
                        $return['invoData']=$invoice['invoData'];

                    }
                    break;
                case AIRPRINT:
                    if(!isset($invoice['invoData'])){
                        $query="CALL incrementInvoNum(".$parastatiko_type.")";
                        $result=$this->db->process($query);
                        if($result['response']){
                            $invoice['invoData']=$result['result'][0];
                        }else{
                            return $result;
                        }
                        $invoice['date']=date('d/m/Y H:i:s');
                        $reprint=false;
                    }else{
                        $reprint=true;
                    }
                    $customer=(new Customers())->fetchCustomerData(array('id'=>$invoice['customer']));
                    if(!$customer['response']){
                        $return=$customer;
                    }else{
                        $print=new invoice($receipt,$invoice,$creditCards,$customer['body'],$reprint);
                        $return=$print->getDocument();
                        $return['invoData']=$invoice['invoData'];
                    }
                    break;
                case NONE:
                    $return['response']=false;
                    $return['error']="Μήνυμα: Δεν έχετε επιλέξει κάποιον εκτυπωτή από τις ρυθμίσεις";
                    break;
                default:
                    $return['response']=false;
                    $return['error']="Μήνυμα: Ελέγξτε τις ρυθμίσεις του εκτυπωτή";
            }
        }else{
            $return['response']=false;
            $return['error']="Μήνυμα: Παρακαλώ ενεργοποιήστε τις εκτυπώσεις στις ρυθμίσεις";
        }
        return $return;
    }

    public function printReceipt(){

    }

    public function printCustomerSupplierSummary(){

    }
}