<?php


class BarcodeApi
{
    private $url;
    private $version;
    private $port;
    private $active;

    public function __construct()
    {
        $settings=new Settings();
        $ruthmiseis=array();
        $group=1;
        $setting=$settings->getSettings($group);
        foreach ($setting['body'][$group] as $key=>$set){
            $ruthmiseis[$key]=$set['value'];
        }
        $this->url=$ruthmiseis['apiIp'];
        $this->version=$ruthmiseis['apiVersion'];
        $this->port=$ruthmiseis['apiPort'];
        $this->active=$ruthmiseis['groupname'];
    }

    private function apiCalls($method, $endpoint, $data){
        $url=$this->url."/".$this->version."/".$endpoint;
        $curl=curl_init();
        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_PORT, $this->port);
        //curl_setopt($curl, CURLOPT_HTTPHEADER, NULL);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result['body']=curl_exec($curl);
        $result['error']=curl_error($curl);
        $result['httpCode']=curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        return $result;
    }

    public function categoriesFromApi(){
        $endpoint='categories';
        $method='GET';
        $data=false;
        return $this->apiCalls($method,$endpoint,$data);
    }

    public function categoryFromApi($categoryId){
        $endpoint='category/'.$categoryId;
        $method='GET';
        $data=false;
        return $this->apiCalls($method,$endpoint,$data);
    }

    public function omadesFromApi($categoryId){
        $endpoint='omades/'.$categoryId;
        $method='GET';
        $data=false;
        return $this->apiCalls($method,$endpoint,$data);
    }

    public function omadaFromApi($omadaId){
        $endpoint='omada/'.$omadaId;
        $method='GET';
        $data=false;
        return $this->apiCalls($method,$endpoint,$data);
    }

    public function itemsFromApi($omadaId){
        $endpoint='items/'.$omadaId;
        $method='GET';
        $data=false;
        return $this->apiCalls($method,$endpoint,$data);
    }

    public function itemFromApi($itemId){
        $endpoint='item/'.$itemId;
        $method='GET';
        $data=false;
        return $this->apiCalls($method,$endpoint,$data);
    }

    public function isActive(){
        if((int)$this->active===0){
            return false;
        }else{
            return true;
        }
    }
}