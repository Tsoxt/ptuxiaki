<?php
/**
 * Created by PhpStorm.
 * User: Tsoxt
 * Date: 15/12/2018
 * Time: 2:48 PM
 */

class Logs
{
    private $model,$actions;

    public function __construct()
    {
        $db=new dbmodel;
        $this->actions=new dbactions();
        $this->model=$db->_log;

    }

    public function getLastLog($data){
        $query="SELECT MAX(".$this->model['date'].") as lastDate FROM ".$this->model['table']
            ." WHERE ".$this->model['type']." = ".(int)$data['type']." AND ".$this->model['date']." < ".$this->actions->quote($data['date']).";";
        $result=$this->actions->db_query($query);
        if (mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['lastLog']=mysqli_fetch_assoc($result);
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function insertLog($data){
        $query="INSERT INTO ".$this->model['table']." VALUES (NULL, ".(int)$data['type'].", ".$this->actions->quote($data['date']).");";
        $result=$this->actions->db_query($query);
        if ($result!==false){
            $return['response']=$result;
        }else{
            $return['response']=false;
        }
        return $return;
    }
}