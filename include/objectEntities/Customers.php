<?php
/**
 * Created by PhpStorm.
 * User: Tsoxt
 * Date: 11/12/2018
 * Time: 9:43 AM
 */

class Customers
{
    private $model;
    private $actions;
    private $discounts;

    public function __construct()
    {
        $model=new dbmodel;
        $this->actions=new dbactions();
        $this->model=$model->_customers;
        $this->discounts=$model->_customers_proion;

    }

    public function searchCustomers($data)
    {
        if ((int)$data['type']===1){//anazitisi me id
            $query="SELECT * FROM ".$this->model['table']." WHERE ".$this->model['id']." = ".(int)$data['search'].";";
        }else{//anazitisi fulltext
            $query="SELECT * FROM ".$this->model['table']." WHERE ((MATCH(`afm`) AGAINST ('".$data['search'].
                "*' IN BOOLEAN MODE)) OR (MATCH(`name`) AGAINST ('".$data['search'].
                "*' IN BOOLEAN MODE)) OR (MATCH(`phone`) AGAINST ('".$data['search'].
                "*' IN BOOLEAN MODE)) OR (MATCH(`mobile`) AGAINST ('".$data['search'].
                "*' IN BOOLEAN MODE)) OR (MATCH(`member`) AGAINST ('".$data['search'].
                "*' IN BOOLEAN MODE))) ORDER BY `name` ASC;";
        }
        $result=$this->actions->db_query($query);
        if (mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['customers']=array();
            $return['gsis']=false;
            $i=0;
            while ($row=mysqli_fetch_assoc($result)){
                $return['customers'][$i]['body']=$row;
                $customer['customer']=$row['id'];
                $getYpoloipo=$this->getYpoloipo($customer);
                if ($getYpoloipo['response']){
                    $return['customers'][$i]['ypoloipo']=$getYpoloipo['body'];
                }else{
                    $return['customers'][$i]['ypoloipo']=0;
                }
                $i++;
            }
        }else{
            if (strlen($data['search'])===9){
                //todo search afm gsis
                //$return['gsis']=true;
            }
            $return['response']=false;
            $return['error']='Δεν βρέθηκαν αποτελέσματα';
        }
        return $return;
    }

    public function fetchCustomerData($data){
        $query="select * from `customers` where `id`=".(int)$data['id'];
        $result=$this->actions->db_query($query);
        if($result!==false){
            if(mysqli_num_rows($result)>0){
                $return['response']=true;
                $return['body']=mysqli_fetch_assoc($result);
            }else{
                $return['response']=false;
                $return['error']="Δεν υπάρχει ο Πελάτης";
            }
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function insertCustomer($data)
    {
        if (strlen($data['afm'])===0){
            $data['afm']='NULL';
        }else{
            //todo search afm from gsis
            $data['afm']=$this->actions->quote($data['afm']);
        }
        if (strlen($data['phone'])===0){
            $data['phone']='NULL';
        }else{
            $data['phone']=$this->actions->quote($data['phone']);
        }
        if (strlen($data['mobile'])===0){
            $data['mobile']='NULL';
        }else{
            $data['mobile']=$this->actions->quote($data['mobile']);
        }
        if (strlen($data['job'])===0){
            $data['job']='NULL';
        }else{
            $data['job']=$this->actions->quote($data['job']);
        }
        if (strlen($data['doy'])===0){
            $data['doy']='NULL';
        }else{
            $data['doy']=$this->actions->quote($data['doy']);
        }
        if (strlen($data['member'])===0){
            $data['member']='NULL';
        }else{
            $data['member']=$this->actions->quote($data['member']);
        }
        $query="INSERT INTO ".$this->model['table']." VALUES (NULL, "
            .$data['afm'].", ".$this->actions->quote($data['name']).", ".$this->actions->quote($data['address']).", ".$this->actions->quote($data['city']).", "
            .$this->actions->quote($data['tk']).", ".$data['phone'].", ".$data['mobile'].", ".$data['job'].", ".$data['doy'].
            ", ".$data['member'].", ".(int)$data['type'].", 0, NOW());";
        $result=$this->actions->db_query($query);
        if ($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function getDiscounts($data)
    {
        $query='SELECT customersDiscounts.id_proion,customersDiscounts.discount,customersDiscounts.type,proion.perigrafi,proion.timi FROM `customersDiscounts` 
                INNER JOIN proion ON customersDiscounts.id_proion=proion.id WHERE id_customer= '.(int)$data['customer'].';';
        $result=$this->actions->db_query($query);
        if (mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['body']=array();
            while($row=mysqli_fetch_assoc($result)){
                array_push($return['body'],$row);
            }
        }else{
            $return['response']=false;
            $return['error']='Δεν έχετε προσθέσει εκπτώσεις στον συγκεκριμένο πελάτη';
        }
        return $return;
    }

    public function getYpoloipo($data)
    {
        if (!isset($data['date'])){
            $data['date']=date('Y-m-d');
        }
        $data['date']=$this->actions->quote($data['date']);
        $query="SELECT MAX(receipts.date) as lastdate, payments.amount FROM `parastatika` INNER JOIN receipts ON parastatika.receipt_id=receipts.id".
            " INNER JOIN payments ON receipts.id=payments.receipt WHERE parastatika.customer_id=".(int)$data['customer'].
            " AND parastatika.type=2 AND DATE(receipts.date)<=".$data['date'].";";
        $result=$this->actions->db_query($query);
        if (mysqli_num_rows($result)>0){
            $lastmetafora=mysqli_fetch_assoc($result);
            $lastdate=date("Y-m-d",strtotime($lastmetafora['lastdate']));
            $amount=$lastmetafora['amount'];
            if (!isset($lastdate)){
                $lastdate=date('Y-m-d');;
                $lastdate=$this->actions->quote($lastdate);
                $amount=0;
            }
            $query="SELECT SUM(payments.amount) as sum FROM parastatika INNER JOIN payments on parastatika.receipt_id=payments.receipt".
                " INNER JOIN receipts on payments.receipt=receipts.id WHERE parastatika.customer_id=".(int)$data['customer']." AND parastatika.type!=2 AND payments.card=1".
                " AND DATE(receipts.date) BETWEEN ".$lastdate." AND ".$data['date'].";";
            $result=$this->actions->db_query($query);
            if (mysqli_num_rows($result)>0){
                $return['response']=true;
                $response=mysqli_fetch_assoc($result);
                $sum_pistoseis=$response['sum'];
                if (!isset($sum_pistoseis)){
                    $sum_pistoseis=0;
                }
                $ypoloipo=(float)$amount+(float)$sum_pistoseis;
                $return['body']=$ypoloipo;
            }else{
                $return['response']=false;
                $return['error']=$result;
            }
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function updateCustomer($data)
    {
        if ($data['name']==="type"){
            $data['value']=(int)$data['value'];
        }else{
            $data['value']=$this->actions->quote($data['value']);
        }
        if ($data['name']==="name"){
            $field=$this->model['name'];
        }elseif ($data['name']==="member"){
            $field=$this->model['member'];
        }elseif ($data['name']==="address"){
            $field=$this->model['address'];
        }elseif ($data['name']==="city"){
            $field=$this->model['city'];
        }elseif ($data['name']==="tk"){
            $field=$this->model['postcode'];
        }elseif ($data['name']==="phone"){
            $field=$this->model['phone'];
        }elseif ($data['name']==="mobile"){
            $field=$this->model['mobile'];
        }elseif ($data['name']==="job"){
            $field=$this->model['job'];
        }elseif ($data['name']==="afm"){
            $field=$this->model['afm'];
        }
        elseif ($data['name']==="type"){
            $field=$this->model['type'];
        }
        $query="UPDATE ".$this->model['table']." SET ".$field." = ".$data['value']." WHERE ".$this->model['id']." = ".(int)$data['id'].";";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=$result;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

}