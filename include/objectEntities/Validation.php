<?php
/**
 * Created by PhpStorm.
 * User: tsoxt
 * Date: 27/12/2018
 * Time: 3:03 μμ
 */

class Validation
{
    private $pubKeyPath;
    //private $privKeyPath;
    private $programInfo;
    private $licensePath;
    private $license;
    private $ifExists;
    private $actions;

    public function __construct()
    {
        $root="/home/ecs/ECSPos";
        $this->ifExists=false;
        $this->pubKeyPath=$root."/files/keys/public.key";
        $this->licensePath=$root."/files/keys/license";
        $this->actions=new dbactions;
        $this->programInfo=$this->getProgramInfo();
        if($licenseEncrypted=file_get_contents($this->licensePath)){
            $this->ifExists=true;
        }
        if($this->ifExists){
            $this->license=json_decode($licenseEncrypted,true);
        }
    }
    private function validate($serial,$date,$signature){
        $digest=sha1($serial.$date);
        if($signature===$digest){
            return true;
        }else{
            return false;
        }
    }

    private function decryptKey($appKey)
    {
        $key=openssl_pkey_get_public(file_get_contents($this->pubKeyPath));
        openssl_public_decrypt($appKey,$decrypted,$key,OPENSSL_PKCS1_PADDING);
        return $decrypted;
    }

    public function insertLicense($data){
        $return=array();
        if(!$this->ifExists){
            $return['response']=false;
            $return['error']="δεν υπάρχει αρχείο αδείας";
            return $return;
        }
        $data=base64_decode($data);
        $licenseKey=$this->decryptKey($data);
        $date=substr($licenseKey,0,8);
        $signature=substr($licenseKey,8,strlen($licenseKey));
        if($this->validate($this->license['serial'],$date,$signature)){
            $datetime=new DateTime($date);
            $date=$datetime->format('Y-m-d H:i:s');
            //$date=date("dmY",strtotime($date));
            //$date=date("Y-m-d",strtotime($date));
            $return['response']=true;
            $dbactions=new dbactions;
            $query="UPDATE systemInfo SET signature=".$this->actions->quote($signature).
                ",exp_date=".$this->actions->quote($date).",start_date=NOW() WHERE serial=".$this->actions->quote($this->license['serial']).";";
            $result=$dbactions->db_query($query);
            if($result!==true){
                $return['response']=false;
                $return['error']=$result;
            }else{
                $return['response']=true;
            }
        }else{
            $return['response']=false;
            $return['error']="Το κλειδί δεν είναι έγγυρο";
        }
        return $return;
    }

    public function getProgramInfo()
    {
        $query="SELECT * FROM systemInfo;";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $response['response'] = true;
            while ($line=mysqli_fetch_assoc($result)){
                $response['body']=$line;
            }
        }else{
            $response['response'] = false;
        }
        return $response;
    }

    public function checkExpirationDate()
    {
        if(!$this->ifExists){
            $return['response']=false;
            $return['error']="δεν υπάρχει αρχείο αδείας";
            return $return;
        }
        $datetime=new DateTime($this->programInfo['body']['exp_date']);
        $date=$datetime->format('Ymd');
        if($this->validate($this->license['serial'],$date,$this->programInfo['body']['signature'])){
            $curdate=new DateTime('now');
            if ($this->programInfo['response']){
                $return['response']=true;
                $exp_date=$this->programInfo['body']['exp_date'];
                $exp_date=new DateTime($exp_date);
                $interval=$curdate->diff($exp_date);
                $interval=$interval->format('%a');
                if ($exp_date > $curdate){
                    $return['lock']=false;
                    if ((int)$interval<=30){
                        $return['msg']='Το πρόγραμμα σας λήγει σε '.$interval.' μέρες. Παρακαλώ επικοινωνήστε με κάποιον υπεύθυνο.';
                    }
                }else{
                    $return['lock']=false;
                    $return['msg']='Το πρόγραμμα σας έχει λήξει και σύντομα θα κλειδώσει. Παρακαλώ ανανεώστε άμεσα.';
                    if ((int)$interval>=5){
                        $return['lock']=true;
                        $return['msg']='Παρακαλώ επικοινωνήστε με κάποιον υπεύθυνο για να ανανεώσετε την συνδρομή σας.';
                    }
                }
            }else{
                $return=$this->programInfo;
            }
        }else{
            $return['response']=true;
            $return['lock']=true;
            $return['msg']="Η άδεια χρήσης σας είναι άκυρη, επικοινωνήστε με κάποιον υπεύθυνο για τη διευθέτηση του θέματος";
        }

        return $return;
    }
}