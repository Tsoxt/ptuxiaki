<?php
//$files=glob($_SERVER['DOCUMENT_ROOT'].'/include/Database/*.php');
//foreach ($files as $file){
//    require_once $file;
//}
//require_once 'Settings.php';



class SpecialBarcodes
{
    private $barcodes;
    private $pretyped;
    private $actions;

    public function __construct(){
        if(!defined("PRETYPED")){
            define("PRETYPED",array(
                26=> 0,
                27=> 1,
                29=> 0
            ));
        }
        $model=new dbmodel;
        $this->barcodes=$model->_barcodes;
        $Settings=new Settings();
        $group=3;
        $temp=$Settings->getSettings($group);
        $settings=array();
        $this->actions=new dbactions();
        foreach ($temp['body'][$group] as $key=>$setting){
            $settings[$key]=$setting['value'];
        }
        if((int)$settings['pretypedBarcodes']===0){
            $this->pretyped=false;
        }else{
            $this->pretyped=true;
        }
    }

    public function isSpecial($barcode){
        $special=$this->getBarcodes();
        if(isset($special['body'][substr($barcode,0,2)])){
            return true;
        }else{
            return false;
        }
    }

    public function getAllBarcodes(){
        if ($this->pretyped){
            $response['pretyped']=true;
            $data['active']=1;
            $data['group']='all';//ToDO refactor them with update barcode;
        }else{
            $data['active']=0;
            $data['group']='all';
            $response['pretyped']=false;
        }
        foreach (PRETYPED as $key => $value){
            $data['start']=$key;
            $data['type']=$value;
            $this->updateBarcodes($data);
        }
        $query="select * from ".$this->barcodes['table'].";";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $response['response'] = true;
            $response['body']=array();
            while ($line=mysqli_fetch_assoc($result)){
                array_push($response['body'],$line);
            }
        }else{
            $response['response'] = false;
            $response['error']=$result;
        }
        return $response;
    }

    public function getBarcodes(){
        $query="select * from ".$this->barcodes['table']." where ".$this->barcodes['active']."=1;";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $response['response'] = true;
            while ($line=mysqli_fetch_assoc($result)){
                $response['body'][$line['start']]=$line['type'];
            }
        }else{
            $response['response'] = false;
        }
        return $response;
    }

    public function updateBarcodes($data){
        $this->actions=new dbactions;
        if ($data['group']==="all"){
            $query="update ".$this->barcodes['table']." set ".$this->barcodes['type']."=".(int)$data['type']
                .", ".$this->barcodes['active']."=".(int)$data['active']." where ".$this->barcodes['start']."=". $this->actions->quote($data['start']).";";
        }else if ($data['group']==="active"){
            $query="update ".$this->barcodes['table']." set ".$this->barcodes['active']."=".(int)$data['value']
                ." where ".$this->barcodes['start']."=". $this->actions->quote($data['start']).";";
        }else{
            $query="update ".$this->barcodes['table']." set ".$this->barcodes['type']."=".(int)$data['value']
                ." where ".$this->barcodes['start']."=". $this->actions->quote($data['start']).";";
        }

        $result=$this->actions->db_query($query);
        if($result){
            $response['response']=true;
        }else{
            $response['response']=false;
        }
        return $response;
    }

    public function ispreTyped($barcode){
        if(!$this->isSpecial($barcode)&&$this->pretyped){
            return true;
        }else{
            return false;
        }
    }


}