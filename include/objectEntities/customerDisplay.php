<?php
/**
 * Created by PhpStorm.
 * User: tsoxt
 * Date: 17/12/2018
 * Time: 7:56 μμ
 */
//TODO fix the problem with multiplication and put sum back to zero
class customerDisplay
{
    private $settings=array();

    function __construct()
    {
        $Settings=new Settings();
        $group=4;
        $Sets=$Settings->getSettings($group);
        foreach ($Sets['body'][$group] as $key=>$setting){
            $this->settings[$key]=$setting['value'];
        }
    }

    private function isActive(){
        if((int)$this->settings['groupname']===1){
            return true;
        }else{
            return false;
        }
    }

    public function sendLine($data){
        if($this->isActive()){
            if((int)$this->settings['type']===0){
                $line1=iconv("UTF-8",$this->settings['encoding']."//IGNORE//TRANSLIT",$data['description']);
                $line1=mb_substr($line1,0,19);
                $line2=number_format($data['temaxia'], 2).' X '.number_format($data['timi'], 2).'= '.number_format((double)$data['timi']*(double)$data['temaxia'],2)."E";
                $line2=iconv("UTF-8",$this->settings['encoding']."//IGNORE//TRANSLIT",$line2);
                $line2=mb_substr($line2,0,20);
                system('echo -e "\014" >'.$this->settings['path'],$exitCode);
                if((int)$exitCode!==0){
                    $return['response'] = false;
                    $return['error'] = 'Πρόβλημα με την</br>οθόνη πελάτη';
                    return $return;
                }
                $command="echo -e '\013".$line1."\n".$line2."' >".$this->settings['path'];
                system($command,$exitCode);
                if((int)$exitCode!==0){
                    $return['response'] = false;
                    $return['error'] = 'Πρόβλημα με την</br>οθόνη πελάτη';
                    return $return;
                }
                $return['response']=true;
                return $return;
            }else{
                $line1=$data['description'];
                $line2=number_format($data['temaxia'], 3).' X '.number_format($data['timi'], 2).' = '.number_format((double)$data['timi']*(double)$data['temaxia'],2);
                $line['line1']=$line1;
                $line['line2']=$line2;
                $line['ammount']=(double)$data['timi']*(double)$data['temaxia'];
                $line['id']=$data['id'];
                $line['mode']=1;
                $output=file_get_contents($this->settings['path']."/receipt.json");
                $output=substr($output,0,-1);
                $output.=",".json_encode($line,true)."]";
                if(file_put_contents($this->settings['path']."/receipt.json",$output)===false) {
                    $return['response'] = false;
                    $return['error'] = 'Πρόβλημα με την</br>οθόνη πελάτη';
                }else{
                    $return['response']=true;
                }
                return $return;
            }

        }else{
            $return['response']=true;
            return $return;
        }
    }

    public function deleteLine($data){
        if($this->isActive()){
            if((int)$this->settings['type']===1){
                $output=file_get_contents($this->settings['path']."/receipt.json");
                $line['id']=$data['id'];
                $line['mode']=0;
                $output=substr($output,0,-1);
                $output.=",".json_encode($line,true)."]";
                if(file_put_contents($this->settings['path']."/receipt.json",$output)===false) {
                    $return['response'] = false;
                    $return['error'] = 'Πρόβλημα με την</br>οθόνη πελάτη';
                }else{
                    $return['response']=true;
                }
                return $return;
            }else{
                $return['response']=true;
                return $return;
            }
        }else{
            $return['response']=true;
            return $return;
        }
    }

    public function updateLine($data){
        if($this->isActive()){
            if((int)$this->settings['type']===1){
                $line1=$data['description'];
                $line2=number_format($data['temaxia'], 3).' X '.number_format($data['timi'], 2).' = '.number_format((double)$data['timi']*(double)$data['temaxia'],2);
                $output=file_get_contents($this->settings['path']."/receipt.json");
                $line['id']=$data['id'];
                $line['mode']=2;
                $line['ammount']=(double)$data['timi']*(double)$data['temaxia'];
                $line['line2']=$line2;
                $line['line1']=$line1;
                $output=substr($output,0,-1);
                $output.=",".json_encode($line,true)."]";
                if(file_put_contents($this->settings['path']."/receipt.json",$output)===false) {
                    $return['response'] = false;
                    $return['error'] = 'Πρόβλημα με την</br>οθόνη πελάτη';
                }else{
                    $return['response']=true;
                }
                return $return;
            }else{
                $return['response']=true;
                return $return;
            }
        }else{
            $return['response']=true;
            return $return;
        }
    }

    public function resetDisplay(){
        if($this->isActive()){
            if((int)$this->settings['type']===0){
                $line1=iconv("UTF-8",$this->settings['encoding']."//IGNORE//TRANSLIT",$this->settings['line1']);
                $line2=iconv("UTF-8",$this->settings['encoding']."//IGNORE//TRANSLIT",$this->settings['line2']);
                system('echo -e "\014" >'.$this->settings['path'],$exitCode);
                if((int)$exitCode!==0){
                    $return['response'] = false;
                    $return['error'] = 'Πρόβλημα με την</br>οθόνη πελάτη';
                    return $return;
                }
                system('echo -e "\013'.$line1.'\n'.$line2.'" >'.$this->settings['path'],$exitCode);
                if((int)$exitCode!==0){
                    $return['response'] = false;
                    $return['error'] = 'Πρόβλημα με την</br>οθόνη πελάτη';
                    return $return;
                }
                $return['response']=true;
                return $return;
            }else{
                $line['line1']=$this->settings['line1'];
                $line['line2']=$this->settings['line2'];
                $line['append']=0;
                $line['mode']=-1;
                $output="[".json_encode($line,true)."]";
                if(file_put_contents($this->settings['path']."/receipt.json",$output)===false) {//group of the file in the symlink must be www-data
                    $return['response'] = false;
                    $return['error'] = 'Πρόβλημα με την</br>οθόνη πελάτη';
                }else{
                    $return['response']=true;
                }
                return $return;
            }
        }else{
            $return['response']=true;
            return $return;
        }
    }

    public function closeDisplay(){
        if($this->isActive()){
            if((int)$this->settings['type']===0){
                $this->settings['line2']="ΤΑΜΕΙΟ ΚΛΕΙΣΤΟ";
                $line1=iconv("UTF-8",$this->settings['encoding']."//IGNORE//TRANSLIT",$this->settings['line1']);
                $line2=iconv("UTF-8",$this->settings['encoding']."//IGNORE//TRANSLIT",$this->settings['line2']);
                system('echo -e "\014" >'.$this->settings['path'],$exitCode);
                if((int)$exitCode!==0){
                    $return['response'] = false;
                    $return['error'] = 'Πρόβλημα με την</br>οθόνη πελάτη';
                    return $return;
                }
                system('echo -e "\013'.$line1.'\n'.$line2.'" >'.$this->settings['path'],$exitCode);
                if((int)$exitCode!==0){
                    $return['response'] = false;
                    $return['error'] = 'Πρόβλημα με την</br>οθόνη πελάτη';
                    return $return;
                }
                $return['response']=true;
                return $return;
            }else{
                $line['line1']=$this->settings['line1'];
                $line['line2']=$this->settings['line2'];
                $line['append']=0;
                $line['mode']=-1;
                $output="[".json_encode($line,true)."]";
                if(file_put_contents($this->settings['path']."/receipt.json",$output)===false) {//group of the file in the symlink must be www-data
                    $return['response'] = false;
                    $return['error'] = 'Πρόβλημα με την</br>οθόνη πελάτη';
                }else{
                    $return['response']=true;
                }
                return $return;
            }
        }else{
            $return['response']=true;
            return $return;
        }
    }
}