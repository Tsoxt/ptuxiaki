<?php
/**
 * Created by PhpStorm.
 * User: Tsoxt
 * Date: 7/12/2018
 * Time: 10:46 AM
 */

class Stats
{
    private $model,$user,$tmima,$omada,$isozugio,$receipt,$log,$paymethods,$payments,$proion,$actions;

    public function __construct()
    {
        $model=new dbmodel;
        $this->model=$model->_kiniseis;
        $this->receipt=$model->_receipts;
        $this->user=$model->_users;
        $this->tmima=$model->_fpa;
        $this->omada=$model->_omada;
        $this->isozugio=$model->_isozigio;
        $this->log=$model->_log;
        $this->paymethods=$model->_payment_methods;
        $this->payments=$model->_payments;
        $this->proion=$model->_proion;
        $this->actions=new dbactions();
    }

    public function getUserStats($data)
    {
        $query="SELECT ".$this->user['table'].".".$this->user['name'].", SUM(".$this->model['table'].".".$this->model['sum'].") AS sunolo, COUNT(DISTINCT ".$this->model['table'].".".$this->model['receipt'].") AS receipt FROM ((".$this->user['table'].
            " INNER JOIN ".$this->receipt['table']." ON ".$this->user['table'].".".$this->user['id']." = ".$this->receipt['table'].".".$this->receipt['operator'].") LEFT JOIN ".$this->model['table'].
            " ON ".$this->receipt['table'].".".$this->receipt['id']." = ".$this->model['table'].".".$this->model['receipt'].") WHERE ".$this->receipt['table'].".".$this->receipt['date']." BETWEEN ".$this->actions->quote($data['datefrom']).
            " AND ".$this->actions->quote($data['dateto'])." GROUP BY ".$this->receipt['table'].".".$this->receipt['operator'].";";
        $result=$this->actions->db_query($query);
        if (!($result===false)){
            if(mysqli_num_rows($result)>0){
                $return['response']=true;
                $return['userstats']=array();
                while($row=mysqli_fetch_assoc($result)){
                    array_push($return['userstats'], $row);
                }
            }else{
                $return['response']=false;
                $return['error']="Δεν βρέθηκαν στατιστικά για τις ημερομηνίες που επιλέξατε";
            }
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function getTmimataStats($data)
    {
        $query="SELECT ".$this->tmima['table'].".".$this->tmima['name'].", SUM(".$this->model['table'].".".$this->model['sum'].") AS sunolo FROM ((".$this->tmima['table']." INNER JOIN ".$this->model['table'].
            " ON ".$this->model['table'].".".$this->model['tmima']." = ".$this->tmima['table'].".".$this->tmima['id'].") LEFT JOIN ".$this->receipt['table'].
            " ON ".$this->receipt['table'].".".$this->receipt['id']." = ".$this->model['table'].".".$this->model['receipt'].")  WHERE ".$this->receipt['table'].".".$this->receipt['date']." BETWEEN ".$this->actions->quote($data['datefrom']).
            " AND ".$this->actions->quote($data['dateto'])." GROUP BY ".$this->model['table'].".".$this->model['tmima'].";";
        $result=$this->actions->db_query($query);
        if (!($result===false)){
            if(mysqli_num_rows($result)>0){
                $return['response']=true;
                $return['tmimastats']=array();
                while($row=mysqli_fetch_assoc($result)){
                    array_push($return['tmimastats'], $row);
                }
            }else{
                $return['response']=false;
                $return['error']="Δεν βρέθηκαν στατιστικά για τις ημερομηνίες που επιλέξατε";
            }
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function getOmadesStats($data)
    {
        $query="SELECT ".$this->omada['table'].".".$this->omada['perig'].", SUM(".$this->model['table'].".".$this->model['sum'].
            ") AS sunolo, SUM(".$this->model['table'].".".$this->model['quan'].") AS quantity FROM ".$this->model['table']." LEFT JOIN ".$this->omada['table'].
            " ON ".$this->model['table'].".".$this->model['omada']." = ".$this->omada['table'].".".$this->omada['id']." LEFT JOIN ".$this->receipt['table'].
            " ON ".$this->receipt['table'].".".$this->receipt['id']." = ".$this->model['table'].".".$this->model['receipt'].
            " WHERE ".$this->receipt['table'].".".$this->receipt['date']." BETWEEN ".$this->actions->quote($data['datefrom']).
            " AND ".$this->actions->quote($data['dateto'])." GROUP BY ".$this->model['table'].".".$this->model['omada'].";";
        $result=$this->actions->db_query($query);
        if (!($result===false)){
            if(mysqli_num_rows($result)>0){
                $return['response']=true;
                $return['omadastats']=array();
                while($row=mysqli_fetch_assoc($result)){
                    array_push($return['omadastats'], $row);
                }
            }else{
                $return['response']=false;
                $return['error']="Δεν βρέθηκαν στατιστικά για τις ημερομηνίες που επιλέξατε";
            }
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function getIsozigio($data)
    {
        $query="select ".$this->omada['table'].".".$this->omada['perig'].", ".$this->isozugio['table'].".".$this->isozugio['id_om']
            .", SUM(".$this->isozugio['table'].".".$this->isozugio['quantity'].") as sunolo from ("
            .$this->omada['table']." right join ".$this->isozugio['table']." on "
            .$this->omada['table'].".".$this->omada['zugaria']."=".$this->isozugio['table'].".".$this->isozugio['id_om'].")"
            ." where MONTH(".$this->isozugio['table'].".".$this->isozugio['date'].") = ".(int)$data['datefrom']
            ." and YEAR(".$this->isozugio['table'].".".$this->isozugio['date'].") = ".(int)$data['dateto']
            ." group by ".$this->isozugio['table'].".".$this->isozugio['id_om'].";";
        $result=$this->actions->db_query($query);
        if (!($result===false)){
            if(mysqli_num_rows($result)>0){
                $return['response']=true;
                $return['omadesisozigio']=array();
                while($row=mysqli_fetch_assoc($result)){
                    array_push($return['omadesisozigio'], $row);
                }
            }else{
                $return['response']=false;
                $return['error']="Δεν βρέθηκαν στατιστικά για τον μήνα που επιλέξατε";
            }
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function getStatsPerHour($data)
    {
        $query="SELECT HOUR(".$this->receipt['table'].".".$this->receipt['date'].") AS hour, SUM(".$this->model['table'].".".$this->model['sum'].
            ") AS sunolo, COUNT(DISTINCT ".$this->model['table'].".".$this->model['receipt'].") AS receipt FROM ".$this->model['table']. " LEFT JOIN ".$this->receipt['table'].
            " ON ".$this->model['table'].".".$this->model['receipt']." = ".$this->receipt['table'].".".$this->receipt['id']." WHERE ".$this->receipt['table'].".".$this->receipt['date'].
            " BETWEEN ".$this->actions->quote($data['datefrom'])." AND ".$this->actions->quote($data['dateto'])." GROUP BY HOUR(".$this->receipt['table'].".".$this->receipt['date'].");";
        $result=$this->actions->db_query($query);
        if (!($result===false)){
            if(mysqli_num_rows($result)>0){
                $return['response']=true;
                $return['statsperhour']=array();
                while($row=mysqli_fetch_assoc($result)){
                    array_push($return['statsperhour'], $row);
                }
            }else{
                $return['response']=false;
                $return['error']="Δεν βρέθηκαν στατιστικά για τις ημερομηνίες που επιλέξατε";
            }
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function getStatsByPaymentMethods($data){
        $query="SELECT ".$this->paymethods['table'].".".$this->paymethods['name'].", SUM(".$this->payments['table'].".".$this->payments['amount'].") AS sunolo FROM ".$this->payments['table'].
            " LEFT JOIN ".$this->receipt['table']." ON ".$this->payments['table'].".".$this->payments['receipt']." = ".$this->receipt['table'].".".$this->receipt['id']." LEFT JOIN ".$this->paymethods['table'].
            " ON ".$this->payments['table'].".".$this->payments['card']." = ".$this->paymethods['table'].".".$this->paymethods['id']." WHERE ".$this->receipt['table'].".".$this->receipt['date']." BETWEEN ".$this->actions->quote($data['datefrom']).
            " AND ".$this->actions->quote($data['dateto'])." GROUP BY ".$this->payments['table'].".".$this->payments['card'].";";
        $result=$this->actions->db_query($query);
        if (!($result===false)){
            if(mysqli_num_rows($result)>0){
                $return['response']=true;
                $return['paymentmethodsstats']=array();
                while($row=mysqli_fetch_assoc($result)){
                    array_push($return['paymentmethodsstats'], $row);
                }
            }else{
                $return['response']=false;
                $return['error']="Δεν βρέθηκαν στατιστικά για τις ημερομηνίες που επιλέξατε";
            }
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;

    }

}