<?php

class Category
{
    private $model;
    private $api;
    private $actions;

    public function __construct()
    {
        $db=new dbmodel;
        $this->model=$db->_category;
        $this->api=new BarcodeApi();
        $this->actions=new dbactions();
    }

    private function returnCategory($categories){
        $categories=json_decode($categories,true);
        if(!empty($categories)){
            $category=$categories[0];
            $cat['id']=$category['id'];
            $cat['altId']=$category['alt_id'];
            $cat['name']=$category['description'];
            $cat['active']=1;
            $cat['seira']=NULL;
            $cat['api']=1;
            $return['response']=true;
            $return['category']=$cat;
        }else{
            $return['response']=false;
            $return['error']="Η κατηγορία δεν υπάρχει";
        }
        return $return;
    }

    public function getCategory($data){
        $query="select * from ".$this->model['table']." where ".$this->model['id']." = ".(int)$data['id']." ;";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['category']=mysqli_fetch_assoc($result);
        }else{
            if($this->api->isActive()){
                $response=$this->api->categoryFromApi($data['id']);
                if($response['httpCode']===200){
                    $return=$this->returnCategory($response['body']);
                }else{
                    $return['response']=false;
                    $return['error']=$response['error'];
                }
            }else{
                $return['response']=false;
                $return['error']="η κατηγορία δεν υπάρχει";
            }
        }
        return $return;
    }

    public function getCategories(){
        $query="select * from ".$this->model['table'].";";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['categories']=array();
            while($row=mysqli_fetch_assoc($result)){
                array_push($return['categories'],$row);
            }
        }else{
            $return['response']=false;
            $return['error']="Υπήρξε κάποιο πρόβλημα προσπαθήστε πάλι";
        }
        return $return;
    }

    public function insertCategory($data){
        if(strlen($data['altId'])===0){
            $data['altId']='NULL';
        }else{
            $data['altId']=(int)$data['altId'];
        }
        if(!(int)$data['api']===1){
            $data['id']='NULL';
        }else{
            $data['id']=(int)$data['id'];
        }
        if(strlen($data['seira'])===0){
            $data['seira']='NULL';
        }else{
            $data['seira']=(int)$data['seira'];
        }
        $query  = "insert into ".$this->model['table'].
            " values ".
            "(".$data['id'].", ".$data['altId'].", ".$this->actions->quote($data['name']).
            ", ".(int)$data['active'].", ".$data['seira'].", ".(int)$data['api'].", 0) ;";
        $result=$this->actions->db_query($query);
        if($result===true) {
            $return['response']=$result;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function updateCategory($data){
        if ((int)$data['api']===1){
            $query = "UPDATE ".$this->model['table']." SET ".$this->model['active']." = ".(int)$data['active']." WHERE ".$this->model['id']." = ".(int)$data['id'].";";
        }else{
            $query = "update ".$this->model['table']." SET ".$this->model['active']." = ".(int)$data['active'].", ".$this->model['perig']." = ".$this->actions->quote($data['name']).
                " WHERE ".$this->model['id']." = ".(int)$data['id'].";";
        }
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }
}