<?php

class Receipt
{
    private $kiniseis;
    private $isozigio;
    private $fpa;
    private $item;
    private $omada;
    private $cashier;
    private $receipts;
    private $payments;
    private $parastatika;
    private $printer;
    private $actions;

    public function __construct()
    {
        $db=new dbmodel;
        $this->kiniseis=$db->_kiniseis;
        $this->isozigio=$db->_isozigio;
        $this->fpa=$db->_fpa;
        $this->receipts=$db->_receipts;
        $this->payments=$db->_payments;
        $this->item=new Proion();
        $this->omada=new Omada();
        $this->cashier=new Cashier();
        $this->parastatika=new Parastatika();
        $this->printer=new Printer();
        $this->actions=new dbactions();
    }

    public function payCustomerYpoloipo($data)
    {
        $return['response']=true;
        $data['tameio']=$data['receipt'][0]['tameio'];
        $data['operator']=$data['receipt'][0]['operator'];
        $apodeiksi=$this->receipt($data);
        if ($apodeiksi['response']){
            $tameio['tameio']=$data['tameio'];
            $maxid=$this->maxReceiptId($tameio);
            if ($maxid['response']){
                foreach ($data['payments'] as $pliromi){
                    $pliromi['receipt']=$maxid['body']['lastid'];
                    $payment=$this->credits($pliromi);
                    if (!$payment['response']){
                        $return=$payment;
                        break;
                    }
                }
                if ($payment['response']){
                    $data['type']=4;
                    if (isset($data['aa'])){
                        $data['invoiceNumber']=$data['aa'];
                    }else{
                        $data['invoiceNumber']='NULL';
                    }
                    $receipt=array();
                    if (isset($data['matched'])){
                        $receipt['matched']=$data['matched'];
                        $data['matched']=1;
                    }else{
                        $receipt['matched']=$data['matched'];
                    }
                    $data['receipt']=$maxid['body']['lastid'];
                    $parastatiko=$this->parastatika->insertParastatiko($data);
                    if ($parastatiko['response']){
                        if ((int)$data['invoice']!==2){//einai ektiposimo
                            if($this->printer->isActive()){
                                $receipt['apodeiksi_eispraksis']=true;
                                if (isset($data['par_type'])){
                                    $invoice['par_type']=(int)$data['par_type'];
                                }
                                $invoice['customer']=$data['customer'];
                                $invoice['type']=(int)$data['invoice'];
                                $printer=$this->printer->printInvoice($receipt,$data['payments'],$invoice);
                                if(isset($printer['url'])){
                                    $return['url']=$printer['url'];
                                }
                                if (!$printer['response']){
                                    $return['response']=false;
                                    $return['error']=$printer['error'];
                                }
                            }

                        }
                    }else{
                        $return['response']=false;
                        $return['error']=$parastatiko['error'];
                    }
                }else{
                    $return['response']=false;
                    $return['error']=$payment['error'];
                }
            }else{
                $return['response']=false;
                $return['error']=$maxid['error'];
            }
        }else{
            $return['response']=false;
            $return['error']=$apodeiksi['error'];
        }
        return $return;
    }

    private function maxReceiptId($apodeiksi){
        $query="select MAX(".$this->receipts['id'].") as lastid from ".$this->receipts['table']." where ".$this->receipts['tameio']." = ".(int)$apodeiksi['tameio'].";";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['body']=mysqli_fetch_assoc($result);
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    private function receipt($apodeiksi){
        if (isset($apodeiksi['date'])){
            $time=date("H:i:s");
            $date=$apodeiksi['date']." ".$time;
            $date=$this->actions->quote($date);
        }else{
            $date="NOW()";
        }
        $query="insert into ".$this->receipts['table']." values (NULL, ".$apodeiksi['tameio'].", ".$apodeiksi['operator'].", ".$date.");";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    private function credits($card){
        $return['response']=true;
        $query="insert into ".$this->payments['table']." values (NULL, ".$card['receipt'].", ".$card['amount'].", ".$card['id'].");";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=$result;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    private function createReceipt($data){
        $tmima=new Fpa();
        $receipt=array();
        foreach ($data as $row){
            $receiptRow['description']=$row['desc'];
            $receiptRow['temaxia']=$row['temaxia'];
            $receiptRow['tmima']=$row['tmima'];
            $askTmima['id']=$row['tmima'];
            $receiptRow['fpa']=(double)$tmima->findTmima($askTmima)['proion']['price']*100;
            $receiptRow['timi']=$row['price'];
            array_push($receipt, $receiptRow);
        }
        return $receipt;
    }

    private function storeData($receiptLine){
        $return['response']=true;
        if(strlen($receiptLine['barcode'])===0||$receiptLine['barcode']==='null'){
            $receiptLine['barcode']='NULL';
        }else{
            $receiptLine['barcode']=$this->actions->quote($receiptLine['barcode']);
        }
        $query="insert into ".$this->kiniseis['table']." values (NULL, ".(int)$receiptLine['mothercode'].", ".(int)$receiptLine['id'].
            ", ".$receiptLine['barcode'].", ".(int)$receiptLine['id_om'].", ".(double)$receiptLine['temaxia'].
            ", ".(double)$receiptLine['price'].", ".(int)$receiptLine['fpa'].", ".(double)$receiptLine['sunolo'].
            ", ".(int)$receiptLine['tmima'].", ".(int)$receiptLine['package'].", ".$receiptLine['receipt'].", 0)";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=$result;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function storeIsozigio($receiptLine){
        if(strlen((string)$receiptLine['id_om'])>2){
            $data['id']=$receiptLine['id_om'];
            $result=$this->omada->getOmada($data);
            $barcode=$this->actions->quote($receiptLine['barcode']);
        }else{
            $result['response']=true;
            $result['omada']['omada_zugaria']=$receiptLine['id_om'];
            $result['omada']['isozigio']=1;
            $barcode='NULL';
        }
        if (isset($receiptLine['month'])){
            $date=$receiptLine['month'].'-25 23:59:59';
        }else{
            $date=date("Y-m-d H:i:s");
        }
        if ($result['response']){
            if ((int)$result['omada']['isozigio']===1){
                $query="insert into ".$this->isozigio['table']." values (NULL, ".(int)$result['omada']['omada_zugaria'].", ".$barcode.
                    ", ".(double)$receiptLine['temaxia'].", ".$this->actions->quote($date).");";
                $result=$this->actions->db_query($query);
                if ($result===true){
                    $return['response']=$result;
                }else{
                    $return['response']=false;
                    $return['error']=$result;
                }
            }else{
                $return=$result;
            }
        }else{
            $return['response']=false;
            $return['error']=$result['error'];
        }
        return $return;


    }

    public function handleReceipt($data){
        $return['response']=true;
        $receiptData=$data['receipt'];
        $creditCards=$data['payments'];
        $invoice['customer']=$data['customer'];
        $invoice['type']=(int)$data['invoice'];
        if (isset($data['par_type'])){
            $invoice['par_type']=(int)$data['par_type'];
        }
        if($invoice['type']===1){//timologio
            if($this->printer->isActive()){
                $receipt=$this->createReceipt($receiptData);
                $return=$this->printer->printInvoice($receipt,$creditCards,$invoice);
                if(isset($return['url'])){
                    $document=$return['url'];
                }
            }
        }else if ($invoice['type']===0){//apodeiksi
            if($this->cashier->isActive()){
                $receipt=$this->createReceipt($receiptData);
                $return=$this->cashier->sendReceipt($receipt,$creditCards);
            }
        }else{//xwris ektiposi
            $return['invoData']['id']= $invoice['par_type'];
            $return['invoData']['lastNumber']=$data['aa'];
        }
        if($return['response']){
            $apodeiksi['operator']=$receiptData[0]['operator'];
            $apodeiksi['tameio']=$receiptData[0]['tameio'];
            if (isset($data['date'])){
                if ($data['date']!=='NULL'){
                    $apodeiksi['date']=$data['date'];
                }
            }
            $receipt=$this->receipt($apodeiksi);
            if($receipt['response']){
                $receiptId=$this->maxReceiptId($apodeiksi);
                if ($receiptId['response']){
                    if ($invoice['customer']!==0){
                        $invoice['receipt']=(int)$receiptId['body']['lastid'];
                        //todo fix return invodata when cashier inactive and not timologio
                        $invoice['type']=(int)$return['invoData']['id'];
                        $invoice['invoiceNumber']=$return['invoData']['lastNumber'];
                        if (isset($data['matched'])){
                            $invoice['matched']=1;
                            foreach ($data['matched'] as $deltio){
                                $par['id']=$deltio;
                                $updateParastatiko=$this->parastatika->updateParastatiko($par);
                                if ($updateParastatiko['response']===false){
                                    $return['response']=false;
                                    $return['error']=$updateParastatiko['error'];
                                    break;
                                }
                            }
                        }else{
                            $invoice['matched']=0;
                        }
                        $timologio=$this->parastatika->insertParastatiko($invoice);
                        if ($timologio['response']){
                            $return['response']=true;
                        }else{
                            $return=$timologio;
                        }
                    }
                    $creditcard['response']=true;
                    if (!empty($creditCards)){
                        foreach ($creditCards as $card){
                            if((int)$card['id']===-1){
                                continue;
                            }
                            $card['receipt']=(int)$receiptId['body']['lastid'];
                            $creditcard=$this->credits($card);
                            if (!$creditcard['response']){
                                $return=$creditcard;
                                break;
                            }
                        }
                    }
                    if ($creditcard['response']){
                        $barcodesInserted= array();
                        foreach ($data['receipt'] as $receiptLine){
                            $check=in_array($receiptLine['barcode'],$barcodesInserted);
                            if(((int)$receiptLine['new']===1)&&!$check){
                                $return=$this->item->insertItem($receiptLine);
                                array_push($barcodesInserted,$receiptLine['barcode']);
                            }else{
                                if(strlen($receiptLine['id'])!==0){
                                    $toUpdate['id']=$receiptLine['id'];
                                    if (isset($data['update_stock'])){//enimerosi apothematos parastatika pelaton
                                        if ((int)$data['update_stock']===1){//polisi posotitas
                                            $toUpdate['stock']=(-(double)$receiptLine['temaxia']);
                                            $return=$this->item->updateStock($toUpdate);
                                        }elseif ((int)$data['update_stock']===2){//epistrofi posotitas
                                            $toUpdate['stock']=(double)$receiptLine['temaxia'];
                                            $return=$this->item->updateStock($toUpdate);
                                        }else{
                                            $return['response']=true;
                                        }
                                    }else{
                                        $toUpdate['stock']=(-(double)$receiptLine['temaxia']);
                                        $return=$this->item->updateStock($toUpdate);
                                    }
                                    if($return['response']&&((int)$receiptLine['api']===1)&&((int)$receiptLine['flag_timi']===0)){
                                        $toUpdate['ftimi']=1;
                                        $toUpdate['price']=$receiptLine['price'];
                                        $return=$this->item->setPrice($toUpdate);
                                    }
                                }else{
                                    $receiptLine['id']=1;
                                }
                            }
                            if(!$return['response']){
                                break;
                            }
                            $tmima=new Fpa();
                            $askTmima['id']=$receiptLine['tmima'];
                            $receiptLine['fpa']=(double)$tmima->findTmima($askTmima)['proion']['price']*100;
                            unset($tmima);
                            $receiptLine['temaxia']=-(double)$receiptLine['temaxia'];
                            $receiptLine['receipt']=(int)$receiptId['body']['lastid'];
                            $return=$this->storeData($receiptLine);
                            if(!$return['response']){
                                break;
                            }
                            $return=$this->storeIsozigio($receiptLine);
                            if (!$return['response']){
                                break;
                            }
                        }
                    }
                    if(isset($document)){
                        $return['url']=$document;
                    }
                }
            }else{
                $return=$receipt;
            }
        }
        return $return;
    }
}