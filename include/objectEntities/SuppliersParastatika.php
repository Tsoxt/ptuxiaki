<?php
/**
 * Created by PhpStorm.
 * User: Tsoxt
 * Date: 11/2/2019
 * Time: 12:31 PM
 */

class SuppliersParastatika
{
    private $kiniseis;
    private $actions;
    private $orders;
    private $parastatika;
    private $payments;
    private $printer;

    public function __construct()
    {
        $model=new dbmodel;
        $this->actions=new dbactions();
        $this->kiniseis=$model->_suppliersKiniseis;
        $this->orders=$model->_suppliersOrders;
        $this->parastatika=$model->_suppliersParastatika;
        $this->payments=$model->_suppliersPayments;
        $this->printer=new Printer();

    }

    private function createReceipt($data)
    {
        $receipt = array();
        foreach ($data as $row) {
            $receiptRow['description'] = $row['desc'];
            $receiptRow['temaxia'] = $row['temaxia'];
            $receiptRow['tmima'] = $row['tmima'];
            $askTmima['id'] = $row['tmima'];
            $receiptRow['fpa'] = (float)$row['fpa_proion']*100;
            $receiptRow['timi'] = (float)$row['price']*((100+$receiptRow['fpa'])/100);
            $receiptRow['timi']=number_format($receiptRow['timi'],2);
            if ((int)$row['choose_discount']===0){//aksiaki
                $receiptRow['ekptosi'] = $row['ekptosi_proion'];
            }else{//epi tis %
                $receiptRow['ekptosi'] =((float)$row['temaxia']*(float)$row['price'])*((float)$row['ekptosi_proion']/100);
            }
            array_push($receipt, $receiptRow);
        }
        return $receipt;
    }

    private function updateParastatiko($data)
    {
        $query="UPDATE ".$this->parastatika['table']." SET ".$this->parastatika['matched']." = 1 WHERE ".$this->parastatika['id']." = ".(int)$data['id'].";";
        $result=$this->actions->db_query($query);
        if ($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['query']=$query;
            $return['error']=$result;
        }
        return $return;

    }

    public function getSupplierParastatika($data)
    {
        $return['response']=true;
        $fromdate=date("Y-m-d 00:00:00",strtotime($data['fromdate']));
        $todate=date("Y-m-d 23:59:59",strtotime($data['todate']));
        $query="CALL suppliersSum(".(int)$data['supplier'].",".$this->actions->quote($fromdate).",".$this->actions->quote($todate).")";
        $result=$this->actions->process($query);
        if ($result['response']) {
            $return['parastatika']=$result['result'];
        }else{
            $return['response']=false;
            $return['error']=$result['error'];
        }
        return $return;
    }

    public function supplierParastatikaInfo($data)
    {
        $query="SELECT suppliersKiniseis.proion_id,suppliersKiniseis.quantity,suppliersKiniseis.timi_agoras,suppliersKiniseis.discount, suppliersKiniseis.fpa,".
            "suppliersKiniseis.final_sum,proion.tmima,proion.perigrafi,proion.temaxia,proion.stock,proion.barcode,proion.mothercode,proion.id_omadas,proion.zugistiko"
            ." FROM `suppliersKiniseis` INNER JOIN proion".
            " ON suppliersKiniseis.proion_id=proion.id WHERE parastatika_id=".(int)$data['id'].";";
        $result=$this->actions->db_query($query);
        if (mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['details']=array();
            while ($row=mysqli_fetch_assoc($result)){
                array_push($return['details'], $row);
            }
        }else{
            $return['response'] = false;
            $return['error'] = $result;
        }
        return $return;
    }

    public function getOrderDetails($data)
    {
        $query="SELECT suppliersKiniseis.proion_id,suppliersKiniseis.quantity,suppliersKiniseis.timi_agoras,proion.tmima,proion.perigrafi,".
            "proion.temaxia,proion.stock,proion.barcode,proion.mothercode,proion.id_omadas,proion.zugistiko FROM `suppliersKiniseis`".
            " INNER JOIN proion ON suppliersKiniseis.proion_id=proion.id WHERE suppliersKiniseis.orders_id=".(int)$data['order_id'].";";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0) {
            $return['response'] = true;
            $return['orders'] = array();
            while ($row = mysqli_fetch_assoc($result)) {
                array_push($return['orders'], $row);
            }
        }else{
            $return['response'] = false;
            $return['error'] = $result;
        }
        return $return;
    }

    public function getOrders($data)
    {
        if ((int)$data['supplier']!==0){
            $query="SELECT suppliersOrders.id,suppliersOrders.supplier_id,suppliersOrders.date,suppliers.name FROM suppliersOrders INNER JOIN suppliers".
                " ON suppliersOrders.supplier_id=suppliers.id WHERE suppliersOrders.supplier_id = ".(int)$data['supplier'].
                " AND suppliersOrders.received=0 ORDER BY suppliersOrders.date DESC;";
        }else{
            $query="SELECT suppliersOrders.id,suppliersOrders.supplier_id,suppliersOrders.date,suppliers.name FROM suppliersOrders INNER JOIN suppliers".
                " ON suppliersOrders.supplier_id=suppliers.id WHERE suppliersOrders.received=0 ORDER BY suppliersOrders.date DESC;";
        }
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0) {
            $return['response'] = true;
            $return['body'] = array();
            while ($row = mysqli_fetch_assoc($result)) {
                array_push($return['body'], $row);
            }
        }else{
            $return['response'] = false;
            $return['error'] = $result;
        }
        return $return;
    }

    public function matchParastatiko($data)
    {
        if ((int)$data['update_flag']===0){//deltio apostolis
            $updateflag=1;
        }else{
            $updateflag=0;
        }
        if ((int)$data['supplier']!==0){
            $query="SELECT suppliersParastatika.id,suppliersParastatika.type,suppliersParastatika.supplier_id,suppliersParastatika.aa,suppliersParastatika.parastatiko_date,".
                "suppliers.name,parastatikaInfo.shortname FROM suppliersParastatika INNER JOIN suppliers".
                " ON suppliersParastatika.supplier_id=suppliers.id INNER JOIN parastatikaInfo ON suppliersParastatika.type=parastatikaInfo.id".
                " WHERE suppliersParastatika.supplier_id = ".(int)$data['supplier'].
                " AND parastatikaInfo.flow_flag=".(int)$data['flow_flag']." AND parastatikaInfo.update_flag=".$updateflag." AND suppliersParastatika.matched=0".
                " ORDER BY suppliersParastatika.parastatiko_date DESC;";
        }else{
            $query="SELECT suppliersParastatika.id,suppliersParastatika.type,suppliersParastatika.supplier_id,suppliersParastatika.aa,suppliersParastatika.parastatiko_date,".
                "suppliers.name,parastatikaInfo.shortname FROM suppliersParastatika INNER JOIN suppliers".
                " ON suppliersParastatika.supplier_id=suppliers.id INNER JOIN parastatikaInfo ON suppliersParastatika.type=parastatikaInfo.id".
                " WHERE parastatikaInfo.flow_flag=".(int)$data['flow_flag']." AND parastatikaInfo.update_flag=".$updateflag." AND suppliersParastatika.matched=0".
                " ORDER BY suppliersParastatika.parastatiko_date DESC;";
        }
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0) {
            $return['response'] = true;
            $return['body'] = array();
            while ($row = mysqli_fetch_assoc($result)) {
                array_push($return['body'], $row);
            }
        }else{
            $return['response'] = false;
            $return['error'] = $result;
        }
        return $return;
    }

    private function getLatestId($type){
        if ((int)$type===1){
            $query="select max(".$this->parastatika['id'].") as max from ".$this->parastatika['table']." ;";
        }else{
            $query="select max(".$this->orders['id'].") as max from ".$this->orders['table']." ;";
        }
        return (int)mysqli_fetch_assoc($this->actions->db_query($query))["max"];
    }

    public function insertParastatiko($data)
    {
        if ($data['aa']===''){
           $data['aa']='-';
        }
        if ($data['matched']!=='NULL'){
            $matched=1;
        }else{
            $matched=0;
        }
        if ($data['date']!=='NULL'){
            if ($data['date']===date('Y-m-d')){
                $parastatiko_date='NOW()';
            }else{
                $parastatiko_date="'".$data['date']." 00:00:00'";
            }
        }else{
            $parastatiko_date='NOW()';
        }
        $query="INSERT INTO ".$this->parastatika['table']." VALUES(NULL,".(int)$data['supplier'].", ".(int)$data['type'].", NOW(), 0, ".$this->actions->quote($data['aa']).",0,"
            .$parastatiko_date.",".$matched.");";
        $result=$this->actions->db_query($query);
        if ($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['query']=$query;
            $return['error']=$result;
        }
        return $return;
    }

    private function insertOrder($data)
    {
        $query="INSERT INTO ".$this->orders['table']." VALUES(NULL,".(int)$data['supplier'].", NOW(), 0);";
        $result=$this->actions->db_query($query);
        if ($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['query']=$query;
            $return['error']=$result;
        }
        return $return;
    }

    private function insertKinisi($data)
    {
        if (isset($data['parastatiko_id'])){
            $data['parastatiko_id']=(int)$data['parastatiko_id'];
        }else{
            $data['parastatiko_id']='NULL';
        }
        if (isset($data['order_id'])){
            $data['order_id']=(int)$data['order_id'];
        }else{
            $data['order_id']='NULL';
        }
        if ((int)$data['choose_discount']===1){//ekptosi epis tis %
            $data['ekptosi_proion']=((float)$data['temaxia']*(float)$data['price'])*(((float)$data['ekptosi_proion'])/100);
        }
        $query="INSERT INTO ".$this->kiniseis['table']." VALUES(NULL, ".(int)$data['id'].", ".(float)$data['temaxia'].
            ", ".(float)$data['temaxia'].", ".(float)$data['price'].", ".(float)$data['ekptosi_proion'].", ".(float)$data['fpa_proion'].", ".(float)$data['sunolo'].", ".
            $data['parastatiko_id'].", ".$data['order_id'].");";
        $result=$this->actions->db_query($query);
        if ($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['query']=$query;
            $return['error']=$result;
        }
        return $return;
    }

    private function updateKinisi($data)
    {
        if ((int)$data['choose_discount']===1){//ekptosi epis tis %
            $data['ekptosi_proion']=((float)$data['temaxia']*(float)$data['price'])*(((float)$data['ekptosi_proion'])/100);
        }
        $query="UPDATE ".$this->kiniseis['table']." SET ".$this->kiniseis['quantity']." = ".(float)$data['temaxia'].",".$this->kiniseis['rest']." = ".$this->kiniseis['rest']."-".number_format($data['temaxia'],3,'.','').
            ", ".$this->kiniseis['timi']." = ".(float)$data['price'].", ".$this->kiniseis['discount']." = ".(float)$data['ekptosi_proion'].", "
            .$this->kiniseis['fpa']." = ".(float)$data['fpa_proion'].",".$this->kiniseis['sum']." = ".(float)$data['sunolo'].", "
            .$this->kiniseis['parastatiko']." = ".(int)$data['parastatiko_id']." WHERE ".$this->kiniseis['proion']." = ".(int)$data['id'].
            " AND ".$this->kiniseis['order']." = ".(int)$data['order_no'].";";
        $result=$this->actions->db_query($query);
        if ($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['query']=$query;
            $return['error']=$result;
        }
        return $return;
    }

    private function updateOrder($data)
    {
        $query="UPDATE ".$this->orders['table']." SET ".$this->orders['received']." =1, ".$this->orders['date']." = NOW() WHERE ".
            $this->orders['id']." = ".(int)$data['order_id'].";";
        $result=$this->actions->db_query($query);
        if ($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['query']=$query;
            $return['error']=$result;
        }
        return $return;
    }

    public function removeOrder($data)
    {
        $return['response']=true;
        $query="DELETE FROM ".$this->kiniseis['table']." WHERE ".$this->kiniseis['order']." = ".(int)$data['order_id'].";";
        $result=$this->actions->db_query($query);
        if ($result===true){
            $query2="DELETE FROM ".$this->orders['table']." WHERE ".$this->orders['id']." = ".(int)$data['order_id'].";";
            $result2=$this->actions->db_query($query2);
            if ($result2===false){
                $return['response']=false;
                $return['query']=$query2;
                $return['error']=$result2;
            }
        }else{
            $return['response']=false;
            $return['query']=$query;
            $return['error']=$result;
        }
        return $return;
    }

    public function insertDeltioEpistrofis($data)
    {
        $proion=new Proion();
        $return['response']=true;
        $deltio=array();
        $invoice['supplier']=$data['supplier'];
        $invoice['par_type']=$data['deltio']['parastatiko_type'];
        $invoice['type']=(int)$data['deltio']['invoice'];
        $receiptData=$data['deltio']['receipt'];
        $creditCards=$data['deltio']['payments'];
        if ($invoice['type']===1){//timologio
            if($this->printer->isActive()){
                $receipt=$this->createReceipt($receiptData);
                $sendprint=$this->printer->printInvoice($receipt,$creditCards,$invoice);
                if ($sendprint['response']){
                    $return['response']=true;
                    if(isset($sendprint['url'])){
                        $document=$return['url'];
                    }
                }else{
                    $return=$sendprint;
                }

            }
        }else{//xwris ektiposi

        }
        if(isset($document)){
            $return['url']=$document;
        }
        if ($return['response']){
            $parastatiko['supplier']=$data['supplier'];
            $parastatiko['type']=$data['deltio']['parastatiko_type'];
            $parastatiko['date']=$data['date'];
            if (isset($sendprint['invoData']['lastNumber'])){
                $parastatiko['aa']=$sendprint['invoData']['lastNumber'];
            }else{
                $parastatiko['aa']=$data['aa'];
            }
            $parastatiko['matched']=$data['deltio']['matched'];
            $insertParastatiko=$this->insertParastatiko($parastatiko);
            if ($insertParastatiko['response']){
                $lastId=(int)$this->getLatestId(1);
                if ($data['deltio']['matched']!=='NULL'){
                    foreach ($data['deltio']['matched'] as $deltio){
                        $par['id']=$deltio;
                        $updateParastatiko=$this->updateParastatiko($par);
                        if ($updateParastatiko['response']===false){
                            $return['response']=false;
                            $return['error']=$updateParastatiko['error'];
                            break;
                        }
                    }
                }
                if ($return['response']){
                    foreach ($data['deltio']['payments'] as $payment){
                        $payment['parastatiko']=$lastId;
                        $payments=$this->insertSupplierPayments($payment);
                        if ($payments['response']===false){
                            $return['error']=$payments['error'];
                            break;
                        }
                    }
                    if ($payments['response']){
                        foreach ($data['deltio']['receipt'] as $deltio){
                            $deltio['parastatiko_id']=$lastId;
                            $deltio['supplier']=$data['supplier'];
                            $kinisi=$this->insertKinisi($deltio);
                            if ($kinisi['response']===false){
                                $return['error']=$kinisi['error'];
                                $return['response']=false;
                                break;
                            }
                            if ((int)$data['deltio']['update_price']===1){
                                $item['id']=$deltio['id'];
                                $item['timi_agoras']=$deltio['price'];
                                $updateTimiAgoras=$proion->updateTimiAgoras($item);
                                if ($updateTimiAgoras['response']===false){
                                    $return['response']=false;
                                    $return['error']=$updateTimiAgoras['error'];
                                    break;
                                }
                            }
                            if ((int)$data['deltio']['update_stock']===1){
                                $item['id']=$deltio['id'];
                                $item['stock']=((float)$deltio['temaxia']*-1);
                                $updateStock=$proion->updateStock($item);
                                if ($updateStock['response']===false){
                                    $return['response']=false;
                                    $return['error']=$updateStock['error'];
                                    break;
                                }
                            }
                        }
                    }else{
                        $return['response']=false;
                        $return['error']=$payments['error'];
                    }
                }
            }else{
                $return['response']=false;
                $return['error']=$insertParastatiko['error'];
            }
        }
        return $return;
    }

    public function insertDeltioParalavis($data)
    {
        $proion=new Proion();
        $return['response']=true;
        $deltio=array();
        $invoice['supplier']=$data['supplier'];
        $invoice['par_type']=$data['deltio']['parastatiko_type'];
        $invoice['type']=(int)$data['deltio']['invoice'];
        $receiptData=$data['deltio']['receipt'];
        $creditCards=$data['deltio']['payments'];
        if ($invoice['type']===1){//timologio
            if($this->printer->isActive()){
                $receipt=$this->createReceipt($receiptData);
                $sendprint=$this->printer->printInvoice($receipt,$creditCards,$invoice);
                if ($sendprint['response']){
                    $return['response']=true;
                    if(isset($sendprint['url'])){
                        $document=$return['url'];
                    }
                }else{
                    $return=$sendprint;
                }

            }
        }elseif ($invoice['type']===0){//apodeiksi

        }else{//xwris ektiposi

        }
        if(isset($document)){
            $return['url']=$document;
        }
        if ($return['response']){
            $parastatiko['supplier']=$data['supplier'];
            $parastatiko['type']=$data['deltio']['parastatiko_type'];
            $parastatiko['date']=$data['date'];
            $parastatiko['aa']=$data['aa'];
            $parastatiko['matched']=$data['deltio']['matched'];
            $insertParastatiko=$this->insertParastatiko($parastatiko);
            if ($insertParastatiko['response']){
                $lastId=(int)$this->getLatestId(1);
                if ($data['deltio']['matched']!=='NULL'){
                    foreach ($data['deltio']['matched'] as $deltio){
                        $par['id']=$deltio;
                        $updateParastatiko=$this->updateParastatiko($par);
                        if ($updateParastatiko['response']===false){
                            $return['response']=false;
                            $return['error']=$updateParastatiko['error'];
                            break;
                        }
                    }
                }
                if ($return['response']){
                    foreach ($data['deltio']['payments'] as $payment){
                        $payment['parastatiko']=$lastId;
                        $payments=$this->insertSupplierPayments($payment);
                        if ($payments['response']===false){
                            $return['error']=$payments['error'];
                            break;
                        }
                    }
                    if ($payments['response']){
                        foreach ($data['deltio']['receipt'] as $deltio){
                            $deltio['parastatiko_id']=$lastId;
                            $deltio['supplier']=$data['supplier'];
                            if ((int)$deltio['order_no']!==0){
                                $order['order_id']=$deltio['order_no'];
                                $updateOrder=$this->updateOrder($order);
                                if ($updateOrder['response']){
                                    $updateKinisi=$this->updateKinisi($deltio);
                                    if ($updateKinisi['response']===false){
                                        $return['response']=false;
                                        $return['error']=$updateKinisi['error'];
                                        break;
                                    }
                                }
                            }else{
                                $kinisi=$this->insertKinisi($deltio);
                                if ($kinisi['response']===false){
                                    $return['error']=$kinisi['error'];
                                    $return['response']=false;
                                    break;
                                }
                            }
                            if ((int)$data['deltio']['update_price']===1){
                                $item['id']=$deltio['id'];
                                $item['timi_agoras']=$deltio['price'];
                                $updateTimiAgoras=$proion->updateTimiAgoras($item);
                                if ($updateTimiAgoras['response']===false){
                                    $return['response']=false;
                                    $return['error']=$updateTimiAgoras['error'];
                                    break;
                                }
                            }
                            if ((int)$data['deltio']['update_stock']===1){
                                $item['id']=$deltio['id'];
                                $item['stock']=$deltio['temaxia'];
                                $updateStock=$proion->updateStock($item);
                                if ($updateStock['response']===false){
                                    $return['response']=false;
                                    $return['error']=$updateStock['error'];
                                    break;
                                }
                            }
                        }


                    }else{
                        $return['response']=false;
                        $return['error']=$payments['error'];
                    }
                }
            }else{
                $return['response']=false;
                $return['error']=$insertParastatiko['error'];
            }
        }
        return $return;
    }

    public function insertDeltioParaggelias($data)
    {
        $return['response']=true;
        $invoice['supplier']=$data['supplier'];
        $invoice['par_type']=$data['deltio']['parastatiko_type'];
        $invoice['type']=(int)$data['deltio']['invoice'];
        $receiptData=$data['deltio']['receipt'];
        $creditCards=$data['deltio']['payments'];
        if ($invoice['type']===1){//timologio
            if($this->printer->isActive()){
                $receipt=$this->createReceipt($receiptData);
                $sendprint=$this->printer->printInvoice($receipt,$creditCards,$invoice);
                if ($sendprint['response']){
                    $return['response']=true;
                    if(isset($sendprint['url'])){
                        $document=$return['url'];
                    }
                }else{
                    $return=$sendprint;
                }

            }
        }else{//xwris ektiposi

        }
        if(isset($document)){
            $return['url']=$document;
        }
        if ($return['response']){
            $promitheutis['supplier']=$data['supplier'];
            //todo sent order to supplier via email
            $insertOrder=$this->insertOrder($promitheutis);
            if ($insertOrder['response']){
                $orderId=(int)$this->getLatestId(0);
                foreach ($data['deltio']['receipt'] as $proion){
                    $proion['order_id']=$orderId;
                    $proion['supplier']=$data['supplier'];
                    $kinisi=$this->insertKinisi($proion);
                    if ($kinisi['response']===false){
                        $return['error']=$kinisi['error'];
                        $return['response']=false;
                        break;
                    }
                }
            }else{
                $return['reponse']=false;
                $return['error']=$insertOrder['error'];
            }
        }
        return $return;
    }

    public function insertSupplierPayments($data)
    {
        $return['response']=true;
        $query="INSERT INTO ".$this->payments['table']." VALUES (NULL,".(int)$data['parastatiko'].",".(float)$data['amount'].",".(int)$data['id'].");";
        $result=$this->actions->db_query($query);
        if ($result===false) {
            $return['response'] = false;
            $return['error'] = $result;
        }
        return $return;
    }

    public function payYpoloipo($data)
    {
        $return['response']=true;
        $query="SELECT SUM(sunolo) AS ypoloipo,timologio FROM `timologia-suppliers` WHERE type!=2 AND pelatis=".(int)$data['supplier'].
            "  GROUP BY timologio HAVING SUM(sunolo)>0 ORDER BY imerominia;";
        $result=$this->actions->db_query($query);
        if (mysqli_num_rows($result)>0){
            $ypoloipo=(float)$data['poso'];
            while ($row=mysqli_fetch_assoc($result)){
                $ypoloipo-=(float)$row['ypoloipo'];
                if ($ypoloipo>=0){
                    $query="CALL payoffSupplierParastatiko(".(int)$row['timologio'].",-".(float)$row['ypoloipo'].")";
                    $result2=$this->actions->process($query);
                    if ($result2['response']===false){
                        $return['response']=false;
                        $return['error']=$result2['error'];
                        break;
                    }
                }else{
                    $diafora=-($ypoloipo+(float)$row['ypoloipo']);
                    $query="UPDATE suppliersPayments SET suppliersPayments.amount=suppliersPayments.amount+".(float)$diafora.
                        " WHERE suppliersPayments.method=3 AND suppliersPayments.parastatiko_id=".(int)$row['timologio'].";";
                    $result2=$this->actions->db_query($query);
                    if ($result2['response']===false){
                        $return['response']=false;
                        $return['error']=$result2['error'];
                    }
                    break;
                }
            }
            if ($return['response']){
                $data['type']=4;
                $insertParastatiko=$this->insertParastatiko($data);
                if ($insertParastatiko['response']){
                    $payment['parastatiko']=$this->getLatestId(1);
                    $payment['amount']=-(float)$data['poso'];
                    $payment['type']=1;
                    $insertPayment=$this->insertSupplierPayments($payment);
                    if ($insertPayment['response']===false){
                        $return['response']=false;
                        $return['error']=$insertPayment['error'];
                    }
                }else{
                    $return['response']=false;
                    $return['error']=$insertParastatiko['error'];
                }
            }
        }else{
            $return['response']=false;
            $return['error']="Δεν υπάρχουν παραστατικά για να πληρωθούν";
        }
        return $return;
    }
}