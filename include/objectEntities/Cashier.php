<?php

foreach (glob($_SERVER['DOCUMENT_ROOT'].'/include/cashiers/*.php') as $file){
    require_once $file;
}

class Cashier
{
    private $vendor;//cashiers type
    private $ip;
    private $port;
    private $protocol;
    private $active;

    public function __construct()
    {
        define("MAT", 0);
        define("SAREMA", 1);
        $Sets = new Settings();
        $group=0;
        $temp=$Sets->getSettings($group);
        $settings=$temp['body'][$group];
        $this->vendor=(int)$settings['cashierVendor']['value'];
        $this->ip=$settings['cashierIp']['value'];
        $this->port=$settings['cashierPort']['value'];
        $this->protocol=$settings['cashierProtocol']['value'];
        $this->active=$settings['groupname']['value'];
    }

    public function isActive(){
        if((int)$this->active===1){
            return true;
        }else{
            return false;
        }
    }

    public function toggleCashier(){
        $this->active=abs((int)$this->active-1);
        $settings=new Settings();
        $data['id']=6;
        $data['value']=$this->active;
        $return=$settings->updateSetting($data);
        return $return;
    }

    public function sendInvoice($receipt,$creditCards,$invoice){
        $response=array();
        switch ($this->vendor){
            case MAT:
                $cashier = new MatCashier($this->ip, $this->port, $this->protocol);
                $response = $cashier->sendInvoice($receipt,$creditCards,$invoice);
                break;
            case SAREMA:
                $cashier = new SaremaCashier($this->ip, $this->port, $this->protocol);
                $response = $cashier->sendReceipt($receipt);//for each new copy
                break;
        }
        unset($cashier);
        return $response;
    }

    public function sendReceipt($receipt,$creditCards){
        $response=array();
        switch ($this->vendor){
            case MAT:
                $cashier = new MatCashier($this->ip, $this->port, $this->protocol);
                $response = $cashier->sendReceipt($receipt,$creditCards);
                break;
            case SAREMA:
                $cashier = new SaremaCashier($this->ip, $this->port, $this->protocol);
                $response = $cashier->sendReceipt($receipt);//for each new copy
                break;
        }
        unset($cashier);
        return $response;
    }

    public function XReport(){
        if($this->isActive()){
            $response=array();
            switch ($this->vendor){
                case MAT:
                    $cashier = new MatCashier($this->ip, $this->port, $this->protocol);
                    $response = $cashier->XReport();
                    break;
                case SAREMA:
                    $cashier = new SaremaCashier($this->ip, $this->port, $this->protocol);
                    $response = $cashier->XReport();//for each new copy
                    break;
            }
            unset($cashier);
        }
        else{
            $response['return']=false;
            $response['error']="Δεν υπάρχει συνδεδεμένη ταμειακή μηχανή";
        }
        return $response;
    }

    public function ZReport(){
        if($this->isActive()){
            $response=array();
            switch ($this->vendor){
                case MAT:
                    $cashier = new MatCashier($this->ip, $this->port, $this->protocol);
                    $response = $cashier->ZReport();
                    break;
                case SAREMA:
                    $cashier = new SaremaCashier($this->ip, $this->port, $this->protocol);
                    $response = $cashier->ZReport();//for each new copy
                    break;
            }
            unset($cashier);
        }else{
            $response['response']=false;
            $response['error']="Δεν υπάρχει συνδεδεμένη ταμειακή μηχανή";
        }
        if($response['response']){
            $date=date("Y-m-d H:i:s");
            $logs=new Logs();
            $data['type']=2;
            $data['date']=$date;
            $return=$logs->insertLog($data);
            if(!$return['response']){
                $response=$return;
            }
        }
        return $response;
    }

    public function freePrint($data){
        if($this->isActive()){
            $response=array();
            switch ($this->vendor){
                case MAT:
                    $cashier=new MatCashier($this->ip,$this->port,$this->protocol);
                    $response=$cashier->freePrint($data);
                    break;
            }
            unset($cashier);
        }else{
            $response['response']=false;
            $response['error']="Δεν υπάρχει συνδεδεμένη ταμειακή μηχανή";
        }
        return $response;
    }

    public function clearCashier(){
        exec('phpRestart');
    }
}