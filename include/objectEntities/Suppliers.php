<?php
/**
 * Created by PhpStorm.
 * User: Tsoxt
 * Date: 24/1/2019
 * Time: 3:41 PM
 */
//todo class extend with customers
date_default_timezone_set('Europe/Athens');

class Suppliers
{
    private $model;
    private $actions;
    private $products;
    private $parastatika;

    public function __construct()
    {
        $model=new dbmodel;
        $this->actions=new dbactions();
        $this->model=$model->_suppliers;
        $this->products=$model->_suppliersProducts;
        $this->parastatika=new SuppliersParastatika();

    }

    public function searchSupplier($data)
    {
        if((int)$data['type']===2){//anazitisi fulltext
            $query="SELECT `id`, `afm`, `name`, `address`, `phone`, `mobile` FROM ".$this->model['table']." WHERE ((MATCH(`afm`) AGAINST ('".$data['search'].
                "*' IN BOOLEAN MODE)) OR (MATCH(`name`) AGAINST ('".$data['search'].
                "*' IN BOOLEAN MODE)) OR (MATCH(`phone`) AGAINST ('".$data['search'].
                "*' IN BOOLEAN MODE)) OR (MATCH(`mobile`) AGAINST ('".$data['search'].
                "*' IN BOOLEAN MODE))) ORDER BY `name` ASC;";
        }else{//anazitisi me id
            $query="SELECT * FROM ".$this->model['table']." WHERE ".$this->model['id']." = ".(int)$data['search'].";";
        }
        $result=$this->actions->db_query($query);
        if (mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['suppliers']=array();
            $return['gsis']=false;
            $i=0;
            while ($row=mysqli_fetch_assoc($result)){
                $return['suppliers'][$i]['body']=$row;
                $supplier['supplier']=$row['id'];
                $supplier['fromdate']=$supplier['todate']=date("Y-m-d",time() +86400);
                $getYpoloipo=$this->parastatika->getSupplierParastatika($supplier);
                if ($getYpoloipo['response']){
                    $return['suppliers'][$i]['ypoloipo']=$getYpoloipo['parastatika'][0]['pistosi'];
                }else{
                    $return['suppliers'][$i]['ypoloipo']=0;
                }
                $i++;
            }
        }else{
            $return['response']=false;
            $return['error']='Δεν βρέθηκαν αποτελέσματα';
        }
        return $return;
    }

    public function fetchSupplierData($data){
        $actions=new dbactions;
        $query="select * from `suppliers` where `id`=".(int)$data['id'];
        $result=$actions->db_query($query);
        if($result!==false){
            if(mysqli_num_rows($result)>0){
                $return['response']=true;
                $return['body']=mysqli_fetch_assoc($result);
            }else{
                $return['response']=false;
                $return['error']="Δεν υπάρχει ο Προμηθευτής";
            }
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function insertSupplier($data)
    {
        if (strlen($data['afm'])===0){
            $data['afm']='NULL';
        }else{
            //todo search afm from gsis
            $data['afm']=$this->actions->quote($data['afm']);
        }
        if (strlen($data['phone'])===0){
            $data['phone']='NULL';
        }else{
            $data['phone']=$this->actions->quote($data['phone']);
        }
        if (strlen($data['mobile'])===0){
            $data['mobile']='NULL';
        }else{
            $data['mobile']=$this->actions->quote($data['mobile']);
        }
        if (strlen($data['job'])===0){
            $data['job']='NULL';
        }else{
            $data['job']=$this->actions->quote($data['job']);
        }
        if (strlen($data['doy'])===0){
            $data['doy']='NULL';
        }else{
            $data['doy']=$this->actions->quote($data['doy']);
        }
        if (strlen($data['email'])===0){
            $data['email']='NULL';
        }else{
            $data['email']=$this->actions->quote($data['email']);
        }
        $query="INSERT INTO ".$this->model['table']." VALUES (NULL, "
            .$data['afm'].", ".$this->actions->quote($data['name']).", ".$this->actions->quote($data['address']).", ".$this->actions->quote($data['city']).", "
            .$this->actions->quote($data['tk']).", ".$data['phone'].", ".$data['mobile'].", ".$data['job'].", ".$data['doy'].", ".$data['email'].
            ",NOW());";
        $result=$this->actions->db_query($query);
        if ($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function getSupplierYpoloipo($data)
    {
        if (!isset($data['date'])){
            $data['date']=date('Y-m-d');
        }
        $data['date']=$this->actions->quote($data['date']);
        $query="SELECT MAX(suppliersParastatika.date) as lastdate, suppliersParastatika.pistosi FROM suppliersParastatika".
            "  WHERE suppliersParastatika.supplier_id=".(int)$data['supplier'].
            " AND suppliersParastatika.type=2 AND DATE(suppliersParastatika.date)<=".$data['date'].";";
        $result=$this->actions->db_query($query);
        if (mysqli_num_rows($result)>0){
            $lastmetafora=mysqli_fetch_assoc($result);
            $lastdate=date("Y-m-d",strtotime($lastmetafora['lastdate']));
            $amount=$lastmetafora['amount'];
            if (!isset($lastdate)){
                $lastdate=date('Y-m-d');;
                $lastdate=$this->actions->quote($lastdate);
                $amount=0;
            }
            $query="SELECT SUM(suppliersParastatika.pistosi) as sum FROM suppliersParastatika".
                " WHERE suppliersParastatika.supplier_id=".(int)$data['supplier']." AND suppliersParastatika.type!=2".
                " AND DATE(suppliersParastatika.date) BETWEEN ".$lastdate." AND ".$data['date'].";";
            $result=$this->actions->db_query($query);
            if (mysqli_num_rows($result)>0){
                $return['response']=true;
                $response=mysqli_fetch_assoc($result);
                $sum_pistoseis=$response['sum'];
                if (!isset($sum_pistoseis)){
                    $sum_pistoseis=0;
                }
                $ypoloipo=(float)$amount+(float)$sum_pistoseis;
                $return['body']=$ypoloipo;
            }else{
                $return['response']=false;
                $return['error']=$result;
            }
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function updateSupplier($data)
    {
        $data['value']=$this->actions->quote($data['value']);
        if ($data['name']==="name"){
            $field=$this->model['name'];
        }elseif ($data['name']==="email"){
            $field=$this->model['email'];
        }elseif ($data['name']==="address"){
            $field=$this->model['address'];
        }elseif ($data['name']==="city"){
            $field=$this->model['city'];
        }elseif ($data['name']==="tk"){
            $field=$this->model['postcode'];
        }elseif ($data['name']==="phone"){
            $field=$this->model['phone'];
        }elseif ($data['name']==="mobile"){
            $field=$this->model['mobile'];
        }elseif ($data['name']==="job"){
            $field=$this->model['job'];
        }elseif ($data['name']==="afm"){
            $field=$this->model['afm'];
        }
        elseif ($data['name']==="doy"){
            $field=$this->model['doy'];
        }
        $query="UPDATE ".$this->model['table']." SET ".$field." = ".$data['value']." WHERE ".$this->model['id']." = ".(int)$data['id'].";";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=$result;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function getSupplierProducts($data)
    {
        $query='SELECT suppliersProducts.id_proion,suppliersProducts.timi_agoras,proion.perigrafi,proion.temaxia,proion.stock,proion.stock_recommend,proion.stock_alert,'.
            'proion.tmima,proion.barcode,proion.mothercode,proion.id_omadas,proion.zugistiko FROM `suppliersProducts` 
                INNER JOIN proion ON suppliersProducts.id_proion=proion.id WHERE id_supplier= '.(int)$data['supplier'].';';
        $result=$this->actions->db_query($query);
        if (mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['body']=array();
            while($row=mysqli_fetch_assoc($result)){
                array_push($return['body'],$row);
            }
        }else{
            $return['response']=false;
            $return['error']="Δεν έχετε προσθέσει προϊόντα στον συγκεκριμένο προμηθευτή";
        }
        return $return;
    }

    public function addSupplierProduct($data)
    {
        $query="INSERT INTO ".$this->products['table']." VALUES (".(int)$data['productId'].", ".(int)$data['supplier'].
            ", ".(float)$data['productBuyPrice'].", NOW());";
        $result=$this->actions->db_query($query);
        if($result===true) {
            $return['response']=$result;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function updateSupplierProduct($data)
    {
        $query="UPDATE ".$this->products['table']." SET ".$this->products['price']." = ".(float)$data['timi_agoras']
            .",".$this->products['date']."=NOW() WHERE ".$this->products['proion']." = ".(int)$data['proion']
            ." AND ".$this->products['supplier']." = ".(int)$data['supplier'].";";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=$result;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function deleteSupplierProduct($data)
    {
        $query="DELETE FROM ".$this->products['table']." WHERE ".$this->products['proion']." = ".(int)$data['proion']
            ." AND ".$this->products['supplier']." = ".(int)$data['supplier'].";";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=$result;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }
}