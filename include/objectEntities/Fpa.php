<?php

class Fpa
{
    private $model;
    private $actions;

    public function __construct(){
        $model=new dbmodel;
        $this->model=$model->_fpa;
        $this->actions=new dbactions();
    }

    public function findTmima($data){
        $query="SELECT * FROM ".$this->model['table']." WHERE ".$this->model['id']."=".(int)$data['id'].";";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['proion']=mysqli_fetch_assoc($result);
        }else{
            $return['response']=false;
            $return['error']="Το τμήμα δεν υπάρχει";
        }
        return $return;
    }

    public function updateTmima($data){
        $query="UPDATE ".$this->model['table']." SET ".$this->model['name']." = ".$this->actions->quote($data['tmima']).", ".$this->model['price']." = ".$data['price'].
            " WHERE ".$this->model['id']." = ".(int)$data['id'].";";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=$result;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        $return['query']=$query;
        return $return;
    }

    public function updateTmimaActive($data){
        $query="UPDATE ".$this->model['table']." SET ".$this->model['active']." = ".(int)$data['active'].
            " WHERE ".$this->model['id']." = ".(int)$data['id'].";";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=$result;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        $return['query']=$query;
        return $return;

    }

    public function insertTmima($data){
        if(strlen($data['id'])===0){
            $data['id']='NULL';
        }else{
            $data['id']=(int)$data['id'];
        }
        $query="INSERT INTO ".$this->model['table']." VALUES ".
         "(".$data['id'].", ".$this->actions->quote($data['tmima']).", ".(float)$data['price'].", 1, 1);";
        $result=$this->actions->db_query($query);
        if($result===true) {
            $return['response']=$result;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        $return['query']=$query;
        return $return;
    }

    public function getTmimata(){
        $query="SELECT * FROM ".$this->model['table'].";";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['tmimata']=array();
            while ($row=mysqli_fetch_assoc($result)){
                array_push($return['tmimata'], $row);
            }

        }else{
            $return['response']=false;
            $return['error']="Υπήρξε κάποιο πρόβλημα προσπαθήστε ξανά";
        }
        return $return;
    }
}