<?php
/**
 * Created by PhpStorm.
 * User: Tsoxt
 * Date: 9/12/2018
 * Time: 1:01 PM
 */

class Payments
{
    private $model, $methods,$actions;

    public function __construct()
    {
        $db=new dbmodel;
        $this->model=$db->_payments;
        $this->methods=$db->_payment_methods;
        $this->actions=new dbactions();
    }

    public function getPaymentMethods(){
        $query="SELECT * FROM ".$this->methods['table']." WHERE ".$this->methods['active']." = 1 AND ".$this->methods['id'].">1;";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['payment_methods']=array();
            while($row=mysqli_fetch_assoc($result)){
                array_push($return['payment_methods'],$row);
            }
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }
}