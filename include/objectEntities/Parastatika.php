<?php
/**
 * Created by PhpStorm.
 * User: Tsoxt
 * Date: 11/12/2018
 * Time: 10:14 AM
 */

class Parastatika
{
    private $model;
    private $actions;
    private $customer;

    public function __construct()
    {
        $model=new dbmodel;
        $this->actions=new dbactions();
        $this->model=$model->_parastatika;
        $this->customer=new Customers();

    }

    public function getAllParastatikaInfo()
    {
        $query="SELECT * FROM `parastatikaInfo`;";
        $result=$this->actions->db_query($query);
        if (mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['body']=array();
            while ($row=mysqli_fetch_assoc($result)){
                $return['body'][$row['id']]=$row;
            }
        }else{
            $return['response'] = false;
            $return['error'] = $result;
        }
        return $return;
    }

    public function insertParastatiko($data)
    {
        if ((int)$data['type']===0){//ean einai apli apodeiksi
            $data['type']=3;
        }
        $query="INSERT INTO ".$this->model['table']." VALUES (NULL,".(int)$data['customer'].", ".(int)$data['receipt'].", ".(int)$data['type'].", ".(int)$data['invoiceNumber'].",".(int)$data['matched'].");";
        $result=$this->actions->db_query($query);
        if ($result){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function parastatikaInfo($id)
    {
        $query = "SELECT * FROM parastatika WHERE id=" . (int)$id . ";";
        $result = $this->actions->db_query($query);
        if (mysqli_num_rows($result) > 0) {
            $parastatiko = mysqli_fetch_assoc($result);
            $query = "SELECT * FROM parastatikaInfo WHERE id=" . (int)$parastatiko['type'];
            $result = $this->actions->db_query($query);
            if (mysqli_num_rows($result) < 1) {
                $return['response'] = false;
                $return['error'] = "Δεν βρέθηκε ο τύπος παραστατικόυ";
                return $return;
            }
            $invoiceInfo = mysqli_fetch_assoc($result);
            $invoice = array();
            $invoice['invoData'] = array("id" => (int)$id,"name" => $invoiceInfo['name'],"shortname" => $invoiceInfo['shortname'], "lastNumber" => $parastatiko['aa']);
            $query = "SELECT * FROM receipts WHERE id=" . (int)$parastatiko['receipt_id'];
            $result = $this->actions->db_query($query);
            if (mysqli_num_rows($result) < 1) {
                $return['response'] = false;
                $return['error'] = "Δεν βρέθηκαν στοιχεία απόδειξης";
                return $return;
            }
            $receiptInfo = mysqli_fetch_assoc($result);
            $invoice['date'] = date('d/m/Y H:i:s', strtotime($receiptInfo['date']));
            $invoice['customer'] = $parastatiko['customer_id'];
            $query = "SELECT * FROM kiniseis WHERE receipt=" . (int)$receiptInfo['id'];
            $result = $this->actions->db_query($query);
            $receipt = array();
            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $item = new Proion();
                    $proion=$item->findId(array("id" => $row['id']));
                    $receiptRow['description'] = $proion['proion']['perigrafi'];
                    $receiptRow['id_om'] = $proion['proion']['id_omadas'];
                    $receiptRow['zigos'] = $proion['proion']['zugistiko'];
                    $receiptRow['stock'] = $proion['proion']['stock'];
                    unset($item);
                    $receiptRow['temaxia'] = abs($row['quantity']);
                    $receiptRow['tmima'] = $row['tmima'];
                    $receiptRow['fpa'] = $row['fpa'];
                    $receiptRow['timi'] = $row['timi'];
                    $receiptRow['id'] = $row['id'];
                    $receiptRow['barcode'] = $row['barcode'];
                    $receiptRow['mothercode'] = $row['mothercode'];
                    $receiptRow['quantity'] = $row['quantity'];
                    array_push($receipt, $receiptRow);
                }
            }else{
                if ($result===false){
                    $return['response'] = false;
                    $return['error'] = "Δεν βρέθηκαν οι κινήσεις της απόδειξης";
                    return $return;
                }
            }
            $query = "SELECT * FROM payments WHERE receipt=" . (int)$receiptInfo['id'];
            $result = $this->actions->db_query($query);
            if (mysqli_num_rows($result) < 1) {
                $return['response'] = false;
                $return['error'] = "Δεν βρέθηκαν οι πληρωμές της απόδειξης";
                return $return;
            }
            $payments = array();
            while ($row = mysqli_fetch_assoc($result)) {
                array_push($payments, array("id" => $row['card'], "amount" => $row['amount']));
            }
            $return['response']=true;
            $return['parastatiko']=array("receipt"=>$receipt,"payments"=>$payments,"info"=>$invoice);
        }else{
            $return['response']=false;
            $return['error']="δεν υπάρχει παραστατικό";
        }
        return $return;
    }

    public function reprintParastatiko($data){
        $parastatiko=$this->parastatikaInfo($data['id']);
        if($parastatiko['response']){
            $printer=new Printer();
            $receipt=$parastatiko['parastatiko']['receipt'];
            $payments=$parastatiko['parastatiko']['payments'];
            $invoice=$parastatiko['parastatiko']['info'];
            return $printer->printInvoice($receipt,$payments,$invoice);
        }else{
            return $parastatiko;
        }
    }

    public function getParastatika($data)
    {
        $data['fromdate']=date("Y-m-d 00:00:00",strtotime($data['fromdate']));
        $data['todate']=date("Y-m-d 23:59:59",strtotime($data['todate']));
        $metafora['customer']=$data['customer'];
        $date=date_create($data['fromdate']);
        $date=date_sub($date,date_interval_create_from_date_string("1 days"));
        $metafora['date']=date_format($date,"Y-m-d");
        $ypoloipo=$this->customer->getYpoloipo($metafora);
        if ($ypoloipo['response']){
            $return['apometafora']=$ypoloipo['body'];
            $query="SELECT parastatika.id FROM (parastatika INNER JOIN receipts on parastatika.receipt_id = receipts.id)".
                " WHERE (date BETWEEN ".$this->actions->quote($data['fromdate']).
                " AND ".$this->actions->quote($data['todate']).") AND (customer_id=".(int)$data['customer'].") AND (parastatika.type!=2);";
            $result=$this->actions->db_query($query);
            if (mysqli_num_rows($result)>0){
                $parastatika=array();
                while ($row=mysqli_fetch_assoc($result)){
                    $parastatiko=$this->parastatikaInfo((int)$row['id']);
                    if(!$parastatiko['response']){
                        return $parastatiko;
                    }
                    array_push($parastatika,$parastatiko['parastatiko']);
                }
                $return['response']=true;
                $return['parastatika']=$parastatika;
            }else{
                $return['response']=false;
                $return['error']='Δεν βρέθηκαν παραστατικά για τις συγκεκριμένες ημερομηνίες';
            }
        }else{
            $return['response']=false;
            $return['error']=$ypoloipo;
        }
        return $return;
    }

    public function matchCustomerParastatika($data)
    {
        if ((int)$data['update_flag']===0){//deltio apostolis
            $updateflag=1;
        }else{
            $updateflag=0;
        }
        if ((int)$data['flow_flag']===100){//ean einai apodeiksi eispraksis
            $condition="parastatikaInfo.flow_flag=1 AND (parastatikaInfo.update_flag=1 OR parastatikaInfo.update_flag=2)";
        }else{
            $condition="parastatikaInfo.flow_flag=".(int)$data['flow_flag']." AND parastatikaInfo.update_flag=".$updateflag." AND parastatika.matched=0";
        }
        if ((int)$data['customer']!==0){
            $query="SELECT parastatika.id,parastatika.type,parastatika.customer_id,parastatika.aa,receipts.date,".
                "customers.name,parastatikaInfo.shortname FROM parastatika INNER JOIN customers".
                " ON parastatika.customer_id=customers.id INNER JOIN parastatikaInfo ON parastatika.type=parastatikaInfo.id".
                " INNER JOIN receipts ON parastatika.receipt_id=receipts.id".
                " WHERE parastatika.customer_id = ".(int)$data['customer'].
                " AND ".$condition.
                " ORDER BY receipts.date DESC;";
        }else{
            $query="SELECT parastatika.id,parastatika.type,parastatika.customer_id,parastatika.aa,receipts.date,".
                "customers.name,parastatikaInfo.shortname FROM parastatika INNER JOIN customers".
                " ON parastatika.customer_id=customers.id INNER JOIN parastatikaInfo ON parastatika.type=parastatikaInfo.id".
                " INNER JOIN receipts ON parastatika.receipt_id=receipts.id".
                " WHERE ".$condition.
                " ORDER BY receipts.date DESC;";
        }
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0) {
            $return['response'] = true;
            $return['body'] = array();
            while ($row = mysqli_fetch_assoc($result)) {
                array_push($return['body'], $row);
            }
        }else{
            $return['response'] = false;
            $return['error'] = $result;
        }
        return $return;
    }

    public function updateParastatiko($data)
    {
        $query="UPDATE ".$this->model['table']." SET ".$this->model['matched']." = 1 WHERE ".$this->model['id']." = ".(int)$data['id'].";";
        $result=$this->actions->db_query($query);
        if ($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['query']=$query;
            $return['error']=$result;
        }
        return $return;
    }
}