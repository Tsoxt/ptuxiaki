<?php
/**
 * Created by PhpStorm.
 * User: Tsoxt
 * Date: 12/12/2018
 * Time: 10:30 AM
 */

class CustomersDiscounts
{
    private $actions;
    private $discounts;

    public function __construct()
    {
        $model=new dbmodel;
        $this->actions=new dbactions();
        $this->discounts=$model->_customers_proion;

    }

    public function addDiscount($data)
    {
        $query="INSERT INTO ".$this->discounts['table']." VALUES (".(int)$data['productId'].", ".(int)$data['customer'].
            ", ".(float)$data['productDiscPrice'].", ".(int)$data['productDiscType'].");";
        $result=$this->actions->db_query($query);
        if($result===true) {
            $return['response']=$result;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function updateDiscount($data)
    {
        if ((int)$data['type']===0){
            $data['discount']=(float)$data['discount']/100;
        }
        $query="UPDATE ".$this->discounts['table']." SET ".$this->discounts['discount']." = ".(float)$data['discount']
            .", ".$this->discounts['type']." = ".(int)$data['type']." WHERE ".$this->discounts['proion']." = ".(int)$data['proion']
            ." AND ".$this->discounts['customer']." = ".(int)$data['customer'].";";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=$result;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function deleteDiscount($data)
    {
        $query="DELETE FROM ".$this->discounts['table']." WHERE ".$this->discounts['proion']." = ".(int)$data['proion']
            ." AND ".$this->discounts['customer']." = ".(int)$data['customer'].";";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=$result;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }


}