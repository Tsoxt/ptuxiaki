<?php
//Todo on update price make a transaction
class Proion
{
    private $model;
    private $api;
    private $special;
    private $actions;

    public function __construct()
    {
        $model=new dbmodel;
        $this->model=$model->_proion;
        $this->special=new SpecialBarcodes();
        $this->api=new BarcodeApi();
        $this->actions=new dbactions();
    }

//<editor-fold desc="Utilities">

    private function getLatestId(){
        $query="select max(".$this->model['id'].") from ".$this->model['table']." ;";
        return (int)mysqli_fetch_assoc($this->actions->db_query($query))["max(".$this->model['id'].")"]+1;
    }

    public function getSumStock($data){
        $query="select SUM(".$this->model['stock']." * ".$this->model['units'].") as `sumstock` from ".$this->model['table'].
            " where ".$this->model['mcode']."=".(int)$data['mcode']." ;";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['proion']=mysqli_fetch_assoc($result);
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    private function isZugistiko($barcode){
        if($this->special->isSpecial($barcode)){
            return 1;
        }else{
            return 0;
        }
    }

    private function returnProion($items){
        $items=json_decode($items,true);
        if(!empty($items)){
            $item=$items[0];
            $return['response']=true;
            $proion['id']=$this->getLatestId();
            $proion['barcode']=$item['barcode'];
            $proion['mothercode']=$this->getLatestId();
            $proion['perigrafi']=$item['description'];
            $proion['perigrafi2']=$item['description'];
            $proion['id_omadas']=$item['id_omadas'];
            $proion['timi']=0;
            $proion['flag_timi']=0;
            $proion['tmima']=$item['tmima'];
            $proion['stock']=0;
            $proion['temaxia']=1;
            $proion['active']=1;
            $proion['new']=1;
            $proion['seira']='';
            $proion['epistrofi']='';
            $proion['zugistiko']=$this->isZugistiko($item['barcode']);
            $proion['api']=1;
            $proion['zugaria']=0;
            $proion['temaxiako']=0;
            $proion['alt_id']='';
            $return['proion']=$proion;
            $omades=new Omada();
            $omada['id']=$item['id_omadas'];
            $response=$omades->getOmada($omada);
            if($response['response']){
                $return['proion']['id_category']=$response['omada']['id_category'];
                $return['proion']['om_desc']=$response['omada']['name'];
                $category['id']=$return['proion']['id_category'];
                $categories=new Category();
                $response=$categories->getCategory($category);
                if($response['response']){
                    $return['proion']['cat_desc']=$response['category']['name'];
                }else{
                    $return=$response;
                }
            }else{
                $return=$response;
            }
        }else{
            $return['response']=false;
            $return['error']="Το προιόν δεν υπάρχει";
        }
        return $return;
    }

//</editor-fold>


//<editor-fold desc="Seek Items">
    public function findAltID($data, $type){
        if ($type===1){//anazitisi entypou
            $query="SELECT * FROM ".$this->model['table']." WHERE ".$this->model['alt_id']."=".(int)$data['alt_id']." AND ".$this->model['id']."=".(int)$data['alt_id'].";";

        }else{//anazitisi ekdosis
            $query="SELECT * FROM ".$this->model['table']." WHERE ".$this->model['alt_id']."=".(int)$data['alt_id']." AND ".$this->model['id']."!=".(int)$data['alt_id'].";";
        }

        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['proion']=mysqli_fetch_assoc($result);
            $omades=new Omada();
            $omada['id']=$return['proion']['id_omadas'];
            $response=$omades->getOmada($omada);
            if($response['response']){
                $return['proion']['id_category']=$response['omada']['id_category'];
                $return['proion']['om_desc']=$response['omada']['name'];
                $category['id']=$return['proion']['id_category'];
                $categories=new Category();
                $response=$categories->getCategory($category);
                if($response['response']){
                    $return['proion']['cat_desc']=$response['category']['name'];
                }else{
                    $return=$response;
                }
            }else{
                $return=$response;
            }
        }else{
            $return['response']=false;
            $return['error']="Το προιόν δεν υπάρχει";
        }
        return $return;
    }

    //</editor-fold>

    public function findSimilarProducts($data)
    {
        $query="SELECT  `id`, `barcode`, `perigrafi`, `id_omadas`, `stock`, `temaxia`, `active` FROM `proion`  WHERE "
            .$this->model['mcode']."=".(int)$data['id']." OR ".$this->model['id']."=".(int)$data['id'].";";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['proionta']=array();
            while($row=mysqli_fetch_assoc($result)){
                array_push($return['proionta'],$row);
            }
        }else{
            $return['response']=false;
            $return['error']="Υπήρξε κάποιο σφάλμα στην αναζήτηση. Παρακαλώ προσπαθήστε ξανά";
        }
        return $return;
    }

    public function findId($data){
        $query="SELECT * FROM ".$this->model['table']." WHERE ".$this->model['id']."=".(int)$data['id'].";";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['proion']=mysqli_fetch_assoc($result);
            $omades=new Omada();
            $omada['id']=$return['proion']['id_omadas'];
            $response=$omades->getOmada($omada);
            if($response['response']){
                $return['proion']['id_category']=$response['omada']['id_category'];
                $return['proion']['om_desc']=$response['omada']['name'];
                $category['id']=$return['proion']['id_category'];
                $categories=new Category();
                $response=$categories->getCategory($category);
                if($response['response']){
                    $return['proion']['cat_desc']=$response['category']['name'];
                }else{
                    $return=$response;
                }
            }else{
                $return=$response;
            }
        }else{
            $return['response']=false;
            $return['error']="Το προιόν δεν υπάρχει";
        }
        return $return;
    }

    public function findBarcode($data){
        $query="SELECT * FROM ".$this->model['table']." WHERE ".$this->model['code']." LIKE ".$this->actions->quote($data['id'].'%')." AND ".$this->model['active']."=1;";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $return['response']=true;
            if (mysqli_num_rows($result)>1){
                $return['same_bcode']=array();
                while($row=mysqli_fetch_assoc($result)){
                    array_push($return['same_bcode'], $row);
                }
            }else{
                $return['proion']=mysqli_fetch_assoc($result);
                $omades=new Omada();
                $omada['id']=$return['proion']['id_omadas'];
                $response=$omades->getOmada($omada);
                if($response['response']){
                    $return['proion']['id_category']=$response['omada']['id_category'];
                    $return['proion']['om_desc']=$response['omada']['name'];
                    $category['id']=$return['proion']['id_category'];
                    $categories=new Category();
                    $response=$categories->getCategory($category);
                    if($response['response']){
                        $return['proion']['cat_desc']=$response['category']['name'];
                    }else{
                        $return=$response;
                    }
                }else{
                    $return=$response;
                }
            }

        }else{
            if($this->api->isActive() && $this->special->ispreTyped($data['id'])){
                $fromApi=$this->api->itemFromApi($data['id']);
                if($fromApi['httpCode']===200){
                    $return=$this->returnProion($fromApi['body']);
                }else{
                    $return['response']=false;
                    $return['error']=$fromApi['error'];
                }
            }else{
                $return['response']=false;
                $return['error']="Το προιόν δεν υπάρχει";
            }
        }
        return $return;
    }

    public function itemsByDate($data){
        $this->actions= new dbactions;
        $query="select * from ".$this->model['table']." where ".$this->model['day']."=".$this->actions->quote($data['date']);
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['items']=array();
            while($row=mysqli_fetch_assoc($result)){
                array_push($return['items'], $row);
            }
        }else{
            $return['response']=false;
            $return['error']="Δεν υπάρχουν επιστροφές για την ημερομηνία που επιλέξατε";
        }
        return $return;
    }

    public function itemsBetweenDates($data){
        $this->actions= new dbactions;
        $query="select * from ".$this->model['table']." where ".$this->model['day']." between ".$this->actions->quote($data['fromdate'])." and ".$this->actions->quote($data['todate']).";";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['items']=array();
            while($row=mysqli_fetch_assoc($result)){
                array_push($return['items'], $row);
            }
        }else{
            $return['response']=false;
            $return['error']="Δεν υπάρχουν επιστροφές για το διάστημα που επιλέξατε";
        }
        return $return;
    }

    public function callItems($data){
        if($data['omada']===NULL){
            $query="SELECT ".$this->model['id'].", ".$this->model['perig']." FROM ".$this->model['table']." WHERE ".$this->model['active']." = 1 AND ".$this->model['code']." IS NULL ORDER BY ".$this->model['perig']." ASC;";
        }else{
            $query="select ".$this->model['id']." ,".$this->model['perig']." from ".$this->model['table']." where ".$this->model['id_omad']." = ".(int)$data['omada']." AND (".$this->model['alt_id']." IS NULL OR (".
                $this->model['alt_id']." IS NOT NULL AND ".$this->model['alt_id']." != ".$this->model['mcode']." AND ".$this->model['code']." IS NULL)) ORDER BY ".$this->model['perig'].";";
        }
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['items']=array();
            while($row=mysqli_fetch_assoc($result)){
                array_push($return['items'], $row);
            }
        }else{
            $return['response']=false;
            $return['error']="Δεν βρέθηκαν προϊόντα";
        }
        return $return;
    }

    public function itemsToScale(){
        $query="select ".$this->model['table'].".*, `omada_zugaria` from (".$this->model['table']." left join `omada` on "
            .$this->model['table'].".".$this->model['id_omad']."=`omada`.`id`) where ".$this->model['zugaria']."= 1";
        $result=$this->actions->db_query($query);
        if(!($result===false)){
            if(mysqli_num_rows($result)>0){
                $return['response']=true;
                $return['items']=array();
                while($row=mysqli_fetch_assoc($result)){
                    array_push($return['items'], $row);
                }
            }else{
                $return['response']=false;
                $return['error']='Δεν υπάρχουν αποτελέσματα';
            }
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

//</editor-fold>


//<editor-fold desc="Update Items">
    public function updateStockRecommend($data){
        $query="update ".$this->model['table']." set ".$this->model['recommend']." = ".(float)$data['recommend']
            .", ".$this->model['updated']."=NOW() where ".$this->model['id']." = ".(int)$data['id']." ;";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function updateStockAlert($data){
        $query="update ".$this->model['table']." set ".$this->model['alert']." = ".(float)$data['alert']
            .", ".$this->model['updated']."=NOW() where ".$this->model['id']." = ".(int)$data['id']." ;";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function updateTimiAgoras($data){
        if (isset($data['flag_agoras']) && (int)$data['flag_agoras']===1){
            $query="update ".$this->model['table']." set ".$this->model['agora']." = ".(double)$data['timi_agoras']
                .", ".$this->model['updated']."=NOW() where ".$this->model['mcode']." = ".(int)$data['mothercode']." ;";
        }else{
            $query="update ".$this->model['table']." set ".$this->model['agora']." = ".(double)$data['timi_agoras']
                .", ".$this->model['updated']."=NOW() where ".$this->model['id']." = ".(int)$data['id']." ;";
        }
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function updateZugaria($data){
        $query="update ".$this->model['table']." set ".$this->model['zugaria']." = ".(int)$data['zugaria']
            .", ".$this->model['updated']."=NOW() where ".$this->model['id']." = ".(int)$data['id']." ;";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function updateEpistrofi($data){
        $query="update ".$this->model['table']." set ".$this->model['day']." = ".$this->actions->quote($data['epistrofi']).
            ", ".$this->model['updated']."=NOW() where ".$this->model['id']." = ".(int)$data['id']." ;";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function updateActive($data){
        $query="update ".$this->model['table']." set ".$this->model['active']." = ".(int)$data['active']
            .", ".$this->model['updated']."=NOW() where ".$this->model['id']." = ".(int)$data['id']." ;";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function setDescription($data){
        $query="update ".$this->model['table']." set ".$this->model['perig']." = ".$this->actions->quote($data['description'])
            .", ".$this->model['updated']." =NOW() where ".$this->model['id']." = ".(int)$data['id']." ;";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function setAltDescription($data){
        $query="update ".$this->model['table']." set ".$this->model['perig2']." = ".$this->actions->quote($data['description2'])
            .", ".$this->model['updated']." =NOW() where ".$this->model['id']." = ".(int)$data['id']." ;";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function setOmada($data){
        $query="update ".$this->model['table']." set ".$this->model['id_omad']." = ".(int)$data['omada']
            .", ".$this->model['updated']." =NOW() where ".$this->model['id']." = ".(int)$data['id']." ;";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        $return['query']=$query;
        return $return;
    }

    public function setBcode($data){
        if(strlen($data['barcode'])===0){
            $data['barcode']='NULL';
        }else{
            $data['barcode']=$this->actions->quote($data['barcode']);
        }
        $query="update ".$this->model['table']." set ".$this->model['code']." = ".$data['barcode'] .
            ", ".$this->model['zugos']." = ".(int)$data['zugistiko'].", ".$this->model['zugaria']." = ".(int)$data['zugaria'].
            ", ".$this->model['temaxiako']." = ".(int)$data['temaxiako'].", ".$this->model['updated']." =NOW() where ".$this->model['id']." = ".(int)$data['id']." ;";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function setPrice($data){
        if((int)$data['ftimi']===0){
            $query="update ".$this->model['table']." set ".$this->model['flag']." = 0, ".$this->model['timi']." = 0, ".$this->model['updated']." =NOW()  where "
                .$this->model['id']." = ".(int)$data['id']." ;";
        }else{
            $query="update ".$this->model['table']." set ".$this->model['flag']." = 1, ".$this->model['timi']." = " .(double)$data['price']
                .", ".$this->model['updated']."=NOW() where ".$this->model['id']." = ".(int)$data['id']." ;";
        }
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function setTmima($data){
        $query="update ".$this->model['table']." set ".$this->model['tmima']." = ".(int)$data['tmima']
            .",".$this->model['updated']."=NOW() where ".$this->model['id']." = ".(int)$data['id']." ;";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function updateStock($data){
        $query="update ".$this->model['table']." set ".$this->model['stock']." = ".$this->model['stock']." + ".number_format($data['stock'],3,'.','')
            .",".$this->model['updated']."=NOW() where ".$this->model['id']." = ".(int)$data['id']." ;";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function setPackage($data){
        $query="update ".$this->model['table']." set ".$this->model['units']." = ".(int)$data['package']
            ." where ".$this->model['id']." = ".(int)$data['id']." ;";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function setMothercode($data){
        $query="update ".$this->model['table']." set ".$this->model['mcode']." = ".(int)$data['mcode']
            ." where ".$this->model['id']." = ".(int)$data['id']." ;";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=true;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function insertItem($data){
        $category=new Category();
        $categories=$category->getCategories();
        if(!in_array($data['id_cat'],array_column($categories['categories'], 'id'))) {
            $toAsk['id']=$data['id_cat'];
            $katigoria=$category->getCategory($toAsk);
            if($katigoria['response']) {
                $result = $category->insertCategory($katigoria['category']);
                $return=$result;
            }else{
                $return=$katigoria;
            }
        }else{
            $return['response']=true;
        }
        if($return['response']){
            $omada=new Omada();
            $toAsk['category']=$data['id_cat'];
            $omades=$omada->getOmades($toAsk);
            if(!in_array($data['id_om'],array_column($omades['omades'], 'id'))){
                $toAsk['id']=$data['id_om'];
                $omad=$omada->getOmada($toAsk);
                if($omad['response']){
                    $result = $omada->insertOmada($omad['omada']);
                    $return=$result;
                }else{
                    $return=$omad;
                }
            }
        }
        if($return['response']){
            if(strlen($data['barcode'])===0){
                $data['barcode']='NULL';
            }else{
                $data['barcode']=$this->actions->quote($data['barcode']);
            }

            if(strlen($data['id'])===0){
                $data['id']=$this->getLatestId();
            }

            if(strlen($data['mothercode'])===0){
                $data['mothercode']=$this->getLatestId();
            }
            if((int)$data['api']===1){
                $data['id']=$this->getLatestId();
                $data['mothercode']=$this->getLatestId();
            }

            if(((int)$data['api']===1)&&(double)($data['price'])>0){
                $data['flag_timi']=1;
            }

            if(isset($data['alt_id'])){
                if(strlen($data['alt_id'])===0){
                    $data['alt_id']='NULL';
                }else{
                    $data['alt_id']=(int)$data['alt_id'];
                }
            }else{
                $data['alt_id']='NULL';
            }

            if(isset($data['days'])){
                $data['days']=(int)$data['days'];
            }else{
                $data['days']='NULL';
            }

            if(isset($data['zugaria'])){
                $data['zugaria']=(int)$data['zugaria'];
            }else{
                $data['zugaria']=0;
            }

            if (strlen($data['temaxia'])===0){
                $data['temaxia']=0;
            }else{
                if(!isset($data['temaxiako'])){
                    $data['temaxia']=-(double)$data['temaxia'];
                }else{
                    $data['temaxia']=(double)$data['temaxia'];
                }
            }

            if(isset($data['temaxiako'])){
                $data['temaxiako']=(int)$data['temaxiako'];
            }else{
                $data['temaxiako']=0;
            }

            if(isset($data['seira'])){
                $data['seira']=(int)$data['seira'];
            }else{
                $data['seira']='NULL';
            }

            if(isset($data['epistrofi'])){
                $data['epistrofi']=$this->actions->quote($data['epistrofi']);
            }else{
                $data['epistrofi']='NULL';
            }

            if(!isset($data['active'])){
                $data['active']=1;
            }
            if((!isset($data['desc2'])) || (strlen($data['desc2'])===0) ){
                $data['desc2']=$data['desc'];
            }
            if(!isset($data['agora'])){
                $data['agora']=0;
            }
            if(!isset($data['alert'])){
                $data['alert']=1;
            }
            if(!isset($data['recommend'])){
                $data['recommend']=1;
            }
            $query="insert into ".$this->model['table']." values (".(int)$data['id']." ,".$data['barcode'].
                ", ".(int)$data['mothercode'].", ".$this->actions->quote($data['desc']).", ".$this->actions->quote($data['desc2']).", ".(int)$data['id_om'].
                ", ".(double)$data['price'].", ".number_format($data['agora'],2).", ".(int)$data['flag_timi'].", ".(int)$data['tmima'].", ".number_format($data['temaxia'],3).
                ", ".number_format($data['alert'],3).", ".number_format($data['recommend'],3).", ".(int)$data['package']. ", ".$data['active'].", 0, ".$data['seira'].", ".$data['epistrofi'].
                ", NOW(), NOW(), ".(int)$data['zigos'].", ".(int)$data['api'].", ".$data['zugaria'].", ".$data['temaxiako'].", ".$data['alt_id'].", ".$data['days'].");";
            $result=$this->actions->db_query($query);
            if($result===true){
                $return['response']=true;
                if (isset($data['flag_timi_agoras']) && (int)$data['flag_timi_agoras']===1){
                    $agora['mothercode']=$data['mothercode'];
                    $agora['flag_agoras']=1;
                    $agora['agora']=$data['agora'];
                    $updateAgora=$this->updateTimiAgoras($agora);
                    if ($updateAgora['response']===false){
                        $return['response']=false;
                        $return['error']=$updateAgora;
                    }
                }
            }else{
                $return['response']=false;
                $return['query']=$query;
                $return['error']=$result;
            }
        }
        return $return;
    }

//</editor-fold>

}