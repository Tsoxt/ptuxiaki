<?php

class Omada
{
    private $model;
    private $api;
    private $actions;

    public function __construct()
    {
        $db=new dbmodel;
        $this->model=$db->_omada;
        $this->api=new BarcodeApi();
        $this->actions=new dbactions();
    }

    private function returnOmada($omades){
        $omades=json_decode($omades,true);
        if(!empty($omades)){
            $omada=$omades[0];
            $omad['id']=$omada['id'];
            $omad['altId']=$omada['alt_id'];
            $omad['name']=$omada['description'];
            $omad['id_category']=$omada['id_category'];
            $omad['active']=1;
            $omad['seira']='';
            $omad['api']=1;
            $omad['omada_zugaria']='';
            $omad['isozigio']=0;
            $return['response']=true;
            $return['omada']=$omad;
        }else{
            $return['response']=false;
            $return['error']="Η ομάδα δεν υπάρχει";
        }
        return $return;
    }

    public function getOmada($data){
        $query="select * from ".$this->model['table']." where ".$this->model['id']."=".(int)$data['id']." ;";
        $result=$this->actions->db_query($query);
        if(mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['omada']=mysqli_fetch_assoc($result);
        }else{
            if($this->api->isActive()){
                $response=$this->api->omadaFromApi($data['id']);
                if($response['httpCode']===200){
                    $return=$this->returnOmada($response['body']);
                }else{
                    $return['response']=false;
                    $return['error']=$response['error'];
                }
            }else {
                $return['response'] = false;
                $return['error'] = "Η ομάδα δεν υπάρχει";
            }
        }
        return $return;
    }

    public function getOmades($data){
        $query="select * from ".$this->model['table']." where ".$this->model['id_cat']." = ".(int)$data['category']." ;";
        $result=$this->actions->db_query($query);
        if (mysqli_num_rows($result)>0){
            $return['response']=true;
            $return['omades']=array();
            while($row=mysqli_fetch_assoc($result)){
                array_push($return['omades'],$row);
            }
        }else{
            $return['response']=false;
            $return['error']="Υπήρξε κάποιο πρόβλημα προσπαθήστε ξανά";
        }
        return $return;
    }

    public function updateOmada($data){
        if(strlen($data['altId'])===0){
            $data['altId']='NULL';
        }else{
            $data['altId']=(int)$data['altId'];
        }
        if(strlen($data['seira'])===0){
            $data['seira']='NULL';
        }else{
            $data['seira']=(int)$data['seira'];
        }
        if(strlen($data['omada_zugaria'])===0){
            $data['omada_zugaria']='NULL';
        }else{
            $data['omada_zugaria']=(int)$data['omada_zugaria'];
        }
        $query="update ".$this->model['table']. "set ".$this->model['altId']." = ".$data['altId'].", ".$this->model['perig']." = ".$this->actions->quote($data['name']).
            ", ".$this->model['id_cat']." = ".(int)$data['id_category'].", ".$this->model['active']." = ".(int)$data['active'].
            ", ".$this->model['order']." = ".$data['seira'].", ".$this->model['api']." = ".(int)$data['api'].", ".$this->model['zugaria']." = ".$data['omada_zugaria'].
            ", ".$this->model['isozigio']." = ".$data['isozigio']." where ".$this->model['id']." = ".(int)$data['id'].";";
        $result=$this->actions->db_query($query);
        if($result===true){
            $return['response']=$result;
        }else{
            $return['query']=$query;
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }

    public function insertOmada($data){
        $action = new dbactions;
        if(strlen($data['id'])===0){
            $data['id']='NULL';
        }else{
            $data['id']=(int)$data['id'];
        }
        if(strlen($data['altId'])===0){
            $data['altId']='NULL';
        }else{
            $data['altId']=(int)$data['altId'];
        }
        if(strlen($data['seira'])===0){
            $data['seira']='NULL';
        }else{
            $data['seira']=(int)$data['seira'];
        }
        if(strlen($data['omada_zugaria'])===0){
            $data['omada_zugaria']='NULL';
        }else{
            $data['omada_zugaria']=(int)$data['omada_zugaria'];
        }
        $query  = "insert into ".$this->model['table'].
            " values ".
            "(".$data['id'].", ".$data['altId'].", ".$action->quote($data['name']).
            ", ".(int)$data['id_category'].", ".(int)$data['active'].", ".$data['seira'].", ".(int)$data['api'].", 0, ".$data['omada_zugaria'].", ".$data['isozigio'].") ;";
        $result=$action->db_query($query);
        if($result===true) {
            $return['response']=$result;
        }else{
            $return['response']=false;
            $return['error']=$result;
        }
        return $return;
    }
}